#include "escore.h"


void fprint_block_info( 
       FILE* strm, 
       struct block_info_t* bi) {

  uint64_t n = get_uint64n( &bi->entryCount ); 

  fprintf(strm, "block info: \n");
  fprintf(strm, "  this block time: %lX\n", get_int64n( &bi->thisBlockTime));
  fprintf(strm, "  prev block time: %lX\n", get_int64n( &bi->prevBlockTime));

  fprintf(strm, "  prev block hash: ");
  
  for(size_t i=0; i< 32;i++) {
    fprintf(strm,"%02X",bi->prevBlockHash[i]); 
  } 
  fprintf(strm, "\n                   ");
  for(size_t i=32; i< 64;i++) {
    fprintf(strm,"%02X",bi->prevBlockHash[i]); 
  }
  fprintf(strm,"\n");

  for (size_t j=0; j < n; ++j) {
    fprintf(strm, "  entry %04lu hash: ",j);
    for(size_t i=0; i< 32;i++) {
      fprintf(strm,"%02X",(bi->entryHash[j])[i]); 
    } 
    fprintf(strm, "\n                   ");
    for(size_t i=32; i< 64;i++) {
      fprintf(strm,"%02X",(bi->entryHash[j])[i]); 
    }
    fprintf(strm,"\n");
  }

}




void fprint_esd(
      FILE* strm,
      const ESD* esd) {


  if ( esd == NULL ) {
    fprintf(strm,"ERROR (fprint_esd_header): Invalid Argument\n"); 
    return;
  }

  fprint_psd_header( strm, (PS_D*)esd );

  fprintf(strm,"  pbt  : %lX\n",
                  get_int64n( &esd->header.prevBlockTime ) ); 
  
  fprintf(strm,   "  pbh  : ");
 
  for(size_t i=0; i< 32;i++) {
    fprintf(strm,"%02X",esd->header.prevBlockHash[i]); 
  } 
  fprintf(strm, "\n         ");
  for(size_t i=32; i< 64;i++) {
    fprintf(strm,"%02X",esd->header.prevBlockHash[i]); 
  }


  fprintf(strm,"\n  data : ");

  size_t z = ps_datagramSize( (PS_D*)esd ) - ES_HEADER_SIZE;
  size_t l;
  for(l=0; l < z/32; l++) { 
    for(size_t i=0; i < 32;i++) {
      fprintf(strm,"%02X",esd->payload[32*l+i]); 
    }
    fprintf(strm,"\n         ");
  }

  for(size_t i=0; i < z%32;i++) {
    fprintf(strm,"%02X",esd->payload[32*l+i]); 
  }
  fprintf(strm,"\n");




}



struct block_info_t* es_mk_blockinfo(
      PSPRF_BD* prfs,
      const IP6Addr* dst_addr,
      const uint64_t dst_port,
      const MicroTime thisTime ) {
 
  char bcBlockPath[ES_PATH_MAX], bcHeadBlockPath[ES_PATH_MAX],
       bcHeadHashPath[ES_PATH_MAX], bcPrevBlockPath[ES_PATH_MAX];
  size_t biSz,i;
  MicroTime prevTime;
  struct block_info_t* bi; /* return value */

  /* create block path for input */
  if ( es_mk_blockchainBlockPath( bcBlockPath,
                                  dst_addr,
                                  dst_port,
                                  thisTime ) 
         == NULL ) {
    fprintf(stderr,"ERROR (%s): Could create block path %s (%s)\n",
                   ps_mainIdent,bcBlockPath,strerror(errno)); 
    return NULL;
  }

  
  /* TODO: store in network notation, or as  */
  vstpcpy( bcPrevBlockPath,bcBlockPath,"/prev", NULL ); 
  if ( atomic_load_from_file( bcPrevBlockPath, 
                              &prevTime, 
                              sizeof(MicroTime) ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not open path %s (%s)\n",
                    ps_mainIdent,bcPrevBlockPath,strerror(errno)); 
    return NULL;
  }

  biSz = sizeof(struct block_info_t) + prfs->entry_count * ES_HASH_SIZE ; 
  bi = malloc(biSz); 
  if (bi == NULL) {
    fprintf(stderr,"ERROR (%s): Out of memory\n",ps_mainIdent);
    return NULL;
  }

  
  set_uint64n( &bi->entryCount, prfs->entry_count );

  set_int64n(&bi->thisBlockTime,thisTime);
  set_int64n(&bi->prevBlockTime,prevTime);

  if ( prevTime != 0 ) {
    /* TODO: Optimize: Use a procedure that does not do mkdirs here,
     *       this path must exist */
    if ( es_mk_blockchainBlockPath( bcHeadBlockPath,
                                    dst_addr,
                                    dst_port,
                                    prevTime ) 
            == NULL ) {
       fprintf(stderr,"ERROR (%s): Could create block path %s (%s)\n",
                      ps_mainIdent,bcBlockPath,strerror(errno)); 
       goto FAIL;
    }

    vstpcpy(bcHeadHashPath,bcHeadBlockPath,"/hash.blake2b",NULL);
 
    if ( atomic_load_from_file( bcHeadHashPath, 
                                &bi->prevBlockHash, 
                                ES_HASH_SIZE ) < 0 ) {
      fprintf(stderr,"ERROR (%s): Could not open %s:\n",
                    ps_mainIdent,bcHeadHashPath); 
      goto FAIL;
    }
  } else {
    /* initial hash defaults to 0 */
    memset( &bi->prevBlockHash, 0, ES_HASH_SIZE );
  }

  for( i = 0 ; i < prfs->entry_count ; ++i ) {
    memcpy( &bi->entryHash[i], &prfs->entry[i].data->hash,HASH_SIZE );
  }

  return bi;

FAIL:
  
  free(bi);
  return NULL;

}



/* at the moment lookup is done first using a deadline
 * to determine the block, that using a data prefix
 * to determine index of the requested entry in the block
 * The prefix thus must be a uid relative to the block-timestamp,
 * if a determined packed should be looked up.
 *
 * TODO: Interface that return all matches if there are many. 
 *
 */


int es_load_block(
       ShmBd* psdShmbd,
       ShmBd* prfShmbd,
       MicroTime t0, 
       const PS* sock ) {

  char basePath[ES_PATH_MAX], blockPath[ES_PATH_MAX], proofPath[ES_PATH_MAX];  
  int r;
  MicroTime dt, deadline;

  dt = ps_baseInterval;
  deadline = (((t0-1)/dt)+1)*dt;
  
  if ( es_mk_blockchainBlockPath(
         basePath, 
         &sock->addr, 
         get_uint64n(&sock->port),
         deadline ) == NULL ) {
     fprintf( stderr, 
              "UNLIKELY ERROR (%s): Could not create block path string (%s)\n",
              ps_mainIdent, strerror(errno) );        
     return -1;
  }

  /* TODO: If block.bd.part exist wait until write down is finished
   *       Also, check if write down creates .part file first */
                  
  vstpcpy( blockPath, basePath, "/block.bd", NULL );
  r = shmbd_mmap_file_ro(psdShmbd,blockPath);
  if ( r < 0 ) {
    fprintf(stderr, "ERROR (%s): Can not read block data from "
                    "%s (%s)", ps_mainIdent, blockPath, strerror(errno));        
    return r;
  }

  if ( prfShmbd != NULL ) {
    vstpcpy( proofPath, basePath, "/proof.bd", NULL ); 
    r = shmbd_mmap_file_ro(prfShmbd,proofPath);
    if ( r < 0 ) { 
      fprintf(stderr, "ERROR (%s): Can not read proof data from "
                      "%s (%s)", ps_mainIdent, blockPath, strerror(errno));        
      return r;
    }
  }

  return 0;

}



/* check hashes and signatures */

bool es_verify_block( const PS* sock,
                      MicroTime _t0 ) {

  ShmBd esds;
  ShmBd prfs;
  PSPRF_BD* prfs_bd;
  char basePath[ES_PATH_MAX], bcBlockHashPath[ES_PATH_MAX],
       bcHeadHashPath[ES_PATH_MAX];
  MicroTime dt = ps_baseInterval; 
  MicroTime deadline = ((_t0+dt-1)/dt)*dt ;
  size_t i, j, biSz;
  ESD* esd;
  PSPRF* prf;
  uint8_t hash[HASH_SIZE],storedHash[HASH_SIZE];
  PSS* signature;
  struct block_info_t* bi;

  /* path used for error reporting */
  if ( es_mk_blockchainBlockPath(
         basePath, 
         &sock->addr, 
         get_uint64n(&sock->port),
         deadline ) == 0 ) {
     fprintf( stderr, 
              "UNLIKELY ERROR (%s): Could not create block path string (%s)\n",
              ps_mainIdent, strerror(errno) );        
     return -1;
  }


  if ( es_load_block( &esds, &prfs, deadline, sock ) < 0 ) {
    return false;
  } 


  prfs_bd = new_batchdata_from_shmbd( PSPRF, &prfs );
  if ( prfs_bd == NULL ) {
    fprintf(stderr, "ERROR (%s): Out of memory\n", ps_mainIdent );
    return false;
  }

  for ( i = 0 ; i < esds.entryCount ; ++i ) {

    esd = (ESD*)shmbd_entryData( i, &esds ); 
    
    assert( esd != NULL );
    
    ps_mk_packetDatagramHash( hash, (PSPKT*)esd );
    
    prf = (PSPRF*)shmbd_entryData( i, &prfs );
   
    assert( prf != NULL);
   
    if ( memcmp( hash, prf->hash, HASH_SIZE ) != 0 ) {
      fprintf( stderr, "ERROR (%s): Packet corruption for packet %zu "
                       "in block %s (hash invalid)\n", 
                        ps_mainIdent, i, basePath );
      goto FAIL;
    }

    /* TODO: The eidetic sockets should store a history of used keys
     *       and must use correct key for the deadline of the packet */
    for (j=0; j < get_uint64n( &prf->signatureCount); ++j ) {
      
      uint8_t pk[PSD_PK_SIZE]; 
      signature = &prf->signature[j];

      if ( ps_load_auth_pk( 
             pk,
             &signature->addr,
             get_uint64n( &esd->header.src_port ) ) < 0 ) goto FAIL;
      
 
      if ( crypto_sign_verify_detached(
             signature->data, 
             hash,
             HASH_SIZE,
             pk ) != 0 ) {

             fprintf( stderr, 
                      "ERROR (%s): Packet corruption for packet %zu "
                      "in block %s (Signature %zu invalid)\n",ps_mainIdent,
                      i, basePath,j );
 
      
        goto FAIL;
 
      }
    
    } 

  }

  /* all hashes and signatures ok */

  bi = es_mk_blockinfo( prfs_bd, 
                        &sock->addr, 
                        get_uint64n(&sock->port), 
                        deadline );
  if ( bi == NULL ) goto FAIL;
 

  vstpcpy(bcBlockHashPath,basePath,"/hash.blake2b",NULL);

  if ( atomic_load_from_file(bcBlockHashPath,&storedHash, HASH_SIZE) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not store %s:\n",
                    ps_mainIdent,bcHeadHashPath); 
    goto FAIL1;
  } 

  biSz = sizeof(struct block_info_t) + prfs.entryCount * HASH_SIZE ; 
 
  if ( crypto_generichash( hash, HASH_SIZE, (uint8_t*)bi, biSz, NULL, 0 )
                  < 0 ) {
    es_mk_blockchainBlockPath( basePath,
                               &sock->addr,
                               get_uint64n( &sock->port ),
                               deadline );
    /* hash functions should not fail */
    fprintf(stderr,"ERROR (%s): Could not compute block hash of %s\n",
                    ps_mainIdent,basePath); 
    goto FAIL1; 
  }

  if ( memcmp(storedHash,hash,HASH_SIZE) != 0 ) {
    fprintf( stderr, "ERROR (%s): Blockchain brocken. Block %s or stored"
                     " hash invalid\n",
                      ps_mainIdent, basePath ); 
    goto FAIL1; 
  }

  free( bi );
  delete_batchdata( PSPRF, prfs_bd );

  return true;

FAIL1:

  free(bi);

FAIL:

  delete_batchdata(PSPRF,prfs_bd);
  return false;

}





_Thread_local size_t es_lookupPrefixSize;
_Thread_local const ShmBd* es_lookupShmbd;
_Thread_local const uint8_t* es_lookupPrefix;
_Thread_local MicroTime es_lookupTime;

int es_lookupCmp( const uint64_t* lOffset, const uint64_t* rOffset ) {

  ESD *aEsd,*bEsd; 
  const uint8_t *a,*b;
  MicroTime at,bt;

  if ( *lOffset != UINT64_MAX ) {
    aEsd  = (ESD*)(es_lookupShmbd->data + *lOffset);
    a = aEsd->payload;
    at = get_int64n( &aEsd->header.deadline );
  } else {
    a  = es_lookupPrefix;
    at = es_lookupTime;
  }

  if ( *rOffset != UINT64_MAX ) {
    bEsd  = ((ESD*)(es_lookupShmbd->data + *rOffset));
    b = bEsd->payload;
    bt = get_int64n( &bEsd->header.deadline );
  } else {
    b  = es_lookupPrefix;
    bt = es_lookupTime;
  }

  if ( at < bt ) return -1;
  if ( at > bt ) return  1;

  return lxcmp( a ,b , es_lookupPrefixSize );

}




size_t es_block_lookup( 
         const ShmBd* block,
         MicroTime t,
         void* prefix,
         size_t prefixSize ) {

   void* cand;
   const uint64_t* offset;
   uintptr_t uintOffset,uintCand;
   size_t ret;
   uint64_t keyOffset = UINT64_MAX;

   /* pass implicit parameters for es_lookupCmp */
   es_lookupPrefixSize = prefixSize;
   es_lookupPrefix = prefix;
   es_lookupTime = t;
   es_lookupShmbd = block;

   offset = (const uint64_t*)block->data;

   cand = bsearch( &keyOffset, 
                   offset,
                   block->entryCount, 
                   sizeof(uint64_t),
                   (STD_COMPARE)es_lookupCmp );

   if (cand == NULL) return SIZE_MAX;

   uintOffset = (uintptr_t)offset;
   uintCand   = (uintptr_t)cand;

   ret = (uintCand - uintOffset) / sizeof(uint64_t);

   assert( &shmbd_offset(es_lookupShmbd)[ret] == cand );

   return ret;

}



/* If os has good path  and file caching that lookup
 * might not be so bad - and read-only mmaps should work
 * well with concurrent access */

int es_lookup(
      ESD* retPsd,
      PSPRF* retPrf,
      const PS* sock,
      MicroTime deadline,
      void* prefix,
      size_t prefixSize ) {

  size_t k;
  ShmBd psds;
  ShmBd prfsBuf;
  ShmBd* prfs = (retPrf == NULL) ? NULL : &prfsBuf;

  if ( es_load_block(&psds,prfs,deadline,sock) < 0 ) return -1;

  k = es_block_lookup(&psds,deadline,prefix,prefixSize);

  if (k == SIZE_MAX) return -1;

  memcpy(retPsd,shmbd_entryData(k,&psds),shmbd_entrySize(k,&psds));
  shmbd_unlink(&psds);
  shmbd_unload(&psds);

  if (retPrf != NULL) {
    memcpy(retPrf,shmbd_entryData(k,prfs),shmbd_entrySize(k,prfs));
    shmbd_unlink(prfs);
    shmbd_unload(prfs);
  }
 
  return 0;

}



int es_interlock( ESD_BD* esds,
                  IP6Addr* src_addr,
                  uint64_t src_port ) {

  char bcHeadPath[ES_PATH_MAX], bcBlockPath[ES_PATH_MAX],
       bcPath[ES_HASH_SIZE], bcBlockHashPath[ES_PATH_MAX];
  uint8_t hash[ES_HASH_SIZE];
  MicroTime headTime;
  size_t i;
  struct stat st;

  es_mk_blockchainPath(bcPath,src_addr,src_port);

  if ( stat(bcPath,&st) < 0 ) {
    fprintf(stderr,"ERROR (%s): Persistent socket not eidetic. "
                   "Could not stat %s:\n",
                    ps_mainIdent,bcPath); 
    return -1;
  }

  es_mk_blockchainHeadPath(bcHeadPath,src_addr,src_port);

  if ( atomic_load_from_file(bcHeadPath,&headTime,sizeof headTime) < 0 ) {
    if (errno == ENOENT) { /* empty blockchain */
      headTime = 0; 
      errno = 0;
    } else {
      fprintf(stderr,"ERROR (%s): Could not blockchain head data %s:\n",
                      ps_mainIdent,bcHeadPath);
      return -1;
    }
  }

  if ( es_mk_blockchainBlockPath( bcBlockPath,
                                  src_addr,
                                  src_port,
                                  headTime ) 
          == NULL ) {
     fprintf(stderr,"ERROR (%s): Could create block path %s (%s)\n",
                    ps_mainIdent,bcBlockPath,strerror(errno)); 
     return -1;
  }

  vstpcpy(bcBlockHashPath,bcBlockPath,"/hash.blake2b",NULL);

  if ( atomic_load_from_file( bcBlockHashPath, 
                              hash, 
                              ES_HASH_SIZE ) < 0 ) {
    
    if (errno == ENOENT ) { /* empty blockchain */
      memset(hash,0,ES_HASH_SIZE);
      errno = 0;
    } else {
      fprintf(stderr,"ERROR (%s): Could not open %s:\n",
                      ps_mainIdent,bcBlockPath);
    
      return -1;
    }
  }

  for( i = 0 ; i < esds->entry_count; ++i ) {
    memcpy( &esds->entry[i].data->header.prevBlockHash,hash,HASH_SIZE);
    set_int64n( &esds->entry[i].data->header.prevBlockTime, headTime);
  }

  return 0;
}


