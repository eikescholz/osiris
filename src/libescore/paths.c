#include "escore.h"


char* es_mk_rootPath(char* pathBuf) {
  
  char* rootprefix = getenv("PSROOTPREFIX");
  if ( rootprefix == NULL) rootprefix = PSD_ROOT_PREFIX;

  if ( strlen(rootprefix) > PSD_MAX_ROOT_PATH_SIZE - strlen("/es")) {
    fprintf(stderr,"ERROR: Environment PSROOTPREFIX too long (max is %d)\n",
                    PSD_MAX_ROOT_PATH_SIZE);
    exit(EXIT_FAILURE); 
  }  
  
  return vstpcpy(pathBuf,rootprefix,"/es",NULL);

}



char* es_mk_nodePath(
    char* pathBuf,
    const struct in6_addr* addr) {
        

  char ipstr[INET6_ADDRSTRLEN];

  ip6ntos(addr,ipstr); 

  return vstpcpy(es_mk_rootPath(pathBuf),"/",ipstr,NULL); 

}



char* es_mk_blockchainPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port 
    ) {

  const size_t portStrSz = 2*sizeof(uint64_t)+1;
  char portStr[portStrSz];

  if ( snprintf(portStr,portStrSz,"%lX",port) < 1) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    exit(EXIT_FAILURE);
  }    
 
  return vstpcpy(es_mk_nodePath(pathBuf,addr),"/",portStr,NULL); 
   
}





char* es_mk_receivePath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port 
    ) {

  return vstpcpy(es_mk_blockchainPath(pathBuf,addr,port),"/receive",NULL); 
   
}





char* es_mk_sendPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port 
    ) {

  return vstpcpy(es_mk_blockchainPath(pathBuf,addr,port),"/send",NULL); 
   
}




char* es_mk_blockchainHeadPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port ) {        

  return vstpcpy(es_mk_blockchainPath(pathBuf,addr,port),"/head",NULL); 

}




/* blocks are stored using prefix a tree, for fast access */
char* es_mkdir_blockchainBlockPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port,
    MicroTime time ) {
  
  char sockPath[ES_PATH_MAX];
  char t0s[3],t1s[3],t2s[3],t3s[3],t4s[3],t5s[7];
  MicroTime t0,t1,t2,t3,t4,t5;

  t5 = time & 0xFFFFFF;   /* last three bytes yield a 16.7 seconds bin */
  t4 = (time>>24) & 0xFF; 
  t3 = (time>>32) & 0xFF;
  t2 = (time>>40) & 0xFF; 
  t1 = (time>>48) & 0xFF; 
  t0 = (time>>56);        

  es_mk_blockchainPath(sockPath,addr,port);

  if ( snprintf(t0s,3,"%02lX",t0 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,NULL);

  if ( mkdir(pathBuf,0770) < 0 && errno != EEXIST ) return NULL;


  if ( snprintf(t1s,3,"%02lX",t1 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,NULL);

  if ( mkdir(pathBuf,0770) < 0 && errno != EEXIST ) return NULL;




  if ( snprintf(t2s,3,"%02lX",t2 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,NULL);

  if ( mkdir(pathBuf,0770) < 0 && errno != EEXIST ) return NULL;



  
  if ( snprintf(t3s,3,"%02lX",t3 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,"/",t3s,NULL);

  if ( mkdir(pathBuf,0770) < 0 && errno != EEXIST ) return NULL;




  if ( snprintf(t4s,3,"%02lX",t4 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,"/",t3s,"/",t4s,NULL);

  if ( mkdir(pathBuf,0770) < 0 && errno != EEXIST ) return NULL;




  if ( snprintf(t5s,7,"%06lX",t5) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,"/",t3s,"/",t4s,"/",t5s,
                  NULL);

  if ( mkdir(pathBuf,0770) < 0 && errno != EEXIST ) return NULL;


  return pathBuf;
}

/* blocks are stored using prefix a tree, for fast access */
char* es_mk_blockchainBlockPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port,
    MicroTime time ) {
  
  char sockPath[ES_PATH_MAX];
  char t0s[3],t1s[3],t2s[3],t3s[3],t4s[3],t5s[7];
  MicroTime t0,t1,t2,t3,t4,t5;

  t5 = time & 0xFFFFFF;   /* last three bytes yield a 16.7 seconds bin */
  t4 = (time>>24) & 0xFF; 
  t3 = (time>>32) & 0xFF;
  t2 = (time>>40) & 0xFF; 
  t1 = (time>>48) & 0xFF; 
  t0 = (time>>56);        

  es_mk_blockchainPath(sockPath,addr,port);

  if ( snprintf(t0s,3,"%02lX",t0 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,NULL);



  if ( snprintf(t1s,3,"%02lX",t1 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,NULL);



  if ( snprintf(t2s,3,"%02lX",t2 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,NULL);


  
  if ( snprintf(t3s,3,"%02lX",t3 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,"/",t3s,NULL);



  if ( snprintf(t4s,3,"%02lX",t4 ) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,"/",t3s,"/",t4s,NULL);




  if ( snprintf(t5s,7,"%06lX",t5) < 1 ) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    return NULL; /* this really should be impossible */
  }
 
  vstpcpy(pathBuf,sockPath,"/",t0s,"/",t1s,"/",t2s,"/",t3s,"/",t4s,"/",t5s,
                  NULL);



  return pathBuf;
}




