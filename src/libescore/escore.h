#ifndef ESCORE_H
#define ESCORE_H
#include "pscore.h"

#define ES_HASH_SIZE 64
#define ES_HEADER_SIZE (PSD_HEADER_SIZE+72)
#define ES_MAX_PAYLOAD_SIZE 1024

/* worst case $prefix/ps/$addr/$port/$T0/$T1/$T2/$T3/$T4/$T5/$FILE */
#define ES_PATH_MAX (12*256)


#define ES_DATAPKT 0  

_Static_assert( ES_HEADER_SIZE + ES_MAX_PAYLOAD_SIZE + PSPKT_FOOTER_SIZE 
                 == IP6_GUARANTEED_MTU,"Max Payload Size Wrong");

struct esd_header_t {
  uint32n_t vcf; /* version class and flow */
  uint16n_t packetSize; 
  uint8n_t nextHeader;
  uint8n_t hopLimit;
  IP6Addr src_addr;
  IP6Addr dst_addr;
  /* Above entries are the IPv6 Header */
  psd_nonce_t nonce;  
  int64n_t mpid;  
  /* the following data will be encrypted by the node-to-node encryption */
  uint64n_t src_port; 
  uint64n_t dst_port;  
  int64n_t deadline; 
  /* the following is the actual eidetic packet header */
  uint8n_t prevBlockHash[ES_HASH_SIZE]; /* from sender */ 
  int64n_t prevBlockTime; 
} __attribute__ ((packed));

_Static_assert( ES_HASH_SIZE == HASH_SIZE, "Inconsistent Hash Sizes");


struct es_datagram_t {
  struct  esd_header_t header;
  uint8_t payload[ES_MAX_PAYLOAD_SIZE]; /* may be smaller  ... */
} __attribute__ ((packed));

typedef struct es_datagram_t ESD;

typedef BE_T(ESD) ESD_BE;
typedef BD_T(ESD) ESD_BD;

_Static_assert( ES_HEADER_SIZE == sizeof(struct esd_header_t),
                "Wrong size of eidetic socket header" );

typedef uint8_t PSPKTHASH[HASH_SIZE];


/* used to compute the next blocks hash */
struct block_info_t {
  int64n_t thisBlockTime;
  uint64n_t entryCount;
  int64n_t prevBlockTime;
  uint8n_t  prevBlockHash[ES_HASH_SIZE];
  PSPKTHASH entryHash[];
} __attribute__ ((packed));




int es_send_main(int argc,char** argv);
int es_receive_main(int argc,char** argv);

char* es_mk_rootPath(char* pathBuf);

int es_interlock( ESD_BD* esds,
                  IP6Addr* addr,
                  uint64_t port );

char* es_mk_nodePath( 
        char* pathBuf, 
        const struct in6_addr* addr);
        

char* es_mk_blockchainPath(
       char* pathBuf,
       const struct in6_addr* addr,
       uint64_t port 
      );


char* es_mk_receivePath(
        char* pathBuf,
        const struct in6_addr* addr,
        uint64_t port 
      );

char* es_mk_sendPath(
        char* pathBuf,
        const struct in6_addr* addr,
        uint64_t port 
      );

char* es_mk_blockchainHeadPath(
        char* pathBuf,
        const struct in6_addr* addr,
        uint64_t port );

/* blocks are stored using prefix a tree, for fast access */
char* es_mk_blockchainBlockPath(
        char* pathBuf,
        const struct in6_addr* addr,
        uint64_t port,
        MicroTime time );

int es_load_block(
       ShmBd* psdShmbd,
       ShmBd* prfShmbd,
       MicroTime deadline, 
       const PS* sock );

int es_lookupCmp( const uint64_t* lOffset, const uint64_t* rOffset );

size_t es_block_lookup( 
         const ShmBd* block,
         MicroTime t,
         void* prefix,
         size_t prefixSize );


int es_lookup(
      ESD* retEsd,
      PSPRF* retPrf,
      const PS* sock, 
      MicroTime deadline,
      void* prefix,
      size_t prefixSize );


struct block_info_t* es_mk_blockinfo(
      PSPRF_BD* prfs,
      const IP6Addr* dst_addr,
      const uint64_t dst_port,
      const MicroTime thisTime );
 

bool es_verify_block( const PS* sock,
                      MicroTime _t0 );

int es_receive( const IP6Addr* dst_addr,
                const uint64_t dst_port,
                MicroTime t,
                ESD_BD* esds, 
                PSPRF_BD* prfs,
                char** argv );

char* es_mkdir_blockchainBlockPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port,
    MicroTime time );





#endif 
