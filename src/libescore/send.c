#include "escore.h"


int es_send_main(int argc, char** argv) {

  char pssend[ES_PATH_MAX];
  ESD_BD* esds;
  IP6Addr src_addr;
  uint64_t src_port;

  if ( ps_receiver_init("essend",argc,argv) < 0 ) exit(EXIT_FAILURE); 

  if (! ps_getenv_PSSRCIP(&src_addr) ) exit(EXIT_FAILURE);
  
  if (! ps_getenv_PSSRCPORT(&src_port) ) exit(EXIT_FAILURE);
 
  if ( ps_read_receiverInput((PS_D_BD**)&esds,NULL) < 0 ) exit(EXIT_FAILURE);

  if (! ps_verify_pktbd_src_addr((PSPKT_BD*)esds,&src_addr) ) goto FAIL;
  
  if (! ps_verify_pktbd_src_port((PSPKT_BD*)esds,src_port) ) goto FAIL;

  if ( es_interlock(esds,&src_addr,src_port) < 0) exit(EXIT_FAILURE);

  psd_mkSendPath(pssend);

  /* argv[1] is the name of the input shmbatch, we pass that through */
  argv[0] = pssend ;
  ps_execve(pssend, argv, environ );

FAIL:

  delete_batchdata(ESD,esds);

  return EXIT_FAILURE;                

}


