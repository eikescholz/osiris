#include "escore.h"




int es_receive( const IP6Addr* dst_addr,
                const uint64_t dst_port,
                MicroTime t,
                ESD_BD* esds, 
                PSPRF_BD* prfs,
                char** argv ) {

  struct block_info_t *bi;
  uint8_t hash[ES_HASH_SIZE];
  size_t biSz;
  char bcHeadPath[ES_PATH_MAX], bcBlockPath[ES_PATH_MAX],
       bcBlockHashPath[ES_PATH_MAX], bcBlockProofPath[ES_PATH_MAX], 
       bcBlockBatchPath[ES_PATH_MAX], receive[ES_PATH_MAX],
       bcPrevBlockPath[ES_PATH_MAX];
  MicroTime headTime;
  pid_t pid;

  assert( t % ps_baseInterval == 0);

  if ( es_mk_blockchainHeadPath( bcHeadPath, dst_addr, dst_port ) == NULL ) {
     fprintf( stderr, 
              "UNLIKELY ERROR (%s): Could not create block chain head path "
              "string (%s)\n",
              ps_mainIdent, strerror(errno) );        
     return -1;
  }

  if ( es_mkdir_blockchainBlockPath( bcBlockPath, dst_addr,dst_port,t)
           == NULL ) {
     fprintf( stderr, /* TODO: Better error message */
              "ERROR (%s): Could not create new block path (%s)\n",
              ps_mainIdent, strerror(errno) );        
     goto FAIL;
  }



  if ( atomic_load_from_file( bcHeadPath, &headTime, sizeof headTime ) < 0 ) {
    if ( errno == ENOENT ) {
      headTime = 0; /* Default of empty blockchain */
      errno = 0;
    } else { 
      fprintf( stderr, "ERROR (%s): Could not load blockchain head data %s:\n",
                        ps_mainIdent, bcHeadPath ); 
      goto FAIL;
    }
  }

  vstpcpy(bcPrevBlockPath,bcBlockPath,"/prev",NULL);
  if ( atomic_store_as_file( bcPrevBlockPath,
                             0440,
                             &headTime,
                             sizeof(headTime) ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not store %s:\n",
                   ps_mainIdent,bcHeadPath); 
    goto FAIL;
  }

  bi = es_mk_blockinfo( prfs, dst_addr, dst_port,t );
  if ( bi == NULL ) return EXIT_FAILURE;

  /* For speed reasons we trust the dispatchers proofs and do not verify 
   * them here and now. Verification is a good task for an "idle" process." */

  biSz = sizeof(struct block_info_t) + prfs->entry_count * ES_HASH_SIZE ; 
 
  if ( crypto_generichash( hash, ES_HASH_SIZE, (uint8_t*)bi, biSz, NULL, 0 )
         < 0 ) {
   /* hash functions should not fail */
    fprintf(stderr,"ERROR (%s): Could not compute block hash of %s\n",
                    ps_mainIdent,bcBlockPath); 
    free(bi);
    goto FAIL; 
  }

  free(bi);

  vstpcpy(bcBlockHashPath,bcBlockPath,"/hash.blake2b",NULL);

  if ( atomic_store_as_file(bcBlockHashPath,0440,hash,ES_HASH_SIZE) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not store %s:\n",
                    ps_mainIdent,bcBlockHashPath); 
    goto FAIL;
  } 


  if ( atomic_store_as_file( bcHeadPath,
                             0440,
                             &t,
                             sizeof(t ) ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not store %s:\n",
                   ps_mainIdent,bcHeadPath); 
    goto FAIL;
  }

  
  /* blockchain "link" data is updated the next block can be appended,
   * however the actual block is not written to dist. To not
   * let the dispatcher process longer wait we fork a write process for 
   * that. The writer will also execute the eidetic sockets receiver
   * program. 
   */

  pid = fork();
  if ( pid == -1 ) {
    es_mk_blockchainBlockPath( bcBlockPath,
                               dst_addr,dst_port,ps_inputBatchTime);
    fprintf(stderr,"ERROR (%s): Could fork block writer (%s):\n"
                    "Blockchain not corrupted, but payload data of block"
                    " %s lost\n",
                    ps_mainIdent,strerror(errno),bcBlockPath); 
     goto FAIL;
  }
  
  if ( pid != 0 ) return EXIT_SUCCESS; /* let psd dispatcher continue */ 

  vstpcpy(bcBlockBatchPath,bcBlockPath,"/block.bd",NULL);

  if ( store_batchfile(ESD,bcBlockBatchPath,esds) < 0 ) {
     fprintf(stderr,"ERROR (%s): Could not write block payload data (%s):\n"
                    "Blockchain not corrupted, but payload data of block"
                    " %s lost\n",
                    ps_mainIdent,strerror(errno),bcBlockBatchPath); 
     goto FAIL;
  }

  vstpcpy(bcBlockProofPath,bcBlockPath,"/proof.bd",NULL);

  if ( store_batchfile(PSPRF,bcBlockProofPath,prfs) < 0 ) {
     fprintf(stderr,"ERROR (%s): Could not write block payload data (%s):\n"
                    "Blockchain not corrupted, but payload data of block"
                    " %s lost\n",
                    ps_mainIdent,strerror(errno),bcBlockProofPath); 
     goto FAIL;
  }

  es_mk_receivePath(receive,dst_addr,dst_port);

  /* argv[1] is the name of the input shmbatch, we pass that 
   * through, along with the proofs file in argv[2] */
  argv[0] = receive;
  ps_execve(receive, argv, environ );

  /*unreachable */

FAIL:

  delete_batchdata(ESD,esds);
  delete_batchdata(PSPRF,prfs);

  return EXIT_FAILURE; 

}




int es_receive_main(int argc, char** argv) {

  ESD_BD* esds;
  PSPRF_BD* prfs;
  IP6Addr dst_addr;
  uint64_t dst_port;

  if ( ps_receiver_init("esreceive",argc,argv) < 0 ) exit(EXIT_FAILURE); 

  if (! ps_getenv_PSDSTIP(&dst_addr) ) exit(EXIT_FAILURE);
  
  if (! ps_getenv_PSDSTPORT(&dst_port) ) exit(EXIT_FAILURE);
 
  if ( ps_read_receiverInput((PS_D_BD**)&esds,&prfs) < 0 ) exit(EXIT_FAILURE);

  if (esds->entry_count != prfs->entry_count) {
    fprintf(stderr,"INTERNAL ERROR (%s): Psd dispatcher provided inconsistent" 
            "input!\n",ps_mainIdent );
    exit(EXIT_FAILURE);
  }

  if (! ps_verify_pktbd_dst_addr((PSPKT_BD*)esds,&dst_addr) ) {
    exit(EXIT_FAILURE);
  }
  
  if (! ps_verify_pktbd_dst_port((PSPKT_BD*)esds,dst_port) ) {
    exit(EXIT_FAILURE);
  }
 
  return es_receive( &dst_addr, dst_port, ps_inputBatchTime, esds, prfs, argv);     

}


