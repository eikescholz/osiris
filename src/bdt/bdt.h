#ifndef PDT_H
#define PDT_H

#include "batchdata.h"
#include "psd.h"
#include "sysutils.h"

#include <stdio.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <sodium.h>


int cropToPsdPayloads_main(int argc,char** argv);
int psEchoTimeEval_main(int argc,char** argv);
int sort_main(int argc,char** argv);
int mkRandomPSDB_main(int argc,char** argv);
int zeroFooter_main(int argc,char** argv);
int merge_main(int argc,char** argv);
int ls_main(int argc,char** argv);
int shmGet_main(int argc,char** argv);
int setDeadline_main(int argc,char** argv);
int shmPut_main(int argc,char** argv);
int split_main(int argc,char** argv);
int countEntries_main(int argc,char** argv);
int pkt2psd_main(int argc,char** argv);


#endif
