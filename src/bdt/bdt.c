/* the batch data (multi) tool
 *
 * Handling batchdata files from the command line.
 *
 * For debugging, testing and introspection.
 *
 */

#include "bdt.h"


int main(int argc, char** argv) {
  int ret = -1;
  if (sodium_init() < 0) {
    fprintf(stderr,"ERROR (pdt): Could not initailize libsodium");
    return EXIT_FAILURE;
  }  

  if( argc >= 2 ) {
    if (strcmp(argv[1], "countEntries") == 0) {
      ret = countEntries_main(argc,argv);
      sync();
      return ret;
    }
  }

  if( argc < 3) {
    printf("ERROR (bdt): Too few arguments applied for batch data tool\n"
           "Usage: pdt [TOOL] ... \n"
           "  where \n"
           "    bdt ls [FORMAT] \n" 
           "      lists contents. \n"
           "    bdt zipargs ARG_1 ... ARG_N\n" 
           "      creates batch file with N entries consisting of ARG1_ to ARG_N.\n"
           "    bdt append FILE [FORMAT] [ARG_1 ... ARG_N] \n"
           "      adds a new entry to batchfile FILE format data given by\n"
           "      ARG1 to ARG_N. If the file does not exist it is created.\n"
           "  and FORMAT can be \"ip\", \"nonce\" or \"u64\". \n"
           );
    exit(EXIT_FAILURE); }

  char* tool = argv[1];

  if (strcmp(tool, "ls") == 0) ret = ls_main(argc,argv);

//  if (strcmp(tool, "zipargs") == 0) ret = zipargs_main(argc,argv);

//  if (strcmp(tool, "append") == 0) ret = append_main(argc,argv);

  if (strcmp(tool, "split") == 0) ret = split_main(argc,argv);

  if (strcmp(tool, "merge") == 0) ret = merge_main(argc,argv);

  if (strcmp(tool, "sort") == 0) ret = sort_main(argc,argv);

  if (strcmp(tool, "shmPut") == 0) ret = shmPut_main(argc,argv);

  if (strcmp(tool, "shmGet") == 0) ret = shmGet_main(argc,argv);

  if (strcmp(tool, "setDeadline") == 0) ret = setDeadline_main(argc,argv);

  if (strcmp(tool, "pkt2psd") == 0) ret = pkt2psd_main(argc,argv);

  if (strcmp(tool, "zeroFooter") == 0) ret = zeroFooter_main(argc,argv);

  if (strcmp(tool, "mkRandomPSDB") == 0) ret = mkRandomPSDB_main(argc,argv);

  if (strcmp(tool, "psEchoTimeEval") == 0) 
          ret = psEchoTimeEval_main(argc,argv);

  if (strcmp(tool, "cropToPsdPayloads") == 0)  
          ret = cropToPsdPayloads_main(argc,argv);

  sync(); /* successful termination should guarantee write to disk */

  if (ret == 0) return ret;

  if (ret == -1) { /* FIXME: OK since other mains do not return on failure */
    printf("ERROR (bdt): unkown batch datat tool '%s'\n",tool);
  }

  return ret;

}

