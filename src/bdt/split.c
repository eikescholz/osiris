#include "bdt.h"
#include "sort.h"

int split_main(int argc,char** argv) {
  
  char* file = argv[2];
  enum sort_mode_e mode;
  size_t ret = 0;

  if (argc < 4) {
    fprintf(stderr,"ERROR (bdt): Missing aguemnt MODE! 'bdt split %s MODE'\n",file);   
    exit(EXIT_FAILURE);
  }

  mode = parse_sort_mode(argv[3]);
  if (mode == UNKNOWN_SORT) {
    fprintf(stderr,"ERROR (bdt): Unknown sort mode '%s'\n",argv[3]);
    exit(EXIT_FAILURE);
  }

  BD* bd = load_batchfile(uint8_t,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read input file" );
    exit(EXIT_FAILURE); 
  }

  if (strcmp(file+strlen(file)-3,".bd") == 0) {
    file[strlen(file)-3] = 0;
  }

  BD** bins = alloc_split_batchdata( uint8_t,
                                     bd,
                                     (BE_COMPARE)get_sort_compare(mode)
                                     ,NULL);
  if (bins == NULL) {
    fprintf(stderr,"ERROR (bdt): Out of memory\n");
    exit(EXIT_FAILURE); 
  }
 
  size_t c = 0;
  for (BD** curBin = bins; *curBin != NULL; ++curBin) {
    char* fileName = mprintf("%s.%zu.bd",file,c);
    if( store_batchfile(uint8_t,fileName,*curBin) < 0 ) {
      fprintf(stderr,"Could not write file '%s' (%s)\n",file,strerror(errno));
      ret = 1;
    }
    free(fileName);
    delete_batchdata(uint8_t,*curBin);
    ++c;  
  }
 
  free(bins);
 
  delete_batchdata(uint8_t,bd);

  return ret;

}

