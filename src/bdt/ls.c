#include "bdt.h"
#include "pscore.h"

enum formatid_t {
  IP_FMT,
  U64_FMT,
  NONCE_FMT,
  PSD_FMT,
  PSPKT_FMT
};

enum formatid_t parse_format(const char* fmt) {
  if (strcmp(fmt,"ip") == 0) return IP_FMT;
  if (strcmp(fmt,"u64") == 0) return U64_FMT;
  if (strcmp(fmt,"nonce") == 0) return NONCE_FMT;
  if (strcmp(fmt,"PSD") == 0) return PSD_FMT;
  if (strcmp(fmt,"PSPKT") == 0) return PSPKT_FMT;
 
  fprintf(stderr,"ERROR (bdt): Invalid batch entry format %s\n",fmt);
  exit(EXIT_FAILURE); 
}


static inline char* push_uint64(char* buf, uint64_t i,const char* bufEnd) {
  char* ret = buf+sizeof(i); 
  if ( ret > bufEnd) {
    fprintf(stderr,"ERROR (bdt): Entry size too big\n");
    exit(EXIT_FAILURE);
  }    
  
  uint64_t *ptr = (uint64_t*)buf;
  *ptr = i;

  return ret;
}

static inline char* push_ip6addr(char* buf, const char* str,const char* bufEnd) {
  if ( buf+16 > bufEnd) {
    fprintf(stderr,"ERROR (bdt): Entry size too big\n");
    exit(EXIT_FAILURE);
  }    
 
  if(! inet_pton(AF_INET6,str,buf) ) {
      fprintf(stderr,"Invalid addr %s\n",str);
      exit(EXIT_FAILURE); } 
  return buf+16;

}

static inline char* push_nonce(char* buf, const char* bufEnd) {
  char* ret = buf + sizeof(psd_nonce_t);
   if ( ret > bufEnd) {
    fprintf(stderr,"ERROR (bdt): Entry size too big\n");
    exit(EXIT_FAILURE);
  }    

  randombytes_buf(buf,sizeof(psd_nonce_t));  

  return ret;
}




void print_data_line(ssize_t offset, unsigned char* byte, size_t nbyte) {
   printf("  %08lx:",offset);
   for(size_t i = 0; i < nbyte; ++i) {
     if (i % 2 == 0) printf(" ");
     printf("%02x",byte[i]); 
   }
   
   for(size_t i = nbyte; i < 16; ++i) {
     if (i % 2 == 0) printf(" ");
     printf("  "); 
   }
   
   printf("  ");
   for(size_t i = 0; i < nbyte; ++i) {
     if (isprint(byte[i])) {
       printf("%c",byte[i]); }
     else {
       printf("."); }
   }
   printf("\n");  
     
}

void* print_format_data(const char* fmt, unsigned char* data,ssize_t z) {
  char tmpstr[256];
  tmpstr[0] = 0;
  uint64n_t sd;
  psd_nonce_t* nonce;
 
  if (fmt == NULL) return data;

  printf("\n");

  switch(parse_format(fmt)) {
    case IP_FMT:
      if (z < 16) break; 
      if (inet_ntop(AF_INET6,data,tmpstr,256) == NULL) break;
      printf("    - %s %s\n",fmt,tmpstr);
      return data+16;
    case U64_FMT:
      memcpy(sd.data,data,sizeof(uint64_t));
      if (z < (ssize_t)sizeof(sd)) break; 
      printf("    - %s %lX\n",fmt,get_uint64n(&sd));
      return data+sizeof(sd);
    case NONCE_FMT:
      nonce = (psd_nonce_t*)data;
      if (z < (ssize_t)sizeof(psd_nonce_t)) break; 
      printf("    - nonce ");
      for(size_t i = 0; i < sizeof(*nonce); ++i) {
        printf("%02X",(*nonce)[i]); }
      printf("\n"); 
      return data+sizeof(*nonce);
    case PSD_FMT:
      fprint_psd_entry(stdout,(PS_D*)data,z);
      return data+z;
    case PSPKT_FMT:
      fprint_pspkt_entry(stdout,(PSPKT*)data,z);
      return data+z;
    default:
      printf("  FORMAT NOT IMPLEMENTED AND IGNORED");
      return data;
  }
  
  printf("  INVALID DATA FOR FORMART. FORMAT IGNORED!\n");
  return data;
}


/* List entries of the batch data */
int ls_main(int argc,char** argv) {
  
  ssize_t l;
  char* file = argv[2];
  size_t offset = 0;
  
  /* TODO: Implement format */
  BD* bd = load_batchfile(uint8_t,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read input file" );
    exit(EXIT_FAILURE); 
  }

  printf("\n");
  for (size_t i = 0; i < bd->entry_count; ++i) {
    printf("Entry %zd of %zd bytes:\n",i,bd->entry[i].size);
    unsigned char* data = bd->entry[i].data;
    ssize_t z  = bd->entry[i].size;  
    for(ssize_t j=3; j<argc; ++j) {
      data = print_format_data(argv[j],data,z);
      offset = data - (unsigned char*)bd->entry[i].data;
      if ( bd->entry[i].size < offset ) return 0; 
      z = bd->entry[i].size - offset; }  
    ssize_t lines = z / 16;
    printf("\n");
    for ( l = 0; l < lines; ++l) {
      print_data_line(offset+l*16,&data[l*16],16); }
    if (z % 16 != 0) {
      print_data_line(offset+l*16,&data[l*16],z%16); 
    }
    printf("\n");
  } 
  

  return 0;

}


