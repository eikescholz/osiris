#include "bdt.h"

/* bdt mkRrandomPSBD $ENTCOUNT $ENTSIZE $SRCADDR $SRCPORT $DSTADDR $DSTPORT */
int mkRandomPSDB_main(int argc,char** argv) {
 
  unsigned char seed[RG_SEEDBYTES];
  char* end;      
  size_t entry_count;
  size_t payload_size;
  size_t entry_size;
  struct in6_addr src_addr;
  struct in6_addr dst_addr;
  uint64_t src_port;
  uint64_t dst_port;
  size_t ret = 0;
  PSPKT_BD* bd;

  if (argc < 8) {
    fprintf(stderr,"ERROR (bdt): Missing arguments!\n "  
    "bdt mkRrandomPSBD $ENTCOUNT $ENTSIZE $SRCADDR $SRCPORT $DSTADDR $DSTPORT"
    "\n");   
    exit(EXIT_FAILURE);
  }

  entry_count = strtoll(argv[2],&end,0);
  if (*end != 0) {
    fprintf(stderr,"ERROR (bdt): Invalid entry count '%s'\n",argv[2]);
    exit(EXIT_FAILURE);
  }

  entry_size = strtoll(argv[3],&end,0);
  if (*end != 0) {
    fprintf(stderr,"ERROR (bdt): Invalid entry size '%s'\n",argv[3]);
    exit(EXIT_FAILURE);
  }

  if (entry_size < PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE) {
    fprintf(stderr,"ERROR (bdt): entry size too small. "
                   "Must be at least '%d'\n",
                   PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE);
    exit(EXIT_FAILURE);
  }
  
  payload_size = entry_size - PSD_HEADER_SIZE - PSPKT_FOOTER_SIZE;

  if ( ip6pton(argv[4],&src_addr) != 0) {
    fprintf(stderr,"ERROR (bdt): Invalid source address '%s'\n",argv[4]);
    exit(EXIT_FAILURE);
  }

  src_port = strtoll(argv[5],&end,16);
  if (*end != 0) {
    fprintf(stderr,"ERROR (bdt): Invalid source port '%s'\n",argv[5]);
    exit(EXIT_FAILURE);
  }

  if ( ip6pton(argv[6],&dst_addr) != 0) {
    fprintf(stderr,"ERROR (bdt): Invalid destination address '%s'\n",argv[6]);
    exit(EXIT_FAILURE);
  }

  dst_port = strtoll(argv[7],&end,16);
  if (*end != 0) {
    fprintf(stderr,"ERROR (bdt): Invalid destination port '%s'\n",argv[7]);
    exit(EXIT_FAILURE);
  }

  //printf("Generating %lu %lu",entry_count, entry_size);

  bd = new_batchdata(PSPKT, entry_count, entry_count*entry_size );
    
  if( bd == 0 ) {
    perror("ERROR (bdt): Could not allocate batch\n");
    exit(EXIT_FAILURE);
  }

  randombytes_buf(seed,RG_SEEDBYTES);
 
  for(size_t i=0; i<entry_count; ++i) {
     if ( 
      new_random_pspkt_batchentry(
            bd,
            seed,
            &src_addr,
            &dst_addr,
            src_port,
            dst_port,
            payload_size ) == NULL ) {
       fprintf(stderr,
               "ERROR (bdt): Could not create random batch entry %zu : %s\n",
                i,strerror(errno));
       exit(EXIT_FAILURE);
    }        
  }

  /* we tread all packets created at one */
  MicroTime t0 = microtime(NULL);
  for(size_t i=0; i<entry_count; ++i) {
    PSPKT_BE* be = &bd->entry[i];
    set_int64n(&be->data->header.deadline, t0 + 100000 );
    be->size -= PSPKT_FOOTER_SIZE; /* dont write out the footers */
  }
    
  write_batchdata(PSPKT,1,bd); /* write to stdout */

  delete_batchdata(PSPKT,bd);

  return ret;

}


