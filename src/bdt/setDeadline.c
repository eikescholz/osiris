#include "bdt.h"


int setDeadline_main(int argc,char** argv) {

  char* file = argv[2];
  size_t ret = 0;
  MicroTime offset,newDeadline;

  if ( argc == 3 ) {
    fprintf(stderr,"Argument missing\n"
                   "Usage: bdt setTime FILE now+MICROSECONDS\n!");
    exit(EXIT_FAILURE);
  }
  assert(argc >= 4);

  if ( sscanf(argv[3],"now+%ld",&offset) != 1 ) {
    fprintf(stderr,"ERROR (bdt): Invalid argument '%s'\n"
                   "Usage: bdt setTime FILE now+MICROSECONDS\n!",argv[3]); 
    exit(EXIT_FAILURE);
  }

  PSPKT_BD* bd = load_batchfile(PSPKT,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read file" );
    exit(EXIT_FAILURE); 
  }

  newDeadline = microtime(NULL) + offset;

  for(size_t i =0 ; i < bd->entry_count; ++i) {
    set_int64n(&bd->entry[i].data->header.deadline,newDeadline);
  }

  if ( store_batchfile(PSPKT,file,bd) < 0 ) {
    perror("ERROR (bdt): Could not write file" );
    exit(EXIT_FAILURE); 
  }

  delete_batchdata(PSPKT,bd);

  return ret;

}



