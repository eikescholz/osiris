#include "bdt.h"

int cropToPsdPayloads_main(int argc,char** argv) {
 
  argc = argc ; /* avoid warning about unused argc */ 
  
  char* file = argv[2];

  PSPKT_BD* bd = load_batchfile(PSPKT,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read input file" );
    exit(EXIT_FAILURE); 
  }

  size_t size = 0;
  for( size_t i=0; i< bd->entry_count; ++i ) {
    if ( bd->entry[i].size < PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE ) {
      fprintf(stderr,"ERROR (bdt): Corrupted Batch file! "
                     "Entry %zu too small for valid datagram.\n",i );
      exit(EXIT_FAILURE);  
    }
    size += bd->entry[i].size - PSD_HEADER_SIZE - PSPKT_FOOTER_SIZE;
  }


  BD* retBD = new_batchdata(uint8_t,bd->entry_count,size);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read input file" );
    exit(EXIT_FAILURE); 
  }

  for(size_t i=0; i< bd->entry_count; ++i ) {
    add_batchentry(
        uint8_t,
        i,
        retBD,
        bd->entry[i].data->payload,
        bd->entry[i].size - PSD_HEADER_SIZE - PSPKT_FOOTER_SIZE );
  }

  if ( store_batchfile(uint8_t,file,retBD) < 0 ) {
    fprintf(stderr,"Could not write file '%s' (%s)\n",file,strerror(errno));
     exit(EXIT_FAILURE);   
  }

  delete_batchdata(PSPKT,bd);
  delete_batchdata(uint8_t,retBD);

  return 0;

}
