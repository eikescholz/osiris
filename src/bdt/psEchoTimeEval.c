#include "bdt.h"
#include "pscore.h"

int psEchoTimeEval_main(int argc,char** argv) {
 
  MicroTime t1,t2,dt,dt_sum,dt_avg,max_dt,min_dt,min_t1,max_t2,timeout;
  uint64_t packets_per_second; 
  
  argc = argc; /* avoid warning about unused argc */ 
  
  char* file = argv[2];

  PSPKT_BD* bd = load_batchfile(PSPKT,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read input file" );
    exit(EXIT_FAILURE); 
  }

  dt_sum = 0;
  max_dt = 0;
  min_dt = MICROTIME_MAX;
  max_dt = 0;
  min_t1 = MICROTIME_MAX;
  max_t2 = 0;

  for(size_t i=0; i< bd->entry_count; ++i ) {
    if (bd->entry[i].size < PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE) {
      fprintf(stderr,"\nERROR (bdt): Corrupted batch file! "
                     "Entry %zu too small for valid datagram.\n",i );
      exit(EXIT_FAILURE);  
    }

    PSPKT_BE* be = &bd->entry[i];

    /* psecho original packet creation time */
    t1 = get_int64n(&be->data->header.mpid);     

    ps_load_sockTimeout( &timeout,
                         &bd->entry[i].data->header.dst_addr,
                         get_uint64n(&bd->entry[i].data->header.dst_port) );

    /* psecho response packet creation time */
    t2 = get_int64n(&be->data->header.deadline) - timeout;  
  
    if (t2 < t1) {
       fprintf(stderr,"\nWARNING: Time measurement inconsistent. Response "
                      "earlier then request!\n"); 
    }
    
    dt = t2 - t1;
  
    if (dt_sum >= MICROTIME_MAX - dt) {
      fprintf(stderr,"\nERROR: Integer overflow!\n"
                      "earlier then request!\n"); 
      exit(EXIT_FAILURE);
    }


    dt_sum += dt;
    max_dt = (dt > max_dt) ? dt : max_dt; 
    min_dt = (dt < min_dt) ? dt : min_dt; 
    max_t2 = (t2 > max_t2) ? t2 : max_t2; 
    min_t1 = (t1 < min_t1) ? t1 : min_t1; 

  }
  
  dt_avg = ((1000000*dt_sum) / bd->entry_count)/1000000;
/*
  MicroTime avg_seconds = dt_avg/1000000L;
  MicroTime avg_microseconds = dt_avg%1000000L;

  MicroTime max_dt_seconds = max_dt/1000000L;
  MicroTime max_dt_microseconds = max_dt%1000000L;

  MicroTime min_dt_seconds = min_dt/1000000L;
  MicroTime min_dt_microseconds = min_dt%1000000L;
*/
  MicroTime batch_dt = max_t2 - min_t1; 
/*
  MicroTime batch_dt_seconds = batch_dt/1000000L;
  MicroTime batch_dt_microseconds = batch_dt%1000000L;
*/
 
  if ( batch_dt != 0 ) { 
    packets_per_second = (1000000L*bd->entry_count)/batch_dt;
  } else {
    packets_per_second = 0;
  }
 

/*
  printf("%07zu %ld.%ld %ld.%ld %ld.%ld %ld.%ld %lu\n",
                  bd->entry_count,
                  min_dt_seconds,
                  min_dt_microseconds,
                  avg_seconds,
                  avg_microseconds,
                  max_dt_seconds,
                  max_dt_microseconds,
                  batch_dt_seconds,
                  batch_dt_microseconds,
                  packets_per_second
                  );
*/

  printf("%07zu %lu %lu %lu %lu %lu\n",
                  bd->entry_count,
                  min_dt,
                  dt_avg,
                  max_dt,
                  batch_dt,
                  packets_per_second
                  );


  delete_batchdata(PSPKT,bd);

  return 0;

}


