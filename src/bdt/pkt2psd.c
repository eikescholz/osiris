#include "bdt.h"

int pkt2psd_main(int argc,char** argv) {

  argc = argc; /* avoid warning about unused argc */  

  char* file = argv[2];
  size_t ret = 0;

  assert(argc >= 3);

  PSPKT_BD* bd = load_batchfile(PSPKT,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read file" );
    exit(EXIT_FAILURE); 
  }

  for(size_t i =0 ; i < bd->entry_count; ++i) {
    if (bd->entry[i].size < PSPKT_FOOTER_SIZE) {
      printf("ERROR (bdt): Entry to small to contain a psd footer to be "
             "dropped\n");
      exit(EXIT_FAILURE);
    }
    bd->entry[i].size -= PSPKT_FOOTER_SIZE;
  }

  if ( store_batchfile(PSPKT,file,bd) < 0 ) {
    perror("ERROR (bdt): Could not write file" );
    exit(EXIT_FAILURE); 
  }

  delete_batchdata(PSPKT,bd);

  return ret;

}



