#include "bdt.h"

int merge_main(int argc,char** argv) {
  
  if (argc < 3) {
    fprintf(stderr,"ERROR (bdt): Missing arguments! 'bdt merge FILE0 ... FILE0'\n");   
    exit(EXIT_FAILURE);
  }

  size_t n = argc - 2;
  BD** bins = malloc((n+1)*sizeof(*bins));
  
  for (size_t i = 0; i < n; i++) {
    const char* file = argv[i+2]; 
    bins[i] = load_batchfile(uint8_t,file);
    if (bins[i] == NULL) {
      fprintf(stderr,"ERROR (bdt): Could not read input file '%s' (%s)\n",
                      file,strerror(errno));
      exit(EXIT_FAILURE);
    }
  }
  bins[n] = NULL;

  BD* bd_merged = new_merged_batchdata(uint8_t,bins);

  write_batchdata(uint8_t,1,bd_merged); /* write to stdout */

  for(size_t i = 0; i < n; ++i ) {
    delete_batchdata(uint8_t,bins[i]);
  } 

  free(bins);
 
  delete_batchdata(uint8_t,bd_merged);

  return 0;

}


