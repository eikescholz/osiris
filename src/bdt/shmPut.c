#include "bdt.h"

int shmPut_main(int argc,char** argv) {

  argc = argc; /* avoid warning about unused argc */  

  char* file = argv[2];
  size_t ret = 0;

  assert(argc >= 3);

  BD* bd = load_batchfile(uint8_t,file);
  if (bd == NULL) {
    perror("ERROR (bdt shmPut): Could not read file" );
    exit(EXIT_FAILURE); 
  }

  char* basename;

  for (basename = file + strlen(file); 
          basename != file && *basename != '/'; 
              --basename);
  while ( *basename++ == '/');
  basename--;

  if ( strlen(basename+1) > NAME_MAX ) {
      fprintf(stderr,"ERROR (bdt shmPut): File name to long to " 
                      "make shared memory batchdata "
                      "name. \n");
      exit(EXIT_FAILURE);
    }
 
  char shmName[NAME_MAX+1];
  memset(shmName,0,NAME_MAX+1);

  shmName[0] = '/';
  strcpy(&shmName[1],basename);

  if ( shmbd_put_batchdata(bd,shmName) < 0 ) {
      fprintf(stderr,"ERROR (bdt shmPut): Could not put %s "
                      "to shared memory (%s). \n",
                      file,strerror(errno));
      exit(EXIT_FAILURE);
  }

  printf("%s\n",shmName);

  delete_batchdata(uint8_t,bd);

  return ret;

}



