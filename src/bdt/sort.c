#include "bdt.h"
#include "sort.h"


PSPKT_COMPARE get_sort_compare(enum sort_mode_e mode) {
  switch (mode) {
    case SORT_BY_PSSRCIP:   return pspktBeSrcIP6Cmp;
    case SORT_BY_PSDSTIP:   return pspktBeDstIP6Cmp;
    case SORT_BY_PSSRCPORT: return pspktBeSrcPortCmp;
    case SORT_BY_PSDSTPORT: return pspktBeDstPortCmp;     
    case SORT_BY_PSNONCE:   return pspktBeNonceCmp;
    case UNKNOWN_SORT:       return (PSPKT_COMPARE)NULL;
  }   
  return (PSPKT_COMPARE)NULL;
}

enum sort_mode_e parse_sort_mode(const char* mode) {
  if (strcmp(mode,"byPSSRCIP") == 0) return SORT_BY_PSSRCIP;
  if (strcmp(mode,"byPSDSTIP") == 0) return SORT_BY_PSDSTIP;
  if (strcmp(mode,"byPSSRCPORT") == 0) return SORT_BY_PSSRCPORT;
  if (strcmp(mode,"byPSDSTPORT") == 0) return SORT_BY_PSDSTPORT;
  if (strcmp(mode,"byPSNONCE") == 0) return SORT_BY_PSNONCE; 
  return UNKNOWN_SORT;
}

int sort_main(int argc,char** argv) {
  
  char* file = argv[2];
  enum sort_mode_e mode;
  size_t ret = 0;

  if (argc < 4) {
    fprintf(stderr,"ERROR (bdt): Missing aguemnt MODE! 'bdt sort %s MODE'\n",
                    file);   
    exit(EXIT_FAILURE);
  }

  mode = parse_sort_mode(argv[3]);
  if (mode == UNKNOWN_SORT) {
    fprintf(stderr,"ERROR (bdt): Unknown sort mode '%s'\n",argv[3]);
    exit(EXIT_FAILURE);
  }

  PSPKT_BD* bd = load_batchfile(PSPKT,file);
  if (bd == NULL) {
    perror("ERROR (bdt): Could not read input file" );
    exit(EXIT_FAILURE); 
  }

  sort_batchdata(PSPKT,bd,get_sort_compare(mode));
  if ( store_batchfile(PSPKT,file,bd) < 0 ) {
    fprintf(stderr,"Could not write file '%s' (%s)\n",file,strerror(errno));
    ret = 1;
  }

  delete_batchdata(PSPKT,bd);

  return ret;

}


