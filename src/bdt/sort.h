#ifndef BDT_SORT_H
#define BDT_SORT_H

enum sort_mode_e {
  SORT_BY_PSSRCIP,
  SORT_BY_PSDSTIP,
  SORT_BY_PSSRCPORT,
  SORT_BY_PSDSTPORT,
  SORT_BY_PSNONCE,
  UNKNOWN_SORT
};

PSPKT_COMPARE get_sort_compare(enum sort_mode_e mode);
enum sort_mode_e parse_sort_mode(const char* mode);



#endif
