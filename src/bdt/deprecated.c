/* Not used anymore, but might become useful again */

int this_translation_unit_is_not_empty_and_valid_iso_C = 1;

#if 0
/* dumps a batchdata file containing the arguments are entryies
 * to standard out. (For creating simple test data) */
int zipargs_main(int argc, char** argv) {
 if (argc < 3) {
   printf("ERROR (bdt): Empty batchdata files are not possible\n");
   exit(EXIT_FAILURE);  }

 size_t size = 0;
 for(ssize_t i=0; i < argc -2 ; ++i) size += strlen(argv[i+2]);
   

 BD* bd = new_batchdata(argc - 2,size);
 if (bd == NULL) {
   printf("ERROR (bdt): could not allocate batchdata\n");
   exit(EXIT_FAILURE);
 }

 for(ssize_t i=0; i < argc -2 ; ++i) {
   if ( new_batchentry(bd,argv[2],strlen(argv[i+2])) == NULL) {
     printf("ERROR (bdt): out of memory");
     exit(EXIT_FAILURE); 
   }    
 }
   

 write_batchdata(1,bd);

 delete_batchdata(bd);
        
 return 0;

}


int append_main(int argc, char** argv) {
 if (argc < 3) {
   fprintf(stderr,"ERROR (bdt): Empty batchdata files are not possible\n");
   exit(EXIT_FAILURE);  }
 uint64_t sd;
 char* end;
 char  buf[PSD_MAX_IP6PACKET_SIZE];
 char* ptr = buf;
 char* bufEnd = buf+PSD_MAX_IP6PACKET_SIZE;
 const char* arg;

 if (sodium_init() < 0) {
   fprintf(stderr,"ERROR (bdt): Could not initialize libsodium");
   exit(EXIT_FAILURE);
 }  

 if (argc > 3) {
   for(ssize_t i=0; i< argc - 3; i+=1 ) {
    const char* fmt = argv[3+i];
    switch (parse_format(fmt)) {
       case IP_FMT:
         if (i+4 >= argc) {
           fprintf(stderr,"ERROR: Last argument missing\n");
           exit(EXIT_FAILURE); }     
         arg = argv[4+i];
         ++i;
         ptr = push_ip6addr(ptr,arg,bufEnd);
         break;       
       case U64_FMT: 
         if (i+4 >= argc) {
           fprintf(stderr,"ERROR: Last argument missing\n");
           exit(EXIT_FAILURE); }      
         arg = argv[4+i];
          ++i;
         sd = strtoull(arg,&end,0);
         if (*end != '\0') {
           fprintf(stderr,"ERROR: Invalid persistent socket descriptor %s\n",arg);
           exit(EXIT_FAILURE); }
         ptr = push_uint64(ptr,hton_uint64(sd),bufEnd);
         break;
       case NONCE_FMT:
         ptr = push_nonce(ptr,bufEnd);
         break;
       case PSD_FMT:
         fprintf(stderr,"ERROR: PSD can not be provided as arg %s\n",arg);
         exit(EXIT_FAILURE);         
         
     }
    }
  }
  
  ssize_t z = ptr - buf;
  ssize_t n = uninterruptable_read(0,ptr,PSD_MAX_IP6PACKET_SIZE-z);
 
  BD* bd = load_batchfile(argv[2]);
 
  if ( set_batchdata_capacities(bd,bd->entry_count+1,bd->data_size+z) < 0 ) {
    fprintf(stderr,"ERROR (bdt): Out of memory");
    exit(EXIT_FAILURE);
  }

  if ( new_batchentry(bd,buf,z+n) == NULL) { 
    fprintf(stderr,"ERROR (bdt): out of memory");
    exit(EXIT_FAILURE);
  }

  int fd = uninterruptable_open( argv[2], O_RDWR | O_CREAT | O_TRUNC,0640 ); 
  if (fd == -1) {
    perror("");
    fprintf(stderr,"while trying to open %s\n",argv[2]);
    exit(EXIT_FAILURE); }
    
  if ( write_batchdata(fd,bd) < 0) {
    perror("");
    fprintf(stderr,"while writing file %s\n",argv[2]);
    exit(EXIT_FAILURE);
  }

  delete_batchdata(bd);

  return 0;
}
#endif


