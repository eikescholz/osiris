#include "bdt.h"

int shmGet_main(int argc,char** argv) {

  argc = argc; /* avoid warning about unused argc */  

  char* file = argv[2];
  size_t ret = 0;

  assert(argc >= 3);

  char* basename;

  for (basename = file + strlen(file); 
          basename != file && *basename != '/'; 
              --basename);
  while ( *basename++ == '/');
  basename--;

  if ( strlen(basename+1) > NAME_MAX ) {
      fprintf(stderr,"ERROR (bdt shmGet %s): File name to long to make shared" 
                      "memory batchdata name. \n",file);
      exit(EXIT_FAILURE);
    }
 

  char shmName[NAME_MAX+1];
  memset(shmName,0,NAME_MAX+1);

  shmName[0] = '/';
  strcpy(&shmName[1],basename);


  if ( geteuid() == 0) { 
    struct group* gr = getgrnam("PSDAEMON");
    if ( gr == NULL ) {
      fprintf(stderr,"WARNING (psd): Could get group entry for PSDAEMON. "
                     "Failed to acquire PSDAEMON rights\n"); 
    } else {
      if ( setregid(gr->gr_gid,gr->gr_gid) < 0 ) {
        fprintf(stderr,"WARNING (psd): Set egid to PSDAEMON. "
                       "Failed to acquire PSDAEMON rights\n");
      }
    }
  }


  ShmBd shmbd; 

  if( shmbd_load(&shmbd,shmName) < 0 ) {      
    fprintf(stderr,"ERROR (bdt shmGet %s): " 
                   "Could not open shared memory batchdata %s\n"
                   " (%s)\n",file, shmName,strerror(errno) );
    exit(EXIT_FAILURE); 
  }

  BD* bd = new_batchdata_from_shmbd(uint8_t,&shmbd);
  if (bd == NULL) {
    fprintf(stderr,"ERROR (bdt shmGet %s): Unable to create new batchdata "
                    "(%s) \n", file,strerror(errno) );
    exit(EXIT_FAILURE); 
  }

  char partFile[NAME_MAX+1+5]; /* .part*/ 

  stpcpy(stpcpy(partFile,file),".part");        
 
  if ( store_batchfile(uint8_t,partFile,bd) < 0 ) {
    fprintf(stderr,"ERROR (bdt shmGet %s): Could write %s (%s)\n",file,file,
                    strerror(errno));
    exit(EXIT_FAILURE); 
  }

  if ( unlink(file) < 0 && errno != ENOENT ) {
    fprintf(stderr,"ERROR (bdt shmGet %s): Could not unlink %s (%s)\n",
                    file,file,strerror(errno));
    exit(EXIT_FAILURE); 
  } 
 
  /* atomic result creation */ 
  if ( link(partFile,file) < 0 ) {
    fprintf(stderr,"ERROR (bdt shmGet %s): Could not create %s (%s)\n",
                    file,file,strerror(errno));
    exit(EXIT_FAILURE); 
  } 

  if ( unlink(partFile) < 0 && errno != ENOENT ) {
    fprintf(stderr,"WARNING (bdt shmGet %s): Could not unlink %s (%s)\n",
                    file,partFile,strerror(errno));
  } 
 

  if( shmbd_unlink(&shmbd) < 0 ) {
    fprintf(stderr,"WARNING (bdt shmGet %s): Could unlink shared memory "
                    "batchdata (%s)\n",file,strerror(errno) );
  }


  delete_batchdata(uint8_t,bd);

  return ret;

}



