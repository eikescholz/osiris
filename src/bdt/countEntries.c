#include "bdt.h"

int countEntries_main(int argc,char** argv) {
  
  if (argc < 3) {
    printf("0\n");
    exit(EXIT_SUCCESS);
  }

  size_t count = 0;

  size_t n = argc - 2;
 
  for (size_t i = 0; i < n; i++) {
    const char* file = argv[i+2]; 
    BD* bd = load_batchfile(uint8_t,file);
    if (bd == NULL) continue; /* we ignore wrong files*/
   
    count += bd->entry_count;
/*
    fprintf(stderr,"File %s has %zu entries (cur sum %zu) \n",
                    file,bd->entry_count,count); 
*/
    delete_batchdata(uint8_t,bd);
  }

  printf("%lu\n",count);

  return 0;

}


