#include "eacore.h"


int ea_atomic_update_saldo(
      const PS* thisAccount,
      int64_t amount,
      const ART* article ) {

  char artPath[EA_MAX_PATH_SIZE];
  char saldoPath[EA_MAX_PATH_SIZE];
  struct stat st;
  int fd,r;
  int64_t newSaldo,curSaldo;
  FILE* strm;


  if ( ea_mkdir_inventoryArticlePath(artPath,thisAccount,article) < 0 ) {
    fprintf(stderr,"ERROR (%s): Can not create inventory article path %s"
                   " (%s)\n", ps_mainIdent,saldoPath,strerror(errno) );
    return -1;   
  }
 
  vstpcpy(saldoPath,artPath,"/saldo",NULL);

  fd = flopen(saldoPath,O_CREAT,O_RDWR,0660); 
  if (fd < 0 ) {
    fprintf(stderr,"ERROR (%s): Can not open and lock saldo file %s (%s)\n", 
                   ps_mainIdent,saldoPath,strerror(errno) );
    return -1; 
  } 

  if ( fstat(fd,&st) < 0 ) {
    fprintf(stderr,"ERROR (%s): Can not fstat open saldo file %s (%s)\n", 
                   ps_mainIdent,saldoPath,strerror(errno) );
    return -1;  
  }
  
  strm = fdopen(fd,"r+");
  if ( strm == 0 ) {
    fprintf(stderr,"ERROR (%s): Can not fdopen file descriptor of "
                   " file %s (%s)\n",ps_mainIdent,saldoPath,strerror(errno));
    return -1;  
  }

  if ( st.st_size == 0 ) { /* empty thus newly created file */

    newSaldo = amount;

  } else {
    if ( fscanf(strm,"%ld",&curSaldo ) != 1 ) {
      fprintf(stderr,"ERROR (%s): Can not read saldo in file %s (%s)\n", 
                   ps_mainIdent,saldoPath,strerror(errno) );
      return -1;   
    }

    newSaldo = curSaldo + amount; 

    if ( fseek(strm,0L,SEEK_SET) ) {
      fprintf(stderr,"UNLIKELY ERROR (%s): Can seek to beginning of saldo "
                     "file %s (%s)\n", 
                     ps_mainIdent,saldoPath,strerror(errno) );
      return -1;   
 
    }

  }

  if ( fprintf(strm,"%ld",newSaldo) < 0 ) {
    fprintf( stderr,"ERROR (%s): Can not update saldo in file %s (%s)\n", 
             ps_mainIdent,saldoPath,strerror(errno) );
    return -1;        
  }

  do {
    r = fclose(strm); 
  } while ( r == -1 && errno == EINTR );
 
  if ( r < 0 ) {
    fprintf( stderr,"WARNING (%s): Could not close file %s (%s)\n", 
             ps_mainIdent,saldoPath,strerror(errno) );
    if ( flock(fd,LOCK_UN) < 0 ) { /* disaster avoidance unlock */      
      fprintf( stderr,"OMEN OF DOOM (%s): Deadlock of %s "
                      "imminent. Could remove file lock (%s)\n", 
              ps_mainIdent,saldoPath,strerror(errno) ); 
    }
  }

  return 0;

} 



