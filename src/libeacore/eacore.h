#ifndef EACORE_H
#define EACORE_H

/* The eidetic accounting core library 
 *
 * The following conventions are used
 *  A inventory id is given by a
 *   ipv6 address and an 8 byte port id.
 *   a accont id is an inveotry id and an 8 byte "commodity" type
 *
 * Accounts doing exchanges with each other need to agree on the 
 * meaning of the commodity id. 
 *
 * The connection peer with the smaller debit inventory id will
 * send original packets the other dual packets for the debit
 * authorization. We call the exchange parter with the smaller 
 * account id the minor, the other the mayor. 
 *
 * a debit packet is an authorisation if it was send and signed
 * by an owner connection, otherwise it is a request. 
 *
 * The transaction uid is (nonce,time,mpid) and must be the same 
 * for sender and receiver. This implies that for a valid provision
 * both required packets are in the same batch. The max deadline of
 * the both accountants ports can be used to compute deterministic
 * the timestamp of the packets required for the acquisition,
 * futher timestamps are allows by the psd protocol. That results
 * in both of these packets, if they arrive to be in the same batch.   
 *
 * For a debit autorisation *and* request, the header provides the following. 
 *
 *   - dst_addr : Owners account addr part
 *   - dst_port : Owners account inventory 
 *   
 * Unless the autorither and the requester are the same onwner,
 * the debit and credit entries need to have different data layout.
 * If they are equal debit and credit equality are not an issue, its a 
 * feature, the bookeeping can be ignored, since is is the   
 *
 * In the theory paper the minor is Alice and the Mayor is Bob.
 * The protocol is not exactly symmetric so the roles need to be
 * defined. Sine no role is at an disatvatage the account id 
 * is perfectly fine. Note that account ids are unique, so no to persons
 * can have the same account id unless it is the same account. 
 *
 * It seems that we get a feature for free that allows transaction only
 * if they come from a number of different authorizers (I.e. nodes/clusters)
 * TODO: Later 
 *
 * There is a "restriction" that the debit and credit inventories of
 * one exchange partners transaction have to be managed by the same
 * Accountantin service, i.e. node/cluster. Otherwise the protocol
 * would have to be more complex and thus slower. In some cases some 
 * intermediate inventories must be created if that is not wanted.  
 * On the other hand regarding liability the accountants will prefer  
 * this "restricted" solution due to liability issues. 
 *
 * TODO: Nomenclature owners should be differntiated from  users. From 
 *       the accountant perspective they are users of the inventory,
 *       and an owner ... 
 */

#include "escore.h"

/* TODO: Better estimation */
#define EA_MAX_PATH_SIZE (23*256)

/* there are 4 transmission stages 
 *
 * 1. sending provisions
 * 2. sending provision ACKS and clear request's 
 * 3. sending acquisitions and clear acks
 * 4. sending acquisition acks
 *
 * The first nibble of the type is the stage number encoded
 * by the following flags 
 */
#define EA_ACK 0x30 /* bits 2 and 3 */
#define EA_AQU 0xC0 /* bits 0 and 1 zero means provision packet */

/* the second nibble identifies the packet type of the stage */
 
#define EA_DUAL 0x03 /* bits 6 and 7 */ 
#define EA_VER  0x0C /* bits 4 and 5 - verification packets testimony 
                                       between accountants */
/* The middle stage is subdivieded into different taskes
 * for the accountants and clieents so we use the first
 * 6 bits as a task id */

#define EA_TASK_MASK (EA_ACK|EA_AQU|EA_CLR)

/* There would be the possibility to add a short ASCII description
 * but that's a wasteful encoding, there are probably a lot of
 * identical descriptions, So a big enough structured id is good
 * and more deterministic in size and runtime */

struct ea_exchange_description_id_t {
  uint64n_t dscSpecifierID; /* identifiers the same persons as for article ids */
  uint64n_t dscCategory;
  uint64n_t dscClass;
  uint64n_t dscID;
} __attribute__((packed));

typedef struct ea_exchange_description_id_t DID;

struct article_type_t {
  uint64n_t specifierID;
  uint64n_t specificationID;
} __attribute__((packed)) ;

typedef struct article_type_t ART;

/* the inventory id is the associated persistant socket i.e.  (IpAddr,Port) */
typedef PS INVID;


/* we split the uid from the nonce data for higher security. */
struct ea_exchange_uid_t {

  int64n_t commitmentDeadline; 
  int64n_t settlementDeadline;

  uint64n_t ourReference;
  uint64n_t theirReference;
 
  uint8n_t  type;
 
} __attribute((packed));

typedef struct ea_exchange_uid_t EUID; 


/* this is api representation of the exchange data */
/* our/their is interpreted as used by the creator/signer
 * of the packet. */

struct ea_exchange_data_t {
  
  INVID     ourCreditInventory;
  INVID     ourDebitInventory;
  int64n_t  ourDebitAmount; /* should be positive, but has signed arithm. */
  ART       ourDebitArticle; 
 
  INVID     theirCreditInventory;
  INVID     theirDebitInventory;
  int64n_t  theirDebitAmount;
  ART       theirDebitArticle; 

  DID       description;

} __attribute ((packed));

typedef struct ea_exchange_data_t EAED;



/* TODO: This should be ps api definition, as crypto-nonce, packet-uid of ... 
 *       Nomenclature needs clarification */

/* response data from accountant to other accountants testimony */
struct ea_clearing_data_t {
  INVID   theirDebitInventory;  /* where to look for orig. packet (just Port?)*/
  PSPRF proof;   /* proof data */
};

typedef struct ea_clearing_data_t EACD;


union ea_exchange_t {
   EAED  data;              /* for provisions/debit auth; rename to auth?*/
   PSPRF proof;             /* for acks verifications */
   EACD  clear;             /* clearing data */  
   uint8_t hash[HASH_SIZE]; /* For aquisitions */
} __attribute__ ((packed));

typedef union ea_exchange_t EAE; 


/* for internal porcessing */
/* wrong layer there the footers are striped here */
struct ea_datagram_t {
 struct esd_header_t header;
 EUID euid;
 EAE exchange;
} __attribute__ ((packed));


typedef struct ea_datagram_t EAD;

_Static_assert( sizeof(EAD) + PSPKT_FOOTER_SIZE <= IP6_GUARANTEED_MTU, 
                 "EAPKT to big" );

typedef BE_T(EAD) EAD_BE;
typedef BD_T(EAD) EAD_BD;

struct ea_exchange_record_t {
  EAD*   datagram;
  PSPRF* proof;
};

typedef struct ea_exchange_record_t EAREC;

typedef BE_T(EAREC) EAREC_BE;
typedef BD_T(EAREC) EAREC_BD;


static inline void ea_load_src(PS* src,const EAD* pkt) {
  
  ip6cpy( &src->addr, &pkt->header.src_addr);
  set_uint64n(&src->port,get_uint64n(&pkt->header.src_port));

}


/* If applications need longer then 256 byte descritions they
 * have to encode an id into this field. However for general purpose
 * tools, it might be useful 

 * the exchange id is also used for the api, data is
 * put into the psd header */


static inline int ps_cmp(const INVID* a, const INVID* b) {
  
  /* TODO: Use own memcmp since the standard does not seem to specify 
   *       the order in which the bytes are compared. */
  int r = memcmp(a,b,sizeof(*a));

  if ( r < 0) return -1;
  if ( r > 0) return 1;
  return 0;

} 



static inline 
bool is_dual_exchangeData(const EAED* a, const EAED* b) {
  
  if ( memcmp( &a->ourCreditInventory,
               &b->theirCreditInventory, 
               sizeof(INVID) )
         != 0 ) return false;           

  if ( memcmp( &a->ourDebitInventory,
               &b->theirDebitInventory, 
               sizeof(INVID) )
         != 0 ) return false;           

  if ( memcmp( &a->ourDebitAmount,
               &b->theirDebitAmount, 
               sizeof(uint64n_t) )
         != 0 ) return false;           

  if ( memcmp( &a->ourDebitArticle,
               &b->theirDebitArticle, 
               sizeof(ART) )
         != 0 ) return false;           

  if ( memcmp( &a->theirCreditInventory,
               &b->ourCreditInventory, 
               sizeof(INVID) )
         != 0 ) return false;           

  if ( memcmp( &a->theirDebitInventory,
               &b->ourDebitInventory, 
               sizeof(INVID) )
         != 0 ) return false;           

  if ( memcmp( &a->theirDebitAmount,
               &b->ourDebitAmount, 
               sizeof(uint64n_t) )
         != 0 ) return false;           

  if ( memcmp( &a->theirDebitArticle,
               &b->ourDebitArticle, 
               sizeof(ART) )
         != 0 ) return false;           


  if ( memcmp( &a->description,
               &b->description, 
               sizeof(DID) )
         != 0 ) return false;           

  return true;

}




static inline 
bool is_dual_exchangeEUID(const EUID* a, const EUID* b) {
  
  if ( (a->type ^ b->type) == 0 ) return false;
  
  if ( memcmp( &a->commitmentDeadline,
               &b->commitmentDeadline, 
               sizeof(int64n_t) )
         != 0 ) return false;           

  if ( memcmp( &a->settlementDeadline,
               &b->settlementDeadline, 
               sizeof(int64n_t) )
         != 0 ) return false;           

  if ( memcmp( &a->ourReference,
               &b->theirReference, 
               sizeof(a->ourReference) )
         != 0 ) return false;           

  if ( memcmp( &a->theirReference,
               &b->ourReference, 
               sizeof(a->ourReference) )
         != 0 ) return false;           

  return true;

}




static inline 
bool is_dual_exchange(const EAD* a, const EAD* b) {
  
  return    is_dual_exchangeEUID( &a->euid,&b->euid )
         && is_dual_exchangeData( &a->exchange.data, &b->exchange.data );         

}




static inline 
void mk_dual_exchangeEUID(EUID* ret, const EUID* d) {
  
  ret->type = d->type ^ EA_DUAL;
  
  memcpy( &ret->commitmentDeadline, &d->commitmentDeadline, sizeof(int64n_t));

  memcpy( &ret->settlementDeadline, &d->settlementDeadline, sizeof(int64n_t));

  memcpy( &ret->ourReference, &d->theirReference, sizeof(ret->ourReference) );

  memcpy( &ret->theirReference, &d->ourReference,sizeof(ret->theirReference));

}
 



static inline 
void mk_dual_exchangeData(EAED* ret, const EAED* d) {
  
  memcpy( &ret->ourCreditInventory, &d->theirCreditInventory, sizeof(INVID) );

  memcpy( &ret->ourDebitInventory, &d->theirDebitInventory, sizeof(INVID) );

  memcpy( &ret->ourDebitAmount, &d->theirDebitAmount, sizeof(uint64n_t) );

  memcpy( &ret->ourDebitArticle, &d->theirDebitArticle, sizeof(ART) );  

  memcpy( &ret->theirCreditInventory, &d->ourCreditInventory, sizeof(INVID) );

  memcpy( &ret->theirDebitInventory, &d->ourDebitInventory, sizeof(INVID) );

  memcpy( &ret->theirDebitAmount, &d->ourDebitAmount, sizeof(uint64n_t) );

  memcpy( &ret->theirDebitArticle, &d->ourDebitArticle, sizeof(ART) );  

  memcpy( &ret->description, &d->description, sizeof(DID) );

}

static inline int ea_recordBeStageCmp(const EAREC_BE* a, const EAREC_BE* b) {

 uint8_t x = a->data->datagram->euid.type & 0xF0;
 uint8_t y = b->data->datagram->euid.type & 0xF0;

 if ( x < y ) return -1;
 if ( x < y ) return  1;
 
 return 0; 

}


static inline 
int ea_recordBeVerificationFlagsCmp(
      const EAREC_BE* a,
      const EAREC_BE* b ) {

 uint8_t x = a->data->datagram->euid.type & 0x0C;
 uint8_t y = b->data->datagram->euid.type & 0x0C;

 if ( x < y ) return -1;
 if ( x < y ) return  1;
 
 return 0; 

}

#if 0
static inline int ea_recordBeTaskeCmp(const EAREC_BE* a, const EAREC_BE* b) {

 /* compares everything but the dual flag bits */
 uint8_t x = a->data->datagram->euid.type & 0xFC;
 uint8_t y = b->data->datagram->euid.type & 0xFC;

 if ( x < y ) return -1;
 if ( x < y ) return  1;
 
 return 0; 

}
#endif 

static inline int ea_normalizedEUIDCmp(const EUID* a, const EUID* b) {

  EUID x, y;
  int r;

  /* make normal forms (i.e. primal form data) */
  if ( (a->type & EA_DUAL) != 0 ) {
    mk_dual_exchangeEUID( &x, a );
  } else {
    memcpy( &x, a, sizeof(EUID));
  }

  if ( (b->type & EA_DUAL) != 0 ) {
    mk_dual_exchangeEUID( &y, b );
  } else {
    memcpy( &y, b, sizeof(EUID) );
  }

  r = memcmp( &x.commitmentDeadline, &y.commitmentDeadline, sizeof(int64n_t) );
  
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.settlementDeadline, &y.settlementDeadline, sizeof(int64n_t) );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.ourReference, &y.ourReference, sizeof(x.ourReference) );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.theirReference, &y.theirReference, sizeof(x.theirReference) );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  uint8_t s = a->type ;
  uint8_t t = b->type ;

  if ( s < t ) return -1;
  if ( s < t ) return  1;
 
  return 0;

}
 
/* TODO: factorize common be eq_recordBeRevCmp */
static inline int ea_recordBeEUIDCmp(const EAREC_BE* a, const EAREC_BE* b) {

  return ea_normalizedEUIDCmp( &a->data->datagram->euid,
                               &b->data->datagram->euid );

}
 


static inline int ea_recordBeCommitmentDeadlineIntervalCmp(
           const EAREC_BE* a, 
           const EAREC_BE* b ) {

  MicroTime s,t;
  
  s = get_int64n( &a->data->datagram->euid.commitmentDeadline );
  t = get_int64n( &b->data->datagram->euid.commitmentDeadline );

  return ps_baseIntervalTimeCmp(&s,&t);

}


/* compares everything but the type
 * assumes the datagram to not be a proof */

static inline int ea_recordBeExchangeDataCmp(
   const EAREC_BE* a, 
   const EAREC_BE* b) {
  
  EAED x, y;
  int r;

  /* make normal forms (i.e. primal form data) */
  if ( (a->data->datagram->euid.type & EA_DUAL) != 0 ) {
    mk_dual_exchangeData( &x, &a->data->datagram->exchange.data );
  } else {
    memcpy( &x, &a->data->datagram->exchange.data, sizeof(x) );
  }

  if ( (b->data->datagram->euid.type & EA_DUAL) != 0 ) {
    mk_dual_exchangeData( &y, &b->data->datagram->exchange.data );  
  } else {
    memcpy(&y,&b->data->datagram->exchange.data,sizeof(y));
  }

  r = memcmp( &x.ourCreditInventory, &y.ourCreditInventory,sizeof(INVID));        

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.ourDebitInventory,  &y.ourDebitInventory, sizeof(INVID)); 
  
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.ourDebitAmount,     &y.ourDebitAmount, sizeof(uint64n_t));
  
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  
               
  r = memcmp( &x.ourDebitArticle, &y.ourDebitArticle, sizeof(ART) );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.theirCreditInventory, &y.theirCreditInventory,sizeof(INVID));        
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.theirDebitInventory,  &y.theirDebitInventory, sizeof(INVID)); 
  
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.theirDebitAmount,     &y.theirDebitAmount, sizeof(uint64n_t));
  
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  
               
  r = memcmp( &x.theirDebitArticle,   &y.theirDebitArticle, sizeof(ART) );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  r = memcmp( &x.description, &x.description, sizeof(DID) );
         
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  return 0; 

} 



/* compares everything but the type
 * assumes the datagram to not be a proof */

static inline int ea_recordBeExchangeCmp(
   const EAREC_BE* a, 
   const EAREC_BE* b) {
 
  int r = ea_recordBeEUIDCmp(a,b);
  
  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;  

  return ea_recordBeExchangeDataCmp(a,b);

} 



static inline
bool ea_our_is_minor(const EAED* x) {

  return ( ps_cmp( &x->ourDebitInventory, &x->ourDebitInventory ) <= 0 );

}




static inline 
void ea_set_eadHeader( 
        EAD* ead,
        MicroTime deadline,
        const PS* src,
        const PS* dst ) {
  
  set_uint32n(&ead->header.vcf,PSPKT_NORMALFORM_VCF);
  set_uint16n(&ead->header.packetSize, sizeof(EAD));
  ead->header.nextHeader = PS_PROTO_NUM;
  ead->header.hopLimit = 0;

  ip6cpy(&ead->header.src_addr,&src->addr);
  ip6cpy(&ead->header.dst_addr,&dst->addr);
   
  randombytes_buf(&ead->header.nonce,PS_NONCE_BYTES + sizeof(uint64_t) );
  set_int64n( &ead->header.mpid, labs( get_int64n(&ead->header.mpid) ) );

  set_int64n( &ead->header.deadline, deadline );

  memset(&ead->header.prevBlockHash,0,ES_HASH_SIZE);
  set_int64n(&ead->header.prevBlockTime,0);

}



static inline
void ea_mk_exchangeACK( 
        EAD* ead,
        MicroTime deadline,
        const PS* src,
        const PS* dst, 
        const EAREC* orig ) {

  ea_set_eadHeader(ead,deadline,src,dst);

  set_int64n( &ead->euid.commitmentDeadline,
              get_int64n(&orig->datagram->euid.commitmentDeadline ) );

  set_int64n( &ead->euid.settlementDeadline,
              get_int64n(&orig->datagram->euid.settlementDeadline ) );

  memcpy( &ead->euid.ourReference,
          &orig->datagram->euid.ourReference,
          sizeof(ead->euid.ourReference) );
 
  memcpy( &ead->euid.theirReference,
          &orig->datagram->euid.theirReference,
          sizeof(ead->euid.ourReference) );
  
  ead->euid.type = EA_ACK | orig->datagram->euid.type ; 

  memcpy( &ead->exchange.proof,
          &orig->datagram->exchange.proof, 
          psd_proofSize(
              get_uint64n( &orig->datagram->exchange.proof.signatureCount) ) ); 

}



static inline 
bool ea_eadSrc_is(const PS* src, const EAD* ead) {
 
  if ( ip6cmp( &src->addr, &ead->header.src_addr ) != 0 ) return false;
  if (    get_uint64n(&src->port)
       != get_uint64n(&ead->header.src_port) ) return false;

  return true;

}


static inline 
bool ea_eadDst_is(const PS* dst, const EAD* ead) {
 
  if ( ip6cmp( &dst->addr, &ead->header.dst_addr ) != 0 ) return false;
  if (    get_uint64n(&dst->port)
       != get_uint64n(&ead->header.dst_port) ) return false;

  return true;

}







static inline void mk_provision(
                     EAD* ret,
                     const PS* ourClient,
                     const PS* accountant, 
                     EUID* euid,
                     EAED* data ) {

   ea_set_eadHeader( ret, 
                     get_int64n(&euid->commitmentDeadline), 
                     ourClient, 
                     accountant );

   memcpy( &ret->euid, euid, sizeof(ret->euid) );

   memcpy( &ret->exchange.data, data, sizeof(ret->exchange.data) );
  
   ret->euid.type = ea_our_is_minor(data) ? 0 : EA_DUAL;

}



/* this does not check if the provided order and request packtes
 * form a valid request, just copies creates the packet from
 * local to dst as if they where valid 
 *
 * For the Accountant his inventory is given by the destination socket
 * of either provision.
 */

static inline 
void mk_provisionACK(
       EAD* ret,
       const PS* ourAcc, 
       const PS* dst, /* ourClient, theirClient or theirAccount */
       EAREC* primComm,
       EAREC* dualComm ) {

  MicroTime commitmentDeadline,settlementDeadline,timeout,validationDeadline;

  assert(    (primComm->datagram->euid.type ) != 0 
          && (dualComm->datagram->euid.type & EA_DUAL) != 0 );

  assert( is_dual_exchange(primComm->datagram, dualComm->datagram ) );

  assert(cmp_int64n( &primComm->datagram->header.deadline,
                     &dualComm->datagram->header.deadline ) ==0 );

  commitmentDeadline 
     = get_int64n(&primComm->datagram->euid.commitmentDeadline );

  settlementDeadline 
     = get_int64n(&primComm->datagram->euid.settlementDeadline );

  assert( commitmentDeadline 
            == get_int64n( &primComm->datagram->header.deadline) );

  assert( commitmentDeadline 
            == get_int64n( &dualComm->datagram->header.deadline) );

  assert( settlementDeadline > commitmentDeadline );

  timeout            = settlementDeadline - commitmentDeadline;
  validationDeadline = commitmentDeadline + (timeout/3);

  if ( ps_cmp( ourAcc, 
               &primComm->datagram->exchange.data.ourDebitInventory ) 
         == 0 ) {
    /* caller is minor */
    if ( ea_eadSrc_is( dst , dualComm->datagram ) ) {
      /* dst is their client */
      ea_mk_exchangeACK( ret, validationDeadline, ourAcc, dst, primComm);
      return;
    }
    /* dst is either their accountant or our client */

    ea_mk_exchangeACK( ret, validationDeadline, ourAcc, dst, dualComm );

  } else {
    /* caller is mayor */
    assert( ps_cmp( ourAcc, 
                    &dualComm->datagram->exchange.data.ourDebitInventory ) 
              == 0 );

    if ( ea_eadSrc_is( dst , primComm->datagram ) == 0 ) {
      /* dst is their client */
      ea_mk_exchangeACK( ret, validationDeadline, ourAcc, dst,  dualComm );
      return;
    }
    /* dst is either their accountant or our client */

    ea_mk_exchangeACK( ret, validationDeadline, ourAcc, dst, primComm );

  }

}


/* TODO: At the moment the clients send complete acquisitions
 *       not just references */

static inline void mk_aquisition(
                     EAD* ret,
                     const PS* ourClient,
                     const PS* accountant, 
                     EUID* euid,
                     const uint8_t* hash ) {

   
   ea_set_eadHeader( ret, 
                     get_int64n(&euid->commitmentDeadline), 
                     ourClient, 
                     accountant );

   memcpy( &ret->euid, euid, sizeof(ret->euid) );

   memcpy( &ret->exchange.hash, hash, sizeof(ret->exchange.data) );

   ret->euid.type = (euid->type & EA_DUAL) | EA_AQU ;

}



static inline void mk_aquisitionACK(
                    EAD* ret,
                    const PS* ourAcc, 
                    const PS* dst,  /* ourClient,theirClient or theirAccount*/
                    EAREC* primAqu,
                    EAREC* dualAqu ) {

  MicroTime settlementDeadline;

  assert(    (primAqu->datagram->euid.type) != 0 
          && (dualAqu->datagram->euid.type & EA_DUAL) != 0 );

  assert( is_dual_exchange( primAqu->datagram, dualAqu->datagram ) );
 
  assert( memcmp( &primAqu->datagram->exchange.data,
                  &dualAqu->datagram->exchange.data, 
                  sizeof(EAED) ) 
           == 0 );

  assert( cmp_int64n( &primAqu->datagram->header.deadline,
                      &dualAqu->datagram->header.deadline )
            ==0 );

  settlementDeadline 
    = get_int64n(&primAqu->datagram->euid.settlementDeadline );

  ea_set_eadHeader(ret,settlementDeadline,ourAcc,dst);

  if ( ps_cmp( ourAcc, &dualAqu->datagram->exchange.data.ourDebitInventory ) 
         == 0 ) {

    /* caller is minor accountant */ 
    if ( ea_eadSrc_is( dst, dualAqu->datagram ) ) {
      /* destination is minor client */

      ea_mk_exchangeACK(ret, settlementDeadline, ourAcc, dst, primAqu );
 
    } else {
      assert( ea_eadSrc_is( dst, primAqu->datagram ) );
      /* dstintation is mayour client */

      ea_mk_exchangeACK( ret, settlementDeadline, ourAcc, dst, dualAqu );
 
    }

  } else {
    assert( ps_cmp( ourAcc, 
                    &primAqu->datagram->exchange.data.ourDebitInventory ) 
              == 0 );
    /* caller is mayor accountant */

    if ( ea_eadSrc_is( dst, primAqu->datagram) ) {
      /* destination is minor client */

      ea_mk_exchangeACK( ret, settlementDeadline, ourAcc, dst, dualAqu );
 
    } else {
      assert( ea_eadSrc_is( dst, dualAqu->datagram) );
      /* dstintation is mayour client */

      ea_mk_exchangeACK( ret, settlementDeadline, ourAcc, dst, primAqu );
 
    }

  }

}




static inline
void ea_mk_rootPath( char* path ) {
  stpcpy( psd_mkRootPath(path),"/ea" ); 
}




static inline
void ea_mk_inventoryPath( 
          char* path,
          const PS* inventory ) {

  char root[PSD_MAX_PATH_SIZE];
  char addrStr[INET6_ADDRSTRLEN];

  ea_mk_rootPath(root);

  ip6ntos( &inventory->addr, addrStr );

  if ( snprintf( path,
                 EA_MAX_PATH_SIZE,      
                 "%s/inventory/%s/%lX",
                 root,
                 addrStr,
                 get_uint64n(&inventory->port) )
         < 0 ) {
    fprintf(stderr,"UNLIKELY WARING (%s): Could not create/store string "
                   "%s/inventory/%s/%lX ",
                   ps_mainIdent,
                   root,
                   addrStr,
                   get_uint64n( &inventory->port ) );
  }

}
 


/* path at the moment only used for the eval hook for
 * client side smart contracts and analysis 
 *
 * It is possible that the same eidetic socket is a client
 * as well as a protocol to construct certain form of smart contract logic. 
 */

static inline
char* ea_mk_clientPath( 
          char* path,
          const PS* inventory ) {

  char root[PSD_MAX_PATH_SIZE];
  char addrStr[INET6_ADDRSTRLEN];

  ea_mk_rootPath(root);

  ip6ntos( &inventory->addr, addrStr );

  if ( snprintf( path,
                 EA_MAX_PATH_SIZE,      
                 "%s/client/%s/%lX",
                 root,
                 addrStr,
                 get_uint64n(&inventory->port) )
         < 0 ) {
    fprintf(stderr,"UNLIKELY WARING (%s): Could not create/store string "
                   "%s/client/%s/%lX ",
                   ps_mainIdent,
                   root,
                   addrStr,
                   get_uint64n( &inventory->port ) );
  }

  return path + strlen(path);

}
 
/* TODO: Investigate if a own/additional eval for specific inventory 
 *       articles is a good interface */

static inline
char* ea_mk_clientEvalPath( 
          char* path,
          const PS* inventory ) {

  return stpcpy(ea_mk_clientPath(path,inventory),"/eval");

}
 





static inline
int ea_mkdir_inventoryArticlePath( 
          char* path,
          const PS* inventory,
          const ART* art) {
  
  char invPath[PSD_MAX_PATH_SIZE];

  ea_mk_inventoryPath(invPath,inventory);

  if ( snprintf( path, EA_MAX_PATH_SIZE, "%s/%lX", invPath,
                 get_uint64n(&art->specifierID) ) < 0 ) {

     fprintf(stderr, "UNLIKELY ERROR (%s): Could not create inventory path " 
                    "string\n", ps_mainIdent ); 
     return -1;
  }

  if ( mkdir( path, 0770 ) < 0 && errno != EEXIST ) {
    fprintf(stderr, "ERROR (%s): Could not create %s\n", 
                    ps_mainIdent, invPath ); 
    return -1;
  }   

  if ( snprintf( path, EA_MAX_PATH_SIZE, "%s/%lX/%lX", invPath,
                 get_uint64n( &art->specifierID ),
                 get_uint64n( &art->specificationID )) < 0 ) {
 
    fprintf(stderr, "UNLIKELY ERROR (%s): Could not create inventory path " 
                    "string\n", 
            ps_mainIdent ); 
    return -1;                 
  }; 

  if ( mkdir( path, 0770 ) < 0 && errno != EEXIST ) {
    fprintf(stderr, "ERROR (%s): Could not create %s\n", 
                    ps_mainIdent, invPath ); 
    return -1;
  }  

  return 0;

}
                              



int ea_commit_main(int argc, char** argv);
int ea_receive_main(int argc, char** argv);



void ea_fprint_euid( 
     FILE* strm,
     EUID* euid);




void ea_fprint_inventoryID( 
  FILE* strm,
  INVID* inv );



void ea_fprint_exchangeDescription(
  FILE* strm,
  DID* arg );


void ea_fprint_articleType(
  FILE* strm,
  ART* arg );


void ea_fprint_exchange( 
  FILE* strm,
  EAED* ex );

 

void ea_fprint_ead( 
  FILE* strm,
  EAD* ead );


int ea_validate( EAREC_BD* recs, PS* thisInventory, char** argv );

int ea_verify( EAREC_BD* recs, PS* thisSock);

int ea_clear( EAREC_BD* recs, PS* thisSock, char** argv );

int ea_settle( EAREC_BD* recs, PS* thisSock );

int ea_atomic_update_saldo(
      const PS* thisAccount,
      int64_t amount,
      const ART* article );

#endif
