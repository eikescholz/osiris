#include "eacore.h"


/* TODO: Make batch commit, i.e. that an shm batch of exchange data
 *       as input */


int ea_commit_main(int argc, char** argv) {

  MicroTime commitmentDeadline, settlementDeadline,dt;
  PS thisSock;
  EUID euid;
  EAED data;
  EAD eadA,eadB;
  char* end;
  uint64_t specifierID, specificationID, dscCat, dscClass, dscID;
  ShmBd shmbd;
  char name[NAME_MAX], essend[PSD_MAX_PATH_SIZE];

  if (argc < 14 ) {

  fprintf(stderr,
"USAGE: eacommit $OURSOCK \n"
"                $COMMITDEADLINE $SETTLEDEADLINE $OURREF $THEIRREF \n"
"                $OURCREDINV $OURDEBINV $OUDEBAMOUT $OURDEBART \n"
"                $THEIRCREDINV $THEIRDEBINV $THEIREBAMOUT $THEIRDEBART \n"
"                $DESCRIPTIONID\n" );       
    exit(EXIT_FAILURE);
  }

  ps_core_init("escommit");

  if (! ps_getenv_PSDINTERVAL(&dt) ) return EXIT_FAILURE;


  if ( ps_pton(argv[1],&thisSock) < 0) {
    fprintf(stderr,"ERROR (%s): Invalid socket %s\n",ps_mainIdent, argv[1]);
    return EXIT_FAILURE;
  }

  commitmentDeadline = strtol(argv[2],&end,16);
  if ( *end != 0 ) {
    fprintf(stderr,"ERROR (%s): Invalid commitment deadline %s\n",
                    ps_mainIdent,argv[2] );
    return EXIT_FAILURE;
  }

  settlementDeadline = strtol(argv[3],&end,16);
  if ( *end != 0 ) {
    fprintf(stderr,"ERROR (%s): Invalid settlement deadline %s\n",
                    ps_mainIdent, argv[3] );
    return EXIT_FAILURE;
  }

  if ( settlementDeadline < commitmentDeadline + 3*dt ) {
    fprintf(stderr,"ERROR (%s): Invalid settlement deadline %s to close\n",
                    ps_mainIdent, argv[3] );
    return EXIT_FAILURE;
  }

  set_int64n( &euid.commitmentDeadline,commitmentDeadline );
  set_int64n( &euid.settlementDeadline,settlementDeadline );

  set_uint64n( &euid.ourReference, strtol(argv[4],&end,16) );
  if ( *end != 0 ) {
    fprintf(stderr,"ERROR (%s): Our reference %s is invalid\n",
                    ps_mainIdent, argv[4]);
    return EXIT_FAILURE;
  }

  set_uint64n( &euid.theirReference, strtol(argv[5],&end, 16) );
  if ( *end != 0 ) {
    fprintf(stderr,"ERROR (%s): Their reference %s is invalid\n",
                    ps_mainIdent, argv[5] );
    return EXIT_FAILURE;
  }


  if ( ps_pton( argv[6], &data.ourCreditInventory ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Our credit inventory %s is invalid\n",
                    ps_mainIdent, argv[6] );
    return EXIT_FAILURE;
  }

  
  if ( ps_pton(argv[7], &data.ourDebitInventory) < 0 ) {
    fprintf(stderr,"ERROR (%s): Our credit inventory %s is invalid\n",
                    ps_mainIdent, argv[7] );
    return EXIT_FAILURE;
  }

  
  set_int64n( &data.ourDebitAmount, strtol(argv[8],&end, 0) );
  if ( *end != 0 ) {
    fprintf(stderr,"ERROR (%s): Our amount %s is invalid\n",
                    ps_mainIdent, argv[8] );
    return EXIT_FAILURE;
  }

  if ( scanf(argv[9],"%lX/%lX",&specifierID,&specificationID) != 2 ) {
    fprintf(stderr,"ERROR (%s): Our Article ID %s is invalid\n",
                    ps_mainIdent, argv[9] );
    return EXIT_FAILURE;
  }

  set_uint64n(&data.ourDebitArticle.specifierID,specifierID);
  set_uint64n(&data.ourDebitArticle.specificationID,specificationID);



  if ( ps_pton( argv[10], &data.theirCreditInventory ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Their credit inventory %s is invalid\n",
                    ps_mainIdent, argv[10] );
    return EXIT_FAILURE;
  }

  
  if ( ps_pton( argv[11], &data.theirDebitInventory ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Their credit inventory %s is invalid\n",
                    ps_mainIdent, argv[11] );
    return EXIT_FAILURE;
  }

  
  set_int64n( &data.theirDebitAmount, strtol(argv[12],&end, 0) );
  if ( *end != 0 ) {
    fprintf(stderr,"ERROR (%s): Their amount %s is invalid\n",
                    ps_mainIdent, argv[12] );
    return EXIT_FAILURE;
  }

  if ( scanf(argv[13],"%lX/%lX",&specifierID,&specificationID) != 2 ) {
    fprintf(stderr,"ERROR (%s): Their Article ID %s is invalid\n",
                    ps_mainIdent, argv[13] );
    return EXIT_FAILURE;
  }

  set_uint64n( &data.theirDebitArticle.specifierID, specifierID );
  set_uint64n( &data.theirDebitArticle.specificationID, specificationID );

  if ( scanf( argv[13],"%lX/%lX/%lX/%lX",
              &specifierID,&dscCat,&dscClass,&dscID ) != 4 ) {
    fprintf(stderr,"ERROR (%s): The description ID %s is invalid\n",
                    ps_mainIdent, argv[13] );
    return EXIT_FAILURE;
  }

  /* parsing command line done, creating packet */

  
  mk_provision(&eadA, &thisSock , &data.ourDebitInventory, &euid, &data );
  mk_provision(&eadB, &thisSock , &data.theirDebitInventory, &euid, &data );


  if ( snprintf(name,NAME_MAX,"/%lX.%X.comm.bd",microtime(NULL),getpid()) < 0){
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create commitment " 
                   "batch name\n", ps_mainIdent );
    return EXIT_FAILURE;
  }

  if ( shmbd_create(&shmbd,name,2,sizeof(eadA)+sizeof(eadB)) < 0 ) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create commitment " 
                   " shared memory batch\n", ps_mainIdent );
    return EXIT_FAILURE; 
  }

  if ( shmbd_append(&shmbd,&eadA,sizeof(eadA)) < 0 ) {
     fprintf(stderr,"INTERNAL ERROR (%s): Can not fill commitment " 
                   " shared memory batch\n", ps_mainIdent );
    return EXIT_FAILURE; 
  }

  if ( shmbd_append(&shmbd,&eadB,sizeof(eadB)) < 0 ) {
     fprintf(stderr,"INTERNAL ERROR (%s): Can not fill commitment " 
                   " shared memory batch\n", ps_mainIdent );
    return EXIT_FAILURE; 
  }

  es_mk_sendPath( essend,
                  &thisSock.addr,
                  get_uint64n(&thisSock.port) );

  argv[0] = essend;

  ps_execve(essend,argv,environ);

  /* should be unreachable */

  return EXIT_FAILURE;

}
