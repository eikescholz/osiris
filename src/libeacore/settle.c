#include "eacore.h"

int ea_settle( EAREC_BD* recs, 
               PS* thisSock ) {

  char name[NAME_MAX+1], evalPath[EA_MAX_PATH_SIZE];
  size_t i,i0,i1;
  EAREC* ack;
  EAD_BD *ret;
 
  if ( snprintf(name,NAME_MAX,"/%lX.%X.eval.bd",microtime(NULL),getpid())<0) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create commitment " 
                   "batch name\n", ps_mainIdent );
    return EXIT_FAILURE;
  }

  ret = new_batchdata( EAD, recs->entry_count,2*recs->entry_count*sizeof(EAD));
  if ( ret == NULL ) {
    fprintf( stderr, "SEVERE WARNING (%s): Out of memory\n", ps_mainIdent );
    return -1;
  }


  /* sort by euid and then signer */
  if ( sort_batchdata( EAREC, recs, ea_recordBeEUIDCmp ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not sort record batch (%s)\n"
                  ,ps_mainIdent,strerror(errno));
    return EXIT_FAILURE;
  }
 
  ea_mk_clientEvalPath(evalPath,thisSock);

  i=0;
  while ( i < recs->entry_count ) {
    
    /* TODO: Count and evaluate number of different primal and dual clients */
    
    ack = recs->entry[i].data;
    i0 = i;

    /* In therms of EUID there must be two equal packets */
    be_skip_equal_span(EAREC,&i,recs,ea_recordBeEUIDCmp);     
    if ( i == recs->entry_count ) break;
    i1 = i; /* the dual packet */

    if ( i1 - i0 < 2 ) {
      fprintf(stderr,"WARNING (%s): Missing acquisition acknowledgements. "
                     " for transaction  %lX/%lX/%lX/%lX\n", 
                     ps_mainIdent,
                     get_int64n( &ack->datagram->euid.commitmentDeadline ),
                     get_int64n( &ack->datagram->euid.settlementDeadline ),
                     get_uint64n( &ack->datagram->euid.ourReference ),
                     get_uint64n( &ack->datagram->euid.theirReference ) );
      continue; /* process next */
    }
     
    assert( (ack->datagram->euid.type & EA_ACK) != 0 );

    /* TODO: normalize packets */
    if ( new_batchentry( EAD, ret, ack->datagram, sizeof(EAD) ) == NULL ) {
      goto INTERNAL_ERROR;
    }
 
    continue;

  INTERNAL_ERROR:
    fprintf( stderr,
             "INTERNAL ERROR (%s): Can not fill acquisition batch. "
             " Transaction %lX/%lX/%lX/%lX lost. \n",
             ps_mainIdent,
             get_int64n( &ack->datagram->euid.commitmentDeadline ),
             get_int64n( &ack->datagram->euid.settlementDeadline ),
             get_uint64n( &ack->datagram->euid.ourReference ),
             get_uint64n( &ack->datagram->euid.theirReference ) );
 
  }

  ps_fork_exec(evalPath,name,(BD*)ret,NULL,NULL);

  return 0;
}



