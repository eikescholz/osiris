#include "eacore.h"



int ea_receive_main(int argc, char** argv ) {

  EAD_BD *eads;
  PSPRF_BD *prfs;
  EAREC_BD *recs;
  EAREC rec;
  PS thisSock;
  IP6Addr dst_addr;
  uint64_t dst_port;
  size_t i,n;
  EAREC_BD** stage;
  pid_t cld;
  uint8_t type;

  if ( ps_receiver_init("eavalidate",argc,argv) < 0 ) exit(EXIT_FAILURE); 

  if (! ps_getenv_PSDSTIP(&dst_addr) ) exit(EXIT_FAILURE);
  
  if (! ps_getenv_PSDSTPORT(&dst_port) ) exit(EXIT_FAILURE);
 
  ip6cpy(&thisSock.addr,&dst_addr);
  
  set_uint64n(&thisSock.port,dst_port);

  if ( ps_read_receiverInput((PS_D_BD**)&eads,&prfs) < 0) exit(EXIT_FAILURE);

  recs = new_batchdata( EAREC,
                        eads->entry_count,
                        eads->entry_count*sizeof(EAREC) );
  if ( recs == NULL ) {
    fprintf(stderr,"ERROR (%s): Out of memory\n",ps_mainIdent);
    return EXIT_FAILURE;
  } 

  for ( i=0; i < eads->entry_count; ++i ) {
    rec.datagram = eads->entry[i].data;
    rec.proof = prfs->entry[i].data;
    if ( new_batchentry(EAREC,recs,&rec,sizeof(rec)) == 0) {
      fprintf(stderr,"INTERNAL ERROR (%s): Could not fill record batch\n"
                      ,ps_mainIdent);
      return EXIT_FAILURE;
    }
  }

  stage = alloc_split_batchdata( EAREC, recs, ea_recordBeStageCmp, &n );
  if (stage == NULL) {
    fprintf(stderr,"ERROR (%s): Out of memory\n",ps_mainIdent );
    return EXIT_FAILURE;    
  };


  for(i=0; i < n; i++) {
    cld = ps_fork(ps_mainIdent);
    if (cld == -1) continue;
    if (cld == 0) {
      type =  stage[i]->entry->data->datagram->euid.type;
      if ( (type & EA_AQU) == 0){
        if ( (type & EA_ACK) == 0 ) {
          exit( ea_validate( stage[i], &thisSock, argv ) );
        } else {
          exit( ea_verify( stage[i], &thisSock ) );
        }
      } else {
        if ((type & EA_ACK) == 0) {
          exit( ea_clear( stage[i], &thisSock, argv ) );
        } else {
          exit( ea_settle( stage[i], &thisSock ) );
        }
      }
    }
  }
   
  return EXIT_SUCCESS;

}



