#include "eacore.h"

int ea_validate(EAREC_BD* recs, PS* thisInventory,
                char** argv ) {
 
  EAREC *primPro,*dualPro;
  PS src;
  size_t i,i0,i1,resultCount;
  char name[NAME_MAX+1], essend[PSD_MAX_PATH_SIZE];
  ShmBd shmbd;
  EAD ack0,ack1,ack2; 
  int64_t amount;

  if ( snprintf(name,NAME_MAX,"/%lX.%X.valid.bd",microtime(NULL),getpid())<0) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create commitment " 
                   "batch name\n", ps_mainIdent );
    return EXIT_FAILURE;
  }

  resultCount = ((recs->entry_count/2)+1)*3; 
  if ( shmbd_create(&shmbd,name,resultCount,resultCount*sizeof(EAD) ) < 0 ) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create commitment " 
                   " shared memory batch\n", ps_mainIdent );
    return EXIT_FAILURE; 
  }

  if ( sort_batchdata( EAREC, recs, ea_recordBeEUIDCmp ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not sort record batch (%s)\n"
                  ,ps_mainIdent,strerror(errno));
    return EXIT_FAILURE;
  }
  
  i=0;
  while ( i < recs->entry_count) {
    /* TODO: Count and evaluate number of different primal and dual clients
     *       committing the same transaction */
    primPro = recs->entry[i].data;
    i0 = i;

    /* TODO: Fishy algorithm ... */
    /* skip supposedly equal */
    be_skip_equal_span(EAREC,&i,recs,ea_recordBeEUIDCmp);     
    if ( i == recs->entry_count ) break; 
    dualPro = recs->entry[i].data;
    be_skip_equal_span(EAREC,&i,recs,ea_recordBeEUIDCmp);
    if ( i == recs->entry_count ) break;
    i1 = i;
    /* validate data consistency */
    be_skip_equal_span(EAREC,&i0,recs,ea_recordBeExchangeCmp);
    if ( i0 != i1 ) {
      /* TODO: Send abort packets */      
      fprintf( 
        stderr, 
        "INFO (%s): Transaction %lX/%lX/%lX/%lX not invalid. \n"
        "           Got inconsistent commitments\n",
        ps_mainIdent,
        get_int64n( &primPro->datagram->euid.commitmentDeadline ),
        get_int64n( &primPro->datagram->euid.settlementDeadline ),
        get_uint64n( &primPro->datagram->euid.ourReference ),
        get_uint64n( &primPro->datagram->euid.theirReference ) );
      continue; /* next */
    } /* data equality checked, signatures where checked before */

    if ( ps_cmp( thisInventory,
                 &primPro->datagram->exchange.data.ourDebitInventory) 
           == 0) {

      amount
        = get_int64n(&primPro->datagram->exchange.data.ourDebitAmount);
 
      /* TODO: Make one file access for all debits for the same article type of
       *       the same inventory */      
      
      if ( ea_atomic_update_saldo( 
               thisInventory,
               -amount,
               &primPro->datagram->exchange.data.ourDebitArticle ) 
             < 0 ) {
        fprintf( 
          stderr, 
          "ERROR (%s): Could not update debit inventory saldo for for  "
          "            transaction %lX/%lX/%lX/%lX (%s) \n",
          ps_mainIdent,
          get_int64n( &primPro->datagram->euid.commitmentDeadline ),
          get_int64n( &primPro->datagram->euid.settlementDeadline ),
          get_uint64n( &primPro->datagram->euid.ourReference ),
          get_uint64n( &primPro->datagram->euid.theirReference ),
          strerror(errno) );
        continue; /* next */
      }

      /* create clearing request packt for other accountant */
      mk_provisionACK( 
        &ack0,
        thisInventory,
        &primPro->datagram->exchange.data.theirDebitInventory,
        primPro,
        dualPro );

      /* create provision acks for clients */
      ea_load_src( &src, primPro->datagram );

      mk_provisionACK( &ack1, thisInventory, &src, primPro, dualPro );

      ea_load_src( &src, dualPro->datagram );

      mk_provisionACK( &ack2, thisInventory, &src, primPro, dualPro );

    } else {

      assert( ps_cmp(thisInventory,
                     &dualPro->datagram->exchange.data.ourDebitInventory) 
                == 0 );
 
      amount
        = get_int64n(&dualPro->datagram->exchange.data.ourDebitAmount);
 
      /* TODO: Make one file access for all debits for the same article type of
       *       the same inventory */      
      
      if ( ea_atomic_update_saldo( 
               thisInventory,
               amount,
               &dualPro->datagram->exchange.data.ourDebitArticle ) 
             < 0 ) {
        fprintf( 
          stderr, 
          "ERROR (%s): Could not update debit inventory saldo for for  "
          "            transaction %lX/%lX/%lX/%lX (%s) \n",
          ps_mainIdent,
          get_int64n( &dualPro->datagram->euid.commitmentDeadline ),
          get_int64n( &dualPro->datagram->euid.settlementDeadline ),
          get_uint64n( &dualPro->datagram->euid.ourReference ),
          get_uint64n( &dualPro->datagram->euid.theirReference ),
          strerror(errno) );
        continue; /* next */
      }

      /* create clearing request packt for other accountant */ 
      mk_provisionACK( 
        &ack0,
        thisInventory,
        &dualPro->datagram->exchange.data.theirDebitInventory,
        primPro,
        dualPro );
 
      /* create provision acks for clients */ 
      ea_load_src( &src, dualPro->datagram );

      mk_provisionACK( &ack1, thisInventory, &src, primPro, dualPro );
 
      ea_load_src( &src, primPro->datagram );
     
      mk_provisionACK( &ack2, thisInventory, &src, primPro, dualPro );

    }

    ack0.euid.type = EA_VER | ack0.euid.type ; 

    if ( shmbd_append(&shmbd,&ack0,sizeof(ack0)) < 0 ) goto INTERNAL_ERROR;
    if ( shmbd_append(&shmbd,&ack1,sizeof(ack1)) < 0 ) goto INTERNAL_ERROR;
    if ( shmbd_append(&shmbd,&ack2,sizeof(ack1)) < 0 ) goto INTERNAL_ERROR;

    continue;

    INTERNAL_ERROR: 
      fprintf(
        stderr,
        "INTERNAL ERROR (%s): Can not fill validation shared memory batch. "
        " Transaction %lX/%lX/%lX/%lX lost. \n",
        ps_mainIdent,
        get_int64n( &primPro->datagram->euid.commitmentDeadline ),
        get_int64n( &primPro->datagram->euid.settlementDeadline ),
        get_uint64n( &primPro->datagram->euid.ourReference ),
        get_uint64n( &primPro->datagram->euid.theirReference ) );
 
  }

  es_mk_sendPath( essend,
                  &thisInventory->addr,
                  get_uint64n(&thisInventory->port) );

  argv[0] = essend;

  ps_execve(essend,argv,environ);

  /* should be unreachable */

  return EXIT_FAILURE;

}




