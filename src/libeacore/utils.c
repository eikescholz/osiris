#include "eacore.h"



void ea_fprint_euid( 
     FILE* strm,
     EUID* euid) {

  /* TODO: Make time pretty print */      
  fprintf( strm, "  commitment Deadline: %lX\n", 
                 get_int64n( &euid->commitmentDeadline) );

  fprintf( strm, "  settlement Deadline: %lX\n", 
                 get_int64n( &euid->settlementDeadline) );

  fprintf( strm, "  our/their Reference: %lX/%lX", 
                 get_uint64n( &euid->ourReference ),
                 get_uint64n( &euid->theirReference ) );

  fprintf( strm, "  datagram type: ");

  if ( (euid->type & EA_DUAL) == 0 ) {
    fprintf( strm, "primal "); 
  } else {
    fprintf( strm, "dual ");  
  }

  if ( (euid->type & EA_AQU) == 0 ) {
    fprintf( strm, "provision" ); 
  } else {
    fprintf( strm, "acquisition");
  }

  fprintf( strm,"\n");

} 




void ea_fprint_inventoryID( 
  FILE* strm,
  INVID* inv ) {
  
  char addr[INET6_ADDRSTRLEN];

  ip6ntos(&inv->addr,addr);
  
  fprintf( strm, "%sp%lX", addr, get_uint64n(&inv->port) ); 

}




void ea_fprint_exchangeDescription(
  FILE* strm,
  DID* arg ) {

  /* TODO: Load text descriptions for article types */

  fprintf( strm, 
           "%lX/%lX/%lX/%lX",
           get_uint64n( &arg->dscSpecifierID ),
           get_uint64n( &arg->dscCategory ),
           get_uint64n( &arg->dscClass ),
           get_uint64n( &arg->dscID ) );

}





void ea_fprint_articleType(
  FILE* strm,
  ART* arg ) {

  /* TODO: Load text descriptions for article types */

  fprintf( strm, 
           "%lX/%lX",
           get_uint64n( &arg->specifierID ),
           get_uint64n( &arg->specificationID ) );


}






void ea_fprint_exchange( 
  FILE* strm,
  EAED* ex ) {
  
  fprintf( strm, "  our credit inventory: " ); 
  ea_fprint_inventoryID(strm, &ex->ourCreditInventory );

  fprintf( strm, "\n  our debit inventory: " ); 
  ea_fprint_inventoryID(strm,  &ex->ourDebitInventory );

  fprintf( strm, 
           "\n  our debit amount: %lX\n",
           get_int64n( &ex->ourDebitAmount ) ); 
 
  fprintf( strm, "\n  our debit article: " );
  ea_fprint_articleType( strm, &ex->ourDebitArticle ); 
 
  fprintf( strm, "\n  their credit inventory: " ); 
  ea_fprint_inventoryID( strm, &ex->theirCreditInventory );

  fprintf( strm, "\n  their debit inventory: " ); 
  ea_fprint_inventoryID( strm, &ex->theirDebitInventory );

  fprintf( strm, 
           "\n  their debit amount: %lX\n",
           get_int64n( &ex->theirDebitAmount ) ); 
 
  fprintf( strm, "\n  their debit article: " );
  ea_fprint_articleType( strm, &ex->theirDebitArticle ); 
  
  fprintf( strm, "\n  description: " );
  ea_fprint_exchangeDescription(strm, &ex->description);
  fprintf( strm, "\n" );

}





void ea_fprint_ead( 
  FILE* strm,
  EAD* ead ) {

  fprint_psd_header(strm, (PS_D*)ead ); 
        
  ea_fprint_euid( strm, &ead->euid );


  if ( ( ead->euid.type & EA_ACK ) == 0 ) {
    ea_fprint_exchange( strm, &ead->exchange.data );
  } else {
    fprint_proof( strm, &ead->exchange.proof );   
  }

}

