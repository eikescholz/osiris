#include "eacore.h"

int ea_clear( 
      EAREC_BD* recs, 
      PS* thisInventory,
      char** argv ) {

  EAREC *primAqu,*dualAqu, *clrdat;
  PS src;
  size_t i,resultCount;
  char name[NAME_MAX+1], essend[PSD_MAX_PATH_SIZE];
  ShmBd shmbd;
  EAD ack0,ack1,thisPro; 
  int64_t amount;

  if ( snprintf(name,NAME_MAX,"/%lX.%X.cleared.bd",microtime(NULL),getpid())<0) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create cleared " 
                   "batch name\n", ps_mainIdent );
    return EXIT_FAILURE;
  }

  resultCount = recs->entry_count/3; 
  if ( shmbd_create(&shmbd,name,resultCount,resultCount*sizeof(EAD) ) < 0 ) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create cleared " 
                   " shared memory batch\n", ps_mainIdent );
    return EXIT_FAILURE; 
  }

  if ( sort_batchdata( EAREC, recs, ea_recordBeEUIDCmp ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not sort record batch (%s)\n"
                  ,ps_mainIdent,strerror(errno));
    return EXIT_FAILURE;
  }
  
  i=0;
  while ( i < recs->entry_count) {

    /* TODO: Count and evaluate number of different primal and dual clients
     *       committing the same transaction */

    primAqu = recs->entry[i].data;
    
    /* skip supposedly equal, (should only be one) */
    be_skip_equal_span(EAREC,&i,recs,ea_recordBeEUIDCmp);        
    if ( i == recs->entry_count ) break; 
 
    dualAqu = recs->entry[i].data;

    
    if (! is_dual_exchangeEUID( &primAqu->datagram->euid, 
                                &dualAqu->datagram->euid ) != 0 ) {
      goto INCOMPLETE_CLEARING_DATA;
    }


    /* Due to the sorting order the verification testimony p */      
    clrdat = recs->entry[i].data;

    /* skip supposedly equal, (should only be one) */
    be_skip_equal_span(EAREC,&i,recs,ea_recordBeEUIDCmp);     
    if ( i == recs->entry_count ) break; 
 
    if ( ea_normalizedEUIDCmp( &clrdat->datagram->euid, 
                               &primAqu->datagram->euid ) != 0 ) {
      goto INCOMPLETE_CLEARING_DATA;
    }

    if ( memcmp( clrdat->datagram->exchange.clear.proof.hash,
                 primAqu->datagram->exchange.hash,
                 HASH_SIZE ) != 0 ) {
       goto LOG_FRAUD_SUSPICION;
    }

    if ( memcmp( clrdat->datagram->exchange.clear.proof.hash,
                 dualAqu->datagram->exchange.hash,
                 HASH_SIZE ) != 0 ) {
       goto LOG_FRAUD_SUSPICION;
    }


    if ( es_lookup( 
           (ESD*)&thisPro,
           NULL,
           &clrdat->datagram->exchange.clear.theirDebitInventory,
           get_int64n( &clrdat->datagram->euid.commitmentDeadline ),
           &clrdat->datagram->euid,sizeof(EUID)) < 0) {
      
      fprintf( 
          stderr, 
          "ERROR (%s): Could not lookup provision data for  "
          "            transaction %lX/%lX/%lX/%lX (%s) \n",
          ps_mainIdent,
          get_int64n( &clrdat->datagram->euid.commitmentDeadline ),
          get_int64n(  &clrdat->datagram->euid.settlementDeadline ),
          get_uint64n( &clrdat->datagram->euid.ourReference ),
          get_uint64n( &clrdat->datagram->euid.theirReference ),
          strerror(errno) );
      continue; /* next */
    } 


    amount
      = get_int64n(&thisPro.exchange.data.theirDebitAmount);
 
      /* TODO: Make one file access for all debits for the same article type of
       *       the same inventory */      
      
    if ( ea_atomic_update_saldo( 
               &thisPro.exchange.data.ourCreditInventory,
               amount,
               &thisPro.exchange.data.theirDebitArticle ) 
             < 0 ) {
        fprintf( 
          stderr, 
          "ERROR (%s): Could not update credit inventory saldo for  "
          "            transaction %lX/%lX/%lX/%lX (%s) \n",
          ps_mainIdent,
          get_int64n( &thisPro.euid.commitmentDeadline ),
          get_int64n( &thisPro.euid.settlementDeadline ),
          get_uint64n( &thisPro.euid.ourReference ),
          get_uint64n( &thisPro.euid.theirReference ),
          strerror(errno) );
        continue; /* next */
    }

    /* create provision acks for clients */
    ea_load_src( &src, primAqu->datagram );

    mk_aquisitionACK( &ack0, thisInventory, &src, primAqu, dualAqu );

    ea_load_src( &src, dualAqu->datagram );

    mk_aquisitionACK( &ack1, thisInventory, &src, primAqu, dualAqu );

    if ( shmbd_append(&shmbd,&ack0,sizeof(ack0)) < 0 ) goto INTERNAL_ERROR;
    if ( shmbd_append(&shmbd,&ack1,sizeof(ack1)) < 0 ) goto INTERNAL_ERROR;

    continue;

    INTERNAL_ERROR: 
      fprintf(
        stderr,
        "INTERNAL ERROR (%s): Can not fill validation shared memory batch. "
        " Transaction %lX/%lX/%lX/%lX lost. \n",
        ps_mainIdent,
        get_int64n( &thisPro.euid.commitmentDeadline ),
        get_int64n( &thisPro.euid.settlementDeadline ),
        get_uint64n( &thisPro.euid.ourReference ),
        get_uint64n( &thisPro.euid.theirReference ) );
      continue;
    
    LOG_FRAUD_SUSPICION:
      fprintf(
        stderr,
        "FRAUD SUSPICION (%s): Transaction %lX/%lX/%lX/%lX is probable "
        " fraud attempt . \n",
        ps_mainIdent,
        get_int64n( &clrdat->datagram->euid.commitmentDeadline ),
        get_int64n( &clrdat->datagram->euid.settlementDeadline ),
        get_uint64n( &clrdat->datagram->euid.ourReference ),
        get_uint64n( &clrdat->datagram->euid.theirReference ) );
      continue;

   INCOMPLETE_CLEARING_DATA:
     fprintf(
        stderr,
        "ERROR (%s): Transaction %lX/%lX/%lX/%lX clearing data incomplete.\n",
        ps_mainIdent,
        get_int64n( &clrdat->datagram->euid.commitmentDeadline ),
        get_int64n( &clrdat->datagram->euid.settlementDeadline ),
        get_uint64n( &clrdat->datagram->euid.ourReference ),
        get_uint64n( &clrdat->datagram->euid.theirReference ) );
      continue;

  }

  /* TODO: add smart contract/analysis hook execution */

  es_mk_sendPath( essend,
                  &thisInventory->addr,
                  get_uint64n(&thisInventory->port) );

  argv[0] = essend;

  ps_execve(essend,argv,environ);

  /* should be unreachable */

  return EXIT_FAILURE;


}


