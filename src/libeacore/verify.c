#include "eacore.h"


int ea_verification_by_accountant(
      EAD_BD** _ret,
      EAREC_BD* recs,
      ShmBd* block,
      ShmBd* prfs,
      PS* thisSock ) {

  EAD_BD *ret;
  EUID dualEUID;
  EAREC *rec, ourPro, theirPro;
  EAD rsp;  
  size_t j,k,l; 
  PS dst;

  ret = new_batchdata( EAD, recs->entry_count, recs->entry_count*sizeof(EAD) );
  if ( ret == NULL ) {
    fprintf( stderr, "SEVERE WARNING (%s): Out of memory\n", ps_mainIdent );
    return -1;
  }

  *_ret = ret;

  for ( j = 0 ; j < recs->entry_count ; ++j ) {

    rec = recs->entry[j].data;
 
    k = es_block_lookup(
          block,
          get_int64n( &rec->datagram->header.deadline ),
          &rec->datagram->euid,
          sizeof(rec->datagram->euid) );

    if ( k == SIZE_MAX ) {
      fprintf( stderr,
               "SEVERE WARNING (%s): Either loss of validation data, "
               " or clearing request for unvalidated transaction: ", 
               ps_mainIdent );
      ea_fprint_ead(stderr,rec->datagram);
      return -1;
    }

    /* signatures have been verified by the psd layer, so just need
     * to compare the hashes to see if they are valid */
    if ( memcmp( ((PSPRF*)shmbd_entryData(k,prfs))->hash, 
                 &rec->proof->hash,
                 HASH_SIZE ) != 0 ) {
      fprintf( stderr,
               "SEVERE WARNING (%s): Invalidly signed clearing request for "
               " transaction: ", 
               ps_mainIdent );
      ea_fprint_ead(stderr,rec->datagram);/*other data is in the blockchain*/
      return -1;
    }
      
    ourPro.datagram = (EAD*)shmbd_entryData(k,block); 
    ourPro.proof = (PSPRF*)shmbd_entryData(k,prfs);

    mk_dual_exchangeEUID(&dualEUID,&rec->datagram->euid);

    l = es_block_lookup(
          block,
          get_int64n( &rec->datagram->header.deadline ), 
          &dualEUID,
          sizeof(rec->datagram->euid) );
 
    if ( l == SIZE_MAX ) {
      fprintf( stderr,
               "SEVERE WARNING (%s): Either loss of validation data, "
               " or clearing request for unvalidated transaction: ", 
               ps_mainIdent );
      ea_fprint_ead(stderr,rec->datagram);
      return -1;
    }

    theirPro.datagram = (EAD*)shmbd_entryData(l,block); 
    theirPro.proof = (PSPRF*)shmbd_entryData(l,prfs); 

    ea_load_src( &dst, rec->datagram );

    if ( (theirPro.datagram->euid.type & EA_DUAL) == 0 ) {
       
      mk_provisionACK( &rsp,
                       thisSock,
                       &dst,
                       &theirPro,
                       &ourPro );
    } else {

      mk_provisionACK( &rsp,
                       thisSock,
                       &dst,
                       &ourPro,
                       &theirPro );
    }
       
    rsp.euid.type = EA_VER | rsp.euid.type ; 
  
    if ( new_batchentry( EAD, ret, &rsp, sizeof(rsp) ) == NULL ) {
      goto INTERNAL_ERROR;
    }
 
    continue;

    INTERNAL_ERROR:
      fprintf(
          stderr,
          "INTERNAL ERROR (%s): Can not fill clearing batch. "
          " Transaction %lX/%lX/%lX/%lX lost. \n",
          ps_mainIdent,
          get_int64n( &rec->datagram->euid.commitmentDeadline ),
          get_int64n( &rec->datagram->euid.settlementDeadline ),
          get_uint64n( &rec->datagram->euid.ourReference ),
          get_uint64n( &rec->datagram->euid.theirReference ) );
 
  }

  return 0;

}




int ea_verification_by_client(
      EAD_BD** _ret,
      EAREC_BD* recs,
      PS* thisSock ) {

  EAD_BD *ret;
  EAREC *ack0, *ack1;
  EAD aqu0, aqu1;
  size_t i,i0,i1; 
  PS dst0, dst1;
 
  ret = new_batchdata( EAD, recs->entry_count,2*recs->entry_count*sizeof(EAD));
  if ( ret == NULL ) {
    fprintf( stderr, "SEVERE WARNING (%s): Out of memory\n", ps_mainIdent );
    return -1;
  }

  *_ret = ret;

  /* sort by euid and then signer */
  if ( sort_batchdata( EAREC, recs, ea_recordBeEUIDCmp ) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not sort record batch (%s)\n"
                  ,ps_mainIdent,strerror(errno));
    return EXIT_FAILURE;
  }
 
  i=0;
  while ( i < recs->entry_count) {
    
    /* TODO: Count and evaluate number of different primal and dual clients */
    
    ack0 = recs->entry[i].data;
    i0 = i;

    /* In therms of EUID there must be two equal packets */
    be_skip_equal_span(EAREC,&i,recs,ea_recordBeEUIDCmp);     
    if ( i == recs->entry_count ) break;
    i1 = i; /* the dual packet */

    if ( i1 - i0 < 2 ) {
      fprintf(stderr,"ERROR (%s): Missing provision acknowledgements. Pausing"
                     " processing of transaction  %lX/%lX/%lX/%lX\n", 
                     ps_mainIdent,
                     get_int64n( &ack0->datagram->euid.commitmentDeadline ),
                     get_int64n( &ack0->datagram->euid.settlementDeadline ),
                     get_uint64n( &ack0->datagram->euid.ourReference ),
                     get_uint64n( &ack0->datagram->euid.theirReference ) );
      continue; /* process next */
    }
    
    ack1 = recs->entry[i0+1].data;
 
    assert( (ack0->datagram->euid.type & EA_ACK) != 0 ); 
    assert( (ack1->datagram->euid.type & EA_ACK) != 0 ); 

    ea_load_src(&dst0,ack0->datagram);
    ea_load_src(&dst1,ack1->datagram);

    mk_aquisition( &aqu0, 
                   thisSock,
                   &dst0, 
                   &ack0->datagram->euid,
                   ack0->datagram->exchange.proof.hash );

    mk_aquisition( &aqu1, 
                   thisSock,
                   &dst1, 
                   &ack1->datagram->euid,
                   ack1->datagram->exchange.proof.hash );
         
    if ( new_batchentry( EAD, ret, &aqu0, sizeof(aqu0) ) == NULL ) {
      goto INTERNAL_ERROR;
    }
 
    if ( new_batchentry( EAD, ret, &aqu1, sizeof(aqu1) ) == NULL ) {
      goto INTERNAL_ERROR;
    }
 
    continue;

  INTERNAL_ERROR:
    fprintf( stderr,
             "INTERNAL ERROR (%s): Can not fill acquisition batch. "
             " Transaction %lX/%lX/%lX/%lX lost. \n",
             ps_mainIdent,
             get_int64n( &ack0->datagram->euid.commitmentDeadline ),
             get_int64n( &ack0->datagram->euid.settlementDeadline ),
             get_uint64n( &ack0->datagram->euid.ourReference ),
             get_uint64n( &ack0->datagram->euid.theirReference ) );
 
  }

  return 0;

}




int ea_verify( 
      EAREC_BD* recs, 
      PS* thisSock ) {

  size_t i,j,binCount,subBinCount;
  EAREC_BD **bin, **subBin;
  char name[NAME_MAX+1];
  char essend[EA_MAX_PATH_SIZE];
  ShmBd block,prfs; 
  EAREC *rec; 
  EAD_BD* rsps; /* response packet */


  if ( snprintf(name,NAME_MAX,"/%lX.%X.clear.bd",microtime(NULL),getpid())<0) {
    fprintf(stderr,"INTERNAL ERROR (%s): Can not create clear " 
                   "batch name\n", ps_mainIdent );
    return EXIT_FAILURE;
  }

  es_mk_sendPath( essend,
                  &thisSock->addr,
                  get_uint64n(&thisSock->port) );

  /* is this split required ? */
  bin = alloc_split_batchdata( EAREC,
                               recs,
                               ea_recordBeCommitmentDeadlineIntervalCmp,
                               &binCount );
  if ( bin == 0 ) {
    fprintf( stderr, "ERROR (%s): Out of memory\n", ps_mainIdent );
    return -1;
  } 

  for ( i = 0; i < binCount; ++i ) { 

    rec = bin[i]->entry->data;
 
    /* all commitment deadlines in the bin belong to the same block */

    es_load_block( &block, 
                   &prfs,
                   get_int64n( &rec->datagram->euid.commitmentDeadline ),
                   thisSock );

    subBin = alloc_split_batchdata( EAREC, 
                                    bin[i],
                                    ea_recordBeVerificationFlagsCmp,
                                    &subBinCount );
    if ( subBin == 0 ) {
      fprintf( stderr, "ERROR (%s): Out of memory\n", ps_mainIdent );
      return -1;
    } 

    for ( j = 0 ; j < subBinCount ; ++j ) {

      if ( (subBin[j]->entry->data->datagram->euid.type & EA_VER) == 0 ) {
        ea_verification_by_client(&rsps,recs,thisSock);        
      } else {
        ea_verification_by_accountant(&rsps,recs,&block,&prfs,thisSock);
      }

      ps_fork_exec(essend,name,(BD*)rsps,NULL,NULL);

      delete_batchdata(EAREC,subBin[j]);
    }

    delete_batchdata(EAREC,bin[i]);
                      
  }

  return EXIT_SUCCESS;

}


