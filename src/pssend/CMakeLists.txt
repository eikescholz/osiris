cmake_minimum_required(VERSION 2.8.9)
project(pssend C)
 
file(GLOB SOURCES "${CMAKE_SOURCE_DIR}/src/pssend/*.c")
 
add_executable(pssend ${SOURCES})
