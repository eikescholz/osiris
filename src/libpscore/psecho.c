/* This is the persistent socket echo service. 
 *
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

/* assumes that input datagrams to be send to the same
 * destination */

int psecho_main(int argc,char**argv) {
  char execPathBuf[PSD_MAX_PATH_SIZE];
  char* execPath;
  PS_D* psd;
  char srcAddrStr[INET6_ADDRSTRLEN];
  struct in6_addr* src_addr;
  struct in6_addr dst_addr;
  uint64_t dst_port;
  uint64_t src_port;
  PS_D_BD* psds;
  MicroTime timeout;

  if ( ps_receiver_init("psecho",argc,argv) < 0 ) exit(EXIT_FAILURE); 

  if (! ps_getenv_PSDSTIP(&dst_addr) ) exit(EXIT_FAILURE);
  
  if (! ps_getenv_PSDSTPORT(&dst_port) ) exit(EXIT_FAILURE);
 
  if ( ps_read_receiverInput(&psds,NULL) < 0) exit(EXIT_FAILURE);

  /* TODO: cleanup input semantics. Expects exactly one vote footer ... */
  if (! ps_verify_psdbd_dst_addr(psds,&dst_addr) ) {
    exit(EXIT_FAILURE);
  }
  
  if (! ps_verify_psdbd_dst_port(psds,dst_port) ) {
    exit(EXIT_FAILURE);
  }
  
  PS_D_BD** bins = alloc_split_batchdata(PS_D,psds,psdBeSrcDstCmp,NULL);
  if (bins == NULL) {
    fprintf(stderr,"ERROR (%s): Could not split input datagrams by source (%s)",
             ps_mainIdent,strerror(errno));       
    exit(EXIT_FAILURE); 
  }

  for (PS_D_BD** cur = bins; *cur != NULL; ++cur) {

    ps_load_sockTimeout( &timeout,
                         &(*cur)->entry->data->header.dst_addr,
                         get_uint64n( &(*cur)->entry->data->header.dst_port) );

    for(size_t i=0; i < (*cur)->entry_count; ++i) {

      psd = (*cur)->entry[i].data; 

      ip6swp( &psd->header.src_addr,
              &psd->header.dst_addr );
      uint64_t tmp_port;
      tmp_port = get_uint64n(&psd->header.dst_port);
      set_uint64n(&psd->header.dst_port, get_uint64n(&psd->header.src_port));    
      set_uint64n(&psd->header.src_port,tmp_port );

      /* primary purpose is to measure time, so we use
       * the mpid field to contain the creation time of the echoed packt */
      MicroTime t = microtime(NULL);
      set_int64n(&psd->header.mpid, get_int64n(&psd->header.deadline) ); 
      set_int64n(&psd->header.deadline, t + timeout );
    }

    src_addr = &(*cur)->entry->data->header.src_addr;
    src_port = get_uint64n(&(*cur)->entry->data->header.src_port);

    if ( ps_setenv_PSSRCPORT(src_port) < 0 ) continue;
    if ( ps_setenv_PSSRCIP(src_addr) < 0) continue ;

    execPath = execPathBuf; 
    
    psd_mkSendPath(execPath);
    ip6ntop(src_addr,srcAddrStr);

    /* batches are named to indicate the target service */
    char sendBatchName[NAME_MAX+1];
    ps_mk_portShmbdName( sendBatchName,
                         ps_inputBatchTime,
                         &dst_addr,
                         dst_port,
                         ".send.bd");

    /* TODO: Use proper fork wrappers, i.e. here ps_fork_send, */
    ps_fork_exec(execPath,sendBatchName,(BD*)(*cur),NULL,NULL);

    delete_batchdata(PS_D,*cur);

  }

  free(bins);

  delete_batchdata(PS_D,psds);
  
  return 0;

}

