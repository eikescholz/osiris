#include "pscore.h"


#if 0 /* to be removed */
void ps_inplace_pverify_be(
        struct batchentry_psd_t* be,
        const struct psd_pverify_arg_t* va ) { 

  if (be->size < crypto_sign_BYTES) {
    fprintf(stderr,"WARNING (%s): Packet lost! "
           "Too small to contain socket signature\n",va->logName);
    be->data = NULL;
    return;
  }

  size_t dataLen = be->size - crypto_sign_BYTES - PSD_ROUTE_MAC_SIZE;
  
  unsigned char* data = (unsigned char*)be->data;
    
  unsigned char* sig  = data + dataLen;
  
  if ( crypto_sign_verify_detached( sig,
                                    data,
                                    dataLen,
                                    va->pk ) < 0 ) {
    char nonceStr[PSD_NONCESTR_BYTES];
    sodium_bin2hex( nonceStr,PSD_NONCESTR_BYTES,
                    be->data->header.psd.nonce, PSD_NONCE_BYTES);
    fprintf(stderr,"WARNING (%s): Packet lost! " 
                   "Could not verify source socket of psd packet %s\n",
                   va->logName,nonceStr);
    be->data = NULL; 
  }

}


size_t ps_inplace_verify( 
        BD* bd, 
        const unsigned char* pk,
        const char* mainIdent ) {
  
  struct psd_pverify_arg_t va;
 
  assert(bd != NULL);
  assert(bd->entry_count >= 1);

  va.pk = pk;
  va.logName = mainIdent;

  for(size_t i=0; i<bd->entry_count;++i) {
    ps_inplace_pverify_be((PSDBE*)&bd->entry[i],&va);
  }

  return bd_inplace_strip(bd);       

}

#endif 

/* psd_psign_be_with
 * creates a port signature and *appends* it. 
 */

void ps_inplace_psign_be(
       PSPKT_BE* be, /* plain be */
       const struct psd_psign_arg_t* sa ) { 
  char nonceStr[PS_NONCESTR_BYTES];
  
  if (be->size < crypto_sign_BYTES) {
    fprintf(stderr,"WARNING (%s): Packet lost! "
                   "Too small to contain port signature\n",sa->logName);
    be->data = NULL;
    return;
  }

  size_t dataLen = ps_packetDatagramSize(be->data);
 
  unsigned char* data = (unsigned char*)be->data;
    
  unsigned char* sig  = data + dataLen ; /* append signature */

  unsigned char hashBuf[HASH_SIZE];
 
  
  if ( ps_mk_packetDatagramHash(hashBuf, be->data) < 0) {
    be->data = NULL;
    return;
  }    


  if ( crypto_sign_detached( sig,
                             NULL,
                             hashBuf,
                             HASH_SIZE,
                             sa->sk ) < 0 ) {
    sodium_bin2hex( nonceStr,PS_NONCESTR_BYTES,
                    be->data->header.nonce, PS_NONCE_BYTES);
    fprintf(stderr,"WARNING (%s): Packet lost! " 
                   "Could not sign psd packet %s\n",sa->logName,nonceStr);
    be->data = NULL; 
  }
  
  ip6cpy(&ps_packetSignature(be->data)->addr,&be->data->header.src_addr);
 
}




size_t ps_inplace_normalize_and_sign( 
        PSPKT_BD* bd, 
        const unsigned char* sk,
        const char* mainIdent ) {
  
  struct psd_psign_arg_t sa;
  IP6Addr senderAddr,recvAddr,src_addr,dst_addr;
  uint64_t src_port;

  assert( bd != NULL );
  assert( bd->entry_count >= 1 );

  ip6cpy(&src_addr, &bd->entry->data->header.src_addr);

  src_port = get_uint64n(&bd->entry->data->header.src_port);
  assert( ps_verify_pktbd_src_addr(bd,&src_addr) );
  assert( ps_verify_pktbd_src_port(bd,src_port) );

  /* TODO: Check where packet is put into normalform */
  ps_load_packetSender(&senderAddr,bd->entry->data);
  sa.sk = sk;
  sa.logName = mainIdent;

  for(size_t i=0; i<bd->entry_count;++i) {

    /* TODO: Sort by dst and load receiver once from fs */      
    ps_load_packetReceiver(&recvAddr,bd->entry[i].data);

    ip6cpy(&dst_addr, &bd->entry[i].data->header.dst_addr);
    
    ip6cpy(&bd->entry[i].data->header.dst_addr,&recvAddr);
    ip6cpy(&bd->entry[i].data->header.src_addr,&senderAddr);
 
    ps_inplace_psign_be(&bd->entry[i],&sa);

    ip6cpy(&bd->entry[i].data->header.src_addr,&src_addr);
    ip6cpy(&bd->entry[i].data->header.dst_addr,&dst_addr);
 
  
  }

  return bd_inplace_strip(PSPKT,bd);

}


