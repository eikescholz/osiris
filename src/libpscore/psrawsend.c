#include <sys/types.h>
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "psd.h"
#include "batchdata.h"
#include "sysutils.h"
#include "pscore.h"

/* This tacks a batch of batch_psd packets and sends them
 * though a raw interface. 
 *
 * NOTE: This is setuid root and internal attac sufrace.
 * 
 * TODO: Implement ACK timeouts, and just resend after
 *       each ACK timout interval. Needs some ipc
 *       with the raw_receiver. The raw receiver might
 *       set the timestamp on the shared memory batch to
 *       zero for ACKnowledged packets.
 *
 *       So the protocol becomes "reliable" in IP sense.  
 */


int psrawsend_main(int argc, char** argv) {
  int sd;
  PSPKT_BD* bd;
   
  ps_service_init("psrawsend",argc,argv); 

  if ((sd = socket(AF_INET6, SOCK_RAW, PS_PROTO_NUM)) < 0) {
     fprintf(stderr, "ERROR (psrawsend): Batch Dropped. "
                     "Could not create raw socket\n");
     exit(EXIT_FAILURE);
  }
  
  if ( ps_drop_privileges() < 0) exit(EXIT_FAILURE);
  
  bd = ps_read_serviceInput(); /* TODO FOR ACK, thus must not unlink the shmbd */ 
  if (bd == NULL) exit(EXIT_FAILURE);

  if ( ps_send_psd_batchdata(bd,sd,"psrawsend") < 0 ) exit(EXIT_FAILURE); 
 
  uninterruptable_close(sd);  

  delete_batchdata(PSPKT,bd);

  return EXIT_SUCCESS;

}

