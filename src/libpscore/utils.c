#include "pscore.h"


/* returns true on success */
bool getenv_ip6addr(const char* name,struct in6_addr* ret) {

  char* ipstr = getenv(name);
  if (ipstr == NULL) {
    errno = ENOKEY;         
    return false;
  }

  if ( ip6pton(ipstr,ret) == 0) return true;
  return false;

}



/* returns true on success */
bool getenv_port(const char* name,uint64_t* ret) {

  char* portStr = getenv(name);
  if (portStr == NULL) {
    errno = ENOKEY;         
    return false;
  }

  if ( sscanf(portStr,"%lX",ret) == 1 ) return true;
  return false;
}



bool is_local_psnode(const struct in6_addr* addr ){

  char path[PSD_MAX_PATH_SIZE]; 

  psd_mkLocalNodePath(path,addr);

  struct stat st;

  if ( stat(path,&st) == 0 ) return true;
 
  return false;

}


/* collected information about the persistent socets
 * encoded in and target path 
 */

void free_psockinfo(struct psockinfo_t* psi) {
  size_t i;
  
  for (i = 0; i< psi->node_count; ++i) { 
    free(psi->port[i]);
  }

  free(psi->isLocal);
  free(psi->node_addr);
  free(psi->port_count);
  free(psi->port);
  free(psi);

}       



struct psockinfo_t* alloc_psockinfo_of(const char* path) {
  struct in6_addr cur_addr;
  char cur_path[PSD_MAX_PATH_SIZE];
  uint64_t cur_port;    
  
  struct psockinfo_t* ret = malloc(sizeof(*ret));
  if (ret == NULL) return NULL;


  ret->node_count = 0;
  ret->local_node_count = 0;
  ret->isLocal = NULL;
  ret->node_addr = NULL;
  ret->port_count = NULL;
  ret->port = NULL;

  DIR* ndir = opendir(path);
  if (ndir == NULL) goto FAIL0;

  struct dirent* node_ent;
  struct dirent* port_ent;
  void* tmp;

  for( node_ent = readdir(ndir);
       node_ent != NULL; 
       node_ent = readdir(ndir)) {    
 
    if ( ip6pton(node_ent->d_name,&cur_addr) < 0 ) continue;
   
    size_t i = ret->node_count;
 
    tmp = realloc(ret->isLocal,(i+1)*sizeof(*ret->isLocal));
    if (tmp == NULL) goto FAIL1;
    ret->isLocal = tmp;
   
    tmp = realloc(ret->node_addr,(i+1)*sizeof(*ret->node_addr));
    if (tmp == NULL) goto FAIL1;
    ret->node_addr = tmp;

    tmp = realloc(ret->port_count,(i+1)*sizeof(*ret->port_count));
    if (tmp == NULL) goto FAIL1;
    ret->port_count = tmp;
    
    tmp = realloc(ret->port,(i+1)*sizeof(*ret->port));
    if (tmp == NULL) goto FAIL1;
    ret->port = tmp;

    ret->isLocal[i] = is_local_psnode(&cur_addr);

    if ( ret->isLocal[i] ) ret->local_node_count++;
    
    ret->port_count[i] = 0;
    ret->port[i] = NULL;
    ret->node_count++; 
    
    ip6cpy(&ret->node_addr[i],&cur_addr);

    cur_path[0] = 0;
    stpcpy(stpcpy(stpcpy(cur_path,path),"/"),node_ent->d_name);

    DIR* pdir = opendir(cur_path);
    if (pdir == NULL) goto FAIL1;

    for( port_ent = readdir(pdir);
         port_ent != NULL;
         port_ent = readdir(pdir) ) {

      char* endPtr;
      cur_port = strtoull(port_ent->d_name,&endPtr,16);
      if (! ( *endPtr == '\0' && port_ent->d_name[0] != '\0') ) continue;
 
      size_t j = ret->port_count[i];
      tmp = realloc(ret->port[i],(j+1)*sizeof(*ret->port[i]));
      if (tmp == NULL) {
       closedir(pdir);      
       goto FAIL1; }
      ret->port[i] = tmp;
      ret->port[i][j] = cur_port;
      ret->port_count[i]++;   
      
    }

    closedir(pdir);    
 
  }

  closedir(ndir);

  return ret;

FAIL1: 
  
  closedir(ndir);

FAIL0:

  free_psockinfo(ret);
  
  return NULL;

}




void fprint_psockinfo(FILE* stream,struct psockinfo_t* pi) {
  char addrStr[INET6_ADDRSTRLEN];

  for (size_t i = 0; i < pi->node_count; ++i) {
    ip6ntop(&pi->node_addr[i],addrStr);
   
    for(char* cur = addrStr; *cur != 0; ++cur) {
      if (*cur == 'x') *cur = ':'; 
    }
 
    fprintf(stream,"Node %s (%lu/%lu) ports: "
                 ,addrStr,i+1,pi->node_count);
    for (size_t j = 0; j < pi->port_count[i]; ++j) {
      fprintf(stream,"%lX",pi->port[i][j]);
      if (j+1 < pi->port_count[i]) fprintf(stream,",");
    }

   fprintf(stream," .\n");
  }    
   

}



/* some common components with logging */
int ps_setenv_PSSRCIP(
        struct in6_addr* addr ) {
 
  char buf[INET6_ADDRSTRLEN];
 
  ip6ntos(addr,buf);
 
  if ( setenv("PSSRCIP",buf,1) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not set environment PSSRCIP to %s (%s)",
            ps_mainIdent,buf,strerror(errno));
    return -1; 
  };

  return 0;
  
}




int ps_setenv_PSSRCPORT(
       uint64_t port ) {
 
  char buf[33];
 
  if ( snprintf(buf,33,"%lX",port) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not convert port to string\n",
                    ps_mainIdent);
  }

  if ( setenv("PSSRCPORT",buf,1) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not set environment "
                   "PSSRCPORT to %s (%s)",
                   ps_mainIdent,buf,strerror(errno));
        
    return -1; 
  };
 
  return 0;
  
}




/* sets daemon interval information */
int ps_setenv_PSDINTERVAL(
       MicroTime dt ) {
 
  char buf[33];
 
  if ( snprintf(buf,33,"%ld",dt) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not convert port to string\n",
                    ps_mainIdent);
  }

  if ( setenv("PSDINTERVAL",buf,1) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not set environment "
                   "PSDINTERVAL to %s (%s)",
                   ps_mainIdent,buf,strerror(errno));
        
    return -1; 
  };
 
  return 0;
  
}



int ps_setenv_PSDSTIP(
        struct in6_addr* addr ) {
 
  char buf[INET6_ADDRSTRLEN];
 
  ip6ntos(addr,buf);
 
  if ( setenv("PSDSTIP",buf,1) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not set environment PSDSTIP to %s (%s)",
            ps_mainIdent,buf,strerror(errno));
    return -1; 
  };

  return 0;
  
}



int ps_setenv_PSDSTPORT(
       uint64_t port ) {
 
  char buf[32];
 
  if ( snprintf(buf,32,"%lX",port) < 0 ) return -1;

  if ( setenv("PSDSTPORT",buf,1) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not set environment "
                   "PSDSTPORT to %s (%s)",
                   ps_mainIdent,buf,strerror(errno));
        
    return -1; 
  };
 
  return 0;
  
}



bool ps_getenv_PSDSTIP(
       struct in6_addr* dst_addr ) {
 
  if (! getenv_ip6addr("PSDSTIP",dst_addr) ) {
    fprintf(stderr,"ERROR (%s): "
                   "Environment variable PSDSTIP undefined\n",ps_mainIdent); 
    return false;
  }

  return true;
}



bool ps_getenv_PSDSTPORT(
       uint64_t* dst_port ) {
 
  if (! getenv_port("PSDSTPORT",dst_port) ) {
    fprintf(stderr,"ERROR (%s): "
                   "Environment variable PSDSTPORT undefined\n",ps_mainIdent); 
    return false;
  }
  
  return true;

}



bool ps_getenv_PSDINTERVAL(
       MicroTime* dt ) {
 
  char* portStr = getenv("PSDINTERVAL");
  if (portStr == NULL) {
    errno = ENOKEY;         
    return false;
  }

  if ( sscanf(portStr,"%ld",dt) == 1 ) return true;
 
  return true;

}


bool ps_getenv_PSSRCIP(
       struct in6_addr* addr ) {
 
  if (! getenv_ip6addr("PSSRCIP",addr) ) {
    fprintf(stderr,"ERROR (%s): "
                   "Environment variable PSSRCIP undefined\n",ps_mainIdent); 
    return false;
  }

  return true;
}

bool ps_getenv_PSSRCPORT(
       uint64_t* port ) {
 

  if (! getenv_port("PSSRCPORT",port) ) {
    fprintf(stderr,"ERROR (%s): "
                   "Environment variable PSSRCPORT undefined\n",ps_mainIdent); 
    return false;
  }
  
  return true;

}


/* TODO: Cleanup. Remove. Transition API, */

PSPKT_BD* ps_read_serviceInput() {
 

  if ( shmbd_load(&ps_inputShmbd,ps_inputName) < 0) {
    fprintf(stderr,"\nERROR (%s): Could not load input shared memory "
                   "batchdata %s (%s)\n",
                   ps_mainIdent,ps_inputName,strerror(errno));
    if (errno == EACCES) {
      fprintf(stderr,"  euid: %u, egid: %u\n",geteuid(),getegid());    
    }
    return NULL;
  }

  if ( shmbd_unlink(&ps_inputShmbd ) < 0 ) {
    fprintf(stderr,"\nWARNING (%s): Could not unlink shared memory" 
                   " batchdata %s. System Leaks Memory.\n"
                   " (%s)",ps_mainIdent,ps_inputName,strerror(errno));
    if (errno == EACCES) {
      fprintf(stderr,"  euid: %u, egid: %u\n",geteuid(),getegid());    
    } 
  }

  ps_inputBatchTime = strtoll(&ps_inputName[1],NULL,16);


  PSPKT_BD* ret = new_batchdata_from_shmbd(PSPKT,&ps_inputShmbd);
  if ( ret == NULL ) {
    fprintf(stderr,"ERROR (%s): "
                   "Unable to read shared memory batchdata '%s'\n",
                   ps_mainIdent,ps_inputShmbd.name); 
    return NULL;
  }
 
  return ret;
}



int ps_read_receiverInput(PS_D_BD** pkts, PSPRF_BD** prfs) {
 
  if ( pkts == NULL && prfs == NULL) {
    errno=EINVAL;
    return -1;
  }   
  
  if ( pkts != NULL ) {  
    *pkts = (PS_D_BD*)ps_read_serviceInput(); /* same as for service */
  }

  if ( prfs != NULL ) {  
    if ( shmbd_load(&ps_proofInputShmbd,ps_proofInputName) < 0) {
      fprintf(stderr,"\nERROR (%s): Could not load input shared memory "
                     "batchdata %s (%s)\n",
                     ps_mainIdent,ps_proofInputName,strerror(errno));
      if ( errno == EACCES ) {
        fprintf(stderr,"  euid: %u, egid: %u\n",geteuid(),getegid());    
      }
      delete_batchdata(PS_D,*pkts);
      return -1;
    }

    if ( shmbd_unlink(&ps_proofInputShmbd) < 0 ) {
      fprintf(stderr,"\nWARNING (%s): Could not unlink shared memory" 
                     " batchdata %s. System Leaks Memory.\n"
                   " (%s)",ps_mainIdent,ps_proofInputName,strerror(errno));
      if ( errno == EACCES ) {
        fprintf(stderr,"  euid: %u, egid: %u\n",geteuid(),getegid());    
      } 
    }

    *prfs = new_batchdata_from_shmbd(PSPRF,&ps_proofInputShmbd);
    if ( prfs == NULL ) {
      fprintf(stderr,"ERROR (%s): "
                     "Unable to read shared memory batchdata '%s'\n",
                     ps_mainIdent,ps_inputShmbd.name); 
      delete_batchdata(PS_D,*pkts); 
      return -1;
    }

    if ( pkts != NULL) {
       if ( (*pkts)->entry_count != (*prfs)->entry_count ) {
         fprintf(stderr,"ERROR (%s): "
                        "Invalid service inputs. The number of proofs does "
                        "not match the number of input datagrams'%s'\n",
                       ps_mainIdent,ps_inputShmbd.name); 
         delete_batchdata(PS_D,*pkts); 
         return -1;
       }
    } 
  }
  return 0;
}





/* privileges are gained with setuid and setgid bits.
 * the real user is the unprivileged executer */ 
int ps_drop_privileges() {
  
  if ( become_real_user() != 0 ) { /* drop rights to access private key */ 
    fprintf(stderr,"ERROR (%s): Dropping privileges failed\n",ps_mainIdent); 
    return -1;
  };

  return 0;

}


PSPKT_BD* ps_merge_and_free(
            PSPKT_BD** bins ) {

  PSPKT_BD* bd_merged = new_merged_batchdata(PSPKT,bins); 
  if (bd_merged == NULL) {
    fprintf(stderr,"ERROR (%s): Out of memory\n",ps_mainIdent); 
    return NULL; 
  }

  for ( PSPKT_BD** cur = bins; *cur != NULL; ++cur ) {
    delete_batchdata(PSPKT,*cur);
  }

  free(bins);

  return bd_merged;

}


PSPKT_BD** ps_alloc_and_split(
    PSPKT_BD* bd,
    int (*cmp)(const PSPKT_BE*,const PSPKT_BE*),
    size_t* retCount ) {

  PSPKT_BD** bins = alloc_split_batchdata(PSPKT,bd,cmp,retCount);
  if (bins == NULL) {
    fprintf(stderr,"ERROR (%s): Out of memory\n",ps_mainIdent);
    return NULL;
  }

  return bins;

}


int ps_load_auth_pk(
        uint8_t* pk,
        const struct in6_addr* addr,
        uint64_t port ) {

  char pkPath[PSD_MAX_PATH_SIZE]; 

  psd_mkPortVerifyKeyPath(pkPath,addr,port);

  if ( load_auth_pk(pkPath,pk) < 0 ) {
    fprintf(stderr,"ERROR (%s): Loading %s failed (%s)\n",
                    ps_mainIdent,pkPath,strerror(errno));
    return -1;
  };
 
  return 0;

} 


int ps_load_auth_sk(
        uint8_t* sk,
        const struct in6_addr* addr,
        uint64_t  port ) {

  char skPath[PSD_MAX_PATH_SIZE]; 

  psd_mkPortSignKeyPath(skPath,addr,port);

  if ( load_auth_sk(skPath,sk) < 0 ) {
    fprintf(stderr,"ERROR (%s): Loading %s failed (%s)\n",
                    ps_mainIdent,skPath,strerror(errno));
    return -1;
  };
 
  return 0;

}


int ps_load_port_sk(
        uint8_t* sk,
        const struct in6_addr* addr,
        uint64_t port ) {

  char skPath[PSD_MAX_PATH_SIZE]; 

  psd_mkPortDecryptKeyPath(skPath,addr,port);

  if ( load_sk(skPath,sk) < 0 ) {
    fprintf(stderr,"ERROR (%s): Loading %s failed (%s)\n",
                    ps_mainIdent,skPath,strerror(errno));
    return -1;
  };
 
  return 0;

}

int ps_load_node_sk(
        uint8_t* sk,
        const struct in6_addr* addr ) {

  char skPath[PSD_MAX_PATH_SIZE]; 

  psd_mkNodeDecryptKeyPath(skPath,addr);

  if ( load_sk(skPath,sk) < 0 ) {
    fprintf(stderr,"ERROR (%s): Loading %s failed (%s)\n",
                    ps_mainIdent,skPath,strerror(errno));
    return -1;
  };
 
  return 0;

}



int ps_load_sockTimeout(
        MicroTime* ret,
        const struct in6_addr* addr,
        uint64_t port ) {

  char path[PSD_MAX_PATH_SIZE]; 
  MicroTime timeout;
  

  psd_mkPortTimeoutPath(path,addr,port);

  if ( loadf_from_file(path,"%lu",&timeout) < 0 ) {
    fprintf(stderr,"ERROR (%s): Loading %s failed (%s)\n",
                    ps_mainIdent,path,strerror(errno));
    return -1;
  };

  MicroTime dt = ps_baseInterval;

  *ret = ((timeout + dt - 1)/dt)*dt; /* round up to next interval border */

  return 0;

}



void ps_load_clusterAddr(
        struct in6_addr* ret,
        const struct in6_addr* addr 
        ) {
  
  if (ret == NULL) {
    errno = EINVAL;
    return;
  }

  if ( is_multicast(addr) ) { 
    ip6cpy(ret,addr);
    return;
  };

  char path[PSD_MAX_PATH_SIZE]; 

  psd_mkNodeClusterAddressPath(path,addr);

  char* addrStr = mload_file(path);
  if (addrStr == NULL) { 
    
    if ( errno != ENOENT ) {
      fprintf(stderr,"WARING (%s): Loading %s failed (%s)\n",
                     ps_mainIdent,path,strerror(errno));
      ip6cpy(ret,addr); /* not a (valid) replicated port */
    }

    errno = 0; /* ENOENT not an error - just not a cluster node */

    ip6cpy(ret,addr); 
    return;
  
  };

  if ( ip6pton(addrStr,ret) < 0 ) {
    fprintf(stderr,"WARING (%s): Invalid IPv6 address %s "
                   " in %s ignored\n",
                    ps_mainIdent,addrStr,path);
    ip6cpy(ret,addr); /* defaulting to not a (valid) replicated port */
  }
  
}



size_t ps_load_clusterByzFaultTol(
        const struct in6_addr* senderAddr 
        ) {

  char path[PSD_MAX_PATH_SIZE]; 

  if (! is_multicast(senderAddr) ) return 0;

  psd_mkClusterByzFaultTolPath(path,senderAddr);

  char* tolStr = mload_file(path);
  if (tolStr == NULL) { 
    
    if ( errno != ENOENT ) {   
      fprintf(stderr,"WARING (%s): Loading %s failed (%s)\n",
                      ps_mainIdent,path,strerror(errno));
    }
    
    return 0; /* default to 0 */

  };

  size_t ret;

  if ( sscanf(tolStr,"%zu",&ret) != 1 ) {
    fprintf(stderr,"WARING (%s): Invalid fault tolerance %s "
                   " in %s ignored\n",
                    ps_mainIdent,tolStr,path);
    return 0;
  }
 

  return ret;
  
}


IP6Addr_BD* ps_load_clusterNodes(
              const IP6Addr* clusterAddr 
            ) {

  char path[PSD_MAX_PATH_SIZE]; 
  struct in6_addr addr;

  if (! is_multicast(clusterAddr) ) goto DEFAULT;
  
  psd_mkClusterNodesPath(path,clusterAddr);


  char* str = mload_file(path);
  if (str == NULL) { 
    
    /* not a (valid ) cluster. default to self */      
    if ( errno != ENOENT ) {  
      fprintf(stderr,"WARING (%s): Loading %s failed (%s)\n",
                    ps_mainIdent,path,strerror(errno));
    }

    goto DEFAULT;
    
  };

  /* TODO: better capacity estimations ... strtok is ... well ... old?  */
  IP6Addr_BD* ret = new_batchdata(IP6Addr,(strlen(str)/2),(strlen(str)/2)*16);

  char del[] = " \n\t\r";
  for ( char* tok = strtok(str,del) ; tok != NULL ; tok = strtok(NULL,del) ) {

    if ( ip6pton( tok,& addr ) < 0 ) {
      fprintf( stderr, "WARING (%s): Invalid node entry %s "
                       " in %s ignored\n",
                       ps_mainIdent, tok, path );
    } else {
      new_batchentry(IP6Addr,ret,&addr,sizeof(addr) );
    }
   
  }
 
  return ret;

DEFAULT:
  
  ret = new_batchdata(IP6Addr,1,sizeof(*clusterAddr)); /* default to 0 */
  new_batchentry(IP6Addr,ret,clusterAddr,sizeof(*clusterAddr));
  return ret;

}




size_t ps_load_clusterNodeCount(
        const struct in6_addr* clusterAddr 
        ) {

  if (! is_multicast(clusterAddr) ) return 1;

  IP6Addr_BD* nodes = ps_load_clusterNodes(clusterAddr);
  size_t nodeCount = nodes->entry_count;
  delete_batchdata(IP6Addr,nodes);

  return nodeCount;

}



/* this one fails on normal form packes with errno=EINVAL */
int ps_try_load_packetReplicator(
      struct in6_addr* ret,
      const PSPKT* pkt ) {

  if (! is_multicast( &pkt->header.dst_addr ) ) {
    if ( is_multicast( &pkt->header.src_addr) ) {
      errno = EINVAL; /* data not recoverable from normal form */
      return -1;      /* ... unless we put it in the rmac field */
    }  
    ps_load_clusterAddr(ret,&pkt->header.src_addr);
  } else {
    ip6cpy(ret,&pkt->header.dst_addr); /* dst node is replicator */
  }

  return 0;  

}



void ps_load_packetSender(
       struct in6_addr* ret,
       const PSPKT* pkt) {

  if ( is_multicast(&pkt->header.dst_addr ) ) {
    /* replication case src_addr is replicator address */
    ps_load_clusterAddr(ret,ps_const_packetSigner(pkt));
  } else {
    ps_load_clusterAddr(ret,&pkt->header.src_addr);
  }

}   




void ps_load_packetReceiver(
       struct in6_addr* ret,
       const PSPKT* pkt) {
  if (! is_multicast(&pkt->header.dst_addr ) ) {
     ps_load_clusterAddr(ret,&pkt->header.dst_addr);
  } else {
    ip6cpy(ret,&pkt->header.dst_addr); /* dst node is replicator */
  }
}   


void ps_load_packetSigner(
       struct in6_addr* ret,
       const PSPKT* pkt ) {

  ip6cpy( ret, &ps_const_packetFooter(pkt)->signature.addr );

}   




extern char** environ;


pid_t ps_fork(const char* logName) {
  pid_t ret = fork(); 
  if ( ret == (pid_t)-1) { 
    fprintf(stderr,"ERROR (%s): Could fork process for %s (%s)\n",
                   ps_mainIdent,logName,strerror(errno) );
  }
  return ret;
}



void ps_execve(const char* filename,char *const argv[],char* const envp[]) {

  /* TODO: activate this with a log level 
   
      fprintf(stderr,"Starting Service: %s %s %s\n",argv[0],argv[1],argv[2]);
  */


  int ret = execve(filename,argv,envp);
    
  if ( ret < 0 ) {
      fprintf(stderr,"ERROR (%s): Could not execute %s (%s)\n",
                     ps_mainIdent,argv[0],strerror(errno) );
      exit(EXIT_FAILURE);
  }

}

/* simple interface just up to two arguments */
pid_t ps_fork_exec ( char* const exec,
                     char* const  arg1name,
                     const BD*   arg1bd,
                     char* const arg2name,
                     const BD*   arg2bd ) {

  char *const argv[4] = {exec,arg1name,arg2name,NULL};  

  if ( shmbd_put_batchdata(arg1bd,arg1name) < 0 ) {
   fprintf(stderr,"ERROR (%s): Could not create shared memory batchdata "
                  "for argument one (%s) of %s (%s)\n",
                   ps_mainIdent,argv[1],argv[0],strerror(errno) );
   return -1;
  }

  if ( arg2name != NULL ) {
    if ( shmbd_put_batchdata(arg2bd,arg2name) < 0 ) {
      fprintf(stderr,"ERROR (%s): Could not create shared memory batchdata "
                    "for argument tow (%s) of %s (%s)\n",
                     ps_mainIdent,argv[1],argv[2],strerror(errno) );
     return -1;
    }
  }
  
  pid_t ret = ps_fork(exec);
  if ( ret == -1) return -1; 
  
  if ( ret == 0 ) ps_execve(argv[0],argv,environ); /* the child */

  return ret;

}/* this waits for the receivers to return */




void* ps_malloc(size_t size) {

  void* ret = malloc(size);
  if ( ret == NULL ) {
     fprintf(stderr,"ERROR (%s): Out of memory\n", ps_mainIdent );
     exit(EXIT_FAILURE);
  }

  return ret;

}


size_t ps_inplace_strip_too_small(PSPKT_BD* bd) {

   for (size_t i = 0; i < bd->entry_count; i++) {
    if (bd->entry[i].size < PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE ) {
       bd->entry[i].data = 0;
     }
   }

   return bd_inplace_strip(PSPKT,bd);

}

size_t ps_inplace_strip_duplicates(PSPKT_BD* bd) {

   if ( sort_batchdata(uint8_t,(BD*)bd,batchentryCmp) < 0 ) {
     fprintf(stderr,"ERROR (%s): Unable to strip duplicate batchdata "
                    "entries (%s).",ps_mainIdent,strerror(errno)); 
     return 0;                
   };

   for ( size_t i = 0; i < bd->entry_count; i++ ) {
     for ( size_t j = i+1; j < bd->entry_count; j++ ) {
       size_t curSize = bd->entry[i].size;
       if ( curSize == bd->entry[j].size 
              && memcpy(bd->entry[i].data, bd->entry[j].data, curSize)==0) {
         bd->entry[j].data = NULL;
       } else {
         break; 
       } 
     }
   }

   return bd_inplace_strip(PSPKT,bd);

}




pid_t ps_waitpid(pid_t cld,const char *name ) {
  pid_t pid;

  int wstatus;
  /* this loop is a ipc barrier for the receiver processes */  
  do {     
    pid = waitpid(cld,&wstatus,0);
  } while ( pid == -1 && errno == EINTR );
      
  if ( pid == -1 ) {
    fprintf(stderr,"WARNING (%s): Waiting for %s process failed (%s).\n",
                   ps_mainIdent, name ,strerror(errno));
  }
  if (! WIFEXITED(wstatus) ) {
    fprintf(stderr,"WARNING (%s): Process %s terminated abnormally. ",
                   ps_mainIdent, name );
    if ( WIFSIGNALED(wstatus) ) {
      fprintf(stderr,"It was killed by signal %d\n", WTERMSIG(wstatus));
    } else if ( WIFSTOPPED(wstatus) ) {
      fprintf(stderr,"It was stopped by signal %d\n", WSTOPSIG(wstatus));
    } else if ( WIFCONTINUED(wstatus) ) {
      fprintf(stderr,"It continued\n");
    }
    #ifdef WCOREDUMP
      else if ( WCOREDUMP(wstatus) ) {
      fprintf(stderr,"It core dumped\n");
    }
    #endif
  }

  return pid;

}  
