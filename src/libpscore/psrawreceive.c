#include "psd.h"
#include "pscore.h"
#include <stdbool.h>


/* TODO: split batches for load balancing */

bool routing = false;

int psrawreceive_main(int argc, char** argv) {
 
  char ckrmac[PSD_MAX_PATH_SIZE];
  IP6Addr *src_addr, *dst_addr;
  
  if ( ps_service_init("psrawreceive",argc,argv) < 0 ) exit(EXIT_FAILURE); 

 
  psd_mkCkrmacPath(ckrmac);

  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) exit(EXIT_FAILURE);

  PSPKT_BD** bins = ps_alloc_and_split(bd,pspktBeSrcDstIP6Cmp,NULL);
  if (bins == NULL) exit(EXIT_FAILURE);
   
  for( PSPKT_BD** cur = bins; *cur != NULL; ++cur) {

    src_addr = &(*cur)->entry->data->header.src_addr;
    dst_addr = &(*cur)->entry->data->header.dst_addr;
 
    if ( ps_setenv_PSSRCIP(src_addr) < 0 ) continue; 
    if ( ps_setenv_PSDSTIP(dst_addr) < 0 ) continue; 
    
    char ckrmacBatchName[NAME_MAX+1];
    ps_mk_nodeShmbdName( ckrmacBatchName,
                         ps_inputBatchTime,
                         dst_addr,
                         ".ckrmac.bd" );

    ps_fork_exec(ckrmac,ckrmacBatchName,(BD*)*cur,NULL,NULL);
 
    delete_batchdata(PSPKT,*cur); 
      
  } 
    
  free(bins);
  delete_batchdata(PSPKT,bd);
 
  return EXIT_SUCCESS; 
   
}


