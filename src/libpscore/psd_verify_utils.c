#include "pscore.h"
#include "psdaemon.h"


/* Steps:
 *  1) Sort by source and load public keys 
 *
 *  2) Make normal forms (Normalizes source and destination addresses.
 *  
 *  3) Verify signatures - yields a batch of verified psds  
 *     A verified psd contains
 *      * pointer to the bsd data
 *      * size of the datagram (i.e. hader+footer)
 *      * A proof of validity 
 *        - number of valid signatures (bigger then sender fault tol)
 *        - array of valid signatures
 *
 *  4) Verify datagrams by selecting the datagram with the greatest number
 *     of valid signatures in their proof. 
 *
 */


/* 
 * changes the arguments batches input psd packets into packet normal form
 * returns a batch of PSDPK-s for the sorted batch;
 *
 */

PI_BD* sortBySrc_normalize_and_load_mlocked_pks( 
        PSPKT_BD* bd, 
        struct in6_addr* dst_addr ) {

  struct in6_addr receiverAddr, senderAddr, signerAddr;
  uint64_t signerPort;
  PSPKT_BE *be;
 
  if ( ps_inplace_strip_too_small(bd) == 0 ) {
    fprintf(stderr,"ERROR (%s): All packets too small to be valid (%s)\n",
                       ps_mainIdent,strerror(errno));
    return NULL;
  }

  if (! ps_verify_pktbd_dst_addr(bd,dst_addr) ) {
    fprintf(stderr,"INTERNAL ERROR (%s): Invalid "
                   "sortBySrc_normalize_and_load_mlocked_pks argument\n",
                    ps_mainIdent);
    errno = EINVAL;
    return NULL;
  }

  ps_load_clusterAddr(&receiverAddr,dst_addr);

  ps_inplace_strip_duplicates(bd); /* strip full duplicates */ 
  if ( bd->entry_count == 0 ) return NULL;

  if ( sort_batchdata(PSPKT,bd,pspktBeSrcIP6Cmp) < 0) return NULL;
  
  PI_BD* ret = new_batchdata(PI,bd->entry_count,bd->entry_count*sizeof(PI));
  if ( ret == NULL) return NULL;

  lock_mem(ret->stack,ret->stack_capacity);

  size_t i = 0;
  while ( i < bd->entry_count ) {

    PI info;
    
    ps_load_packetSigner(&signerAddr, bd->entry[i].data);
    ps_load_packetSender(&senderAddr, bd->entry[i].data);
    ps_load_packetReceiver(&receiverAddr,bd->entry[i].data);

    ps_try_load_packetReplicator( 
                  &info.replicatorAddr,
                  bd->entry[i].data );/* packets must not be in normal form already */
    signerPort = get_uint64n(&bd->entry[i].data->header.src_port); 

    if ( ps_load_auth_pk(info.signerPK,&signerAddr,signerPort) < 0 ) {
      ++i; 
      continue;
    }

    do { /* normalize addrs */

      be = &bd->entry[i];
 
      info.packet = bd->entry[i].data;
  
      ip6cpy( &be->data->header.src_addr, &senderAddr );
      ip6cpy( &be->data->header.dst_addr, &receiverAddr );

      add_batchentry(PI,i,ret,&info,sizeof(PI));
      ++i;
    } while ( be_has_equal_prev(PSPKT,i,bd,pspktBeSrcCmp) );
  }

  return ret;

}




/* packets are now sorted by 
   *  1. Datagram Data.
   *  2. For equal datagrams, by signer addresses.
   *  3. For equal datagrams and signer addresses, by signature data 
   *  4. For equal datagrams, signer addresses and signature data 
   *     by replicator addresses. 
   *
   *  The order has blocks of same datagram data 
   *    with subblocks of same signer data with subblocs of same 
   *    replicator data. 
   *
   */


/* pi for packet info */
int pi_datagram_compare(const PI_BE* a,const PI_BE* b) {
 
  if (   ps_packetDatagramSize(a->data->packet) 
       < ps_packetDatagramSize(b->data->packet) ) { 
    return -1;
  }

  if (   ps_packetDatagramSize(a->data->packet) 
       > ps_packetDatagramSize(b->data->packet) ) {
    return  1;
  }

  int r = memcmp( ps_packetDatagram(a->data->packet), 
                  ps_packetDatagram(b->data->packet), 
                  ps_packetDatagramSize(a->data->packet) );
                  
  if (r > 0) return 1;
  if (r < 0) return -1; 

  return 0;

}


int pi_signer_compare(const PI_BE* a,const PI_BE* b) {
  
  return ip6cmp( ps_const_packetSigner(a->data->packet), 
                 ps_const_packetSigner(b->data->packet) );

}



int pi_signatureData_compare(const PI_BE* a,const PI_BE* b) {
 
  int r = memcmp( ps_const_packetSignature(a->data->packet)->data,
                  ps_const_packetSignature(b->data->packet)->data,
                  HASH_SIZE );

  if (r > 0) return 1;
  if (r < 0) return -1; 

  return 0;

}


int pi_replicator_compare(const PI_BE* a,const PI_BE* b) {
 
  return ip6cmp( &a->data->replicatorAddr,&b->data->replicatorAddr );

}


int pi_compare(const PI_BE* a,const PI_BE* b) {

  int r;

  r = pi_datagram_compare( a, b);
                  
  if (r > 0) return 1;
  if (r < 0) return -1; 

  r = pi_signer_compare( a, b);

  if (r > 0) return 1;
  if (r < 0) return -1; 

  r = pi_signatureData_compare( a, b);

  if (r > 0) return 1;
  if (r < 0) return -1; 

  r = pi_replicator_compare( a, b);

  if (r > 0) return 1;
  if (r < 0) return -1; 

  return 0;

}


int pi_signerSpan_compare(const PI_BE* a,const PI_BE* b) {

  int r;

  r = pi_datagram_compare( a, b);
                  
  if (r > 0) return 1;
  if (r < 0) return -1; 

  r = pi_signer_compare( a, b);

  if (r > 0) return 1;
  if (r < 0) return -1; 

  return 0;

}


int pi_signatureDataSpan_compare(const PI_BE* a,const PI_BE* b) {

  int r;

  r = pi_datagram_compare( a, b);
                  
  if (r > 0) return 1;
  if (r < 0) return -1; 

  r = pi_signer_compare( a, b);

  if (r > 0) return 1;
  if (r < 0) return -1; 

  r = pi_signatureData_compare( a, b);

  if (r > 0) return 1;
  if (r < 0) return -1; 

  return 0;

}




/* a helper with proper warning output */
bool is_sender_node( IP6Addr* node,
                     IP6Addr* sender, 
                     IP6Addr_BD* senderNodes ) {
  char nodeStr[INET6_ADDRSTRLEN];
  char senderStr[INET6_ADDRSTRLEN];

  if (! is_in_batchdata(IP6Addr,senderNodes,node,sizeof(*node))) { 
    ip6ntop(node,nodeStr);
    ip6ntop(sender,senderStr);
    fprintf(stderr,"WARNING (%s): Node %s is claiming, by use of "
                   "signatures to be a member of cluster %s. But this " 
                   "system is not aware of it. All packets signed for "
                   "the cluster from this node are dropped!\n",
                   ps_mainIdent, nodeStr, senderStr );
    return false;
  }

  return true;

} 





/* returns VPSD batches, does not vote */
VPSD_BD* ps_verify_signatures ( 
           PI_BD* pi_bd
         ) {
  
  size_t i, senderNodeCount, senderFaultTol, receiverFaultTol;
  PI_BD *sorted;
  VPSD_BD *ret;
  IP6Addr *senderAddr, *receiverAddr, *signerAddr;
  PI *pi,*signer_pi,*signature_pi;
  IP6Addr_BD* senderNodes; 
  PSS *signature;
  uint64_t signerPort;

  /* TODO: use a less wasteful capacity estimation */  
  ret = new_batchdata( VPSD,
                       pi_bd->entry_count,
                       pi_bd->entry_count * PSPKT_MAX_SIZE );
  if (ret == NULL) return NULL;

  sorted = shallow_copy_batchdata(PI,pi_bd);
  if (sorted == NULL) goto FAIL;

  if (sort_batchdata(PI,sorted,pi_compare) < 0 ) goto FAIL;

  i = 0;
  while ( i < sorted->entry_count ) { /* Begin processing a datagram block */
    
    pi = sorted->entry[i].data;
    senderAddr = &pi->packet->header.src_addr; /*since normalized*/ 
    senderNodeCount = ps_load_clusterNodeCount( senderAddr );
    senderFaultTol = ps_load_clusterByzFaultTol( senderAddr ); 
    senderNodes = ps_load_clusterNodes( senderAddr );

    VPSD* vpsd = (VPSD*)alloca( vpsd_size(senderNodeCount) ); 

    if ( ps_mk_packetDatagramHash( vpsd->proof.hash, pi->packet ) < 0 ) {
      fprintf(stderr,"INTERNAL ERROR(%s): Could not compute signature hash "
              "for packet: \n",ps_mainIdent);
      fprint_pspkt(stderr,pi->packet);
      be_skip_equal_span(PI,&i,sorted,pi_datagram_compare);
      continue;
    } 

    receiverAddr = &pi->packet->header.dst_addr; /*since normalized*/ 
    receiverFaultTol = ps_load_clusterByzFaultTol(receiverAddr);

    vpsd->replicationCount = 0;
    set_uint64n( &vpsd->proof.signatureCount, 0 );
    
    do { /* begin processing a signer block */
 
      signer_pi = sorted->entry[i].data;
      signerAddr = ps_packetSigner(signer_pi->packet);
      char addrStr[INET6_ADDRSTRLEN];
      ip6ntos(signerAddr,addrStr);

      if(! is_sender_node(signerAddr,senderAddr,senderNodes) ) {
        be_skip_equal_span(PI,&i,sorted,pi_signerSpan_compare);
        continue; 
      }

      /* if there are signature algorithms that can pre-compute data
       * for validating the same data against the same public key 
       * against different signature candidates. Here is the place to do it.*/ 

      do { /*  begin processing a signature data block */
 
        signature_pi = sorted->entry[i].data;
        signature = ps_packetSignature(signature_pi->packet); 

        assert( ip6cmp( ps_packetSigner(signature_pi->packet),
                        signerAddr ) == 0);
        
        signerPort = get_uint64n(&signature_pi->packet->header.src_port); 

        uint8_t pk[PSD_PK_SIZE]; 
        if ( ps_load_auth_pk(pk,signerAddr,signerPort) < 0 ) {
          assert( memcmp(pk,signer_pi->signerPK,PSD_PK_SIZE) == 0 ); 
          fprintf(stderr,"ERROR (%s): Could not load public key of signer "
                         "of packet:\n",
                          ps_mainIdent);
          fprint_pspkt(stderr,signature_pi->packet);
          continue;
        }
 

        if ( crypto_sign_verify_detached(
                  signature->data, 
                  vpsd->proof.hash,
                  HASH_SIZE,
                  signer_pi->signerPK ) == 0 ) {

          size_t j = get_uint64n(&vpsd->proof.signatureCount);
          set_uint64n(&vpsd->proof.signatureCount,j+1);
        
          memcpy( &vpsd->proof.signature[j],signature, sizeof(*signature) ); 

          vpsd->datagram = ps_packetDatagram(signature_pi->packet);          
          vpsd->replicationCount = 0;
      
          /* TODO: Is checking replicator cluster membership important?
           *       If so add checking it.  */ 
          do { /*  begin processing a replicator block block */
            vpsd->replicationCount++;
            be_skip_equal_span(PI,&i,sorted,pi_compare); 
          } while ( be_has_equal_prev( PI,i,sorted,
                                       pi_signatureDataSpan_compare  ) );  

        } else { /* wrong */
          
          fprintf(stderr,"SEVERE WARNING (%s): There is at least one invalid"
                         " signature for packet:\n",ps_mainIdent);
          fprint_pspkt(stderr,signature_pi->packet);

          be_skip_equal_span(PI,&i,sorted,pi_signatureDataSpan_compare);
    
        }
      
      } while ( be_has_equal_prev(PI,i,sorted,pi_signerSpan_compare) );
      
    } while ( be_has_equal_prev(PI,i,sorted,pi_datagram_compare) );
 
     
    if (    get_uint64n(&vpsd->proof.signatureCount) > senderFaultTol  
         && vpsd->replicationCount > receiverFaultTol ) {
     
        new_batchentry(VPSD ,ret, vpsd, 
                        vpsd_size( get_uint64n(&vpsd->proof.signatureCount))); 
    
    }

    delete_batchdata(IP6Addr,senderNodes);    
    
  } /* iterate to next datagram block */
  
  delete_batchdata(PI,sorted);

  return ret;

FAIL:

  delete_batchdata(VPSD,ret);

  return NULL;

}




int vpsdBeHeaderCmp(const VPSD_BE* a,const VPSD_BE* b) {

  int r = memcmp( a->data->datagram, b->data->datagram, PSD_HEADER_SIZE);
                  
  if (r > 0) return 1;
  if (r < 0) return -1; 
  return 0;

}



int vpsdBeProofStrengthCmp(const VPSD_BE* a,const VPSD_BE* b) {

  if ( a->data->replicationCount < b->data->replicationCount ) return -1;
  if ( a->data->replicationCount > b->data->replicationCount ) return  1;

  if (   get_uint64n( &a->data->proof.signatureCount )  
       < get_uint64n( &b->data->proof.signatureCount ) ) return -1;

  if (   get_uint64n( &a->data->proof.signatureCount )
       > get_uint64n( &b->data->proof.signatureCount ) ) return  1;
  
  return 0;

}



/* determines the true packet of replicated packets 
 * 
 * For a byzantine fault tolerance of k the cluster must
 * have at least k+1 nodes. It may have more, for increased reliability. 
 *
 * The candidate for the true packet is the packet with the most number
 * of signatures whose validity is assumed and not checked again.
 * If there are k+1 votes/signatures for the packet it is accepted
 * since that provides the required fault tolerance level.  
 *
 */

/* sorts the argument batch by soucre
 * sufficient amount of valid signatures
 * returns validated signatures.  
 */

VPSD_BD* ps_verify ( PSPKT_BD*  bd, 
                     struct in6_addr* dst_addr,
                     const char* deadlineEntryPath ) {

  size_t packetCount,ec;
  VPSD_BD* ret = NULL;

  PI_BD* pi_bd = sortBySrc_normalize_and_load_mlocked_pks(bd,dst_addr);
  if (pi_bd == NULL) return NULL;
   
  VPSD_BD* vs = ps_verify_signatures(pi_bd);
  if (vs == NULL) goto CLEANUP0;

  if (vs->entry_count != pi_bd->entry_count) {
    fprintf(stderr,"WARNING (%s): Found invalid signatures in batch %s\n",
                    ps_mainIdent,deadlineEntryPath ); 
  }

  VPSD_BD** pBin = alloc_split_batchdata(VPSD,vs,vpsdBeHeaderCmp,&packetCount);
  if ( pBin == NULL ) {
    fprintf(stderr,"WARNING (%s): Out of memory. Batch %s Lost.",
                    ps_mainIdent,deadlineEntryPath );
    goto CLEANUP1;
  }

  ret = new_batchdata(VPSD,vs->entry_count,vs->data_size);
  if (ret == NULL) goto CLEANUP2;

  for ( size_t i = 0; i < packetCount; ++i ) {
    
    sort_batchdata(VPSD,pBin[i],vpsdBeProofStrengthCmp);

    ec = pBin[i]->entry_count;   

    VPSD* fst = pBin[i]->entry[ec-1].data;
    
    if ( ec > 1 ) {

      VPSD* snd = pBin[i]->entry[ec-2].data;
 
      if ( fst->replicationCount == snd->replicationCount 
            && get_uint64n( &fst->proof.signatureCount) 
                == get_uint64n( &snd->proof.signatureCount ) ) {
         fprintf(stderr,"SEVERE WARNING (%s): Packte ignored due to "
                        " ambiguous signatures. \n"
                        " Sending Cluster probably compromised!\n"
                        " Data of the suspicious packet:\n",
                        ps_mainIdent );
         fprint_psd(stderr,pBin[i]->entry[ec-1].data->datagram);
         continue;
      }
    
    }
    
    /* add the candidate with the most votes,
     * replication consistency priories */
    new_batchentry(VPSD,ret,fst,
                    vpsd_size( get_uint64n( &fst->proof.signatureCount ) ) );
  
  }
  

CLEANUP2:

  for ( size_t i = 0; i < packetCount; ++i ) delete_batchdata(VPSD,pBin[i]);
  free(pBin);

CLEANUP1:

  delete_batchdata(VPSD,vs);

CLEANUP0:

  clear_mem(pi_bd->stack,pi_bd->stack_capacity);
  delete_batchdata(PI,pi_bd);

  return ret;

}
