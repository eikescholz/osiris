#include <sys/types.h>
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "psd.h"
#include "batchdata.h"
#include "sysutils.h"
#include "pscore.h"
#include <time.h>

#define SEND_BATCH_DELAY 00


/* this will get a batch of datagrams as input
 * i.e. data without footers, and provides
 * data with footers to pspsign 
 */

int pssend_main(int argc, char** argv) {
  struct in6_addr* src_addr;
  uint64_t src_port;
  char portSignPath[PSD_MAX_PATH_SIZE];
  size_t n; 
  int ret;
  size_t k;
  PSPKT_BE* be;

  if ( ps_service_init("pssend",argc,argv) < 0 ) exit(EXIT_FAILURE); 

  /* TODO: Actually the input data is not a batch of packets but of 
   *       datagrams. So this is an abuse of interface */

  PSPKT_BD* psds = ps_read_serviceInput();
  if (psds == NULL) exit(EXIT_FAILURE);

  PSPKT_BD** srcPsdBins = ps_alloc_and_split(psds,pspktBeSrcCmp,&k);
  if ( srcPsdBins == NULL) exit(EXIT_FAILURE);
 
  PSPKT_BD** srcPktBins = ps_malloc((k+1)*sizeof(*srcPktBins));
  if ( srcPktBins == NULL) exit(EXIT_FAILURE);

  srcPktBins[k] = 0; 
  
  for(size_t i = 0; i < k ;++i) {
    
    srcPktBins[i] = new_batchdata(
                      PSPKT,
                      srcPsdBins[i]->entry_count,
                      srcPsdBins[i]->data_size 
                          + srcPsdBins[i]->entry_count * PSPKT_FOOTER_SIZE );
    
    if ( srcPktBins[i] == NULL ) {
      fprintf(stderr,"ERROR (pssend): Out of memory !\n");
      exit(EXIT_FAILURE); 
    }
   
    for(size_t j = 0; j < srcPsdBins[i]->entry_count; ++j ) {

      if ( is_complete_pspkt(srcPsdBins[i]->entry[j].data,
                             srcPsdBins[i]->entry[j].size ) ) {
        fprintf(stderr,"WARNING (pssend): A possibly completes packet "
                       "detected and ignored! \n");
        continue; 
      }

      be = new_batchentry( PSPKT,
                           srcPktBins[i],
                           NULL,
                           srcPsdBins[i]->entry[j].size + PSPKT_FOOTER_SIZE);
      assert( be != NULL );
      memcpy( be->data,
              srcPsdBins[i]->entry[j].data,
              srcPsdBins[i]->entry[j].size );

      memset( ps_packetFooter(be->data),0,PSPKT_FOOTER_SIZE);

    }

    delete_batchdata(PSPKT,srcPsdBins[i]);

  }

  free(srcPsdBins);

  struct timespec dt = { 0, SEND_BATCH_DELAY }; 
  struct timespec rem;

  for (PSPKT_BD** srcPktBin = srcPktBins; *srcPktBin != NULL; ++srcPktBin ) {

    n = 1 + (*srcPktBin)->entry_count/(PSCORE_MAX_BIN_SIZE-1);

    PSPKT_BD** bin = ps_malloc( n*sizeof(*bin) );
    if ( bin == NULL ) exit( EXIT_FAILURE );

    if ( split_batchdata(PSPKT,*srcPktBin,n,bin) < 0 ) {
      fprintf(stderr,"ERROR (pssend): Out of memory 3\n");
      exit(EXIT_FAILURE);
    }
    
    for(size_t i=0; i < n; ++i) {
      
      if (bin[i]->entry_count != 0 ) { 
      
        src_port = get_uint64n(&bin[i]->entry->data->header.src_port); 
        src_addr = &bin[i]->entry->data->header.src_addr;

        if ( ps_setenv_PSSRCPORT(src_port) < 0 ) continue; 
      
        if ( ps_setenv_PSSRCIP(src_addr) < 0 ) continue; 
      
        psd_mkPortSignPath(portSignPath,src_addr,src_port);

        char psignBatchName[NAME_MAX+1];
        ps_mk_portShmbdName( psignBatchName,
                             ps_inputBatchTime,
                             src_addr,
                             src_port,
                             ".sign.bd" );
      
        ps_fork_exec(portSignPath,psignBatchName,(BD*)bin[i],NULL,NULL);
 
        do {
          ret = nanosleep(&dt,&rem); /* slow things a bit down */
        } while (ret == -1 && errno == EINTR);
      }
      
      delete_batchdata(PSPKT,bin[i]);
      
    }

    free(bin);
    delete_batchdata(PSPKT,*srcPktBin);
  
  }  

  fflush(stderr);

  free(srcPktBins);
  delete_batchdata(PSPKT,psds);

  return EXIT_SUCCESS;

}

