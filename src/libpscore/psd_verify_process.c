


/* receives sigints from the psd to wake up
 *
 * This must never start a new vote handler if the old none
 * did not finish. It does not discard packets due to timeout
 * anymore, its possible to be relexed about this. 
 * 
 * So new batches are created as 
 * ps/queue/$DEADLINE/$DSTADDRp$DSTPORT
 *
 * The (verify) queued is a single master process that managest two queues
 *  1) A queue for pid of a worker, processing all merge-verify-vote porcesses.
 *     of a given $DEADLINE in parallel.
 *  2) A queue for $DEADLINES of a worker processing all receives for finished
 *     processes of queue 1 in parallel.
 *
 *  The single master porcess "iterates" though the deadlines. If there
 *  is are batches to process fork a worker and put the pid into the end of
 *  queue 1. Then wait (without waiting) if the first elements of queue
 *  1 are ready. If terminated, remove and start a worker for queue 2 for
 *  all of them in order. For the recives the queue stores the deadlines
 *  not jet begun. The entry at the start is started and removed when
 *  the receiver is finished. 
 *
 *  
 *  Yields a single fast master thread and two serialization points with
 *  atomic-fetch-add for queue indices and order between deadlines is
 *  strictly preserved! Master thread runs at speed of essentially two
 *  forks.
 *  
 */

#include "pscore.h"
#include "psdaemon.h"


#if 0
MicroTime deadlineQueue_next(const char* dqPath) {
  DIR *dir 
  MicroTime next = MICROTIME_MAX;
  MicroTime entryTime;
  struct dirent* de;

  dir = opendir(deadlinePath);
  for (de = readdir(dir); de != NULL; de = readdir(dir)) {
    if ( scanf(de->d_name,"%lX",&entryTime) == 1 ) {
      next = entryTime < next ? entryTime : next;
    }
  } 

  if ( next == MICROTIME_MAX ) {
    return (microtime(NULL)/dt)*dt; /* current interval */
  } else {
    return next;
  } 
}
#endif 

int verify_deadlineEntry(const char* inPath, DQE* de ) {

  char entryPath[PSD_MAX_PATH_SIZE];
  ShmBd recvBD;
  ShmBd proofBD;
  char receiveName[NAME_MAX+1];
  char proofName[NAME_MAX+1];

  psd_mkDeadlineEntryPath( entryPath,inPath,
                           de->deadline,&de->addr,de->port );
 
  VPSD_BD* verified = ps_verify(de->bd,&de->addr,entryPath);
  if ( verified == NULL ) {
    fprintf(stderr,"WARNING (%s): Could not verify any packets of %s.\n",
                   ps_mainIdent, entryPath );
    return -1;
  }

  if ( de->bd->entry_count != verified->entry_count ) {
    fprintf(stderr,"WARNING (%s): Some packets with arrival deadline %lX "
                   "could not be verified.\n", ps_mainIdent, de->deadline );
 
  }

  if ( verified->entry_count > 0 ) {

    /* create lookup order */
    if ( sort_batchdata(VPSD, verified, vpsdBe_cmp ) < 0 ) {
      fprintf(stderr,"ERROR (%s): Out of memory. All packets of %s lost\n",
                     ps_mainIdent, entryPath );
      return -1;
    }
             
    size_t proofBD_capacity = 0;
    size_t recvBD_capacity = 0;    
    for (size_t i=0; i < verified->entry_count; ++i ) {
      VPSD* vpsd = (VPSD*)verified->entry[i].data;
      proofBD_capacity 
              += psd_proofSize( get_uint64n(&vpsd->proof.signatureCount) );
      recvBD_capacity += ps_datagramSize(vpsd->datagram);
    }

    ps_mk_recvShmbdName(receiveName,de->deadline,&de->addr,de->port,
                        ".psd.bd");
    ps_mk_recvShmbdName(proofName,de->deadline,&de->addr,de->port,
                        ".psd.proof.bd");

    if ( shmbd_create( &recvBD,
                       receiveName,
                       verified->entry_count,
                       recvBD_capacity ) 
            < 0 ) {
      
      fprintf(stderr, "WARNING (%s): Could allocate shared memory receive "
                      " batch for %s (%s). All packets lost!\n",
                      ps_mainIdent, entryPath, strerror(errno) );
      goto FAIL0;
 
    }

    if ( shmbd_create( &proofBD,
                       proofName,
                       verified->entry_count,
                       proofBD_capacity)
            < 0 ) {

      fprintf(stderr,"WARNING (%s): Could allocate shared memory receive "
                     " batch for %s (%s). All packets lost!\n",
                     ps_mainIdent, entryPath, strerror(errno) );
      goto FAIL1;
 
    } 

    for (size_t i=0; i < verified->entry_count; ++i ) {
       
       VPSD* vpsd = (VPSD*)verified->entry[i].data;

       shmbd_append( &recvBD, 
                     vpsd->datagram, 
                     ps_datagramSize(vpsd->datagram) );
       
       shmbd_append( &proofBD,
                     &vpsd->proof,
                     psd_proofSize(
                             get_uint64n( &vpsd->proof.signatureCount ) ) );
    
    }
       
    shmbd_unload(&recvBD);
    shmbd_unload(&proofBD);
  
  }

  delete_batchdata(VPSD,verified);
  
  return 0;

FAIL1:

  shmbd_unlink(&recvBD);
  shmbd_unload(&recvBD);
  
FAIL0:

  delete_batchdata(VPSD,verified);
 
  return -1;

}


/* started from psd main */
int psd_verify_process_main() {

  ps_mainIdent = "psd verify process";
  MicroTime dt = ps_baseInterval;

  DIR* dir; 
  struct entriesVerifierDescriptor cur;
  char inPath[PSD_MAX_PATH_SIZE];
  char deadlinePath[PSD_MAX_PATH_SIZE];
  pthread_t dispatcher;
 
  MicroTime curTime = microtime(NULL);
  MicroTime next_deadline = (curTime/dt)*dt; /* round to current interval */
 
  psd_mkIncomingPath(inPath);

  umask(0007);

  if ( pthread_create(&dispatcher,&threadAttr,
                       psd_dispatcher_thread,inPath) != 0 ) {
      fprintf(stderr,"WARNING (%s): "
                     "Could not create dispatcher thread (%s)! \n",
                     ps_mainIdent,strerror(errno));
      exit(EXIT_FAILURE);
  } 

  while( running ) {

    MicroTime precision = microtime_adjust_precision(); 

    /* The following is single the time measurement that syncs everything */
    if ( microtime(&curTime) + precision < next_deadline ) {
       microsleep(next_deadline - curTime, NULL ); /*sleep or wait for signal*/
    } 
    while ( microtime(NULL) + precision <= next_deadline) sched_yield(); 

    cur.entriesVerifierPid = -1;
    cur.deadline  = next_deadline;
    next_deadline += dt;

    psd_mkDeadlinePath( deadlinePath,inPath, cur.deadline );

    dir = opendir(deadlinePath);
    if (dir != NULL) { /* there is something with the current deadline */

      cur.entriesVerifierPid = fork(); 
      if ( cur.entriesVerifierPid == -1 ) {
        fprintf(stderr,"WARNING (%s): Could fork verification processes"
                       "for deadline %lX (%s). All batches for this deadline"
                       "lost\n", ps_mainIdent, cur.deadline,strerror(errno) );
        continue;
      }

      if (cur.entriesVerifierPid == 0) {
         int ret = ps_parallel_process_deadlineEntries( 
                        inPath,
                        cur.deadline,
                        verify_deadlineEntry,
                        "verify" );
         exit(ret);
      }

      QUEUE_DEADLINE:
 
      if ( mq_send(recvQue,(void*)&cur,sizeof(cur),0) < 0 ) {
        
        if ( errno == EINTR ) goto QUEUE_DEADLINE;
        
        fprintf(stderr,"UNLIKELY ERROR (%s): Could not queue batch for "
                       " retrieval for deadline %lX (%s).\n"
                       " Terminating daemon. All batches for this deadline "
                       "lost\n", ps_mainIdent, cur.deadline,strerror(errno) );
      
        running = false;
      
      }
    
    } else { 

      if (errno != ENOENT) {
        fprintf(stderr,"ERROR (%s): Could not access deadline data %s (%s)\n",
                       ps_mainIdent, deadlinePath,strerror(errno));
 
      }      


      /* did we suspend or something, skip passed deadlines */
      /* this loop is a sync barrier for the receiver processes */

      if ( microtime(NULL) + precision > next_deadline + dt ) {
         next_deadline = (microtime(NULL)/dt)*dt;  
         fprintf(stderr,"INFO (%s): Verification main process slow,"
                        "or system restarting. Skipping timed out batches.\n",
                        ps_mainIdent);
         /* TODO: Print human readable time stamp for analysis */
         /* TODO: Garbage collection for non properly cleaned up deadline
          *       directories 
          */
      };

    }
    closedir(dir);
 
  }

  pthread_kill(dispatcher,SIGINT);
  pthread_join(dispatcher,NULL);

  return EXIT_SUCCESS;
 
}




