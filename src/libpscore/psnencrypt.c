/* This program gets a batch of psd packets form stdin
 * and applies the node encryption and patch executes
 * the socket decryption programs. 
 *
 * This program should be setuid to psdnode$N.psdio$N
 * user.group and executed as real user psdio$N.psdio$N.
 * See DESIGN file. 
 *
 * FIXME: Add rmacs
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

/* assumes that input packets to be encrypted are from the same source */
int psnencrypt_main(int argc,char** argv) {
  char skPath[PSD_MAX_PATH_SIZE];
  char psnmkrmac[PSD_MAX_PATH_SIZE];
  uint8_t sk[crypto_box_SECRETKEYBYTES];
  struct in6_addr src_addr;

  if ( ps_service_init("psnencrypt",argc,argv) < 0) exit(EXIT_FAILURE);
 
  if (! ps_getenv_PSSRCIP(&src_addr) ) exit(EXIT_FAILURE);;
  
  psd_mkNodeDecryptKeyPath(skPath,&src_addr); /* the secret key */

  if ( ps_load_node_sk(sk,&src_addr) < 0 ) exit(EXIT_FAILURE);;

  if ( ps_drop_privileges() < 0) goto FAIL;
  
  /* The node private key file should not be readable now */ 
 
  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) goto FAIL; 

  if (! ps_verify_pktbd_src_addr(bd,&src_addr) ) goto FAIL; 
   
  PSPKT_BD** nodeBins = ps_alloc_and_split(bd,pspktBeDstIP6Cmp,NULL);
  if (nodeBins == NULL) goto FAIL; 
  
  for ( PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; ++curNodeBin ) {
    ps_inplace_encrypt( *curNodeBin,
                        PSD_NODECRYPTO,
                        sk,
                        &src_addr,      
                        ps_mainIdent );
  }
 
  clear_sk(sk);

  PSPKT_BD* bd_merged = ps_merge_and_free(nodeBins);
  if (bd_merged == NULL) exit(EXIT_FAILURE);
 
  psd_mkMkrmacPath(psnmkrmac);

  char mkrmacBatchName[NAME_MAX+1];
  ps_mk_nodeShmbdName( mkrmacBatchName,
                       ps_inputBatchTime,
                       &src_addr,
                       ".mkrmac.bd" );

  ps_fork_exec(psnmkrmac,mkrmacBatchName,(BD*)bd,NULL,NULL);
 
  delete_batchdata(PSPKT,bd_merged);

  delete_batchdata(PSPKT,bd);
 
  return EXIT_SUCCESS; /* nothing could be encrypted */

FAIL:

  clear_sk(sk);

  return EXIT_FAILURE;

}

