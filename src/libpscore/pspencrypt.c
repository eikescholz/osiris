/* This program gets a batch of psd packets form stdin
 * and applies the port encryption and patch executes
 * the socket decryption programs. 
 *
 * Stored as: /var/psock/ADDR/PORT/send ?
 *
 * looks very clear then ... 
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>


/* assumes that input packeds to be encrypted are from the same source */
int pspencrypt_main(int argc, char** argv) {
  char nodeEncryptPath[PSD_MAX_PATH_SIZE];
  uint8_t sk[crypto_box_SECRETKEYBYTES];
  struct in6_addr src_addr,*dst_addr;
  uint64_t src_port,dst_port;

  if ( ps_service_init("pspencrypt",argc,argv) < 0) exit(EXIT_FAILURE);
 
  if (! ps_getenv_PSSRCIP(&src_addr) ) exit(EXIT_FAILURE);
  
  if (! ps_getenv_PSSRCPORT(&src_port ) ) exit(EXIT_FAILURE);
  
  if ( ps_load_port_sk(sk,&src_addr,src_port ) < 0 ) {
    exit(EXIT_FAILURE);
  }
 
  if ( ps_drop_privileges() < 0) goto FAIL;
  
  /* The node private key file should not be readable now */ 
 
  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) goto FAIL; 

  if (! ps_verify_pktbd_src_addr(bd,&src_addr) ) goto FAIL;
  
  if (! ps_verify_pktbd_src_port(bd,src_port) ) goto FAIL;
  
  PSPKT_BD** nodeBins = ps_alloc_and_split(bd,pspktBeDstCmp,NULL);
  if (nodeBins == NULL) goto FAIL; 
  
  for (PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; ++curNodeBin ) {
    ps_inplace_encrypt( *curNodeBin,
                        PSD_PORTCRYPTO, 
                        sk,
                        &src_addr,
                        ps_mainIdent );
  }
 
  clear_sk(sk);

  for (PSPKT_BD** cur = nodeBins; *cur != NULL; ++cur ) {

    dst_port = get_uint64n(&(*cur)->entry->data->header.dst_port); 
    dst_addr = &(*cur)->entry->data->header.dst_addr;

    if ( ps_setenv_PSDSTPORT(dst_port) < 0 ) continue; 
        
    if ( ps_setenv_PSDSTIP(dst_addr) < 0 ) continue; 

    psd_mkNodeEncryptPath(nodeEncryptPath,&src_addr);

    char nencryptBatchName[NAME_MAX+1];
    ps_mk_nodeShmbdName( nencryptBatchName,
                         ps_inputBatchTime,
                         dst_addr,
                         ".nenc.bd" );

    ps_fork_exec(nodeEncryptPath,nencryptBatchName,(BD*)*cur,NULL,NULL);
 
    delete_batchdata(PSPKT,*cur);
    
  }

  delete_batchdata(PSPKT,bd);

  free(nodeBins);

  return EXIT_SUCCESS; 

FAIL:

  clear_sk(sk);

  return EXIT_FAILURE;
        
}

