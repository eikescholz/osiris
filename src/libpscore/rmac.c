#include "pscore.h"


/* this appends the route MAC */
void ps_inplace_mk_beRmac (
       PSPKT_BE* be, /* plain be */
       const struct psd_rmac_arg_t* ra ) { 

  if (be->size < PSPKT_RMAC_SIZE) {
    fprintf(stderr,"WARNING (%s): Packet lost! "
                   "Too small to rmac\n",ra->logName);
    be->data = NULL;
    return;
  }
 
  size_t dataLen = be->size - PSPKT_RMAC_SIZE;
 
  unsigned char* nonceKey = ra->keyBuf;

  if ( crypto_stream( nonceKey,
                      crypto_onetimeauth_KEYBYTES,
                      be->data->header.nonce,
                      ra->sk ) < 0) {
    fprintf(stderr,"WARNING (%s): Packet lost! "
                   "Could not derive one time auth key\n",ra->logName);
    be->data = NULL;
    return;
  }                 

  unsigned char* data = (unsigned char*)be->data;
    
  unsigned char* mac  = data + dataLen; /* append signature */
 
  if ( crypto_onetimeauth( mac,
                           data,
                           dataLen,
                           nonceKey ) < 0 ) {

    char nonceStr[PS_NONCESTR_BYTES];
    sodium_bin2hex( nonceStr,PS_NONCESTR_BYTES,
                    be->data->header.nonce, PS_NONCE_BYTES);
    fprintf(stderr,"WARNING (%s): Packet lost! " 
                 "Could not make RMAC of psd packet %s\n",ra->logName,nonceStr);
    be->data = NULL; 
  }
 
}


void ps_inplace_ck_beRmac (
       PSPKT_BE* be, /* plain be */
       const struct psd_rmac_arg_t* ra ) { 

  if (be->size < PSPKT_RMAC_SIZE) {
    fprintf(stderr,"WARNING (%s): Packet lost! "
                   "Too small too contain rmac\n",ra->logName);
    be->data = NULL;
    return;
  }
 
  size_t len = be->size - PSPKT_RMAC_SIZE; 
 
  unsigned char* nonceKey = ra->keyBuf;

  if ( crypto_stream( nonceKey,
                      crypto_onetimeauth_KEYBYTES,
                      be->data->header.nonce,
                      ra->sk ) < 0 ) {                 
    fprintf(stderr,"WARNING (%s): Packet lost! "
                   "Could not derive one time auth key\n",ra->logName);
    be->data = NULL;
    return;
  }                 


  unsigned char* data = (unsigned char*)be->data;
    
  unsigned char* mac  = data + len; /* append signature */

  
  if ( crypto_onetimeauth_verify( mac,
                                  data,
                                  len,
                                  nonceKey ) < 0 ) {

    char nonceStr[PS_NONCESTR_BYTES];
    sodium_bin2hex( nonceStr,PS_NONCESTR_BYTES,
                    be->data->header.nonce, PS_NONCE_BYTES);
    fprintf(stderr,"WARNING (%s): Packet lost! " 
                 "Could not verify RMAC of psd packet %s\n",ra->logName,nonceStr); 
    be->data=NULL; 
  }

}


/* returns number of successes */
size_t ps_inplace_mk_rmac( 
          PSPKT_BD* bd, 
          const unsigned char* sk,
          const char* mainIdent ) {
  
  unsigned char keyBuf[crypto_stream_KEYBYTES];
  struct psd_rmac_arg_t ra;
  IP6Addr senderAddr,recvAddr,src_addr,dst_addr;

  assert( bd != NULL );
  assert( bd->entry_count >= 1 );

  ip6cpy( &src_addr, &bd->entry->data->header.src_addr );
  ip6cpy( &dst_addr, &bd->entry->data->header.dst_addr );

  assert( ps_verify_pktbd_src_addr(bd,&src_addr) );
  assert( ps_verify_pktbd_dst_addr(bd,&dst_addr) );

  /* TODO: Check where packet is put into normalform */
  ps_load_packetSender(&senderAddr,bd->entry->data);
  ps_load_packetReceiver(&recvAddr,bd->entry->data);

  if ( lock_mem(keyBuf,crypto_stream_KEYBYTES) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not mlock memory",mainIdent);         
    return 0;
  }

  ra.sk = sk;
  ra.keyBuf = keyBuf;
  ra.logName = mainIdent;

  for(size_t i=0; i<bd->entry_count;++i) {

    ip6cpy(&bd->entry[i].data->header.src_addr,&senderAddr);
    ip6cpy(&bd->entry[i].data->header.dst_addr,&recvAddr);
    
    ps_inplace_mk_beRmac(&bd->entry[i],&ra);

    ip6cpy(&bd->entry[i].data->header.src_addr,&src_addr);
    ip6cpy(&bd->entry[i].data->header.dst_addr,&dst_addr);
 
  }
  
  return bd_inplace_strip(PSPKT,bd);               

}       

size_t ps_inplace_ck_rmac( 
        PSPKT_BD* bd, 
        const unsigned char* sk,
        const char* mainIdent ) {
 
  IP6Addr senderAddr,recvAddr,src_addr,dst_addr;
  unsigned char keyBuf[crypto_stream_KEYBYTES];
  struct psd_rmac_arg_t ra;

  assert(bd != NULL);
  assert(bd->entry_count >= 1);

  ip6cpy(&src_addr, &bd->entry->data->header.src_addr);
  ip6cpy(&dst_addr, &bd->entry->data->header.dst_addr);

  assert( ps_verify_pktbd_src_addr(bd,&src_addr) );
  assert( ps_verify_pktbd_dst_addr(bd,&dst_addr) );

  /* TODO: Check where packet is put into normalform */
  ps_load_packetSender(&senderAddr,bd->entry->data);
  ps_load_packetReceiver(&recvAddr,bd->entry->data);

  if ( lock_mem(keyBuf,crypto_stream_KEYBYTES) < 0 ) {
    fprintf(stderr,"ERROR (%s): Could not mlock memory",mainIdent);         
    return 0;
  }

  ra.sk = sk;
  ra.keyBuf = keyBuf;
  ra.logName = mainIdent;

  for(size_t i=0; i<bd->entry_count;++i) {

    ip6cpy(&bd->entry[i].data->header.src_addr,&senderAddr);
    ip6cpy(&bd->entry[i].data->header.dst_addr,&recvAddr);
       
    ps_inplace_ck_beRmac(&bd->entry[i],&ra);

    ip6cpy(&bd->entry[i].data->header.src_addr,&src_addr);
    ip6cpy(&bd->entry[i].data->header.dst_addr,&dst_addr);
 
  }

  return bd_inplace_strip(PSPKT,bd);

}       

int load_rmac_sk( const struct in6_addr* srcAddr,
                  const struct in6_addr* dstAddr,
                  uint8_t* rk ) {

  char srcKeyPath[PSD_MAX_PATH_SIZE];
  char dstKeyPath[PSD_MAX_PATH_SIZE];
  uint8_t srcKey[RMAC_KEY_SIZE];
  uint8_t dstKey[RMAC_KEY_SIZE];

  if (lock_mem(rk,RMAC_KEY_SIZE) < 0) return -2;

  errno = 0; /* TODO: mlock does not clear errno, report if still a bug */ 

  psd_mkRmacKeyPath(srcKeyPath,srcAddr);
  psd_mkRmacKeyPath(dstKeyPath,dstAddr);

  if (load_sk(srcKeyPath,srcKey) < 0) return -1;
  
  assert(errno == 0);

  if (load_sk(dstKeyPath,dstKey) < 0) return -1;

  assert(errno == 0);

  size_t n = RMAC_KEY_SIZE;
  size_t off = RMAC_KEY_SIZE/2;
  
  /* lets call that a slanted xor, a non commutative operation */
  for(size_t i = 0; i < n; ++i ) {
    rk[i] = srcKey[i] ^ dstKey[(i+off)%n];
  }
 
  clear_sk(srcKey);

  assert(errno == 0);



  clear_sk(dstKey);
 
  assert(errno == 0);

  
  return 0;

}


