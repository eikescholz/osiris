/* This is a simple program for bypassing the restriction that most Unixes
 * ignore setuid and setgid bits on interpreter scripts.
 * It does not have the common race condition problem, but requires 
 * POSIX.1-2008 or later for fstat.
 *
 * This is primary of latency reduction and easier maintainability. 
 * It adds a suid root program, which however only executes
 * open, fstat, getuid,getgid,setuid and setgid before changing the
 * effective user. Is of that is not secure, the OS is probably a lost case. 
 *
 * TODO: Support for "pipe" semantics  
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <grp.h>

#include "uninterruptableIO.h"

extern char** environ;

char* read_cmd(char* buf, char* end, char skip,char* path, 
               int argc,char** argv,
               int* pargc, char** pargv)
                {
  *pargc = 0;
  char* cur = buf;

  while ( cur < end ) {   

    if ( cur+1 < end && cur[0] == skip && isspace(cur[1]) ) {
      cur += 2;
      goto RETURN;
    }

    while ( cur < end && isspace(*cur) ) ++cur; /* skip spaces */
    
    if    ( cur+1 < end && cur[0] == '$') {
      if (! isdigit(cur[1] ) ) {  
        fprintf(stderr,"ERROR (fexec): Syntax error in (%s).\n"
                       "$ must be followed by at least one digit!\n"
               ,path); 
        exit(EXIT_FAILURE);
      }

      int i = atoi(&cur[1]);  
      if ( i < argc ) {  
        pargv[*pargc] = argv[i];
        *pargc += 1;
      }
    } else { 
      pargv[*pargc] = cur;
      *pargc += 1;
    }
 

    while ( cur < end && (! isspace(*cur) ) ) ++cur; /* skip word */
 
    *cur++ = 0; /*terminate word*/
    while ( cur < end && isspace(*cur) ) ++cur; /* skip spaces */
 
   };

RETURN:
  pargv[*pargc] = NULL; /* terminate */
  return cur;

}

int fexec_main(int argc, char** argv) {

  uid_t uid,saved_euid,script_euid;
  gid_t gid,saved_egid,script_egid;

  char* script = argv[1];
 
  if (argc < 2) {
   fprintf(stderr,
   "USAGE: As shebang first and only line in a two line \"script\":\n"
   "\n"
   "       #! fexec \n"
   "       #! prog [args] \n"
   "\n"
   "  If the setuid bit is set on the \"script\" the shebang is in, fexec\n" 
   "  will set its euid to the \"scripts\" owning user otherwise it will set\n"
   "  its euid to its real user id. The analog is done for the egid. However\n"
   "  the supplementary groups list is cleared before.\n" 
  );
   exit(EXIT_FAILURE);
  }

goto START_FEXEC; /* must put error handling before dynamic array alloc */ 

FAIL_SETEUID:
    fprintf(stderr,"ERROR (fexec): Could not seteuid (%s)!\n",strerror(errno));
    exit(EXIT_FAILURE);

FAIL_SETEGID:
    fprintf(stderr,"ERROR (fexec): Could not setegid (%s)!\n",strerror(errno));
    exit(EXIT_FAILURE);

START_FEXEC:
 
  saved_euid = geteuid();
  saved_egid = getegid();
  uid = getuid();
  gid = getgid();
 
  /* become real user */ 
  if ( seteuid(uid) < 0 ) goto FAIL_SETEUID;
  if ( setegid(gid) < 0 ) goto FAIL_SETEGID;

  /* open as real user */
  int fd = uninterruptable_open(script,O_RDONLY,0); 
  if (fd < 0) {
    fprintf(stderr,"ERROR (fexec): Could not open %s (%s)!\n",
            script,strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  struct stat st;
  int ret = fstat(fd,&st);
  if (ret < 0) {
    fprintf(stderr,"ERROR (fexec): Could not fstat open file %s (%s)!\n",
            script,strerror(errno));
    exit(EXIT_FAILURE);
  }

   /* not used anymore */
  long ngroups_max = sysconf(_SC_NGROUPS_MAX);
  gid_t group[ngroups_max];

  long ngroups = getgroups(ngroups_max, group);
  if ( ngroups < 0 ) {
    fprintf(stderr,"ERROR (fexec): Could not execute gettroups() (%s)!\n",
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  if ( st.st_uid == uid ) 
    if (st.st_mode & S_IRUSR) 
      if (st.st_mode & S_IXUSR) goto PERM_ALLOWED;

  if ( st.st_gid == gid ) 
    if (st.st_mode & S_IRGRP) 
      if (st.st_mode & S_IXGRP) goto PERM_ALLOWED;

  for (long i = 0; i < ngroups; ++i) {
    if ( st.st_gid == group[i] ) 
      if (st.st_mode & S_IRGRP) 
        if (st.st_mode & S_IXGRP) goto PERM_ALLOWED;
  }

  if (st.st_mode & S_IROTH) 
    if (st.st_mode & S_IXOTH) goto PERM_ALLOWED;

  fprintf(stderr,"\nERROR (fexec): File %s not executable for you)!\n",script);
  fflush(stderr);
  exit(EXIT_FAILURE);

PERM_ALLOWED:

  script_euid = ( (st.st_mode & S_ISUID) != 0) ? st.st_uid : uid;
  script_egid = ( (st.st_mode & S_ISGID) != 0) ? st.st_gid : gid;

  /* regain seteuid and setegid capabilities */
  if ( seteuid(saved_euid) < 0 ) goto FAIL_SETEUID;
  if ( setegid(saved_egid) < 0 ) goto FAIL_SETEGID;

  /* become effective script user */
  if ( setegid(script_egid) < 0 ) goto FAIL_SETEGID;
  if ( seteuid(script_euid) < 0 ) goto FAIL_SETEUID;

  off_t n = st.st_size;
  char buf[n+1];

  ret = read(fd,buf,n);
  if (ret < 0) {
    fprintf(stderr,"ERROR (fexec): Could not read file %s (%s)!\n",
                    script,strerror(errno)); 
    exit(EXIT_FAILURE);
  };

  close(fd);
  buf[n] = 0;

  if( buf[0] != '#' || buf[1] != '!') {
    fprintf(stderr,"ERROR (fexec): Corrupted shebang in %s!\n",script); 
    exit(EXIT_FAILURE);
  } 

  char* end = buf +n;
  char* cur = &buf[2]; 
  while( *cur != '\n') ++cur; /* skip shebang line */ 
  ++cur;
  
  if( cur[0] != '#' || buf[1] != '!') {
    fprintf(stderr,"ERROR (fexec): Corrupted fexec line in %s!\n",script); 
    exit(EXIT_FAILURE);
  } 
  cur += 2;

  while(isspace(*cur)) ++cur; /* skip spaces */

  char* cmd = cur;
  int wordCount = 0; 

  /* count words, for buffer allocation */ 
  while ( cur < end) {   
    while ( cur < end && isspace(*cur) ) ++cur; /* skip spaces */
    if    ( cur < end && (! isspace(*cur) ) ) ++wordCount; /*count word*/
    while ( cur < end && (! isspace(*cur) ) ) ++cur; /* skip word */
    while ( cur < end && isspace(*cur) ) ++cur; /* skip spaces */
  }

  int pargc = 0;
  char* pargv[wordCount+1];
  cur = read_cmd(cmd,end, 0 ,script,argc-1,&argv[1],&pargc,pargv);

#if 0 /* TODO: Pipe sematics*/ 
 
  int nargc = 0;
  char* nargv[wordCount+1];
 
  cur = read_cmd(cmd,end,'|',script,argc-1,&argv[1],&pargc,pargv);
  cur = read_cmd(cur,end, 0 ,script,argc-1,&argv[1],&nargc,nargv);

  if (nargc != 0) {
    int pfd[2];
    if (pipe(pfd) < 0) {
      fprintf(stderr,"ERROR (fexec): Could not open pipe (%s)!\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }

    if (close(1) < 0) {
      fprintf(stderr,"ERROR (fexec): Could not close stdout (%s)!\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }

    if (dup2(pfd[1],1) < 0) {
      fprintf(stderr,"ERROR (fexec): Could connect pipe to stdout (%s)!\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }
 
    if (close(pfd[1]) < 0) {
      fprintf(stderr,"ERROR (fexec): Could not close pipe write end(%s)!\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }
 
    pid_t cld = fork();
    if (cld == -1) {
      fprintf(stderr,"ERROR (fexec): Could not fork (%s)!\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }


    if (cld == 0) {
 
      /* in the child */
 
      if (close(0) < 0) {
        fprintf(stderr,"ERROR (fexec): Could not close stdout (%s)!\n",
                strerror(errno));
        exit(EXIT_FAILURE);
      }

      if (dup2(pfd[0],0) < 0) {
        fprintf(stderr,"ERROR (fexec): Could not connect pipe to stdin (%s)!\n",
               strerror(errno));
        exit(EXIT_FAILURE);
      }
   
      if (close(pfd[0]) < 0) {
        fprintf(stderr,"ERROR (fexec): Could not close pipe read end(%s)!\n",
                strerror(errno));
        exit(EXIT_FAILURE);
      }

      fprintf(stderr,"EXEC=%s\n",nargv[0]);


      if ( execve(nargv[0],nargv,environ) < 0) {

        fprintf(stderr,"ERROR (fexec): Could not execve pipe input %s (%s)!\n",
             nargv[0],strerror(errno));
        exit(EXIT_FAILURE);
      }

    } else {
      /* in the parent */
      if (close(pfd[0]) < 0) {
        fprintf(stderr,"ERROR (fexec): Could not close pipe read end(%s)!\n",
               strerror(errno));
        exit(EXIT_FAILURE);
      } 
    } 
  } 

  #endif 

  if ( execve(pargv[0],pargv,environ) < 0) {
    fprintf(stderr,"ERROR (fexec): Could not execve file %s (%s)!\n",
            pargv[0],strerror(errno));
    exit(EXIT_FAILURE);
  }

 
  return 0; /* unreachable */

}


