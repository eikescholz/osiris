#include "pscore.h" 

void ps_mk_sockName(
     char* name,
     struct in6_addr* addr,
     uint64_t port) {

     char str[NAME_MAX+1];

     ip6ntos(addr,str);

     if ( snprintf( name,NAME_MAX+1,"%sp%lX", str,port ) < 0 ) {
        fprintf(stderr,"UNLIKELY ERROR (%s): Could not create socket name for" 
                        " %s/%lX\n",ps_mainIdent,str,port);
        exit(EXIT_FAILURE);
     }


}

extern atomic_size_t curQueueBatchId;

void ps_mk_portShmbdName(
     char* name,
     MicroTime time,
     struct in6_addr* dst_addr,
     uint64_t dst_port,
     const char* suffix ) {

     char sockName[NAME_MAX+1];

     memset(name,0,NAME_MAX+1);

     ps_mk_sockName(sockName,dst_addr,dst_port);

     atomic_size_t id = atomic_fetch_add(&curBatchId,1);

     if ( snprintf( name,NAME_MAX+1,"/%lX.%s.%X.%lX%s",
                    time,sockName,getpid(),id,suffix) 
            < 0 ) {
        fprintf(stderr,"UNLIKELY ERROR (%s): Batch Could not create name"
                        "for shared memory batchdata of type %s\n",
                        ps_mainIdent,suffix);
        exit(EXIT_FAILURE);
     }

}


void ps_mk_nodeShmbdName(
     char* name,
     MicroTime time,
     struct in6_addr* addr,
     const char* suffix ) {

     char str[NAME_MAX+1];

     ip6ntos(addr,str);

     memset(name,0,NAME_MAX+1);

     atomic_size_t id = atomic_fetch_add(&curBatchId,1);


     if ( snprintf( name,NAME_MAX+1,"/%lX.%s.%X.%lX%s",
                    time,str,getpid(),id,suffix) 
            < 0 ) {
        fprintf(stderr,"UNLIKELY ERROR (%s): Batch  Could not create name"
                       "for shared memory batchdata of type %s\n",
                        ps_mainIdent,suffix);
        exit(EXIT_FAILURE);
     }

}

/* By semantics this is unique with only the batch time,
 * and its destination address and destination port */
 
void ps_mk_recvShmbdName(
     char* name,
     MicroTime time,
     struct in6_addr* dst_addr,
     uint64_t dst_port,
     const char* suffix ) {

     char sockName[NAME_MAX+1];

     memset(name,0,NAME_MAX+1);

     ps_mk_sockName(sockName,dst_addr,dst_port);

     if ( snprintf( name,NAME_MAX+1,"/%lX.%s%s",
                    time,sockName,suffix) 
            < 0 ) {
        fprintf(stderr,"UNLIKELY ERROR (%s): Batch Could not create name"
                        "for receiver shared memory batchdata of type %s\n",
                        ps_mainIdent,suffix);
        exit(EXIT_FAILURE);
     }

}




