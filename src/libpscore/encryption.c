#include "pscore.h"

struct psd_cryptinfo_t {
  size_t offset;
  unsigned char* nonce;
  unsigned char* cypher;
  unsigned char* plain;
  unsigned long long len;
  enum psd_cryptmode_e mode;
};

/* factorized pointer arithmetic 
* TODO: Cleanup interface "wrong" for in place algorithms 
*/
void psd_cryptinfo( 
    enum psd_cryptmode_e mode, 
    size_t plain_batch_psd_size,
    PSPKT_BE* cbe, /* cypher be */
    PSPKT_BE* pbe, /* plain be */
    struct psd_cryptinfo_t* ci ) {

  ci->mode = mode;


  /* we can use the same nonce twice if keys are different,
   * which they must (TODO: enforce!) be */

  ci->nonce = cbe->data->header.nonce;

  /* FIXME: Xoring the nonces with the addresses */

  switch(mode) {
    case PSD_NODECRYPTO:
      ci->offset = (char*)&cbe->data->header.src_port - (char*)cbe->data;
      ci->cypher = (unsigned char*)&cbe->data->header.src_port;
      ci->plain  = (unsigned char*)&pbe->data->header.src_port;
      break;
    case PSD_PORTCRYPTO:
      ci->offset = (char*)&cbe->data->header.deadline - (char*)cbe->data; 
      ci->cypher = (unsigned char*)&cbe->data->payload[0];
      ci->plain  = (unsigned char*)&pbe->data->payload[0];
      break;
    default: /* should not happen */
      ci->offset = 0; 
      ci->cypher = NULL;
      ci->plain  = NULL;
  }

  /* the space reserved for the rmac needs no encryption */
  ci->len   = plain_batch_psd_size - ci->offset - PSPKT_RMAC_SIZE;

}       


void fprint_psd_cryptinfo(
        FILE* stream, 
        const struct psd_cryptinfo_t* ci) {
        const char* modeStr;



  switch(ci->mode) {
    case PSD_NODECRYPTO:
      modeStr = "PSD_NODECRYPTO";
      break;
    case PSD_PORTCRYPTO:
      modeStr = "PSD_PORTCRYPTO";
      break;
    default:
      modeStr = "ERROR_UNKNOWN_CYPTOMODE";
  }

  fprintf(stream,
         "ci->mode      = %s\n"
         "ci->offset    = %zu\n"
         "ci->cypher    = %p\n"
         "ci->plain     = %p\n"
         "ci->len  = %llu\n"
         "ci->nonce     = %p\n",
         modeStr,
         ci->offset,
         ci->cypher,
         ci->plain,
         ci->len,
         ci->nonce );


}





/* this function is for both node and port decryption */
void ps_inplace_decrypt_be(
       PSPKT_BE* be,
       const struct psd_cryptarg_t* ca) { /* cypher be */ 

  struct psd_cryptinfo_t ci;

  assert(crypto_box_BEFORENMBYTES == crypto_stream_KEYBYTES);

  psd_cryptinfo(ca->mode,be->size,be,be,&ci);

  if ( crypto_stream_xor( ci.plain,
                          ci.cypher,
                          ci.len,
                          ci.nonce,
                          ca->shared_secret_key ) < 0 ) {
    char nonceStr[PS_NONCESTR_BYTES];
    sodium_bin2hex( nonceStr,PS_NONCESTR_BYTES,
                    be->data->header.nonce, PS_NONCE_BYTES );
    fprintf(stderr,"WARNING (%s): Packet lost! " 
                 "Could not decrypt psd packet:\n"
                 "  nonce: %s\n"
                 "  len: %llu\n"
                 ,ca->logName,
                 nonceStr,
                 ci.len );
    be->data = NULL; 
 
  }

}


/* this function is for both node and port decryption */
void ps_inplace_encrypt_be(
       PSPKT_BE* be, /* plain be */
       const struct psd_cryptarg_t* ca ) { 

  struct psd_cryptinfo_t ci;

  assert(crypto_box_BEFORENMBYTES == crypto_stream_KEYBYTES);

  psd_cryptinfo(ca->mode,be->size,be,be,&ci);

  if ( crypto_stream_xor( ci.cypher,
                          ci.plain,
                          ci.len,
                          ci.nonce,
                          ca->shared_secret_key ) < 0 ) {

    char nonceStr[PS_NONCESTR_BYTES];
    sodium_bin2hex( nonceStr,PS_NONCESTR_BYTES,
                    be->data->header.nonce, PS_NONCE_BYTES);
    fprintf(stderr,"WARNING (%s): Packet lost! " 
                 "Could not encrypt psd packet %s\n",ca->logName,nonceStr);
    be->data = NULL; 
  }

}


/* encrypt_bin encrypts batchdata that all are to
 * the same destination ip and from the same source.
 * It replaces the entries in bd with decrypted ones. 
 */

size_t ps_inplace_encrypt( 
        PSPKT_BD* bd, 
        enum psd_cryptmode_e mode,                       
        const unsigned char* sk,
        const struct in6_addr* src_ip,
        const char* mainIdent ) {
 
  char pkPath[PSD_MAX_PATH_SIZE];  
  PSPKT_BE* be; /* fist batchentry */
  struct in6_addr* dst_ip;
  unsigned char dsk[PSD_DSK_SIZE];
  struct psd_cryptarg_t ca;
  uint64_t dst_port,src_port;
 
  assert(bd != NULL);
  assert(bd->entry_count >= 1);

  be = bd->entry; 
  dst_ip = &be->data->header.dst_addr;

  if (! verify_pktbd_src_ip(bd,src_ip) ) {
     fprintf(stderr,"ERROR (%s): Malformed input batch. All source "
                   "IPv6 addresses must be consistent\n",mainIdent);         
    errno = EINVAL;
    return 0;
  }

  if (! verify_pktbd_dst_ip(bd,dst_ip) ) {
     fprintf(stderr,"INTERAL ERROR (%s): Batch splitting by "
                  "destionation IPv6 address failed\n",mainIdent);
    errno = EINVAL;         
    return 0;
  }

  switch(mode) {
    
    case PSD_NODECRYPTO: 
      psd_mkNodeEncryptKeyPath(pkPath,dst_ip);
      break;
    
    case PSD_PORTCRYPTO: 
      dst_port = get_uint64n(&be->data->header.dst_port);
      src_port = get_uint64n(&be->data->header.src_port);

      if (! verify_pktbd_src_port(bd,src_port) ) {
        fprintf(stderr,"ERROR (%s): Malformed input batch all source ports "
                     "must be equal\n",mainIdent);         
        errno = EINVAL;
        return 0;
      }

      if (! verify_pktbd_dst_port(bd,dst_port) ) {
        fprintf(stderr,"INTERAL ERROR (%s): Batch splitting by dst port "
                     "failed\n",mainIdent);
        errno = EINVAL;
        return 0;
      }

      psd_mkPortEncryptKeyPath(pkPath,dst_ip,dst_port);
      break;
   }
 
  if ( load_dsk(pkPath,sk,dsk) < 0 ) {
    fprintf(stderr,"ERROR (%s): could not derive shared secret key\n"
                 "from %s with euid %u (%s)",
                 mainIdent,pkPath,geteuid(),strerror(errno));         
    fflush(stderr); 
    errno = ENOENT;
    return 0;
  }

  ca.mode = mode;
  ca.shared_secret_key = dsk;
  ca.logName = mainIdent;

  for(size_t i=0; i < bd->entry_count; ++i ) {
    ps_inplace_encrypt_be(&bd->entry[i],&ca);
  }

  clear_dsk(dsk);
  
  return bd_inplace_strip(PSPKT,bd);       

}       


/* decrypt_bin decrypts batchdata that all are from
 * the same source ip and to the same destination.
 * It replaces the entries in bd with decrypted ones. 
 * 
 * returns number of sucesses
 */

size_t ps_inplace_decrypt( 
        PSPKT_BD* bd,  
        enum psd_cryptmode_e mode,
        const unsigned char* sk, 
        const struct in6_addr* dst_ip,
        const char* mainIdent ) {
 
  char pkPath[PSD_MAX_PATH_SIZE];  
  PSPKT_BE* be; /* fist batchentry */
  struct in6_addr* src_ip;
  unsigned char dsk[PSD_DSK_SIZE];
  struct psd_cryptarg_t ca;
  uint64_t dst_port,src_port;
 
  assert(bd != NULL);
  assert(bd->entry_count >= 1);

  be = bd->entry; 
  src_ip = &be->data->header.src_addr;
 
  if (! verify_pktbd_dst_ip(bd,dst_ip) ) {
     fprintf(stderr,"ERROR (%s): Malformed input batch all destination IPv6 "
                  "addresses must be consistent\n",mainIdent);         
     errno = EINVAL;
     return 0;
  }

  if (! verify_pktbd_src_ip(bd,src_ip) ) {
     fprintf(stderr,"INTERAL ERROR (%s): Batch splitting by source IPv6 "
                  "address faiuled\n",mainIdent);
    errno = EINVAL;
    return 0;
  } 
 

  switch(mode) {

    case PSD_NODECRYPTO: 
   
      psd_mkNodeEncryptKeyPath(pkPath,src_ip);
      break;

    case PSD_PORTCRYPTO: 

      dst_port = get_uint64n(&be->data->header.dst_port);
      src_port = get_uint64n(&be->data->header.src_port);

      if (! verify_pktbd_src_port(bd,src_port) ) {
        fprintf(stderr,"ERROR (%s): Malformed input batch all source ports "
                     "must be equal\n",mainIdent);
        errno = EINVAL;
        return 0;
      }

      if (! verify_pktbd_dst_port(bd,dst_port) ) {
        fprintf(stderr,"INTERAL ERROR (%s): Batch splitting by dst port "
                     "failed\n",mainIdent);
        errno = EINVAL;
        return 0;
      }

      psd_mkPortEncryptKeyPath(pkPath,src_ip,src_port);
      break;
  }
 
  if ( load_dsk(pkPath,sk,dsk) < 0 ) {
    fprintf(stderr,"ERROR (%s): could not derive shared secret key\n"
                 "from %s with euid %u (%s)",
                 mainIdent,pkPath,geteuid(),strerror(errno));         
    errno = EINVAL;
    return 0;
  }

  ca.mode = mode;
  ca.shared_secret_key = dsk;
  ca.logName = mainIdent;

  for(size_t i=0; i<bd->entry_count;++i) {
    ps_inplace_decrypt_be(&bd->entry[i],&ca);
  }

  clear_dsk(dsk);
 
  return bd_inplace_strip(PSPKT,bd);       

}       


