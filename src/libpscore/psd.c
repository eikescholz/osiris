#include "psdaemon.h"

volatile bool running = true; 

void sigterm_handler(int signum) {
  if (signum) {}; // pacify the gcc -pedantic checker
  running = false;
}

/* dt is the time interval batches are collected into
 * setting a small enough dt parallaxes everything, since it limits
 * the input batch size effectively */
MicroTime dt;

int rawsock;
size_t bufSz;
size_t bufEntryCount;

char queueName[NAME_MAX+1];

mqd_t recvQue;

pthread_attr_t threadAttr; /* used by all processes */
 
#if 0
int bdin; /* we use a pipe to sync the worker threads */
int bdout; 


pid_t queuedPid;
#endif

int psd_main(int argc, char** argv ) {
    
  ps_mainIdent = "psd";

  if ( getuid() != 0) {
    fprintf(stderr,"ERROR (psd): Must be started as real user root\n");
    exit(EXIT_FAILURE);
  }

  int sd = socket( AF_INET6, SOCK_RAW | SOCK_CLOEXEC, PS_PROTO_NUM );
  if (sd == -1) {
    perror("ERROR (psd): Could not open raw IPv6 socket \n"); 
    exit(EXIT_FAILURE);
  }

  /* Reading vars to determine message receiver message queue. 
   * Opening queue as root to be able so set appropriate size.
   */

  if(! ps_getenv_PSDINTERVAL(&ps_baseInterval) ) exit(EXIT_FAILURE);
 
  dt = ps_baseInterval;

  if (dt > 1000000) {
    fprintf(stderr,"ERROR: Invalid batch time interval (>1000000ms)\n!");
    exit(EXIT_FAILURE);
  }  

  struct mq_attr qattr;

  qattr.mq_flags   = 0;
  qattr.mq_maxmsg  = (1000000/dt); /* just a bit buffer, for 1 second*/
  qattr.mq_msgsize = sizeof(struct entriesVerifierDescriptor);
  qattr.mq_curmsgs = 0;

  memset(queueName,0,NAME_MAX+1);
  snprintf(queueName,NAME_MAX,"/psd-%u-recv-queue",getppid());

  recvQue = mq_open(queueName,O_RDWR|O_CREAT,0,&qattr);
  if ( recvQue < 0 ) {
    fprintf(stderr,"ERROR (psd): Unable to open POSIX (realtime) message "
                   "queue %s (%s)",queueName,strerror(errno) );
    return EXIT_FAILURE;
  }

  /* recvQue is accessed by forked processes, so the name is not needed */
  if ( mq_unlink(queueName) < 0 ) { 
    fprintf(stderr,"WARNING (psd): Could not unlink receiver queue (%s)",
                   strerror(errno));
  }

  rawsock = sd;

  int pktInfoOpt = 1;
  if (setsockopt(sd, IPPROTO_IPV6, IPV6_RECVPKTINFO, 
                    &pktInfoOpt, sizeof(int)) == -1 ) {
    fprintf(stderr, "Error (psd): Setting socket option" 
                    "IPV6_RECVPKTINFO failed (%s)\n",strerror(errno));
  
  }

  #ifndef NDEBUG /* debugging disables security so feature not in release*/
  if (argc < 2 || strcmp(argv[1],"--debug") != 0) {
  #endif 
  

  struct passwd* pw = getpwnam("PSDAEMON");
  if ( pw == NULL ) {
    fprintf(stderr,"ERROR (psd): Could not get uid and gid of user PSDAEMON " 
                                                      "from passwd.\n"
                   "             Failed to drop privileges.\n");
    exit(EXIT_FAILURE);
  }

  struct group* gr = getgrnam("PSDAEMON");
  if ( gr == NULL ) {
    fprintf(stderr,"ERROR (psd): Could get group entry for PSDAEMON.\n" 
                   "             Failed to drop privileges.\n");
    exit(EXIT_FAILURE);
  }

  gid_t tmp_groups;
  int ngroups = 0; 
  
  getgrouplist("PSDAEMON", gr->gr_gid, &tmp_groups, &ngroups); /* update ngroups */

  gid_t group[ngroups];
 
  if ( getgrouplist("PSDAEMON", gr->gr_gid, group, &ngroups) == -1) {
    fprintf(stderr, "ERROR (psd): Could not get group list for PSDAEMON\n"
                    "             Failed to drop privileges\n");
    exit(EXIT_FAILURE);           
  }

  if ( setgroups(ngroups,group) <  0) {
    fprintf(stderr,"ERROR (psd): Could not set groups to become PSDAEMON.\n" 
                   "Could not drop privilages.\n");
    exit(EXIT_FAILURE);
  }
 

  if ( setregid(pw->pw_gid,pw->pw_gid) <  0) {
    fprintf(stderr,"ERROR (psd): Could setreuid to become user PSDAEMON. " 
                   "Could not drop privilages.\n");
    exit(EXIT_FAILURE);
  }
 
  if ( setreuid(pw->pw_uid,pw->pw_uid) <  0) {
    fprintf(stderr,"ERROR (psd): Could setreuid to become user PSDAEMON. " 
                   "Could not drop privilages.\n");
    exit(EXIT_FAILURE);
  }

  #ifndef NDEBUG
  }
  #endif 

  /* privileges droped - unless "--debug" was used and supported */

  /* setting up basic signal handlers */

  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  
  action.sa_handler = sigterm_handler;
  
  if ( sigaction(SIGTERM, &action, NULL) == -1 ) {
    perror("ERROR (psd): Could not open raw IPv6 socket \n");
    exit(EXIT_FAILURE);
  }

  /* this is used by the verifier */ 
  if ( pthread_attr_init(&threadAttr) != 0) {
    perror("ERROR (psd): pthread_attr_init() failed\n");
    exit(EXIT_FAILURE);
  }

  if( pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED) != 0) {
    perror("ERROR (psd): pthread_attr_setdetachstate\n");
    exit(EXIT_FAILURE);
  }
 
  /* Verifier runs as DAEMON is only required to read public keys */
  pid_t verifierPid = fork();
  if (verifierPid == (pid_t)-1) {
    perror("ERROR (psd): Could not fork verification process!\n");
    exit(EXIT_FAILURE);
  }

  if ( verifierPid == 0 ) { /* as child */
     int ret = psd_verify_process_main(dt);
     return ret;
  };



  /* core init must be done after forking the verifier. Which will not use
   * a sigchld handler */

  if ( ps_core_init("psd") < 0 ) exit(EXIT_FAILURE);


#if 0
  if ( pipe(pipefd) < 0) {
    perror("ERROR (psd): Could not create batch pipe\n");
    exit(EXIT_FAILURE);
  }
 
  bdin  = pipefd[0];
  bdout = pipefd[1];

  if ( fcntl(bdin,F_SETFD, O_CLOEXEC) < 0) {
    perror("ERROR (psd): Could not set O_CLOEXEC on batch pipe read end\n");
    exit(EXIT_FAILURE);
  }
 
  if ( fcntl(bdout,F_SETFD, O_CLOEXEC) < 0) {
    perror("ERROR (psd): Could not set O_CLOEXEC on batch pipe write end\n");
    exit(EXIT_FAILURE);
  }
#endif

  int workerCount = sysconf(_SC_NPROCESSORS_ONLN);
  if ( workerCount ==  -1 ) {
    perror("ERROR (psd): Could not get number of active processors end\n");
    exit(EXIT_FAILURE);
  }

  /* TODO: Make this processes, the reason for using threads is gone */ 
  pthread_t worker[workerCount];

  if( ip6bind(sd,&in6addr_any) == -1) {
    fprintf(stderr,"ERROR (psd): Binding socket failed (%s)\n",
                   strerror(errno));
    exit(EXIT_FAILURE); 
  }

 
  /* TODO: Optimize thread cheduler parameters */

  /* Setting up buffers: 
   * TODO: use enviroment for setting up the buffer, (or kernel info?)
   *       and Link layer overhead can be included too ...
   *  assume we have 8Gbit input lines that 1gigabyte per second 
   *  a packet has at least 169 bytes (header + signature + rmac)
   *  then there are at most 1000000000/169 packets on the imput per second
   *  
   *  Much too big interval is dt not a second. 
   */

  size_t max_bit_bandwidth = 1000000000l; /*default 8 1Gbit conncetions*/
  size_t max_byte_bandwidth = (max_bit_bandwidth+7)/8; /* rounded up */
  size_t max_packet_bandwidth = (max_byte_bandwidth+PSPKT_MIN_SIZE-1)
                                  / PSPKT_MIN_SIZE; /* rounded up */

  size_t intervals_per_second = 1000000/dt; /* rounded down */
 
  bufEntryCount = max_packet_bandwidth/intervals_per_second;
  bufSz = max_byte_bandwidth/intervals_per_second; /* rounded up */

  /*
  printf("Network max bandwidth: %zd \n",max_bit_bandwidth);
  printf("Network max packet bandwidth: %zu \n",max_packet_bandwidth);
  printf("Using input buffer 0f %zu entries and %zu bytes\n",
                  bufEntryCount,bufSz);
*/
  /* A buffer of recvSz is never full if max_bit_bandwidth is a valid
     upper bound, batches are completely fetched in dt */

  if (setsockopt(sd, SOL_SOCKET, SO_RCVBUF, &bufSz, sizeof(bufSz)) == -1) {
    fprintf(stderr, "ERROR (psd): Error setting socket receive buffer size: "
                    "%s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }


  workerCount = workerCount/8 ; /* TODO: determine good division factor */  

  for(int i = 0; i < workerCount; ++i) {
    if ( pthread_create(&worker[i],&threadAttr,recv_batch,NULL) != 0 ) {
      fprintf(stderr,"WARNING (psd): "
                     "Could not create worker %d thread (%s)! \n",
                     i,strerror(errno));
      exit(EXIT_FAILURE);
    } 
  }

  recv_batch(NULL);

  for(int i =0; i < workerCount; ++i ) {
    pthread_kill(worker[i],SIGINT);
  }

  if ( kill(verifierPid, SIGTERM) < 0 ) {
    perror("WARNING (psd): Could send SIGTERM to verifier process\n");
  }

  waitpid(verifierPid,NULL,0);

  return 0; 

}


