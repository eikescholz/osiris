#include "psdaemon.h"
#include <sys/mman.h>


void* recv_batch(void* unused ) {

  unused = unused; /* pacify compiler warning */

  MicroTime cur_time,next_time; 
  ssize_t sz;
  PSPKT pkt;
  char buf[PS_MAX_IP6PACKET_SIZE];
  char cmsgBuf[CMSGBUF_SIZE];
  char psrawreceive[PSD_MAX_PATH_SIZE];

  psd_mkRawReceivePath(psrawreceive);

  memset(cmsgBuf,0,CMSGBUF_SIZE);

  struct sockaddr_in6 src_sockaddr;
  struct in6_addr* src_addr = &src_sockaddr.sin6_addr;

  struct iovec iov[1];
  iov[0].iov_base=buf;
  iov[0].iov_len=sizeof(buf);

  struct msghdr message;
  message.msg_name=&src_sockaddr;
  message.msg_namelen=sizeof(src_addr);
  message.msg_iov=iov;
  message.msg_iovlen=1;
  message.msg_control=cmsgBuf;
  message.msg_controllen=CMSGBUF_SIZE;

  PSPKT_BD* bd  = new_batchdata(PSPKT,bufEntryCount,0);

  bd->stack = mmap(NULL,bufSz,PROT_READ|PROT_WRITE,
                  MAP_ANONYMOUS|MAP_PRIVATE,-1,0);
  if (bd->stack == MAP_FAILED) {
    perror("ERROR (psd): Unable to mmap packt buffer!");
    exit(EXIT_FAILURE);
  }
  bd->stack_capacity = bufSz;

  madvise(bd->stack,bufSz,MADV_DONTFORK);

  cur_time = (microtime(NULL)/dt)*dt; /* round initial to multiples of dt */ 
  
  next_time = cur_time + dt;
  while( running ) {
 
    if ( microtime(&cur_time) < next_time+dt ) {       
 
      sz = recvmsg(rawsock,&message,MSG_PEEK); /* wait for data */ 
      if (sz < 0) {
        if (errno == EINTR) continue;
        perror("UNLIKELY ERROR (psd): Ceased to be able to peek packets!");
        break; /*TODO: restart ?*/

      }

    };

    /* TODO: Stress analysis with warning if under too much load */

    cur_time = (cur_time/dt)*dt; /* round to multiples of dt */ 
    next_time = cur_time + dt;

    do { /* busy polling of packets until next_time elapesed */

      sz = recvmsg(rawsock,&message,MSG_DONTWAIT );
      if (sz < 0) {
        if (errno == EAGAIN ) continue;
        if (errno == EWOULDBLOCK) continue;
        if (errno == EINTR ) continue;
        perror("UNLIKELY ERROR (psd): Ceased to be able receive packets!");
        exit(EXIT_FAILURE);
      }

      /* next get the dst_addr  ... */
      struct in6_addr* dst_addr = NULL;
      struct cmsghdr *cmsg;
      struct in6_pktinfo* info; 
    
      for ( cmsg = CMSG_FIRSTHDR(&message); 
            cmsg != NULL;
            cmsg = CMSG_NXTHDR(&message, cmsg)) {
        if ( cmsg->cmsg_level == IPPROTO_IPV6 
             && cmsg->cmsg_type == IPV6_PKTINFO) {
         info = (struct in6_pktinfo *) CMSG_DATA(cmsg);
         dst_addr = &info->ipi6_addr;
        }      
      }
    
      if (dst_addr == NULL) {
         fprintf(stderr,"WARNING (psd): Packet Dropped! "
                      " Could not get packet destination address!\n");
        continue; 
      }

      set_uint32n(&pkt.header.vcf, PSPKT_NORMALFORM_VCF);
      set_uint16n(&pkt.header.packetSize, sz + IP6_HEADER_SIZE);  
      pkt.header.nextHeader = PS_PROTO_NUM;
      pkt.header.hopLimit = 0;  
      ip6cpy(&pkt.header.src_addr,src_addr);
      ip6cpy(&pkt.header.dst_addr,dst_addr);
      memcpy(&pkt.header.nonce,&buf,sz);

      if (sz + 2*sizeof(struct in6_addr) > PS_MAX_IP6PACKET_SIZE ) {
       /* should be unreachable */
        fprintf(stderr,"WARNING (psd): Packet Dropped "
                     "(Bigger than validly possible)!\n");
        continue; }
    
      if (sz + 2*sizeof(struct in6_addr) < PSPKT_MIN_SIZE ) {
        fprintf(stderr,"WARNING (psd): Packet Dropped! " 
                       "(Smaller then validly possible)!\n");
        continue; }
 
      new_batchentry(PSPKT,bd,&pkt,ps_packetSize(&pkt));

    } while ( microtime(NULL) < next_time ) ; 
    
    if (bd->entry_count == 0) continue; /* everything dropped */

    PSPKT_BD* readyBD = deep_copy_batchdata(PSPKT,bd);
    if (readyBD == NULL )  {
      fprintf(stderr,"ERROR (psd): Batch dropped. Out of memory! \n");
      continue;
    }

    char rawreceiveBatchName[NAME_MAX+1];
    ps_mk_nodeShmbdName( rawreceiveBatchName,
                         cur_time,
                         src_addr,
                         ".rawrecv.bd" );

    ps_fork_exec(psrawreceive,rawreceiveBatchName,(BD*)readyBD,NULL,NULL);
 
    delete_batchdata(PSPKT,readyBD);

    /* reset buffer bd */
    bd->entry_count = 0;
    bd->stack_size = 0;
 
  } /* closes while(running) */
  
  bd->stack = NULL;
  delete_batchdata(PSPKT,bd);

  return NULL;

}


