/* This program gets a batch of psd packets form stdin
 * and creates the corresponding route macs.
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

/* assumes that input packets to be encrypted are from the same source */
int psnmkrmac_main(int arg, char** argv) {
  char sendPath[PSD_MAX_PATH_SIZE];
  char srcStr[INET6_ADDRSTRLEN];
  char dstStr[INET6_ADDRSTRLEN];
  uint8_t sk[RMAC_KEY_SIZE];
  struct in6_addr* src_addr;
  struct in6_addr* dst_addr;
  int ret = EXIT_SUCCESS;
  size_t n;

  if ( ps_service_init("psnmkrmac",arg,argv) < 0) exit(EXIT_FAILURE);
 
  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) exit(EXIT_FAILURE); 
 
  PSPKT_BD** nodeBins = ps_alloc_and_split(bd,pspktBeSrcDstIP6Cmp,NULL);
  if (nodeBins == NULL) exit(EXIT_FAILURE); 
  
  for ( PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; ++curNodeBin ){
    
    dst_addr = &(*curNodeBin)->entry->data->header.dst_addr;
    src_addr = &(*curNodeBin)->entry->data->header.src_addr;
    
    if ( load_rmac_sk(src_addr,dst_addr,sk) < 0 ) {
      ip6ntop(src_addr,srcStr);
      ip6ntop(dst_addr,dstStr);
      fprintf(stderr, "ERROR (psnmkrmac): Batch Dropped. "
                      "Route keys for route from "
                      "%s to %s incomplete (%s))\n",
                      srcStr,dstStr,strerror(errno));
      fprintf(stderr,"             euid: %u uid: %u egid: %u gid: %u\n",
                     geteuid(),getuid(),getegid(),getgid() );
 
      delete_batchdata(PSPKT,*curNodeBin);  
      continue;  
    }
 
    n = (*curNodeBin)->entry_count;

    if ( ps_inplace_mk_rmac(*curNodeBin,sk,ps_mainIdent) != n ) {
        /* Fails if a single verification fails */ 
        ret = EXIT_FAILURE;
    }  
 
    clear_sk(sk);
 
    psd_mkRawSendPath(sendPath);

    char rawSendBatchName[NAME_MAX+1];
    ps_mk_nodeShmbdName( rawSendBatchName,
                         ps_inputBatchTime,
                         src_addr,
                         ".rawsend.bd" );

    ps_fork_exec(sendPath,rawSendBatchName,(BD*)*curNodeBin,NULL,NULL);
 
    delete_batchdata(PSPKT,*curNodeBin);      
  }
 
  free(nodeBins);

  delete_batchdata(PSPKT,bd);
 
  return ret; 

}

