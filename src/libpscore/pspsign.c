/* This program gets a batch of psd packets form stdin
 * and signs them, i.e. updates the footers. 
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

int pspsign_main(int argc, char** argv) {
  char portEncryptPath[PSD_MAX_PATH_SIZE];
  uint8_t sk[crypto_sign_SECRETKEYBYTES];
  struct in6_addr src_addr;
  uint64_t src_port;

  if ( ps_service_init("pspsign",argc,argv) < 0 ) exit(EXIT_FAILURE);
 
  if (! ps_getenv_PSSRCIP(&src_addr) ) exit(EXIT_FAILURE);

  if (! ps_getenv_PSSRCPORT(&src_port) ) exit(EXIT_FAILURE);
   
  if ( ps_load_auth_sk(sk,&src_addr,src_port) < 0 ) {
    exit(EXIT_FAILURE);
  }

  if ( ps_drop_privileges() < 0) goto FAIL;
 
  /* The node private key file should not be readable now */ 
 
  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) goto FAIL;
 
  assert( ps_verify_pktbd_src_addr(bd,&src_addr) );
  assert( ps_verify_pktbd_src_port(bd,src_port) );

  ps_inplace_normalize_and_sign(bd,sk,ps_mainIdent);

  clear_auth_sk(sk);

  psd_mkPortEncryptPath(portEncryptPath,&src_addr,src_port);

  char pencryptBatchName[NAME_MAX+1];
  ps_mk_portShmbdName( pencryptBatchName,
                       ps_inputBatchTime,
                       &src_addr,
                       src_port,
                       ".penc.bd" );

  ps_fork_exec(portEncryptPath,pencryptBatchName,(BD*)bd,NULL,NULL);
 
  delete_batchdata(PSPKT,bd);

  return EXIT_SUCCESS; /* nothing could be encrypted */

FAIL:

  clear_auth_sk(sk);

  return EXIT_FAILURE;

}

