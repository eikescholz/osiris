#include "pscore.h"

  /* This sorts the input batch into time bins and stores this
   * into the deadline directory. (A tmpfs memdisk prefearably )
   *
   *
   */
/* The batch for time n*dt consists of packets, so that for each
 * packets deadline t0 in the bin the it holds that 
 * given by n so that t0 is element of ((n-1)*dt,n*dt].
 *
 */
 

int ps_deadlineQueue_store( 
         PSPKT_BD* bd,
         const char* deadlineDirPath, 
         IP6Addr* dst_addr,
         uint64_t dst_port
         ) {
  
  size_t binCount;
  MicroTime deadline,pktDeadline,dt,curTime;
  char deadlinePath[PSD_MAX_PATH_SIZE]; 
  char deadlineEntryPath[PSD_MAX_PATH_SIZE]; 
  char deadlineDataPath[PSD_MAX_PATH_SIZE];  

  assert( ps_verify_pktbd_dst_addr(bd,dst_addr) );
  assert( ps_verify_pktbd_dst_port(bd,dst_port) );

  dt = ps_baseInterval;

  PSPKT_BD** timeBin = ps_alloc_and_split( bd,
                                           pspktBeDeadlineIntervalCmp,
                                           &binCount );
  if (timeBin == NULL) return -1;

  for ( size_t i =0; i < binCount; ++i ) {

    /* batch queue deadlines must be rounded to multiples of dt */
    pktDeadline = get_int64n(&timeBin[i]->entry->data->header.deadline);

    deadline = ((pktDeadline+dt-1)/dt)*dt;

#ifndef NDEBUG
    for ( size_t j = 0; j < timeBin[i]->entry_count; ++j ) {
       MicroTime d0 = get_int64n(&timeBin[i]->entry[j].data->header.deadline);
       assert( d0 > deadline - dt);
       assert( d0 <= deadline ); 
    }
#endif

    assert( deadline % dt == 0 );

    psd_mkDeadlinePath(deadlinePath,deadlineDirPath,deadline);
    
    psd_mkDeadlineEntryPath(deadlineEntryPath,deadlineDirPath,
                                deadline,dst_addr,dst_port);
   
    /* if we are precision close to the deadline we don't bother and
     * assume IPC is too slow, to make it*/

    if ( microtime(&curTime) + microtime_precision() < deadline ) {
   
      if ( mkdir(deadlinePath,0770) < 0 && errno != EEXIST ) {
        fprintf(stderr,"ERROR (%s): Could not create deadline queue path %s "
                       "for verification (%s). Packets lost!\n",
                       ps_mainIdent,deadlinePath,strerror(errno));              
      } else {

        if ( mkdir(deadlineEntryPath,0770) < 0 && errno != EEXIST ) {
          fprintf(stderr,"ERROR (%s): Could not create deadline entry path %s "
                         "for verification (%s). Packets lost!\n",
                       ps_mainIdent,deadlineEntryPath,strerror(errno)); 
        } else {

          atomic_size_t id = atomic_fetch_add(&curBatchId,1);

          if ( snprintf( deadlineDataPath, PSD_MAX_PATH_SIZE, 
                         "%s/%lX.%X.%lX.bd",
                         deadlineEntryPath, curTime, getpid(), id )  < 0 ) {
            fprintf(stderr,"UNLIKELY ERROR (%s): Could not create name"
                            "for entry data of %s\n",
                            ps_mainIdent,deadlineEntryPath);
            continue; /* try next */
          }

          if ( store_batchfile(PSPKT,deadlineDataPath,timeBin[i]) < 0 ) {
            fprintf(stderr,"ERROR (%s): Could not store batch %s "
                           "for verification (%s). Packets lost!\n",
                       ps_mainIdent,deadlineDataPath,strerror(errno));              
 

          }
        }
      }
    } else {
      fprintf(stderr,"WARNING (%s): Deadline for %s passed. Packets lost!\n",
                     ps_mainIdent,deadlineEntryPath );
    }
    delete_batchdata(PSPKT,timeBin[i]);
  }
 
  free(timeBin);

  return 0;

}



int ps_deadlineQueue_load_entry(
            DQE* dqe,
            const char* deadlineDirPath,
            MicroTime deadline,
            IP6Addr* addr,
            uint64_t port
          ) {

  size_t ret = -1;
  struct dirent* de;
  char entryPath[PSD_MAX_PATH_SIZE];
  char partPath[PSD_MAX_PATH_SIZE];

  dqe->deadline = deadline;
  ip6cpy(&dqe->addr,addr);
  dqe->port = port;

  psd_mkDeadlineEntryPath(entryPath,deadlineDirPath,deadline,addr,port);
 
  DIR* dir = opendir(entryPath);
  if (dir == NULL) {
    fprintf(stderr,"WARNING (%s): Could not open socket deadline directory " 
                   " %s (%s). All of its packets are lost !\n",
                   ps_mainIdent, entryPath,strerror(errno));
    return -1; 
  }

  size_t entCount = 0;
  for (de = readdir(dir); de != NULL; de = readdir(dir) ) {
    if (strcmp(de->d_name,".") == 0) continue;
    if (strcmp(de->d_name,"..") == 0) continue;
    entCount++;
  }

  PSPKT_BD** part = malloc((entCount+1)*sizeof(*part));
  if (part == NULL) {
     fprintf(stderr,"WARNING (%s): Out of memory."
                    " Deadline Entry %s with all entries ignored.\n",
                    ps_mainIdent, entryPath );
     goto CLEANUP0;
  }

  size_t i = 0;
 
  rewinddir(dir);
  for (de = readdir(dir); de != NULL; de = readdir(dir) ) {

    if (strcmp(de->d_name,".") == 0) continue;
    if (strcmp(de->d_name,"..") == 0) continue;
 
    vstpcpy(partPath,entryPath,"/",de->d_name,NULL);
    part[i] = load_batchfile(PSPKT,partPath);
    
    if (part[i] == NULL) {
      fprintf(stderr,"WARNING (%s): Could not load batch part %s (%s)."
                     " Packets of this part are lost!\n",
                      ps_mainIdent, partPath, strerror(errno) );
     
    } else {
      i++;
    }

 
  }
 
  entCount = i;
  
  part[entCount] = NULL;

  dqe->bd = ps_merge_and_free(part);
  if (dqe->bd == NULL ) {
    fprintf(stderr,"WARNING (%s): Could not merge batchdata of %s (%s)."
                   " All packets lost!\n",
                   ps_mainIdent,entryPath, strerror(errno) );
    goto CLEANUP1;
  }

  ret  = 0; /* success ! */
  part = NULL;

CLEANUP1: 

  free(part);

CLEANUP0:

  closedir(dir);

  return ret;

}



DQE_BD* ps_deadlineQueue_load( 
           const char* deadlineDirPath,
           MicroTime deadline
         ) {
 
  char addrBuf[NAME_MAX+1];
  char deadlinePath[PSD_MAX_PATH_SIZE];
  char *portName, *addrName;
  IP6Addr addr;
  uint64_t port;
  DQE dqe;
  struct dirent* dirEnt;
  DIR* dir;

  psd_mkDeadlinePath(deadlinePath,deadlineDirPath,deadline);
 
  dir = opendir(deadlinePath);
  if (dir == NULL) {
    fprintf(stderr,"ERROR (%s): Could not open deadline directory %s (%s)."
                   " All of its batches are lost !\n",
                   ps_mainIdent,deadlinePath,strerror(errno));
    return NULL;
  }

  char* end;
 
  size_t entCount = 0;
  for (dirEnt = readdir(dir); dirEnt != NULL; dirEnt = readdir(dir) ) {
    entCount++;
  }

  rewinddir(dir);

  DQE_BD* ret = new_batchdata(DQE,entCount,entCount*sizeof(DQE));
  if (ret == NULL) {
    closedir(dir);
    return NULL;
  }

  for (dirEnt = readdir(dir); dirEnt != NULL; dirEnt = readdir(dir) ) {

    if (strcmp(dirEnt->d_name,".") == 0) continue;
    if (strcmp(dirEnt->d_name,"..") == 0) continue;

    strcpy( addrBuf, dirEnt->d_name);
    addrName = addrBuf;
    portName = addrBuf;
    while ( *portName != 0 && *portName != 'p' ) portName++;
    
    if ( *portName != 'p') goto IGNORE_MALFORMED;
    *portName++ = 0;

    port = strtoll(portName,&end,16);
    if (*end != 0 ) goto IGNORE_MALFORMED;
   
    if ( ip6pton( addrName,&addr) < 0) goto IGNORE_MALFORMED; 

    if ( ps_deadlineQueue_load_entry(&dqe,deadlineDirPath,deadline,&addr,port)
           < 0 ) {
      fprintf(stderr, "WARNING(%s): Could not load deadline %s (%s)\n",
                       ps_mainIdent,deadlinePath,strerror(errno));       
      continue;
    }
 
    new_batchentry(DQE,ret,&dqe,sizeof(DQE));

    continue;
  
  IGNORE_MALFORMED:
    fprintf( stderr,
             "WARNING (%s): Malformed deadline entry %s/%lX/%s ignored.\n",
              ps_mainIdent,deadlineDirPath,deadline,dirEnt->d_name );
    
  }

  closedir(dir);

  return ret;

}

void delete_DQE_BD(DQE_BD* dqe) {
  for (size_t i=0; i < dqe->entry_count; ++i ) {
    delete_batchdata(PSPKT,dqe->entry[i].data->bd);
  }
  delete_batchdata(DQE,dqe);
} 

 


int ps_parallel_process_deadlineEntries(
        const char* deadlineDirPath,
        MicroTime deadline,
        DeadlineEntryHandler entry_main,
        char* handlerName ) {

  char addrStr[INET6_ADDRSTRLEN];
  DQE_BD* dqe;
  size_t i;
  uint64_t port;

  dqe = ps_deadlineQueue_load(deadlineDirPath,deadline);
  if (dqe == NULL) return -1;

  pid_t* pid = ps_malloc(dqe->entry_count*sizeof(*pid));
  if (pid == NULL) {
    for ( i=0; i < dqe->entry_count; ++i ) {
      delete_batchdata(PSPKT,dqe->entry[i].data->bd);
    }
    delete_batchdata(DQE,dqe);;
    return -1;
  }

  size_t j = 0;
  for ( i=0; i < dqe->entry_count; ++i ) {
    pid[j] = fork();
    if (pid[j] == -1) {
      ip6ntos(&dqe->entry[j].data->addr,addrStr);
      port = dqe->entry[j].data->port;
      fprintf(stderr,"ERROR (%s): Could not fork deadline %s "
                     " handler (%s). Deadline entry %s/%lX/%sp%lX "
                     " packets lost.",
                     ps_mainIdent,handlerName,strerror(errno),
                     deadlineDirPath,deadline,addrStr,port );
      continue; 
    }
    
    if ( pid[j] == 0 ) {
      int ret = entry_main(deadlineDirPath,dqe->entry[j].data);
      exit(ret);
    }

    ++j;  

  }

  for ( i=0; i < j ; ++i ) {

    pid_t cld;
    int wstatus;

    do { 
      cld = waitpid(pid[i],&wstatus,0);
    } while( cld == -1 && errno==EINTR );

    if (cld == -1) {
      ip6ntos(&dqe->entry[i].data->addr,addrStr);
      port = dqe->entry[i].data->port;
      fprintf(stderr,"WARNING (%s): Waitpid for termination of deadline"
                     " %s handler for entry %s/%lX/%sp%lX failed (%s)! "
                     "Port synchronisation not guaranteed!\n", 
                     ps_mainIdent,handlerName,
                     deadlineDirPath,deadline,addrStr,port, strerror(errno));
    } else {
      ip6ntop(&dqe->entry[i].data->addr,addrStr);
      port = dqe->entry[i].data->port;
      if (! WIFEXITED(wstatus) ) {
        fprintf(stderr,"WARNING (%s): Deadline %s handler for %s/%lX/%sp%lX " 
                       " terminated abnormally. ",
                       ps_mainIdent,handlerName,
                       deadlineDirPath,deadline, addrStr, port );
        if ( WIFSIGNALED(wstatus) ) {
          fprintf(stderr,"It was killed by signal %d\n", WTERMSIG(wstatus));
        } else if ( WIFSTOPPED(wstatus) ) {
          fprintf(stderr,"It was stopped by signal %d\n", WSTOPSIG(wstatus));
        } else if ( WIFCONTINUED(wstatus) ) {
          fprintf(stderr,"It continued\n");
        }
        #ifdef WCOREDUMP
          else if ( WCOREDUMP(wstatus) ) {
          fprintf(stderr,"It core dumped\n");
        }
        #endif 
        fprintf(stderr,"Port synchronisation not guaranteed!\n"); 

      }                
    
    }
  
  }
 
  free(pid);
  delete_DQE_BD(dqe);

  return EXIT_SUCCESS; 

} 

int ps_deadlineQueue_unlink(const char* inPath, MicroTime deadline) {

  char deadlinePath[PSD_MAX_PATH_SIZE];
  char deadlineEntryPath[PSD_MAX_PATH_SIZE];
  char deadlineEntryPartPath[PSD_MAX_PATH_SIZE];
  DIR *ddir, *edir;
  struct dirent *dde, *ede;

  psd_mkDeadlinePath(deadlinePath,inPath,deadline);
 
  ddir = opendir(deadlinePath);
  if (ddir == NULL) {
    fprintf(stderr,"ERROR (%s): Could not open deadline directory %s for "
                   "cleanup (%s). Possible memory leak.",
                   ps_mainIdent,deadlinePath,strerror(errno));
    return -1;
  }

  for (dde = readdir(ddir); dde != NULL; dde = readdir(ddir) ) {
    
    if (strcmp(dde->d_name,".") == 0) continue;
    if (strcmp(dde->d_name,"..") == 0) continue; 
 
    vstpcpy(deadlineEntryPath,deadlinePath,"/",dde->d_name,NULL);

    edir = opendir(deadlineEntryPath);
    if (edir == NULL) {
      fprintf(stderr,"WARNING (%s): Could not open deadline entry directory %s " 
                   " for cleanup (%s). System leaks memory!\n",
                   ps_mainIdent, deadlineEntryPath,strerror(errno));
     continue; 
    }

    for (ede = readdir(edir); ede != NULL; ede = readdir(edir) ) {
     
      if (strcmp(ede->d_name,".") == 0) continue;
      if (strcmp(ede->d_name,"..") == 0) continue; 
     
      vstpcpy(deadlineEntryPartPath,deadlineEntryPath,"/",ede->d_name,NULL); 
     
      if ( unlink(deadlineEntryPartPath) < 0 ) {
        fprintf(stderr,"WARNING (%s): Could not unlink %s (%s). System leaks "
                       "memory\n",
                       ps_mainIdent,deadlineEntryPartPath, strerror(errno) );
      }
    }

    closedir(edir);
  
    if ( rmdir(deadlineEntryPath) < 0 ) {
      fprintf(stderr,"WARNING (%s): Could not remove %s (%s). System leaks "
                     "memory \n",
                      ps_mainIdent,deadlineEntryPath, strerror(errno) );
    }
  
  }

  closedir(ddir);
 
  if ( rmdir(deadlinePath) < 0 ) {
    fprintf(stderr,"WARNING (%s): Could not remove %s (%s). System leaks "
                   "memory \n",
                   ps_mainIdent,deadlinePath, strerror(errno) );
  }

  return 0;
}

