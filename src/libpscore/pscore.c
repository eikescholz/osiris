/* The library to the header files in the include dir
 *
 * TODO: Cleanup headers, putting all unnecessarily inlined functions here
 */

#include<sodium.h>
#include<signal.h>
#include<unistd.h>
#include<stdio.h> 
#include<sys/types.h>
#include<sys/wait.h>
#include<string.h>

#include "pscore.h"


const char* ps_mainIdent = "LIBPSCORE-UNINITIALIZED";

char* ps_inputName = NULL;
char* ps_proofInputName = NULL;

ShmBd ps_inputShmbd;
ShmBd ps_proofInputShmbd;

MicroTime ps_inputBatchTime = 0;
atomic_size_t curBatchId = 0;
MicroTime ps_baseInterval;



/* services will not wait for children */
void ps_sigchld_handler(int signum) {
  if (signum) {}; // pacify the gcc -pedantic checker
  int child_status = 0;
  while( waitpid((pid_t)-1,&child_status,WNOHANG) > 0 );  
}




int ps_core_init(const char* mainIdent) {

  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = ps_sigchld_handler;
  if ( sigaction(SIGCHLD, &action, NULL) != 0 ) {
    fprintf(stderr,"ERROR (%s): Could install ps_sigchld_handler\n",mainIdent);
    return -1;
  }

  if (sodium_init() < 0) {
    fprintf(stderr,"ERROR (%s): Could not initialize libsodium\n",mainIdent);
    return -1;
  }  
 
  ps_mainIdent = mainIdent;

  if (! ps_getenv_PSDINTERVAL(&ps_baseInterval) ) {
    fprintf(stderr,"ERROR (%s): Could not read environment PSDINTERVAL (%s)\n",
                    mainIdent,strerror(errno));
  }
 
  return 0;
}




int ps_service_init(const char* mainIdent,int argc,char** argv) {
      
  if ( ps_core_init(mainIdent) < 0 ) return -1;
  
  if (argc != 2) {
    fprintf(stderr,"ERROR (%s): A persistent socket service must be called "
                   "with exactly one shared memory batchdata name argument.\n",
                   mainIdent);
    exit(EXIT_FAILURE);
  }

  ps_inputName = argv[1];

  return 0;
 
}


int ps_receiver_init(const char* mainIdent,int argc,char** argv) {
 
  if (argc != 3) {
    fprintf(stderr,"ERROR (%s): A persistent socket receiver must be called "
                   "with exactly two shared memory batchdata name arguments.",
                   mainIdent);
    exit(EXIT_FAILURE);
  }

  /* do *not* call core init since the receiver should not install a child 
   * handler */

  if (sodium_init() < 0) {
    fprintf(stderr,"ERROR (%s): Could not initialize libsodium\n",mainIdent);
    return -1;
  }  
 
  ps_mainIdent = mainIdent;

  if (! ps_getenv_PSDINTERVAL(&ps_baseInterval) ) {
    fprintf(stderr,"ERROR (%s): Could not read environment PSDINTERVAL (%s)\n",
                    mainIdent,strerror(errno));
 
    return -1;
  }

  ps_inputName = argv[1];

  ps_proofInputName = argv[2];

  return 0;
 
}




