#include "pscore.h"

int ps_send_psd_batchdata(
        const PSPKT_BD* bd, 
        int rawsock,
        const char* mainIdent) { 
  int ret = EXIT_SUCCESS;
  PSPKT_BE* curEnt; 
  char tmpstr[INET6_ADDRSTRLEN]; /* used for error reporting */ 

  if (bd->entry_count == 0) {
    fprintf(stderr,"Warning (%s): Sending empty batch, i.e. nothing!\n",
                    mainIdent);
  }

  for(size_t i = 0; i < bd->entry_count; ++i) {
  
    curEnt = &bd->entry[i];
    if ( bd->entry[i].size < PSPKT_MIN_SIZE ) {
      fprintf(stderr,"ERROR (%s): Paket Dropped " 
                "(batch entry too small for valid psd header)\n",mainIdent);
      continue; 
    }
 
    if ( ip6bind(rawsock,&curEnt->data->header.src_addr)  < 0 ) {
      fprintf(stderr, "ERROR (%s): Packet Dropped. " 
               "Could not bind socket source address %s\n (%s) "
              ,mainIdent,ip6ntop(&curEnt->data->header.src_addr,tmpstr),
               strerror(errno));
      continue;
    }  

    if ( ip6sendto( rawsock, 
                    &curEnt->data->header.nonce,
                    ps_packetSize(curEnt->data) - IP6_HEADER_SIZE,
                    0, 
                    &curEnt->data->header.dst_addr) 
            < 0   ) {
      ret = EXIT_FAILURE;
      fprintf(stderr,"ERROR (%s): Packet Dropped (%s)\n",
              mainIdent,strerror(errno));
    }

  }

  return ret;

}



