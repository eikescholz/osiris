/* This program gets a batch of psd packets form stdin
 * and verifies the sources, and puts it into the
 * deadline queue for verification. 
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>
#include <sys/stat.h>

size_t dt;



int pspverify_main(int argc, char** argv) {

  IP6Addr dst_addr;
  uint64_t dst_port;
  PSPKT_BD* bd;
  char inPath[PSD_MAX_PATH_SIZE];

  umask(0007);

  if ( ps_service_init("pspverify",argc,argv) < 0 ) exit(EXIT_FAILURE); 

  if (! ps_getenv_PSDSTIP(&dst_addr) ) exit(EXIT_FAILURE);;
  
  if (! ps_getenv_PSDSTPORT(&dst_port) ) exit(EXIT_FAILURE);;
    
  bd = ps_read_serviceInput();
  if (bd == NULL) exit(EXIT_FAILURE); 
 
  psd_mkIncomingPath(inPath);

  if ( ps_deadlineQueue_store(bd,inPath,&dst_addr,dst_port) < 0 ) {
    exit(EXIT_FAILURE);
  }
 
  delete_batchdata(PSPKT,bd);

  return EXIT_SUCCESS;
}

