/* This program gets a batch of psd packets form stdin
 * and applies the node decryption and patch executes
 * the socket decryption programs. 
 *
 * This program should be setuid to psdnode$N.psdio$N
 * user.group and executed as real user psdio$N.psdio$N.
 * See DESIGN file. 
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

int psndecrypt_main(int argc, char** argv) {
  char psdPortDecrypt[PSD_MAX_PATH_SIZE];
  uint8_t sk[PSD_SK_SIZE];
  uint64_t dst_port;
  IP6Addr *src_addr, dst_addr;

  if ( ps_service_init("psndecrypt",argc,argv) < 0) exit(EXIT_FAILURE);
 
  if (! ps_getenv_PSDSTIP(&dst_addr) ) exit(EXIT_FAILURE);
  
  if ( ps_load_node_sk(sk,&dst_addr) < 0 ) goto FAIL;

  if ( ps_drop_privileges() < 0) goto FAIL;
  
  /* The node private key file should not be readable now */ 
 
  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) goto FAIL; 

  if (! ps_verify_pktbd_dst_addr(bd,&dst_addr) ) goto FAIL;

  PSPKT_BD** nodeBins = ps_alloc_and_split(bd,pspktBeSrcIP6Cmp,NULL);
  if (nodeBins == NULL) goto FAIL; 
  
  for(PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; curNodeBin++ ) { 
    ps_inplace_decrypt( *curNodeBin,
                        PSD_NODECRYPTO,
                        sk,
                        &dst_addr,
                        ps_mainIdent ); 
  }
 
  clear_sk(sk);

  for(PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; curNodeBin++ ) {

    PSPKT_BD** sockBins = ps_alloc_and_split( *curNodeBin,
                                               pspktBeDstPortCmp, NULL);
    if (sockBins == NULL) continue;
       
    for(PSPKT_BD** curSockBin = sockBins; *curSockBin != NULL; curSockBin++) {

      dst_port = get_uint64n(&(*curSockBin)->entry->data->header.dst_port); 
      src_addr = &(*curSockBin)->entry->data->header.src_addr;

      if ( ps_setenv_PSDSTPORT( dst_port ) < 0 ) continue; 
        
      if ( ps_setenv_PSSRCIP( src_addr ) < 0 ) continue; 
 
      psd_mkPortDecryptPath( psdPortDecrypt, &dst_addr, dst_port );

      char pdecryptName[NAME_MAX+1];
      ps_mk_portShmbdName( pdecryptName,
                           ps_inputBatchTime,
                           &dst_addr,
                           dst_port,
                           ".pdec.bd"
                           );

      ps_fork_exec(psdPortDecrypt,pdecryptName,(BD*)*curSockBin,NULL,NULL);
 
      delete_batchdata(PSPKT,*curSockBin);
    
    }

    free(sockBins);
    delete_batchdata(PSPKT,*curNodeBin);
  
  } 
    
  free(nodeBins);

  delete_batchdata(PSPKT,bd);

  return EXIT_SUCCESS;
  
FAIL:
  
  clear_sk(sk);

  return EXIT_FAILURE;

}

