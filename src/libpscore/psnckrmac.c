/* This program gets a batch of psd packets form stdin
 * and checks if the applied rmacs are valid. 
 *
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

int psnckrmac_main(int argc, char** argv ) {
  char psdNodeDecrypt[PSD_MAX_PATH_SIZE];
  uint8_t sk[RMAC_KEY_SIZE];
  IP6Addr *dst_addr, *src_addr,receiverAddr;
  char srcStr[INET6_ADDRSTRLEN], dstStr[INET6_ADDRSTRLEN];
  int ret = EXIT_SUCCESS;
  size_t n;

  if ( ps_service_init("psnckrmac",argc,argv) < 0 ) exit(EXIT_FAILURE);
 
  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) exit(EXIT_FAILURE); 

  PSPKT_BD** nodeBins = ps_alloc_and_split(bd,pspktBeSrcDstIP6Cmp,NULL);
  
  if (nodeBins == NULL) exit(EXIT_FAILURE); 

/* TODO: routing */

  for(PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; curNodeBin++ ) { 
      
      src_addr = &(*curNodeBin)->entry->data->header.src_addr;
      dst_addr = &(*curNodeBin)->entry->data->header.dst_addr;

      if ( load_rmac_sk(src_addr,dst_addr,sk) < 0 ) {

        ip6ntop(src_addr,srcStr);
        ip6ntop(dst_addr,dstStr);
 
        fprintf(stderr,"ERROR (psnckrmac): Batch Dropped. "
                       "Route keys for route from "
                       "%s to %s incomplete (%s))\n",
                       srcStr,dstStr,strerror(errno));
        fprintf(stderr,"             euid: %u uid: %u egid: %u gid: %u\n",
                        geteuid(),getuid(),getegid(),getgid());
 
        delete_batchdata(PSPKT,*curNodeBin);  
        continue;  
      }

      n = (*curNodeBin)->entry_count;

      if ( ps_inplace_ck_rmac(*curNodeBin,sk,ps_mainIdent) != n ) {
        /* Fails if a single verification fails */ 
        ret = EXIT_FAILURE;
      }  
     
      clear_sk(sk);

      ps_load_clusterAddr(&receiverAddr,dst_addr);

      if ( ip6cmp(&receiverAddr,dst_addr) != 0 ) {
        /* destination was not a cluster but is part of a cluster */
        assert(! "unimplemeted" ); /* TODO: replicate */
      }

      psd_mkNodeDecryptPath( psdNodeDecrypt, dst_addr);

      char ndecryptBatchName[NAME_MAX+1];
      ps_mk_nodeShmbdName( ndecryptBatchName,
                           ps_inputBatchTime,
                           dst_addr,
                           ".ndec.bd" );

      ps_fork_exec( psdNodeDecrypt,
                    ndecryptBatchName,
                    (BD*)*curNodeBin,NULL,NULL);
        
      delete_batchdata(PSPKT,*curNodeBin);
 
  }

  free(nodeBins);

  delete_batchdata(PSPKT,bd);

  return ret;
  
}

