#include "pscore.h"
#include "psdaemon.h"

int dispatch_deadlineEntry( const char* deadlineDirPath,
                            DQE* de ) {
   
   deadlineDirPath = deadlineDirPath; /* check unused pacification */

   char receivePath[PSD_MAX_PATH_SIZE];
   char recvName[NAME_MAX+1];
   char proofName[NAME_MAX+1];

   if ( ps_setenv_PSDSTIP(&de->addr) < 0 ) return EXIT_FAILURE;
   if ( ps_setenv_PSDSTPORT(de->port) < 0 ) return EXIT_FAILURE;

   psd_mkPortReceivePath(receivePath,&de->addr,de->port);
   
   ps_mk_recvShmbdName(recvName,de->deadline,&de->addr,de->port, 
                       ".psd.bd");
   ps_mk_recvShmbdName(proofName,de->deadline,&de->addr,de->port,
                       ".psd.proof.bd");

   char *const argv[4] = {receivePath,recvName,proofName,NULL};  

   ps_execve(argv[0],argv,environ); /* calls exit on failure */
        
   /* unreachable */
  
   return EXIT_FAILURE;
}

volatile bool dispatcherBusy = true;

void* psd_dispatcher_thread(void* arg) {

  char *deadlineDirPath = arg;
  char name[NAME_MAX+1];
  pid_t cleanerPid,prevDispPid;
  struct mq_attr attr;
  struct entriesVerifierDescriptor msg;
  MicroTime prevDispDeadline,prevCleanedUpDeadline;

  attr.mq_flags = 0;
  attr.mq_maxmsg = 0;
  attr.mq_msgsize = 0;
  attr.mq_curmsgs = 0;

  prevDispPid = -1; /* starting, no last pid */
  cleanerPid = -1;

  while ( running && dispatcherBusy ) {

    /* this blocks until a queued deadline verification desc arrives */
    if ( mq_receive( recvQue, (void*)&msg,sizeof(msg), 0 ) < 0 ) {
      if (errno == EINTR) continue;
      fprintf(stderr,"UNLIKELY ERROR (%s): Receiver message queue"
                     "stopped working! (%s). All pending batches lost!\n",
                     ps_mainIdent, strerror(errno));
      return NULL;
    }

    /* TODO: replace global sync for receiving with a per sock sync 
     *       with O(1) memory complexity regarding the number of socks
     *       i.e. (Sync structure has probable be a file in 
     *       the socket directory */      
     
    snprintf(name,NAME_MAX,"verify %lX deadline entries",msg.deadline);

    ps_waitpid(msg.entriesVerifierPid,name); 

    /* if there is a last receiver wait until it finished. */
    if ( prevDispPid != -1 ) {
 
      snprintf(name,NAME_MAX,"receive %lX deadline entries",prevDispDeadline );

      ps_waitpid(prevDispPid,name);

      if (cleanerPid != -1 ) {
        snprintf(name,NAME_MAX,"cleanup %lX deadline entries",
                      prevCleanedUpDeadline );
        ps_waitpid(cleanerPid,name);
      }
 
      cleanerPid = fork(); 
      if (cleanerPid == -1 ) {
        fprintf(stderr,"WARNING (%s): Could fork cleanup processes"
                       "for deadline %lX (%s). Memory Leaked.\n", 
                     ps_mainIdent, prevDispDeadline, strerror(errno) );
      }
     
      if ( cleanerPid == 0) {
         ps_mainIdent = "psd local dispatch cleanup process";
         exit( ps_deadlineQueue_unlink(deadlineDirPath,prevDispDeadline));
      }
 
      prevCleanedUpDeadline = prevDispDeadline;

       
    } /* last receiver finished, start the last receiver for the next iter */

    prevDispDeadline = msg.deadline;
    prevDispPid = fork(); /* create a new receiver pid to wait for */
    if (prevDispPid== -1 ) {
      fprintf(stderr,"WARNING (%s): Could fork receive processes"
                     "for deadline %lX (%s). All batches for this "
                     "deadline lost\n", 
                     ps_mainIdent, msg.deadline,strerror(errno) );
    }
    
    /* due to the above waitpid "barrier", the receive processes are 
     * executed strictly only after all prior receivers finished. */
 
    if ( prevDispPid == 0) {
         int ret = ps_parallel_process_deadlineEntries(
                         deadlineDirPath,
                         msg.deadline,
                         dispatch_deadlineEntry,
                         "receive");
         exit(ret);
    }
 
    if ( mq_getattr(recvQue,&attr) < 0 ) {
      fprintf(stderr,"WARNING (%s): mq_getattr on receiver"
                    "message queue failed (%s) - Assuming empty.\n"
                    ,ps_mainIdent, strerror(errno));
      attr.mq_curmsgs = 0;
    } 
    
    if ( attr.mq_curmsgs != 0) {
      dispatcherBusy = true; 
    } else {
      dispatcherBusy = false || running;
    }
    
  }

  return NULL;
 
}


