/* This program gets a batch of psd packets form stdin
 * and applies the porst decryption and patch executes
 * the socket receiver program
 *
 */

#include "pscore.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sodium.h>

int pspdecrypt_main(int argc, char** argv) {

  char portVerifyPath[PSD_MAX_PATH_SIZE];
  uint8_t sk[PSD_SK_SIZE];
  IP6Addr dst_addr, *src_addr;
  uint64_t dst_port,src_port;

  if ( ps_service_init("pspdecrypt",argc,argv) < 0 ) exit(EXIT_FAILURE);

  if (! ps_getenv_PSDSTIP(&dst_addr ) ) exit(EXIT_FAILURE);;
  
  if (! ps_getenv_PSDSTPORT(&dst_port) ) exit(EXIT_FAILURE);;
  
  if ( ps_load_port_sk(sk,&dst_addr,dst_port ) < 0 ) {
    exit(EXIT_FAILURE);
  }

  if ( ps_drop_privileges() < 0) goto FAIL;
  
  /* The node private key file should not be readable now */ 

  PSPKT_BD* bd = ps_read_serviceInput();
  if (bd == NULL) goto FAIL; 

  if (! ps_verify_pktbd_dst_addr(bd,&dst_addr) ) goto  FAIL;
  
  if (! ps_verify_pktbd_dst_port(bd,dst_port) ) goto FAIL;
  
  PSPKT_BD** nodeBins = ps_alloc_and_split(bd,pspktBeSrcCmp,NULL); 
  
  for(PSPKT_BD** curNodeBin = nodeBins; *curNodeBin != NULL; curNodeBin++ ) { 
    ps_inplace_decrypt( *curNodeBin,
                        PSD_PORTCRYPTO, 
                        sk,
                        &dst_addr,
                        ps_mainIdent ); 
  }
 
  clear_sk(sk);

  for (PSPKT_BD** cur = nodeBins; *cur != NULL; ++cur ) {
   
    src_port = get_uint64n(&(*cur)->entry->data->header.src_port); 
    src_addr = &(*cur)->entry->data->header.src_addr;

    if ( ps_setenv_PSSRCPORT(src_port) < 0 ) continue; 
      
    if ( ps_setenv_PSSRCIP(src_addr) < 0 ) continue; 
      
    psd_mkPortVerifyPath(portVerifyPath,&dst_addr,dst_port);


    char verifyBatchName[NAME_MAX+1];
    ps_mk_portShmbdName( verifyBatchName,
                         ps_inputBatchTime,
                         &dst_addr,
                         dst_port,
                         ".verify.bd" );

    ps_fork_exec(portVerifyPath,verifyBatchName,(BD*)*cur,NULL,NULL);
 
    delete_batchdata(PSPKT,*cur);
    
  }  

  free(nodeBins);
  
  delete_batchdata(PSPKT,bd);

  return EXIT_SUCCESS;
 
FAIL:
  
  clear_sk(sk);

  return EXIT_FAILURE;
}

