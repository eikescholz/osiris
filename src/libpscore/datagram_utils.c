#include "pscore.h"


void fprint_hash(FILE*strm, const uint8_t* data) {
  fprintf(strm,"  hash : ");
  for(size_t i=0; i< 32;i++) {
    fprintf(strm,"%02X",data[i]); 
  } 
  fprintf(strm,"\n         ");
  for(size_t i=32; i< 64;i++) {
    fprintf(strm,"%02X",data[i]); 
  }
  fprintf(strm,"\n");
}




/* this is for debugging */ 
void fprint_ps(
      FILE* strm,
      const PS* ps ) {

  char addr[INET6_ADDRSTRLEN];
  uint64_t port;

  ip6ntos( &ps->addr, addr ); 

  port = get_uint64n( & ps->port );

  fprintf(strm,"%sp%lX",addr,port);

}


/* this is for debugging */ 
void fprint_psd_header(
      FILE* strm,
      const PS_D* psd) {

  char fromAddr[INET6_ADDRSTRLEN];
  char toAddr[INET6_ADDRSTRLEN];

  PSPKT* pkt = (PSPKT*)psd;

  if ( pkt == NULL ) {
    fprintf(strm,"ERROR (fprint_psd): Invalid Argument\n"); 
    return;
  }

  size_t pktSize = ps_packetSize(pkt); 

/*  uint8_t  type = pkt->header.type; */

  if (pktSize < PSPKT_MIN_SIZE ) {
    printf("  CORRUPTED: Too small to be a psd\n");
    return;
  }
  
  if ( get_uint32n(&pkt->header.vcf) !=  PSPKT_NORMALFORM_VCF ) {
    fprintf(strm,"  vcf  : %08X (Not in normalform!)\n",
                    get_uint32n(&pkt->header.vcf));
  } 

  fprintf(strm,"  size : %lu\n",pktSize);

  if ( pkt->header.nextHeader != PS_PROTO_NUM ) {
    fprintf(strm,"  nxt  : %d (Invalid!)\n",pkt->header.nextHeader);
  }

  if ( pkt->header.hopLimit != 0 ) {
     fprintf(strm,"  hlim : %d (Not in normalform!)\n",
                   pkt->header.hopLimit);
  }

  uint64_t src_port = get_uint64n(&pkt->header.src_port);
  uint64_t dst_port = get_uint64n(&pkt->header.dst_port);

  ip6ntos(&pkt->header.src_addr,fromAddr);
  ip6ntos(&pkt->header.dst_addr,toAddr);


  fprintf(strm,"  from : %s/%lX\n",fromAddr,src_port);
  fprintf(strm,"  to   : %s/%lX\n",toAddr,dst_port);

/*  fprintf(strm,"  type : %0X \n",type); */

  fprintf(strm,"  nonce: ");
  for (size_t i = 0; i < PS_NONCE_BYTES; ++i ) 
  printf("%02X",pkt->header.nonce[i]); 
  printf("\n");

  int64_t mpid = get_int64n(&pkt->header.mpid);
  int64_t deadline = get_int64n(&pkt->header.deadline);

  fprintf(strm,"  ddln : %lX\n",deadline); 
  fprintf(strm,"  mpid : %lX\n",mpid);


}


void fprint_signature( 
       FILE* strm,
       const PSS* sign ) {

  char addrStr[INET6_ADDRSTRLEN];

  fprintf(strm,"  sign : ");
  for(size_t i=0; i< 32;i++) {
    fprintf(strm,"%02X",sign->data[i]); 
  } 
  fprintf(strm,"\n         ");
  for(size_t i=32; i< 64;i++) {
    fprintf(strm,"%02X",sign->data[i]); 
  }

  ip6ntos(&sign->addr,addrStr);

  fprintf(strm,"\n         %s\n",addrStr);


}


/* this is for debugging */ 
void fprint_psd(
      FILE* strm,
      const PS_D* psd) {

  PSPKT* pkt = (PSPKT*)psd;

  if ( pkt == NULL ) {
    fprintf(strm,"ERROR (fprint_psd): Invalid Argument\n"); 
    return;
  }

  fprint_psd_header(strm,psd);

  fprintf(strm,"  data : ");

  size_t pktSize = ps_packetSize(pkt); 

  size_t z = pktSize - PSD_HEADER_SIZE - PSPKT_FOOTER_SIZE;
  size_t l;
  for(l=0; l < z/32; l++) { 
    for(size_t i=0; i < 32;i++) {
      fprintf(strm,"%02X",pkt->payload[32*l+i]); 
    }
    fprintf(strm,"\n         ");
  }

  for(size_t i=0; i < z%32;i++) {
    fprintf(strm,"%02X",pkt->payload[32*l+i]); 
  }
  fprintf(strm,"\n");

}

void fprint_pspkt(
      FILE* strm,
      const PSPKT* pkt) {
  
  if ( pkt == NULL ) {
    fprintf(strm,"ERROR (fprint_psd): Invalid Argument\n"); 
    return;
  }

  size_t pktSize = ps_packetSize(pkt); 


  fprint_psd(strm,ps_const_packetDatagram(pkt));

  fprint_signature(strm,&ps_const_packetFooter(pkt)->signature);

  unsigned char* rmac = (unsigned char*)pkt + pktSize - PSPKT_RMAC_SIZE; 

  fprintf(strm,"  rmac : ");
  for(size_t i=0; i< 16;i++) {
    fprintf(strm,"%02X",rmac[i]); 
  }

  fprintf(strm,"\n");

}


void fprint_proof(
      FILE* strm,
      const PSPRF* prf ) {

  uint64_t i,n;

  n = get_uint64n(&prf->signatureCount); 
  
  fprintf( strm, "  proof signature count: %zu\n", n );

  fprint_hash( strm, prf->hash);

  for ( i=0 ; i < n; ++i ) {
    fprint_signature(strm,&prf->signature[i]);
  } 


}


/* for datagram the storages size is smaller then the packet size
 * so the following routine can be used check if container entries
 * that have their one entry size bookkeeping, like batchdatas
 * store packets or datagrams */

bool is_complete_pspkt(void* data, size_t dataSize) {
  PSPKT* pkt = data;
  if (get_uint16n(&pkt->header.packetSize) == dataSize ) {
    return true;
  }
  return false;
}


void fprint_pspkt_entry (
      FILE* strm,
      PSPKT* pkt,
      size_t entrySize ) {

  if ( is_complete_pspkt( pkt,entrySize ) ) {
    fprint_pspkt(strm,pkt);
    fprintf(strm,"\n");
  } else {
    fprintf(strm,"  INVALID! Incomplete packet! Maybe a datagram?\n");
  }
  
}
 
void fprint_psd_entry (
      FILE* strm,
      PS_D* psd,
      size_t entrySize ) {

  if (! is_complete_pspkt( (PSPKT*)psd,entrySize ) ) {
    fprint_psd(strm,psd);
    fprintf(strm,"\n");
  } else {
    fprintf(strm,
            "  INVALID! Datagram with footer! Maybe a complete packet?\n");
  }
  
}
 

void fprint_pspkt_bd(
      FILE* strm,
      const PSPKT_BD* bd) {

  for (size_t i = 0; i < bd->entry_count; ++i ) {
    fprintf(strm,"Entry %lu/%lu (%lu/%lu):\n",
                 i+1,bd->entry_count,bd->entry[i].size,bd->data_size );
    fprint_pspkt_entry(strm,(PSPKT*)bd->entry[i].data,bd->entry[i].size );
  }
  
}
 




PSPKT_BE* new_pspkt_batchentry( 
              PSPKT_BD* bd,
              IP6Addr* srcIp,
              IP6Addr* dstIp,
/*              uint8_t type, */
              psd_nonce_t* nonce,
              uint64_t mpid,
              uint64_t deadline,
              uint64_t srcPort,
              uint64_t dstPort,
              uint8_t* payload,
              size_t payloadBytes ) {

  size_t size = payloadBytes + PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE;
 
  if (size > PSPKT_MAX_SIZE ) {
    errno = E2BIG;
    return NULL;
  };  


  PSPKT_BE* be = new_batchentry(PSPKT,bd,NULL,size); 
  if (be == NULL) return NULL;

  set_uint32n(&be->data->header.vcf,PSPKT_NORMALFORM_VCF);
  set_uint16n(&be->data->header.packetSize,size);
  be->data->header.nextHeader = PS_PROTO_NUM;
  be->data->header.hopLimit = 0;  

  ip6cpy(&be->data->header.src_addr,srcIp);
  ip6cpy(&be->data->header.dst_addr,dstIp);
  memcpy(&be->data->header.nonce,nonce,sizeof(psd_nonce_t));

/*  be->data->header.type = type; */

  set_int64n(  &be->data->header.mpid, mpid);
  set_int64n(  &be->data->header.deadline, deadline); 
  set_uint64n( &be->data->header.src_port, srcPort);
  set_uint64n( &be->data->header.dst_port, dstPort);

  memcpy(be->data->payload,payload,payloadBytes);

  memset(be->data->payload+payloadBytes,0,PSPKT_FOOTER_SIZE);

  return be;

}



PSPKT_BE* 
    new_random_pspkt_batchentry(
        PSPKT_BD* bd,
        unsigned char* seed, 
        IP6Addr* srcIp,
        IP6Addr* dstIp,
        uint64_t srcPort,
        uint64_t dstPort,
        size_t payloadBytes) {

  psd_nonce_t nonce;
  uint64_t mpid;

  pseudo_randombytes(&nonce,PS_NONCE_BYTES,seed);
  inc_seed(seed);

  pseudo_randombytes(&mpid,sizeof(mpid),seed);       
  inc_seed(seed);  
 
  uint8_t payload[payloadBytes];

  pseudo_randombytes(&payload,payloadBytes,seed);       
  inc_seed(seed);

  return new_pspkt_batchentry( bd,
                               srcIp,
                               dstIp,
                               &nonce,
                               mpid,
                               microtime(NULL),
                               srcPort,
                               dstPort,
                               payload,
                               payloadBytes );


}





int pspktBeSrcPortCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  uint64_t a_port = get_uint64n(&a->data->header.src_port);
  uint64_t b_port = get_uint64n(&b->data->header.src_port);
  
  if (a_port < b_port) return -1;
  if (a_port > b_port) return 1;
  return 0;

}

int psdBeSrcPortCmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeSrcPortCmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}





int pspktBeDstPortCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  uint64_t a_port = get_uint64n(&a->data->header.dst_port);
  uint64_t b_port = get_uint64n(&b->data->header.dst_port);
  
  if (a_port < b_port) return -1;
  if (a_port > b_port) return 1;
  return 0;

}

int psdBeDstPortCmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeDstPortCmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}







/* TODO: Clean up name: This compares nonce + mpid + time stamp */
int pspktBeNonceCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  int r =  memcmp( &a->data->header.nonce,
                   &b->data->header.nonce,
                   32 );

  if (r > 0) return 1;
  if (r < 0) return -1; 
  return 0;

}

int psdBeNonceCmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeNonceCmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}







int pspktBeSrcIP6Cmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  int r = memcmp( &a->data->header.src_addr,
                  &b->data->header.src_addr,
                  sizeof(a->data->header.src_addr) );
  
  if (r > 0) return 1;
  if (r < 0) return -1; 
  return 0;

}

int psdBeSrcIP6Cmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeSrcIP6Cmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}






int pspktBeDstIP6Cmp(const PSPKT_BE* a,const PSPKT_BE* b) {
 
  int r = memcmp( &a->data->header.dst_addr,
                  &b->data->header.dst_addr,
                  sizeof(a->data->header.dst_addr) );

  if (r > 0) return 1;
  if (r < 0) return -1; 
  return 0;


}

int psdBeDstIP6Cmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeDstIP6Cmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}






int pspktBeSrcDstIP6Cmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  int r = pspktBeSrcIP6Cmp(a,b);
  if ( r == 0) return pspktBeDstIP6Cmp(a,b);  
  return r;

}

int psdBeSrcDstIP6Cmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeSrcDstIP6Cmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}






int pspktBeSrcCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  int r = pspktBeSrcIP6Cmp(a,b);
  if ( r == 0) return pspktBeSrcPortCmp(a,b);  
  return r;

}

int psdBeSrcCmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeSrcCmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}





int pspktBeDstCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  int r = pspktBeDstIP6Cmp(a,b);
  if ( r == 0) return pspktBeDstPortCmp(a,b);  
  return r;

}

int psdBeDstCmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeDstCmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}





int pspktBeSrcDstCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  int r = pspktBeSrcIP6Cmp(a,b);
  if ( r == 0 ) {
    r = pspktBeDstIP6Cmp(a,b);
    if ( r == 0 ) { 
      r = pspktBeSrcPortCmp(a,b);
      if ( r == 0 ) {
        r = pspktBeDstPortCmp(a,b);
      }
    }
  }  

  return r;

}

int psdBeSrcDstCmp(const PS_D_BE* a,const PS_D_BE* b) {

  return pspktBeSrcDstCmp((PSPKT_BE*)a,(PSPKT_BE*)b);

}


/* The batch for time n*dt consists of packets, so that for each
 * packets deadline t0 in the bin the it holds that 
 * given by n so that t0 is element of ((n-1)*dt,n*dt].
 *
 * so for 0 <= epsilon < dt and a packet with deadline t0
 * we look for n so that
 * 
 * t0 = n*dt - epsilon
 * 
 * thus
 *
 * t0 / dt = n
 *
 * and the packets batchtime is (t0/dt)*dt
 * where all divisions round down. 
 *
 */


int ps_baseIntervalTimeCmp(const MicroTime* a, const MicroTime* b ) {

  size_t dt = (size_t)ps_baseInterval;

  assert(dt != 0);

  size_t a_bt = (( *a + dt-1 ) / dt ) * dt;
  size_t b_bt = (( *b + dt-1 ) / dt ) * dt;

  if (a_bt > b_bt) return 1;
  if (a_bt < b_bt) return -1; 
  return 0;

}


int pspktBeDeadlineIntervalCmp(const PSPKT_BE* a,const PSPKT_BE* b) {

  MicroTime s,t;
 
  s = get_int64n(&a->data->header.deadline);
  t = get_int64n(&b->data->header.deadline);
 
  return ps_baseIntervalTimeCmp(&s,&t);
      
}



/* the lookup optimized packet oder in blocks
 * Lexicographically order by
 *
 * It is assumed that the payload is prefiexed with an uid. 
 * So that deadline + uid give an key with log(entry_count) access key
 *
 *  Deadline
 *  payload,
 *  src_addr,
 *  src_port,
 *  dst_addr,
 *  dst_port,
 *  nonce,    
 *  mpid,
 *
 * TODO ? : make order configurable per socket
 * 
 *
 */


int ps_psd_cmp(const PS_D* a,const PS_D* b) {

  int r;

  size_t a_t = get_int64n( &a->header.deadline );
  size_t b_t = get_int64n( &b->header.deadline );

  if (a_t < b_t) return -1;
  if (a_t > b_t) return 1;
 
  uint16_t a_sz = get_int64n( &a->header.deadline );
  uint16_t b_sz = get_int64n( &b->header.deadline );

  if ( a_sz < b_sz ) return -1;
  if ( a_sz > b_sz ) return  1;

  r = lxcmp( &a->payload, &b->payload, a_sz );
 
  if ( r != 0 ) return r;

  r = ip6cmp( &a->header.src_addr, &b->header.src_addr );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;

  uint64_t a_src_port = get_uint64n( &a->header.src_port );
  uint64_t b_src_port = get_uint64n( &b->header.src_port );

  if ( a_src_port < b_src_port ) return -1;
  if ( a_src_port > b_src_port ) return  1;

  r = ip6cmp( &a->header.dst_addr, &b->header.dst_addr );

  if ( r < 0 ) return -1;
  if ( r > 0 ) return  1;

  uint64_t a_dst_port = get_uint64n( &a->header.dst_port );
  uint64_t b_dst_port = get_uint64n( &b->header.dst_port );

  if ( a_dst_port < b_dst_port ) return -1;
  if ( a_dst_port > b_dst_port ) return  1;

  r = lxcmp( &a->header.nonce, &b->header.nonce, PS_NONCE_BYTES );
 
  if ( r != 0) return r; 
                  
  int64_t a_mpid = get_int64n( &a->header.mpid );
  int64_t b_mpid = get_int64n( &b->header.mpid );

  if ( a_mpid < b_mpid ) return -1;
  if ( a_mpid > b_mpid ) return 1;

  return 0;

}


int ps_pspktBe_cmp(const PSPKT_BE *a, const PSPKT_BE *b ) {
  const PS_D *x = (PS_D*)a->data;
  const PS_D *y = (PS_D*)b->data;
  return ps_psd_cmp( x, y );
}




int vpsdBe_cmp(const VPSD_BE *a, const VPSD_BE *b ) {
  return ps_psd_cmp(a->data->datagram,b->data->datagram);
}


bool verify_pktbd_dst_ip(
  const PSPKT_BD* bd,
  const IP6Addr* r) {

  const PSPKT_BE* be;

  assert( r != NULL );
  assert( bd != NULL );

  for (size_t i=0; i< bd->entry_count; ++i) {
    be = &bd->entry[i];
    if (ip6cmp(r,&be->data->header.dst_addr) != 0) return false;
  }

  return true;

}       




bool verify_pktbd_src_ip(
  const PSPKT_BD* bd,
  const IP6Addr* r) {

  const PSPKT_BE* be;

  assert( r != NULL );
  assert( bd != NULL );

  for (size_t i=0; i< bd->entry_count; ++i) {
    be = &bd->entry[i];
    if (ip6cmp(r,&be->data->header.src_addr) != 0) return false;
  }

  return true;

}       




bool verify_pktbd_src_port(
  const PSPKT_BD* bd,
  uint64_t port) {

  const PSPKT_BE* be;

  assert( bd != NULL );


  for (size_t i=0; i< bd->entry_count; ++i) {
    be = &bd->entry[i];
    if (port != get_uint64n(&be->data->header.src_port) ) return false;
  }

  return true;

}       


bool verify_pktbd_dst_port(
  const PSPKT_BD* bd,
  uint64_t port) {

  const PSPKT_BE* be;

  assert( bd != NULL );


  for (size_t i=0; i< bd->entry_count; ++i) {
    be = &bd->entry[i];
    if (port != get_uint64n(&be->data->header.dst_port) ) return false;
  }

  return true;

}       


bool ps_verify_pktbd_dst_addr(
        const PSPKT_BD* bd,
        const IP6Addr* dst_addr ) {

  char buf[INET6_ADDRSTRLEN];
 
  if (! verify_pktbd_dst_ip(bd,dst_addr) ) {
     ip6ntop(dst_addr,buf);
     fprintf(stderr,"ERROR (%s): Malformed input batch all destination "
                    "IPv6 addresses must match %s \n",ps_mainIdent,buf);  
    return false;
  }

  return true;

}

bool ps_verify_psdbd_dst_addr(
  const PS_D_BD* bd,
  const IP6Addr* r) {

  return ps_verify_pktbd_dst_addr((PSPKT_BD*)bd,r);

}



bool ps_verify_pktbd_src_addr(
        const PSPKT_BD* bd,
        const IP6Addr* src_addr ) {

  char buf[INET6_ADDRSTRLEN];
 
  if (! verify_pktbd_src_ip(bd,src_addr) ) {
     ip6ntop(src_addr,buf);
     fprintf(stderr,"ERROR (%s): Malformed input batch all source "
                    "IPv6 addresses must match %s \n",ps_mainIdent,buf);  
    return false;
  }

  return true;

}



bool ps_verify_psdbd_src_addr(
  const PS_D_BD* bd,
  const IP6Addr* r) {

  return ps_verify_pktbd_src_addr((PSPKT_BD*)bd,r);

}



bool ps_verify_pktbd_src_port(
        const PSPKT_BD* bd,
        const uint64_t src_port ) {

  if (! verify_pktbd_src_port(bd,src_port) ) {
     fprintf(stderr,"ERROR (%s): Malformed input batch all source "
                    "ports addresses must match %lu \n",ps_mainIdent,src_port);  
    return false;
  }

  return true;

}


bool ps_verify_psdbd_src_port(
  const PS_D_BD* bd,
  uint64_t port) {

  return ps_verify_pktbd_src_port((PSPKT_BD*)bd,port);

}



bool ps_verify_pktbd_dst_port(
        const PSPKT_BD* bd,
        const uint64_t dst_port ) {

  if (! verify_pktbd_dst_port(bd,dst_port) ) {
     fprintf(stderr,"ERROR (%s): Malformed input batch all destination "
                    "ports addresses must match %lu \n",ps_mainIdent,dst_port);  
    return false;
  }

  return true;

}


bool ps_verify_psdbd_dst_port(
  const PS_D_BD* bd,
  uint64_t port) {

  return ps_verify_pktbd_dst_port((PSPKT_BD*)bd,port);

}




int ps_mk_packetDatagramHash(uint8_t* hash, const PSPKT* pkt) {

    size_t psdSize = ps_packetDatagramSize(pkt);
    const uint8_t* psd = (const uint8_t*)ps_const_packetDatagram(pkt); 

    if ( crypto_generichash( hash, HASH_SIZE, psd, psdSize, NULL, 0 )
                  < 0 ) {
      /* hash functions should not fail */
      fprintf(stderr,"WARNING (%s): Could not compute packet hash. "
                        "The following packet is lost:\n",ps_mainIdent); 
      fprint_pspkt(stderr,pkt); 
      return -1; 
    }
    
    return 0;
}

