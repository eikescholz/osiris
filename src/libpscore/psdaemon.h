#ifndef OSIRIS_PSD_H
#define OSIRIS_PSD_H

#include "microtime.h"
#include "pscore.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <netinet/in.h> 
#include <stdatomic.h>
#include <linux/sched.h>
#include <sys/prctl.h>
#include <fcntl.h>
#include <mqueue.h>



/* ??? To get in6_pktinfo defined in RFC 3542 */ 
#ifndef  __USE_GNU 
struct in6_pktinfo
  {
    struct in6_addr ipi6_addr;/* src/dst IPv6 address */
    unsigned int ipi6_ifindex;/* send/recv interface index */
  };
#endif

/* TODO: Error msgs with pid and timestamps to create proper
 * /var/log files by redirecting stderr 
 *
 */

#define CMSGBUF_SIZE (16*1024) /* big enough ? */


void* recv_batch(void* unused); /* receiver thread */
void* process_batch(void* unused);
void psd_verify_queued_main(MicroTime dt);

extern char queueName[NAME_MAX+1];
mqd_t recvQue;



/* dt is the time interval batches are collected into
 * setting a small enough dt parallaxes everything, since it limits
 * the input batch size effectively */
extern MicroTime dt;

extern int rawsock;
extern size_t bufSz;
extern size_t bufEntryCount;

extern volatile bool running; 
extern volatile bool dispatcherBusy; 

extern pthread_attr_t threadAttr; /* used by all processes */
 
#if 0
extern int bdin; /* we use a pipe to sync the worker threads */
extern int bdout; 
#endif 

int handle_queued_deadlineEntries_main( 
        MicroTime deadline,
        DeadlineEntryHandler handleEntry_main,
        char* handlerName ); 

int psd_verify_process_main();
void* psd_dispatcher_thread(void*);
int psd_main(int argc, char** argv);


struct entriesVerifierDescriptor {
  MicroTime deadline;
  pid_t entriesVerifierPid;
} __attribute__((packed)); 



#endif 
