/* This makes simple key pairs
 * for the libsodium crypto box and stores it into files.
 */

#include "uninterruptableIO.h"
#include "cryptoutils.h"

#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sodium.h>

int main(int argc,char** argv) {

  if (argc != 3) {
    fprintf(stderr,
            "INVALID USAGE\n"
	    "   Usage: dumpdsk PK_PATH SK_PATH\n"
	    "   WARNING: Tool usage comproises keys!\n" );
    exit(EXIT_FAILURE); 
  }


  char* pkPath = argv[1];
  char* skPath = argv[2];

  if (sodium_init() < 0) {
    fprintf(stderr,"ERROR (dumpdsk): Could not initailize libsodium");
    exit(EXIT_FAILURE);
  }  

  unsigned char pk[crypto_box_PUBLICKEYBYTES]; /* Alice's public key */
  unsigned char sk[crypto_box_SECRETKEYBYTES];
  unsigned char dsk[PSD_DSK_SIZE];

  if ( sodium_mlock(pk,crypto_box_PUBLICKEYBYTES) < 0) {
    fprintf(stderr,"ERROR (dumpdsk): Coule not lock public key memory");	
    exit(EXIT_FAILURE);
  } 

  if ( sodium_mlock(sk,crypto_box_SECRETKEYBYTES)  < 0) {
    fprintf(stderr,"ERROR (dumpdsk): Coule not lock private key memory"); 	
    exit(EXIT_FAILURE);
  } 

  if ( sodium_mlock(dsk,PSD_DSK_SIZE)  < 0) {
    fprintf(stderr,"ERROR (dumpdsk): Coule not lock derived secret key memory"); 	
    exit(EXIT_FAILURE);
  } 


  if ( load_pk(pkPath,pk) < 0 ) {
    fprintf(stderr,"ERROR (dumpdsk): Could not open '%s' (%s)\n",
    pkPath,strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  if ( load_sk(skPath,sk) < 0 ) {
    fprintf(stderr,"ERROR (dumpdsk): Could not open '%s' (%s)\n",
    skPath,strerror(errno));
    exit(EXIT_FAILURE);
  }
 
  if ( crypto_box_beforenm(dsk,pk,sk) < 0 ) {
    fprintf(stderr,"ERROR (dumpdsk): Could not derive shared key\n");
    exit(EXIT_FAILURE);
  }

  for(size_t i = 0; i < PSD_DSK_SIZE; ++i) {
    printf("%02X",dsk[i]); }
   printf("\n"); 


  exit(EXIT_SUCCESS); 
  
}		
