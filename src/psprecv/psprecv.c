/* 
 * This is the last stage for the receiving end of the persistent socket 
 * handling stack
 *  It will unify replicated packets and yield two batchdata 
 *  sets as results. (May be split into a separate program later)
 *   1) The Data Batch, containing just the data of each packet
 *   2) The signature Batch, Containing (src_sock,signature) pairs
 *      the relative majority is selected. If it can not be established.
 *      all packets are dropped. If absolute majority is this has to
 *      be checked later. For this signatures should sign using cluster
 *      src/dst address in ipv6 and psd header fields. So that packet
 *      is the same on each replicated node. 
 *
 * Usage psprecv RECVPROG RECVPROG_ARGS SHMBATCHNAME 
 *  
 *  calls RECVPROG RECVPROG_ARGS SHMDATABATCH SHMSIGNATUREBATCH
 *
 * There might also be a nontirival pspsend in case 
 */

int main(int argc, char** argv ) {

  ShmBD* bd = load_shmbd(...);

  /* TODO: Validation ,esp packet sizes */

  /* assumes that packets are in "normalform", i.e. source and dest
     are multicast addrs if applicable, and the rmac field is
     replaced with the signatures source address */

  BD** hBin = alloc_split(bd,entryHeaderCmp); /* connection bins */
 
  size_t  pktCount = cpl_length(hBin); 

  BD* true_bin[pktCount];  

  size_t i;

  for (i=0; i < pktCount; ++i) {

    BD** rBin = alloc_split(hbin[i],entryDataCmp);

    sort_desc_by_bin_size(rbins);

    if ( rBin[0] == rBin[1] ) { /* first bin must be the biggest */
      fprintf(stderr,"WARING: Packets dropped! "
                     "Could not determine true data!\n");
      exit(EXIT_FAILURE);
    }
    
    true_bin[i] = *rBin++;
    
    while(*rBin != NULL) delete_batchdata(*rBin++);
 
    delete_batchdata(hbin[i]);

    free(rBin);

  }

  free(cBins);
 
  size_t total_pktDataSize = 0;
  size_t total_footerSize = 0;
  uint8_t data[pktCount]; /* TODO: use malloc, may be big */
  size_t  dataSize[pktCount];
  size_t  footerCount[pktCount];

  for( i=0; i < pktCount; ++i ) {
    dataSize[i] =  true_bin[i]->entry[0].size - PSD_FOOTER_SIZE;
    total_pktDataSize += dataSize[i];
    data[i] = true_bin[i]->entry[0].data;
    footerCount[i] = true_bin[i]->entry[0].entry_count;    
    total_footerSize += footerCount[i] * PSD_FOOTER_SIZE;
  } 

  BD* pkts  = new_batchdata(pktCount,total_pktDataSize);
  BD* signs = new_batchdata(pktCount,total_footerSize);

  for( i=0; i < pktCount; ++i ) {

    add_batchentry( pkts,data[i],dataSize[i] );

    BE* be = add_batchentry(signs,NULL,footerCount[i]*PSD_FOOTER_SIZE); 

    for(size_t j = 0; j < footerCount[i]; ++j) {
      memcpy( be->data + j*PSD_FOOTER_SIZE,
              true_bin[i]->entry[j].data + dataSize[i],
              PSD_FOOTER_SIZE );
    }
  } 

  exec_next_(...,pkts,signs);

}

