/* Simplified Timer Interface
 * for cluster node syncronization.
 */

#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <sys/timex.h>
#include <string.h>
#include "microtime.h"
#include <limits.h>

/* TODO: Dispose of CLOCK_MONOTONIC and use ntp_adjtime call for
 * everything ... . Figure out what to do with leap seconds first
 * What about time zones */

typedef int64_t MicroTime; 

/* The microEpochTime is the number of microseceonds of thw
 * used system/posix clock
 * epoch i.e. at 1970-01-01 00:00:00 +000 (UTC)
 *
 * The microtime function is the analog 
 * to the c99 time function.
 *
 * microtime is async signal safe */

MicroTime global_microepoch_tics = INT64_MAX;      /* internal vairable */

int64_t global_microepoch_precision = INT64_MAX; /* in nano seconds */

/* in micro seconds */
int64_t global_microtime_precision = INT64_MAX;

int64_t microepoch_nanoprecision() {
  if (global_microepoch_precision == INT64_MAX) microepoch(); /*init globals*/
  return global_microepoch_precision;
}

int64_t microtime_precision() {
  
  return (global_microepoch_precision+999)/1000 + global_microtime_precision;

}



/* microtics retuns number of  microsecond tices relative to counted form
 * an unspedified timepoint, that does not change after system start. */

MicroTime microtics(MicroTime* loc) {
  MicroTime ret;
  struct timespec tp;

  int status = clock_gettime(CLOCK_MONOTONIC,&tp); /*This *is* signal safe!*/
  if (status == -1) { /* panic, shold not be possible */
    perror("UNLIKELY ERROR: "
        "Became unable to perform clock_gettime for CLOCK_MONOTONIC");
    exit(EXIT_FAILURE); }
  
  ret = tp.tv_sec*1000000 + tp.tv_nsec/1000; 

  if (tp.tv_nsec >= 500) ret++; /* rounding a hlaf microsecond up */

  if (loc != NULL) *loc = ret;

  return ret;

}

/* returns microtics since epoch
 *
 * TODO: Analyze Precision 
 * 
 * This uses the property of CLOCK_MONOTONIC that its refrence time
 * point does not change system startup.
 */

MicroTime microepoch() {

  if ( global_microepoch_tics == INT64_MAX ) { 

    char* str = getenv("MICROEPOCH"); /* e.g.: "231+-2.2324" */

    if (str == NULL) goto FAIL;
 
    char* end;
 
    global_microepoch_tics = strtoll(str,&end,10);
 
    if (*end++ != '+') goto FAIL; 
    if (*end++ != '-') goto FAIL; 

    int64_t prec_microsecs = strtoll(end,&end,10);

    if (*end++ != '.' ) goto FAIL; 

    int64_t prec_nanosecs = strtoll(end,&end,10);

    if (*end != 0 ) goto FAIL; 

    if ( microtics(NULL) < global_microepoch_tics ) goto FAIL;
   
    global_microepoch_precision = prec_microsecs*1000 + prec_nanosecs;

    microtime_adjust_precision();
  }

  return global_microepoch_tics;

FAIL:

  fprintf(stderr,
"ERROR: MICROEPOCH must be properly defined in the environment. It must store\n""       the elapsed microseconds of the systems monotonic clock since the unix"
"\n       epoch. Format example:  1234+-56.78\n"        
"       In posix it can for example be defined by:\n" 
"       export MICROEPOCH=$(measure-microepoch)\n");
  exit(EXIT_FAILURE);
 
}

MicroTime microtime_adjust_precision(){

    struct timex tx;
    tx.modes = 0;

    int clock_state = ntp_adjtime(&tx);
    if ( clock_state == -1 ) {
      global_microtime_precision = MICROTIME_NTP_DEFAULT_PRECISION;
      fprintf(stderr,"WARNING: The systems ntp_adjtime call failed (%s).\n "
                    "Setting clock precision to default %lu microseconds.\n",
                     strerror(errno),global_microtime_precision);
    } else {
      /* inserting leap seconds might trigger this warning to */      
      if ( clock_state != TIME_OK ) {
        global_microtime_precision = MICROTIME_NTP_DEFAULT_PRECISION;
        fprintf(stderr,"WARNING: System clock not reliably synchronized.\n "
                     "Setting clock precision to default %lu microseconds.\n",
                     global_microtime_precision); 
      } else {
        if (tx.precision > 0) {
          global_microtime_precision = tx.precision;
        } else {
          global_microtime_precision = 1; /*The kernel rounds down? We don't!*/ 
        }
      }
    }

    return microtime_precision();
} 


/* returns microseconds from the unix epoch */

MicroTime microtime(MicroTime* loc) {
  MicroTime ret = microtics(NULL) - microepoch();
  if ( loc != NULL ) *loc = ret;
  return ret;
}


#if 0 /* untested */
int microtime_string(char* ret,MicroTime t) {

 char str0[NAME_MAX+1];

 MicroTime msecs = t%1000000;
 time_t secs = (time_t)(t/1000000); 

 struct tm tm;

 if ( gmtime_r(&secs,&tm) == NULL ) return -1;

 strftime(str0,NAME_MAX,"UTC_%F_%H-%M-%S",&tm);

 if( snprintf(ret,NAME_MAX,"%s_%ld",str0,msecs) < 3 ) return -1;

 return 0;

}

#endif
