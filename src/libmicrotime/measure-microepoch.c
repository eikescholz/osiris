/* Simplified Timer Interface
 * for cluster node synchronization.
 *
 * Better design: 
 *
 * The newest open group posix documentation states that CLOCK_MONOTONIC
 * has a reference time point that does not change after system startup.
 * 
 * This implies that the microepoch can be measured at start time
 * and written to the environment, for significant speed up and
 * more measurement precision.
 *
 * This program measures the microepoch and prints it to stdout
 * as a signed decimal number. 
 *
 * Further, the measurement does not need to be fast
 *
 */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include "microtime.h"

/* for precision measurement */
uint64_t nanotics() {
  struct  timespec tp;

  int status = clock_gettime(CLOCK_MONOTONIC,&tp); /*This *is* signal safe!*/
  if (status == -1) { /* panic, should not be possible */
    perror("UNLIKELY ERROR: Become unable to perform clock_gettime");
    exit(EXIT_FAILURE); }
  
  return tp.tv_sec*1000000000 + tp.tv_nsec; 

}



int main(int argc,char** argv ) {

  /* Time variable prefix naming convention:
   *
   *   Prefix "r" is for time measured from the unix epoch 
   *   Prefix "m" is for time measured by CLOCK_MONOTONIC 
   *
   * For a time-point N it therefore holds that that
   *
   *    rN = mN - mEpoch
   *
   * where mEpoch is the unix epoch in CLOCK_MONOTOIC time. Consequently
   *
   *    mEpoch = mN - rN
   *
   * */

  size_t MEASURE_COUNT = 10000;

  /* measurement variables */
  struct timespec mts0;
  struct timespec rts1;
  struct timespec mts2; 
  int64_t m0_sec;
  int64_t m0_nsec;
  int64_t r1_sec; 
  int64_t r1_nsec;
  int64_t m2_sec; 
  int64_t m2_nsec;

  /* data calculated from measurements */
  int64_t m1_sec;
  int64_t m1_nsec;
  int64_t epoch_sec;
  int64_t epoch_nsec;
  int64_t microepoch = INT64_MAX;
  int64_t cur_prec;
  int64_t prec = INT64_MAX;

  /* Computers are deterministic, so errors in a loop should be supposed
   * to be correlated. Averaging such measurements is not a good idea.
   * We just search for the best case, assuming that the 
   * hardware is precise, but returning the values is disturbed by context
   * switches and interrupts. The "fastest" measurement should therefore
   * be the best one. */
  
  for(size_t i=0; i < MEASURE_COUNT ; i++) {

    if ( clock_gettime(CLOCK_MONOTONIC,&mts0) == -1) goto FAIL_MONOTONIC; 
    if ( clock_gettime(CLOCK_REALTIME,&rts1) == -1) goto FAIL_REALTIME; 
    if ( clock_gettime(CLOCK_MONOTONIC,&mts2) == -1) goto FAIL_MONOTONIC; 
  
    /* load all values into 64-Bit words */
    m0_sec  = (int64_t)mts0.tv_sec;
    m0_nsec = (int64_t)mts0.tv_nsec;
    r1_sec  = (int64_t)rts1.tv_sec;
    r1_nsec = (int64_t)rts1.tv_nsec;
    m2_sec  = (int64_t)mts2.tv_sec;
    m2_nsec = (int64_t)mts2.tv_nsec;  

    /* Clock CLOCK_MONOTONIC must be monotonic */
    assert(    (m2_sec  > m0_sec)
	    || ((m2_sec == m0_sec) && (m2_nsec >= m0_nsec))  );     
   
    cur_prec =  (m2_sec-m0_sec) * 1000000000 + (m2_nsec-m0_nsec);  
    
    assert(cur_prec >= 0);  

    if ( cur_prec < prec ) {
      prec  = cur_prec;
      
      /* m1 as average of m0 and m2 */
      m1_sec   =  (m2_sec+m0_sec)/2;
      m1_nsec  = ((m2_sec+m0_sec)%2) * 500000000 + (m2_nsec+m0_nsec)/2 ;
      epoch_sec  = m1_sec  - r1_sec;
      epoch_nsec = m1_nsec - r1_nsec;  
      microepoch = epoch_sec * 1000000 + (epoch_nsec/1000);
    }
  }

  if (argc <= 1) {
    printf( "%ld+-%ld.%ld", microepoch, prec/1000, prec%1000 );
    return 0; 
  }

  fprintf(stderr, 
"ERROR: Invalid argument %s .\n"
"\n"
"  USAGE: measure-microepoch\n"
"\n"
"  Measures the number of microsecond tics the systems CLOCK_MONOTONIC clock\n"
"  counted since the unix epoch 1970-01-01 00:00:00 +000000 (UTC). It prints\n"
"  the measured result followed by the measurement precision in microseconds.\n"
"  See 'man 3 clock_gettime' for CLOCK_MONOTONIC on posix compliant systems.\n"
"\n"
"  Typical usage:\n"
"\n"
"    export MICROEPOCH=$(measure-microepoch)\n"
"\n",argv[1]);

  return 1;

FAIL_MONOTONIC:
   perror("UNLIKELY ERROR: "
	  "Become unable to perform clock_gettime on CLOCK_MONOTONIC");
   exit(EXIT_FAILURE); 
 
FAIL_REALTIME:
   perror("UNLIKELY ERROR: "
	  "Become unable to perform clock_gettime on CLOCK_REALTIME");
   exit(EXIT_FAILURE); 
 
}
