#include <sodium.h>
#include <stdbool.h>
#include "sysutils.h"
#include "apitest.h"
#include "list.h"
#include "cryptoutils.h"

const char* okStr = "........................................................................... OK";

const char* failStr = "....................................................................... FAILED";

const char* test_name = "";
unsigned char initialTestRGSeed[TEST_SEEDBYTES];
unsigned char testRGSeed[TEST_SEEDBYTES];
size_t msgCharCount = 0;

void stir_testseed() {
  sodium_increment(testRGSeed,TEST_SEEDBYTES); 
}

void random_testbytes(void* buf, unsigned long long n) {
  pseudo_randombytes(buf,n,testRGSeed);
  inc_seed(testRGSeed); 
}

size_t random_testsize() {
  size_t z;
  pseudo_randombytes((unsigned char*)&z,sizeof(z),testRGSeed);
  inc_seed(testRGSeed);
  return z;
}

char* alloc_random_test_rgseed(){
  unsigned char buf[TEST_SEEDBYTES];
  
  size_t hexLen = 2*TEST_SEEDBYTES + 1;  

  char* hex = malloc( hexLen ); 
  if ( hex == NULL) return NULL;

  randombytes_buf(buf,TEST_SEEDBYTES);
 
  return sodium_bin2hex(hex,hexLen,buf,TEST_SEEDBYTES);

}

void set_initial_testrgseed(const char* seedHexStr) {
  size_t bin_len;

  memset(initialTestRGSeed,0,TEST_SEEDBYTES);
  
  if (sodium_hex2bin(testRGSeed,TEST_SEEDBYTES,
                     seedHexStr,strlen(seedHexStr),
                     NULL,&bin_len,NULL) < 0) {
    fprintf( stderr,
             "Could not read random generator seed from hex string '%s'\n",
             seedHexStr);

    exit(EXIT_FAILURE);
  }

  memcpy(initialTestRGSeed,testRGSeed,TEST_SEEDBYTES);
 
}

void print_initial_testrgseed(const char* msg) {
  char hexstr[TEST_SEEDBYTES*2+1];
 
  sodium_bin2hex( hexstr,TEST_SEEDBYTES*2+1,
                  initialTestRGSeed,TEST_SEEDBYTES);
        
  printf("%s%s",msg,hexstr); 

}

void begin_test(const char* _test_name) {
  test_name = _test_name;

  msgCharCount = printf("%s ",test_name);
  fflush(stdout);
}

void print_test_result(bool ok) {
  size_t z = strlen(okStr);
  char buf[z];
  if (ok) {
    strcpy(buf,okStr);
  }
  else {
    strcpy(buf,failStr);
  }

  size_t nz = strlen(test_name) > z-9 ? z-9 : strlen(test_name);
  memcpy(buf,test_name,nz);
  buf[nz] = ' ';
  printf("\r%s\n",buf);
  fflush(stdout);
 }


void end_test() {
  /* we terminate at first failed check, if this is reached all is ok */

  if (errno != 0) { /* errno was set, so there was an error somewhere */
    print_test_result(false);
    printf("\nAn unchecked error was detected!\n\n");
    printf("  Errno %d: %s\n\n",errno,strerror(errno));
    exit(EXIT_FAILURE);
  }
  print_test_result(true);
  fflush(stdout);
}

/* quiet check */
void _qcheck(bool ok,const char* file,int line,const char* expr) {
  if (ok) return; 
  
  print_test_result(ok);
 
  printf("\n%s:%d:\n\n",file,line);
  if (errno != 0)
    printf("  Errno %d: %s\n\n",errno,strerror(errno));
  printf("    check(%s)\n\n",expr);

  print_initial_testrgseed("  Test Random Generator Seed:\n\n    ");
  printf("\n\n");
  fflush(stdout);
  exit(EXIT_FAILURE); /* terminate after first test as policy */
}  


void _check(bool ok,const char* file,int line,const char* expr) {
  if (ok) {
    if (msgCharCount % 72 == 0) {
      msgCharCount = printf("\r                                                                              \r%s ",test_name);
    } else { 
      msgCharCount += printf("."); /* add dots for progress */
    }
    fflush(stdout);
  }
  _qcheck(ok,file,line,expr);

}  



