#include "psd.h"
#include "apitest.h"
#include "batchdata.h"
#include "cryptoutils.h"
#include "microtime.h"

void libmicrotime_test_main() {

  begin_test("Microtime Low Precision Test");
    check( llabs(microtime(NULL)/1000000 - time(NULL)) <= 1 ); 
  end_test();
 
  begin_test("Microtime High Precision Test");
    int gettime_status;    
    struct timespec ts1;
    MicroTime t0,t1,t1_approx,t2;
 

    /* for this test we assume that a contest */
    for (size_t i = 0; i < 10000; ++i) {
      microtime(&t0);
      gettime_status = clock_gettime(CLOCK_REALTIME,&ts1);   
      microtime(&t2);
      
      check ( gettime_status != -1 );
 
      int64_t t1_sec  = (int64_t)ts1.tv_sec;
      int64_t t1_nsec = (int64_t)ts1.tv_nsec;
 
      t1        = t1_sec*1000000000 + t1_nsec;
      t1_approx = (t0*1000+t2*1000)/2;
 
      /* we assume that there is at most one context switch between
       * the clock_gettime calls, so the bigger err can be ignored */

      int64_t err = llabs(t1_approx-t1);
  
      int64_t prec = (t2-t0)*1000 + microepoch_nanoprecision();


      check( err < prec  );
    
    }
  end_test();

}



