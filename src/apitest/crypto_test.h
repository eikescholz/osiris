#include "apitest.h"
#include "batchdata.h"
#include "cryptoutils.h"

void crypto_test_main() {

  size_t n = 16; /* + 15*32; */

  char* fileName;
  char path[PATH_MAX];
  PSPKT_BE* be;
  char srcStr[INET6_ADDRSTRLEN]; 
  char dstStr[INET6_ADDRSTRLEN]; 
  unsigned char nonceKeyBuf[RMAC_KEY_SIZE];
  struct psd_cryptarg_t ca;
  struct psd_rmac_arg_t ra;
  struct in6_addr* src_addr;
  struct in6_addr* dst_addr;
  unsigned char rmacKey[RMAC_KEY_SIZE];
  unsigned char pk0[crypto_box_PUBLICKEYBYTES]; 
  unsigned char sk0[crypto_box_SECRETKEYBYTES];
  unsigned char dsk0[PSD_DSK_SIZE];
  unsigned char pk1[crypto_box_PUBLICKEYBYTES]; 
  unsigned char sk1[crypto_box_SECRETKEYBYTES];
  unsigned char dsk1[PSD_DSK_SIZE];
  unsigned char sk[crypto_sign_SECRETKEYBYTES];
 
  struct psd_psign_arg_t sa;
  
  uint64_t src_port;
  uint64_t dst_port;
  size_t z;
  PSPKT_BD** bins;
  PSPKT_BD** cur;
  PSPKT_BD* bd;
  PSPKT_BD* sendBd;
  PSPKT_BD* oneBD;
  size_t cur_n;
  PI_BD* pi_bd;
  VPSD_BD* vpsds;

  begin_test("Crypto Derived Stream Key Length Consistency Test");
    check(crypto_box_BEFORENMBYTES == crypto_stream_KEYBYTES);
  end_test();

  begin_test("Crypto Derived RMAC Key Length Consistency Test");
    check( crypto_onetimeauth_KEYBYTES == crypto_box_BEFORENMBYTES );
    check( crypto_onetimeauth_KEYBYTES == RMAC_KEY_SIZE );
    check( RMAC_KEY_SIZE == 32);
  end_test();

  begin_test("Crypto Node Signature Length Test");
    check( crypto_sign_BYTES == HASH_SIZE );
  end_test();

  begin_test("Crypto RMAC Length Test");
    check( crypto_onetimeauth_BYTES == PSPKT_RMAC_SIZE );
  end_test();

  psd_mkRootPath(path);


  begin_test("Crypto Packet Generation Test");

    /* data for crypto tests */
    bd = random_psock_valid_pspkt_batch(n,path); 
  
    check( bd != NULL);

    check( sort_batchdata(PSPKT,bd,pspktBeNonceCmp) == 0);

    check( store_batchfile(PSPKT,"tmp/nodecryptotest.bd",bd) == 0);

    /* create packets for send test, exactly one for each possible connection*/
    sendBd = random_psock_valid_pspkt_batch(16,path);  

    for (size_t i=0; i < bd->entry_count; ++i) {
      be = &sendBd->entry[i] ;
   
      /* remove packet duplications for ports on same addrs */
      if ( get_uint64n(&be->data->header.dst_port) == 0x17 
        || get_uint64n(&be->data->header.src_port) == 0x17
        || get_uint64n(&be->data->header.dst_port) == 0x1
        || get_uint64n(&be->data->header.src_port) == 0x1 
         )  be->data = NULL;
    }
 
    bd_inplace_strip(PSPKT,sendBd); 
    sendBd->entry_capacity = sendBd->entry_count;

    check( sort_batchdata(PSPKT,sendBd,pspktBeNonceCmp) == 0);

    check( store_batchfile(PSPKT,"tmp/sendtest.bd",sendBd) == 0);

    check(sendBd->entry_count == 4);

    delete_batchdata(PSPKT,sendBd);
 

  end_test();

  /* begin actual api tests */

  begin_test("Crypto Packet RMAC Make/Check Test");
  
    be = &bd->entry[random_testsize()%n] ;

    z = be->size;

    src_addr = &be->data->header.src_addr;
    dst_addr = &be->data->header.dst_addr;

    check( errno == 0);

    check( load_rmac_sk(src_addr,dst_addr,rmacKey) == 0 );

    check( errno == 0);

    ra.sk = rmacKey;
    ra.keyBuf = nonceKeyBuf;
    ra.logName = test_name;

    ps_inplace_mk_beRmac(be,&ra);

    check( errno == 0 );

    check( be->data != NULL ); 

    ps_inplace_ck_beRmac(be,&ra);  

    check( be->data != NULL );

    ps_inplace_ck_beRmac(be,&ra);  

    check( be->data != NULL );


  end_test();

 
  begin_test("Crypto Batch RMAC Make/Check Test");
  
    bins = alloc_split_batchdata(PSPKT,bd,pspktBeSrcDstIP6Cmp,NULL); 
    for( cur = bins; *cur != NULL; ++cur ) {
 
      be = &(*cur)->entry[0]; 
      src_addr = &be->data->header.src_addr;
      dst_addr = &be->data->header.dst_addr;
      cur_n = (*cur)->entry_count;

      check( load_rmac_sk(src_addr,dst_addr,rmacKey) == 0 );

      ip6ntos(src_addr,srcStr);
      ip6ntos(dst_addr,dstStr);

      fileName = mprintf("tmp/nodecryptotest%sn%s.bd",srcStr,dstStr);
      store_batchfile(PSPKT,fileName,*cur);
      free(fileName);

      check( ps_inplace_mk_rmac(*cur,rmacKey,test_name) == cur_n );
      check( ps_inplace_ck_rmac(*cur,rmacKey,test_name) == cur_n );
      check( ps_inplace_ck_rmac(*cur,rmacKey,test_name) == cur_n );

      delete_batchdata(PSPKT,*cur);  
    }
    free(bins);

    /* random access check */
    be = &bd->entry[random_testsize()%n] ;

    z = be->size;

    src_addr = &be->data->header.src_addr;
    dst_addr = &be->data->header.dst_addr;

    check( load_rmac_sk(src_addr,dst_addr,rmacKey) == 0 );

    check( errno == 0 );

    ra.sk = rmacKey;
    ra.keyBuf = nonceKeyBuf;
    ra.logName = test_name;

    ps_inplace_ck_beRmac(be,&ra);

    check(be->data != NULL);

  end_test();


  begin_test("Crypto Packet Encryption/Decryption Test");
  
    be = &bd->entry[random_testsize()%n] ;

    z = be->size;

    src_addr = &be->data->header.src_addr;
    dst_addr = &be->data->header.dst_addr;

    check( load_rmac_sk(src_addr,dst_addr,rmacKey) == 0 );

    check( errno == 0 );

    ra.sk = rmacKey;
    ra.keyBuf = nonceKeyBuf;
    ra.logName = test_name;

    ps_inplace_ck_beRmac(be,&ra);

    check( be->data != NULL );
 
    check( crypto_box_seed_keypair(pk0,sk0,testRGSeed) == 0 );
    
    stir_testseed(); 
 
    check( crypto_box_seed_keypair(pk1,sk1,testRGSeed) == 0 );
    
    stir_testseed(); 

    check( crypto_box_beforenm(dsk0,pk1,sk0) == 0 );    

    check( crypto_box_beforenm(dsk1,pk0,sk1) == 0 );

    check( memcmp(dsk0,dsk1,PSD_DSK_SIZE) == 0);    

    ca.mode = PSD_PORTCRYPTO;
    ca.shared_secret_key = dsk0;
    ca.logName = test_name;

    ps_inplace_encrypt_be(be,&ca);

    check( be->data != NULL);
    check( be->size == z);

    ps_inplace_decrypt_be(be,&ca);

    check( be->data != NULL );
    check( be->size == z);

    ps_inplace_ck_beRmac(be,&ra);

    check( be->data != NULL );
    
    ca.mode = PSD_NODECRYPTO;
    ca.shared_secret_key = dsk0;
    ca.logName = test_name;

    ps_inplace_encrypt_be(be,&ca);

    check( be->data != NULL);
    check( be->size == z);

    ps_inplace_decrypt_be(be,&ca);

    check( be->data != NULL );
    check( be->size == z);

    ps_inplace_ck_beRmac(be,&ra);

    check (be->data != NULL);
  
  end_test();


 
  begin_test("Crypto Batch Encryption/Decryption Test");

    bins = alloc_split_batchdata(PSPKT,bd,pspktBeSrcDstCmp,NULL); 
    for( cur = bins; *cur != NULL; ++cur ) {
 
      be = &(*cur)->entry[0]; 
      src_addr = &be->data->header.src_addr;
      dst_addr = &be->data->header.dst_addr;
      src_port = get_uint64n(&be->data->header.src_port);
      dst_port = get_uint64n(&be->data->header.dst_port);
      cur_n = (*cur)->entry_count;

      check( ps_verify_pktbd_src_addr(*cur,src_addr ) );
      check( ps_verify_pktbd_dst_addr(*cur,dst_addr ) );
      check( ps_verify_pktbd_src_port(*cur,src_port ) );
      check( ps_verify_pktbd_dst_port(*cur,dst_port ) );

      check( load_rmac_sk(src_addr,dst_addr,rmacKey) == 0 );

      /* performing node crypto tests */
     
      check( ps_load_node_sk(sk0,src_addr) == 0 );

      check( ps_inplace_encrypt(
                *cur,PSD_NODECRYPTO,
                sk0,
                src_addr,
                test_name) 
              == cur_n );

      check( ps_load_node_sk(sk0,dst_addr) == 0 );

      check( ps_inplace_decrypt(
                *cur,PSD_NODECRYPTO,
                sk0,
                dst_addr,
                test_name) 
              == cur_n );

      check(ps_inplace_ck_rmac(*cur,rmacKey,test_name) == cur_n);
   
      /* performing port crypto tests */
     
      check( ps_load_port_sk(sk0,src_addr,src_port) == 0 );

      check( ps_inplace_encrypt(
                *cur,PSD_PORTCRYPTO,
                sk0,
                src_addr,
                test_name) 
              == cur_n );

      check( ps_verify_pktbd_src_addr(*cur,src_addr ) );
      check( ps_verify_pktbd_dst_addr(*cur,dst_addr ) ); 
      check( ps_verify_pktbd_src_port(*cur,src_port ) );
      check( ps_verify_pktbd_dst_port(*cur,dst_port ) );

      check( ps_load_port_sk(sk0,dst_addr,dst_port ) == 0 );

      check( ps_inplace_decrypt(
                *cur,PSD_PORTCRYPTO,
                sk0,
                dst_addr,
                test_name) 
              == cur_n );

      check( ps_inplace_ck_rmac(*cur,rmacKey,test_name) == cur_n );
     
      delete_batchdata(PSPKT,*cur);
    }
    free(bins);

  end_test();


  begin_test("Crypto Packet Sign/Verify Test");
    
    be = &bd->entry[random_testsize()%n] ;

    check( ps_load_auth_sk( sk,
                            &be->data->header.src_addr,
                            get_uint64n(&be->data->header.src_port) ) 
               == 0 ); 

    stir_testseed(); 
 
    sa.sk = sk;
    sa.logName = test_name;

    ps_inplace_psign_be(be,&sa);

    check( be->data != NULL);

    oneBD = new_batchdata(PSPKT,1,be->size);
    check( oneBD != NULL );

    check( new_batchentry(PSPKT,oneBD,be->data,be->size) != NULL );

    dst_addr = &be->data->header.dst_addr;

    pi_bd = sortBySrc_normalize_and_load_mlocked_pks(oneBD,dst_addr);
    check( pi_bd != NULL );
    check( pi_bd->entry_count == 1 );

    vpsds = ps_verify_signatures(pi_bd);
    check( vpsds != NULL );
    check( vpsds->entry_count == 1);
    check( memcmp( vpsds->entry->data->datagram , 
                   be->data, 
                   ps_datagramSize(vpsds->entry->data->datagram)) 
             == 0 );

    delete_batchdata(PSPKT,oneBD);
    delete_batchdata(PI,pi_bd);
    delete_batchdata(VPSD,vpsds);

  end_test();




  begin_test("Crypto Batch Sign/Verify Test");
  
    /*TODO: check actual voting and duplication removal ... */

    bins = alloc_split_batchdata(PSPKT,bd,pspktBeSrcDstCmp,NULL); 
    for( cur = bins; *cur != NULL; ++cur ) {
 
      be = &(*cur)->entry[0]; 
      src_addr = &be->data->header.src_addr;
      dst_addr = &be->data->header.dst_addr;
      src_port = get_uint64n(&be->data->header.src_port);
      dst_port = get_uint64n(&be->data->header.dst_port);
      cur_n = (*cur)->entry_count;

      check( ps_verify_pktbd_src_addr(*cur,src_addr ) );
      check( ps_verify_pktbd_dst_addr(*cur,dst_addr ) );
      check( ps_verify_pktbd_src_port(*cur,src_port ) );
      check( ps_verify_pktbd_dst_port(*cur,dst_port ) );

      check( ps_load_auth_sk(sk,src_addr,src_port ) == 0 );

      check( ps_inplace_normalize_and_sign(*cur,sk,test_name) == cur_n );

      vpsds = ps_verify(*cur,dst_addr,"(nil)");
      check( vpsds != 0);
      check( vpsds->entry_count == (*cur)->entry_count );

      delete_batchdata(VPSD,vpsds);
      delete_batchdata(PSPKT,*cur);
    }
    free(bins); 

  end_test();

  delete_batchdata(PSPKT,bd);

}



