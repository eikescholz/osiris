#ifndef PSD_APITEST
#define PSD_APITEST
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "psd.h"
#include "pscore.h"
#include "escore.h"

 
#define check(expr) _check(expr,__FILE__,__LINE__,#expr)
#define qcheck(expr) _qcheck(expr,__FILE__,__LINE__,#expr)


extern const char* okStr;
extern const char* failStr;
extern const char* test_name;

extern void begin_test(const char* testname);
extern void end_test();
extern void _check(bool ok,const char* f, int l, const char* expr);
extern void _qcheck(bool ok,const char* f, int l, const char* expr);


extern void random_testbytes(void* buf, unsigned long long n);
extern size_t random_testsize();
extern char* alloc_random_test_rgseed();
extern void set_initial_testrgseed(const char* seedHexStr);

#define TEST_SEEDBYTES crypto_stream_chacha20_KEYBYTES

extern void print_initial_testrgseed(const char* msg); 
extern unsigned char testRGSeed[TEST_SEEDBYTES];
extern void stir_testseed();

static inline int is_sorted_by(
   int (*compar)(const void*,const void*), 
   void* _p,
   size_t n,
   size_t z ) {

  char* p = _p;

  for ( size_t i = 1; i < n; ++i ) {
    if ( compar(p+((i-1)*z),p+(i*z)) > 0 ) return false ;
  }

  return true;

}

/* TODO: clean up random generators */

static inline 
PSPKT_BD* random_psock_valid_pspkt_batch( size_t n,
                                          const char* path) {

  size_t payloadBytes;

  struct psockinfo_t* pi = alloc_psockinfo_of(path);
  if (pi == NULL) return NULL;

  PSPKT_BD* bd = new_batchdata(PSPKT,n,n*PS_MAX_IP6PACKET_SIZE);
  if (bd == NULL) return NULL;

  /* make at least on packet for each possible local connection */
  size_t m = 0;
  for ( size_t  i=0; i < pi->node_count; ++i) {
    for ( size_t k=0; k < pi->port_count[i]; ++k ) {
      for ( size_t j=0; j < pi->node_count; ++j ) {
        for ( size_t l=0; l < pi->port_count[j]; ++l ) {
          payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE;

          check( new_random_pspkt_batchentry(
                        bd,
                        testRGSeed,
                        &pi->node_addr[i],
                        &pi->node_addr[j],
                        pi->port[i][k],
                        pi->port[j][l],
                        payloadBytes) 
                   != NULL );
          ++m;
        }
      }  
    }
  }


  for(size_t i = m; i < bd->entry_capacity; ++i) {
    size_t a = random_testsize() % pi->node_count;
    size_t b = random_testsize() % pi->node_count;
    size_t c = random_testsize() % pi->port_count[a];
    size_t d = random_testsize() % pi->port_count[b];

    payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE;
 
    check( new_random_pspkt_batchentry( 
                bd,
                testRGSeed,
                &pi->node_addr[a],
                &pi->node_addr[b],
                pi->port[a][c],
                pi->port[b][d],
                payloadBytes )
             != NULL );
  }
  
  free_psockinfo(pi);
  
  return bd;
}




static inline 
PSPKT_BD* random_esock_valid_pspkt_batch( size_t n,
                                          size_t minPayloadBytes,
                                          IP6Addr* dst_addr,
                                          uint64_t dst_port,
                                          MicroTime deadline,
                                          MicroTime dt,
                                          const char* path ) {
  PSPKT_BE* be;
  size_t payloadBytes;
  MicroTime t0, t1;

  struct psockinfo_t* pi = alloc_psockinfo_of(path);
  if (pi == NULL) return NULL;

  PSPKT_BD* bd = new_batchdata(PSPKT,n,n*PS_MAX_IP6PACKET_SIZE);
  if (bd == NULL) return NULL;

  minPayloadBytes += ES_HEADER_SIZE - PSD_HEADER_SIZE; 

  /* make at least on packet for each possible local connection */
  size_t m = 0;
  for ( size_t  i=0; i < pi->node_count; ++i) {
    for ( size_t k=0; k < pi->port_count[i]; ++k ) {

      payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE;
      payloadBytes = (payloadBytes < minPayloadBytes)
                         ? minPayloadBytes : payloadBytes;

      be = new_random_pspkt_batchentry(
                        bd,
                        testRGSeed,
                        &pi->node_addr[i],
                        dst_addr,
                        pi->port[i][k],
                        dst_port,
                        payloadBytes); 

      check( be != NULL );
      
      ++m;

      t0 = ((deadline-1)/dt) * dt ;

      t1 = t0 + (random_testsize()%dt);

      set_int64n( &be->data->header.deadline, t1 );


    }
  }


  for(size_t i = m; i < bd->entry_capacity; ++i) {
    size_t a = random_testsize() % pi->node_count;
    size_t c = random_testsize() % pi->port_count[a];

    payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE;
    payloadBytes = (payloadBytes < minPayloadBytes)
                         ? minPayloadBytes : payloadBytes;

    be = new_random_pspkt_batchentry( 
                bd,
                testRGSeed,
                &pi->node_addr[a],
                dst_addr,
                pi->port[a][c],
                dst_port,
                payloadBytes );

    check( be != NULL );

    t0 = ((deadline-1)/dt) * dt ;

    t1 = t0 + (random_testsize()%dt);

    set_int64n( &be->data->header.deadline, t1 );

  }
  
  free_psockinfo(pi);
  
  return bd;

}

static inline 
PSPKT_BD* random_esock_simple_valid_batch( size_t n,
                                         size_t minPayloadBytes,
                                         IP6Addr* src_addr,
                                         uint64_t src_port,
                                         IP6Addr* dst_addr,
                                         uint64_t dst_port,
                                         MicroTime deadline,
                                         MicroTime dt,
                                         const char* path ) {
  PSPKT_BE* be;
  size_t payloadBytes;
  MicroTime t0, t1;

  struct psockinfo_t* pi = alloc_psockinfo_of(path);
  if (pi == NULL) return NULL;

  PSPKT_BD* bd = new_batchdata(PSPKT,n,n*PS_MAX_IP6PACKET_SIZE);
  if (bd == NULL) return NULL;

  minPayloadBytes += ES_HEADER_SIZE - PSD_HEADER_SIZE; 

  /* make at least on packet for each possible local connection */
  payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE;
  payloadBytes = (payloadBytes < minPayloadBytes)
                         ? minPayloadBytes : payloadBytes;

  be = new_random_pspkt_batchentry(
                        bd,
                        testRGSeed,
                        src_addr,
                        dst_addr,
                        src_port,
                        dst_port,
                        payloadBytes); 

  check( be != NULL );
      
  t0 = ((deadline-dt)/dt) * dt ;

  t1 = t0 + (random_testsize()%dt);

  set_int64n( &be->data->header.deadline, t1 );



  for(size_t i = 1; i < bd->entry_capacity; ++i) {

    payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE;
    payloadBytes = (payloadBytes < minPayloadBytes)
                         ? minPayloadBytes : payloadBytes;


    be = new_random_pspkt_batchentry( 
                bd,
                testRGSeed,
                src_addr,
                dst_addr,
                src_port,
                dst_port,
                payloadBytes );

    check( be != NULL );

    t0 = ((deadline-dt)/dt) * dt ;

    t1 = t0 + (random_testsize()%dt);

    set_int64n( &be->data->header.deadline, t1 );

  }
  
  free_psockinfo(pi);
  
  return bd;

}



#endif
