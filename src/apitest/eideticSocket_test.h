
#include "psd.h"
#include "apitest.h"
#include "batchdata.h"
#include "cryptoutils.h"
#include "pscore.h"
#include "escore.h"





#define ES_TEST_PREFIX_SIZE 32

void mk_random_eideticScoketInput_checks(
       ESD_BD** retEsds,
       PSPRF_BD **retPrfs,
       IP6Addr* dst_addr,
       uint64_t dst_port,
       MicroTime deadline,
       MicroTime dt,
       size_t n,
       const char* rootPath
       ) {

  uint64_t src_port;
  IP6Addr* src_addr;
  uint8_t sk[crypto_sign_SECRETKEYBYTES];
  ESD* esd;
  PSPRF prf;
  PSPKT_BD *bd, **bin;
  ESD_BD* esds;
  PSPRF_BD *prfs;
  size_t i;
  PSS* signature;


  bd = random_esock_valid_pspkt_batch( n, 
                                       ES_TEST_PREFIX_SIZE,
                                       dst_addr,
                                       dst_port,
                                       deadline,
                                       dt,
                                       rootPath ); 
  check( bd != NULL);

  esds = new_batchdata( ESD, n, bd->data_size );
  check( esds != NULL );

  prfs = new_batchdata( PSPRF, n, n * bd->data_size );
  check (prfs != NULL );

  bin = alloc_split_batchdata(PSPKT, bd, pspktBeSrcDstCmp,&n);
  check( bin !=  NULL );
 
  /* create validly signed packets */
  for (i=0; i < n ; ++i ) {

    src_addr = &bin[i]->entry->data->header.src_addr;
    src_port = get_uint64n( &bin[i]->entry->data->header.src_port );

    check( ps_load_auth_sk(sk,src_addr,src_port) == 0 );
 
    ps_inplace_normalize_and_sign( bin[i], sk, ps_mainIdent );

    clear_auth_sk(sk);

    delete_batchdata( PSPKT, bin[i] ); 

  }
   
  free(bin);

  sort_batchdata( PSPKT, bd, ps_pspktBe_cmp );

   /* split into datagram and proof batches */
  for (i=0; i < bd->entry_count; ++i ) {
    esd = (ESD*)bd->entry[i].data;
    check( ps_datagramSize((PS_D*)esd) 
               >= ES_TEST_PREFIX_SIZE + ES_HEADER_SIZE ); 
    set_uint64n( &prf.signatureCount, 1 );
    check( ps_mk_packetDatagramHash( prf.hash, (PSPKT*)esd ) == 0 );
    memcpy( &prf.signature[0],
            ps_packetSignature(bd->entry[i].data), 
            sizeof(*signature) ); 
    check( new_batchentry( ESD, esds, esd, ps_datagramSize((PS_D*)esd) )!= 0);
    check( new_batchentry( PSPRF, prfs,&prf,sizeof(prf)) != 0 );
  }

  *retEsds = esds;
  *retPrfs = prfs;

  delete_batchdata(PSPKT,bd);

}
   



 
void eideticSocket_test_main() {

  size_t n = 5;
  char rootPath[ES_PATH_MAX], bcHeadBlockPath[ES_PATH_MAX],
       bcHeadHashPath[ES_PATH_MAX];
  MicroTime dt, t0, t1, prevBlockTime, curDeadline; 
  IP6Addr dst_addr, *interlock_src_addr, interlock_dst_addr;
  uint64_t dst_port, interlock_src_port, interlock_dst_port;
  ESD_BD   *esds0, *esds1, *bd;
  PSPRF_BD *prfs0, *prfs1;
  ESD *esd, storedEsd;
  PSPRF *prf, storedPrf;
  PS sock;
  uint8_t prevBlockHash[ES_PATH_MAX];
  /* test socket data */
  ip6pton( "::2", &dst_addr );
  dst_port = 0x2A ;

  ip6pton( "::1", &interlock_dst_addr );
  interlock_dst_port = 0x1 ;

  dt = 500;  

  interlock_src_addr = &dst_addr;
  interlock_src_port = dst_port;
  
  
  psd_mkRootPath( rootPath );

  t0 = (((microtime(NULL))/dt)+10)*dt;
  char* argv[] = { "eidetidSocketTest", NULL };




  begin_test( "Eidetic Socket Input Generation Test" );

    mk_random_eideticScoketInput_checks( 
                    &esds0,
                    &prfs0,
                    &dst_addr,
                    dst_port,
                    t0,
                    dt,
                    n,
                    rootPath );
   
    mk_random_eideticScoketInput_checks( 
                    &esds1,
                    &prfs1,
                    &dst_addr,
                    dst_port,
                    t0+dt,
                    dt,
                    n,
                    rootPath );

  end_test();
 




  begin_test( "Eidetic Socket Empty Blockchain Interlock Test" );

    t1 = t0 + 15*dt; 

    bd = (ESD_BD*)random_esock_simple_valid_batch( 
                    n, 
                    ES_TEST_PREFIX_SIZE,
                    interlock_src_addr,
                    interlock_src_port,
                    &interlock_dst_addr,
                    interlock_dst_port,
                    t1,
                    dt,
                    rootPath ); 

    check( bd != NULL
                    );
 
    check( es_interlock(bd,interlock_src_addr,interlock_src_port) == 0 );

    for (size_t i=0; i < bd->entry_count; ++i ) {
 
      esd = bd->entry[i].data;
      
      prevBlockTime = get_int64n( &esd->header.prevBlockTime );
     
      curDeadline = get_int64n( &esd->header.deadline );

      check( t1-dt < curDeadline       ); 
      check(         curDeadline <= t1 );
    
      memset( prevBlockHash,0,HASH_SIZE);

      check( memcmp(prevBlockHash,esd->header.prevBlockHash,HASH_SIZE) == 0 );
 
    } 
    
    delete_batchdata(ESD,bd);

  end_test();




  begin_test( "Eidetic Socket Blockchain Creation Test" );

    /* create first block */
    check( es_receive( &dst_addr,
                       dst_port,
                       t0,
                       esds0,
                       prfs0,
                       argv ) == EXIT_SUCCESS );

    /* create second block to test hash link */
    check( es_receive( &dst_addr,
                        dst_port,
                        t0+dt,
                        esds1,
                        prfs1,
                        argv ) == EXIT_SUCCESS );

    ip6cpy( &sock.addr, &dst_addr );
    set_uint64n(&sock.port, dst_port);

    check( es_verify_block( &sock, t0 ) );

    check( es_verify_block( &sock, t0 + dt ) ); 
  
  end_test();  




  begin_test( "Eidetic Socket Blockchain Interlock Test" );

    t1 = t0 + 15*dt; 

    bd = (ESD_BD*)random_esock_simple_valid_batch( 
                    n, 
                    ES_TEST_PREFIX_SIZE,
                    interlock_src_addr,
                    interlock_src_port,
                    &interlock_dst_addr,
                    interlock_dst_port,
                    t1,
                    dt,
                    rootPath ); 

    check( bd != NULL
                    );
 
    check( es_interlock(bd,interlock_src_addr,interlock_src_port) == 0 );

    for (size_t i=0; i < bd->entry_count; ++i ) {
 
      esd = bd->entry[i].data;
      
      prevBlockTime = get_int64n( &esd->header.prevBlockTime );
     
      curDeadline = get_int64n( &esd->header.deadline );

      check( t1-dt < curDeadline       ); 
      check(         curDeadline <= t1 );
     
      check( es_mk_blockchainBlockPath( bcHeadBlockPath,
                                        interlock_src_addr,
                                        interlock_src_port,
                                        prevBlockTime ) != NULL );

      vstpcpy( bcHeadHashPath, bcHeadBlockPath, "/hash.blake2b", NULL );
      
      check( atomic_load_from_file( bcHeadHashPath, 
                                    &prevBlockHash, 
                                    ES_HASH_SIZE ) == 0 ); 
      check( memcmp(prevBlockHash,esd->header.prevBlockHash,HASH_SIZE) == 0 );
 
    } 
    
    delete_batchdata(ESD,bd);

  end_test();

 


  begin_test("Eidetic Socket Blockchain Random Entry Lookup Test");

    /* random lookup */
    for( size_t i=0; i < esds0->entry_count; ++i ) {

      esd = esds0->entry[i].data;
      prf = prfs0->entry[i].data;
  
      check( ps_datagramSize((PS_D*)esd) 
               >= ES_TEST_PREFIX_SIZE + ES_HEADER_SIZE ); 

      check( es_lookup( 
               &storedEsd,
               &storedPrf,
               &sock,
               get_int64n( &esd->header.deadline ),
               esd->payload,
               ES_TEST_PREFIX_SIZE ) 
              == 0 );

      check( memcmp(&storedEsd,esd,ps_datagramSize( (PS_D*)esd )) == 0 ); 

      check( memcmp( &storedPrf,
                     prf,
                     psd_proofSize(get_uint64n(&prf->signatureCount))) == 0 ); 

    }

  end_test();

  delete_batchdata(ESD,esds0);
  delete_batchdata(PSPRF,prfs0); 
  delete_batchdata(ESD,esds1);
  delete_batchdata(PSPRF,prfs1); 

}



