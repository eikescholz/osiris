
#include "psd.h"
#include "apitest.h"
#include "batchdata.h"
#include "cryptoutils.h"
#include "pscore.h"

 
void deadlineQueue_test_main() {

  size_t m,n,entCount,i,j,k;
  char path[PSD_MAX_PATH_SIZE] , inPath[PSD_MAX_PATH_SIZE],
       deadlinePath[PSD_MAX_PATH_SIZE];
  MicroTime curTime, dt, t0; 
  struct stat st;
  DIR* dir;
  struct dirent* dirEnt;
  PSPKT_BD *bd,*bd_copy,**bins,**merged, **cur, **curBD;
  DQE_BD**  dqe_bd;
  IP6Addr *dst_addr;
  uint64_t dst_port,curDeadline;
  PSPKT* curPkt;

  dt = ps_baseInterval;
  m = 5;
  n = dt * m ; 

  dqe_bd = malloc(m*sizeof(*dqe_bd));
  merged = malloc((m+1)*sizeof(*merged));
  
  psd_mkRootPath(path);
  psd_mkIncomingPath(inPath);


  begin_test("DeadlineQueue Store Test");
    
    bd = random_psock_valid_pspkt_batch(n,path);
 
    curTime = ((microtime(NULL)+dt-1)/dt)*dt +1 ; /* round to interval */

    for(i = 0; i < bd->entry_count; ++i) {
      set_int64n(&bd->entry[i].data->header.deadline, curTime + i + 1000000);
    }

    check( sort_batchdata(PSPKT,bd, pspktBeNonceCmp) == 0);

    check( store_batchfile(PSPKT,"tmp/deadlineQueue.in.bd",bd) == 0 );
  
    bins = alloc_split_batchdata(PSPKT,bd,pspktBeDstCmp,NULL); 
    
    for( cur = bins; *cur != NULL; ++cur ) {
      
      dst_addr = &(*cur)->entry->data->header.dst_addr;
      dst_port = get_uint64n(&(*cur)->entry->data->header.dst_port); 
  
      check( ps_deadlineQueue_store(*cur,inPath,dst_addr,dst_port) == 0 );
      
      for(j = 0; j < m; ++j ) {
        psd_mkDeadlinePath(deadlinePath,inPath,curTime + j*dt + 1000000 );
        check( stat(deadlinePath,&st) == 0 );
        errno = 0;
      }

      delete_batchdata(PSPKT,*cur);
    }

    free(bins);

    dir = opendir(inPath);
    check( dir !=0 );

    entCount = 0;
    for (dirEnt = readdir(dir); dirEnt != 0; dirEnt = readdir(dir)) {
      entCount++;
    }
    
    entCount -= 2; /* subtract . and .. */

    check( entCount == m );
    
    closedir(dir);

  end_test();



  begin_test("DeadlineQueue Load Test");

    dir = opendir(inPath);
    check( dir !=0 );

    i = 0;
    for (dirEnt = readdir(dir); dirEnt != 0; dirEnt = readdir(dir)) {
      if (strcmp(dirEnt->d_name,".") == 0) continue;
      if (strcmp(dirEnt->d_name,"..") == 0) continue;

      check( sscanf(dirEnt->d_name,"%lX",&curDeadline) == 1);
      
      dqe_bd[i] = ps_deadlineQueue_load(inPath, curDeadline );
      
      check( dqe_bd[i] != NULL );
      
      curBD = malloc( (dqe_bd[i]->entry_count+1) * sizeof(*curBD) );

      check( curBD != NULL );
 
      for(j = 0; j < dqe_bd[i]->entry_count; ++j) {
        check( dqe_bd[i]->entry[j].data->bd != NULL );
        curBD[j] = dqe_bd[i]->entry[j].data->bd;
        
        dst_addr = &curBD[j]->entry->data->header.dst_addr;
        dst_port = get_uint64n(& curBD[j]->entry->data->header.dst_port ); 
        t0 = dqe_bd[i]->entry[j].data->deadline;

        check( ps_verify_pktbd_dst_addr(curBD[j],dst_addr) );
        check( ps_verify_pktbd_dst_port(curBD[j],dst_port) );

        for( k = 0; k < curBD[j]->entry_count; ++k ) {
          curPkt =  curBD[j]->entry[k].data;
          check( get_int64n(&curPkt->header.deadline) > t0 - dt);
          check( get_int64n(&curPkt->header.deadline) <= t0 );
        }

      } 
      curBD[ j ] = NULL;
 
      merged[i] = new_merged_batchdata(PSPKT,curBD);

      check( merged[i] != NULL );

      free(curBD);

      check( ps_deadlineQueue_unlink(inPath,curDeadline) == 0);

      ++i;
    }
    merged[i] = NULL;
    closedir(dir);

  end_test();



  begin_test("DeadlineQueue Store/Load Validation");

    bd_copy = new_merged_batchdata(PSPKT,merged);

    check( sort_batchdata(PSPKT,bd_copy, pspktBeNonceCmp) == 0);
 
    check( store_batchfile(PSPKT,"tmp/deadlineQueue.out.bd",bd_copy) == 0 );
   
    check( sort_batchdata(PSPKT,bd, pspktBeNonceCmp) == 0);

    check( compare_batchdata(PSPKT,bd,bd_copy) == 0 );
    
    check( bd_copy->entry_count == bd->entry_count );
    
    check( compare_batchdata(PSPKT,bd,bd_copy) == 0 );
    

    for( j = 0; j < i ; j++) {
       delete_DQE_BD(dqe_bd[j]);
       delete_batchdata(PSPKT,merged[j]);
    }

    free(merged);
  
    free(dqe_bd);

    delete_batchdata(PSPKT,bd_copy);

    delete_batchdata(PSPKT,bd);

  end_test();
 

}



