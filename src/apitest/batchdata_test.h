#include "psd.h"
#include "apitest.h"
#include "batchdata.h"


PSPKT_BD* random_pspkt_batch(size_t n,size_t binBytes) {

  struct in6_addr srcIp[binBytes];
  struct in6_addr dstIp[binBytes];

  uint64_t srcSd[binBytes];
  uint64_t dstSd[binBytes];
  PSPKT_BD* bd;
  PSPKT_BE* be;
  
  random_testbytes(srcIp,sizeof(*srcIp)*binBytes);
  random_testbytes(dstIp,sizeof(*dstIp)*binBytes);
  random_testbytes(srcSd,sizeof(*srcSd)*binBytes);
  random_testbytes(dstSd,sizeof(*dstSd)*binBytes);

  bd = new_batchdata(PSPKT,n,n*PS_MAX_IP6PACKET_SIZE);
  check( bd != NULL );
  check( bd->data_size == 0 );

  for(size_t i = 0; i < bd->entry_capacity; ++i) {
    size_t payloadBytes = random_testsize() % PSD_MAX_PAYLOAD_SIZE; 
    uint8_t payload[payloadBytes];
    psd_nonce_t nonce;
    uint64_t mpid;

    random_testbytes(&nonce,PS_NONCE_BYTES);       
    random_testbytes(&mpid,sizeof(mpid));       
    size_t a = random_testsize() % binBytes;
    size_t b = random_testsize() % binBytes;
    size_t c = random_testsize() % binBytes;
    size_t d = random_testsize() % binBytes;
    random_testbytes(&payload,payloadBytes);       
 
    be = new_pspkt_batchentry(
                        bd,
                        &srcIp[a],
                        &dstIp[b],
                        &nonce,
                        mpid,
                        microtime(NULL),
                        srcSd[c],
                        dstSd[d],
                        payload,
                        payloadBytes );
    check( be != NULL );
    check( be->size < PS_MAX_IP6PACKET_SIZE);
  }

  check( bd->data_size != 0); 
  
  return bd;
}


void batchdata_test_main() {
 
  size_t i,n;
  PSPKT_BD* bd;
  PSPKT_BD* bd_copy;
 
  n = 16*128, 

  begin_test("Batchdata Entry Creation Test");

    bd = random_pspkt_batch(n,8); 
    
    check(bd->entry_count == n);

    check(bd->data_size != 0);

  end_test(); 




  begin_test("Batchdata Load/Store Test");
   
    check( store_batchfile(PSPKT,"tmp/test.bd",bd) == 0 ); 
    
    PSPKT_BD* loaded_bd = load_batchfile(PSPKT,"tmp/test.bd");
     
    check(loaded_bd != NULL);

    check(loaded_bd && compare_batchdata(PSPKT,bd,loaded_bd) == 0);
 
    delete_batchdata(PSPKT,loaded_bd);

  end_test();




  begin_test("Batchdata Shallow Copy Test");
 
    bd_copy = shallow_copy_batchdata(PSPKT,bd);

    check(bd_copy != NULL );

    check( compare_batchdata(PSPKT,bd,bd_copy) == 0);
   
    check( bd_copy->data_size == bd->data_size );

    delete_batchdata(PSPKT,bd_copy);

  end_test();




  begin_test("Batchdata Deep Copy Test");
 
    bd_copy = deep_copy_batchdata(PSPKT,bd);

    check(bd_copy && compare_batchdata(PSPKT,bd,bd_copy) == 0);

    delete_batchdata(PSPKT,bd_copy);

  end_test();


  begin_test("Batchdata Batchentry Compare Test");
 
    for(i=0; i < bd->entry_count; ++i) {
      check(     batchentryCmp((BE*)&bd->entry[i],(BE*)&bd->entry[0]) 
             == -batchentryCmp((BE*)&bd->entry[0],(BE*)&bd->entry[i]) );
    } 
 
  end_test();


 
  begin_test("Batchdata Packet Source IP Compare Test");
 
    for(i=0; i < bd->entry_count; ++i) {
      check(     pspktBeSrcIP6Cmp(&bd->entry[i],&bd->entry[0]) 
             == -pspktBeSrcIP6Cmp(&bd->entry[0],&bd->entry[i]) );
    } 
 
  end_test();




  begin_test("Batchdata Packet Destination IP Compare Test");
 
    for(i=0; i < bd->entry_count; ++i) {
      check(     pspktBeDstIP6Cmp(&bd->entry[i],&bd->entry[0]) 
             == -pspktBeDstIP6Cmp(&bd->entry[0],&bd->entry[i]) );
    } 
 
  end_test();

  


  begin_test("Batchdata Packet Source Port Compare Test");
 
    for(i=0; i < bd->entry_count; ++i) {
      check(     pspktBeSrcPortCmp(&bd->entry[i],&bd->entry[0]) 
             == -pspktBeSrcPortCmp(&bd->entry[0],&bd->entry[i]) );
    } 
 
  end_test();




  begin_test("Batchdata Packet Destination Port Compare Test");
 
    for(i=0; i < bd->entry_count; ++i) {
      check(     pspktBeDstPortCmp(&bd->entry[i],&bd->entry[0]) 
             == -pspktBeDstPortCmp(&bd->entry[0],&bd->entry[i]) );
    } 
 
  end_test();




  begin_test("Batchdata Sort by Source Ip Address Test");
    
    PSPKT_BD* bd_sorted = shallow_copy_batchdata(PSPKT,bd);
    
    n = bd_sorted->entry_count;

    sort_batchdata(PSPKT,bd_sorted,pspktBeSrcIP6Cmp);

    store_batchfile(PSPKT,"tmp/testSrcSorted.bd",bd_sorted);
   
    check( bd->entry_count == bd_sorted->entry_count);
    check( bd_sorted && is_sorted_by(
                            (STD_COMPARE)pspktBeSrcIP6Cmp,
                            bd_sorted->entry,
                            n,
                            sizeof(*bd->entry) ) );
  
    delete_batchdata(PSPKT,bd_sorted);

  end_test();




  begin_test("Batchdata Split/Merge Test");
    
    size_t bc;

    PSPKT_BD* bd_splitInput = shallow_copy_batchdata(PSPKT,bd);
    
    PSPKT_BD** bins = alloc_split_batchdata( PSPKT,
                                             bd_splitInput,
                                             pspktBeSrcIP6Cmp,&bc );

    check( bins != NULL );
    
    check( is_sorted_by( (STD_COMPARE)pspktBeSrcIP6Cmp,
                         bd_splitInput->entry,
                         bd_splitInput->entry_count,
                         sizeof(*bd->entry) ) );

    
    size_t entryCount = 0;
    size_t c = 0;
    size_t j = 0;
    for (PSPKT_BD** curBin = bins; *curBin != NULL; ++curBin) {
      check((*curBin)->entry_count > 0);
      size_t curDataSize = 0;
      for(i=0; i < (*curBin)->entry_count; ++i ) {
        check( bd_splitInput->entry[j].size == (*curBin)->entry[i].size );
        curDataSize += (*curBin)->entry[i].size;
        j++;
      }
      check( (*curBin)->data_size == curDataSize );
      entryCount += (*curBin)->entry_count;
      char* fileName = mprintf("tmp/testSrcSortedPart.%zu.bd",c);
      ++c;
      store_batchfile(PSPKT,fileName,*curBin);
      free(fileName);
    }
  
    check( bd_splitInput->entry_count == entryCount );

    PSPKT_BD* bd_merged = new_merged_batchdata(PSPKT,bins);

    check( bd_merged != NULL );
    check( bd_splitInput->entry_count == bd_merged->entry_count );

    store_batchfile(PSPKT,"tmp/testSrcSortedMerged.bd",bd_merged);

    for(i = 0; i < bd_merged->entry_count; ++i) {
      check( bd_merged->entry[i].size == bd_splitInput->entry[i].size );
    }

    for (PSPKT_BD** curBin = bins; *curBin != NULL; ++curBin) 
      delete_batchdata(PSPKT,*curBin);

    free(bins);

    check( compare_batchdata(PSPKT,bd_splitInput,bd_merged) == 0);

    delete_batchdata(PSPKT,bd_merged);

    /* second split algorithm test */
    size_t h = 9;
    PSPKT_BD* out[h+1];
    out[h] = NULL;

    check( bd_splitInput->entry_count % h != 0 );
    
    check( split_batchdata(PSPKT,bd_splitInput,h,out) == 0);
    
    for(i=0;i<h;++i) {
       check(out[i] != NULL);
       size_t curDataSize = 0;
       for(j=0; j < out[i]->entry_count; ++j ) {
         curDataSize += out[i]->entry[j].size;
       }
       check( out[i]->data_size == curDataSize );
    }
    
    bd_merged = new_merged_batchdata(PSPKT,out);
    check( bd_merged != NULL );
    check( bd_merged->entry_count == bd_splitInput->entry_count);

    check( compare_batchdata(PSPKT,bd_splitInput,bd_merged) == 0);

    for (PSPKT_BD** curBin = out; *curBin != NULL; ++curBin) 
      delete_batchdata(PSPKT,*curBin);


    delete_batchdata(PSPKT,bd_merged);
    delete_batchdata(PSPKT,bd_splitInput);

  end_test();




  begin_test("Shared Memory Batchdata Creation Test");
  
    ShmBd shmbd;
    
    check( shmbd_create( &shmbd,
                         "/testShmBD.bd",
                         bd->entry_count,
                         bd->stack_size)
                     == 0 );

    check( shmbd.entryCount == 0 );
    check( shmbd_dataSize(&shmbd) == 0 );
    check( shmbd.dataCapacity == bd->stack_size );

    for(i=0; i< bd->entry_count; ++i) {
      check( shmbd_append_be((BE*)&bd->entry[i],&shmbd) == 0 );
    } 

    check( shmbd.entryCount == bd->entry_count );
    check( shmbd_dataSize(&shmbd) == bd->stack_size );

    for(i=0; i< bd->entry_count; ++i) {
       check( shmbd_entrySize(i,&shmbd) == bd->entry[i].size );
       check( memcmp( shmbd_entryData(i,&shmbd),
                      bd->entry[i].data,
                      bd->entry[i].size )
                 == 0 );
    } 

  end_test();



  begin_test("Shared Memory Batchdata Load Test");
  
    ShmBd shmbd2;

    check( shmbd_load(&shmbd2,"/testShmBD.bd") == 0 );

    check( shmbd2.entryCount == bd->entry_count );

    for (i=0; i<bd->entry_count; ++i) {
      check( shmbd_entrySize(i,&shmbd) == shmbd_entrySize(i,&shmbd2) );
      check( memcmp( shmbd_entryData(i,&shmbd),
                     shmbd_entryData(i,&shmbd2),
                     bd->entry[i].size )
                 == 0 );
    } 

    check( shmbd_unload(&shmbd2) == 0);

  end_test();


  begin_test("Shared Memory Batchdata Unlink Test");

    check( shmbd_unlink(&shmbd) == 0);
    check( shmbd.name[0] == 0 ); 
    check( shm_open("/testShmBD.bd",O_RDONLY,0) == -1 );
    errno = 0; 
  end_test();




  begin_test("Shared Memory Batchdata From Batchdata Test");
  
    check( shmbd_from_batchdata(&shmbd2,"/testShmBD2.bd",(BD*)bd) == 0 );

    for(i=0; i< bd->entry_count; ++i) {
       check( shmbd_entrySize(i,&shmbd2) == bd->entry[i].size );
       check( memcmp( shmbd_entryData(i,&shmbd2),
                      bd->entry[i].data,
                      bd->entry[i].size )
                 == 0 );
    }

    check( shmbd_dataSize(&shmbd2) == bd->data_size );
    check( shmbd_unlink(&shmbd2) == 0 );

  end_test();


  begin_test("Shared Memory Batchdata Put Test");
 
    ShmBd shmbd3;

    check( shmbd_put_batchdata((BD*)bd,"/testShmBD.bd") == 0 );

    check( shmbd_load(&shmbd3,"/testShmBD.bd") == 0 );

    check( shmbd3.entryCount   == shmbd2.entryCount   );
    check( shmbd3.dataCapacity == shmbd2.dataCapacity );
    check( shmbd_dataSize(&shmbd3) == shmbd_dataSize(&shmbd2) );
    check( memcmp(shmbd3.data,shmbd2.data,shmbd_dataSize(&shmbd3)) == 0 );

    check( shmbd_unload(&shmbd3) == 0 );
    check( shmbd_unload(&shmbd2) == 0 );
    check( shmbd_unlink(&shmbd3) == 0 );


  end_test();



  
  begin_test("Batchdata From Shared Memory Batchdata Test");
    
    bd_copy = new_batchdata_from_shmbd(PSPKT,&shmbd);

    check( bd_copy != NULL );
    check( bd_copy->entry_count == shmbd.entryCount );
    check( bd_copy->entry_count == bd->entry_count);
    check( bd_copy->data_size == bd->data_size );
    check( bd_copy->entry_capacity == bd->entry_capacity);
    check( compare_batchdata(PSPKT,bd,bd_copy) == 0);

    delete_batchdata(PSPKT,bd_copy);

  end_test();



  delete_batchdata(PSPKT,bd);

}
