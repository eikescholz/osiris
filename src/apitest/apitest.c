#include <sys/types.h>
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "psd.h"
#include "batchdata.h"
#include "sysutils.h"
#include "libmicrotime_test.h"
#include "batchdata_test.h"
#include "crypto_test.h"
#include "deadlineQueue_test.h"
#include "eideticSocket_test.h"

int apitest_main(int argc,char** argv) {

   begin_test("Core Initialization Test");
     ps_core_init("ApiTest");
   end_test();

   libmicrotime_test_main();
   batchdata_test_main(argc,argv);
   crypto_test_main(argc,argv);
   deadlineQueue_test_main(argc,argv);
   eideticSocket_test_main(argc,argv);

   return 0;

}





int main(int argc, char**  argv) {

  if (sodium_init() < 0) {
    fprintf(stderr,"ERROR (apitest): Could not initailize libsodium");
    return EXIT_FAILURE;
  } 

  if ( argc < 2 ) {
    fprintf(stderr,"ERROR: First argument must be test directroy,\n "
                   "  containing a copy of all neccssary input data files\n");
    return EXIT_FAILURE;
  }

  if ( chdir(argv[1]) < 0 ) {
    fprintf(stderr,"Could not chdir to test directory %s (%s)",
                    argv[1],strerror(errno));
    return EXIT_FAILURE;
  }  

  char* seedStr = (argc > 2) ? argv[2] : alloc_random_test_rgseed();

  set_initial_testrgseed(seedStr); 
 
  if (argc <= 2) free(seedStr);

  apitest_main(argc,argv);

  return EXIT_SUCCESS;

}

