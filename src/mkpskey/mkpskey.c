/* This makes simple key pairs
 * for the libsodium crypto box and stores it into files.
 */

#include "uninterruptableIO.h"
#include "cryptoutils.h"


#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sodium.h>

int decrypt_main() {

  unsigned char pk[crypto_box_PUBLICKEYBYTES]; /* Alice's public key */
  unsigned char sk[crypto_box_SECRETKEYBYTES];
  
  if ( lock_mem(pk,crypto_box_PUBLICKEYBYTES) < 0) {
    fprintf(stderr,"ERROR (mkpskey): Could not lock public key memory (%s)\n",
                    strerror(errno));
    return EXIT_FAILURE;
  } 

  if ( lock_mem(sk,crypto_box_SECRETKEYBYTES)  < 0) {
    fprintf(stderr,"ERROR (mkpskey): Could not lock public key memory (%s)\n",
                    strerror(errno));
    return EXIT_FAILURE;
  } 

  crypto_box_keypair(pk, sk); /* can not fail? */

  if (store_sk("decrypt.key",sk) < 0 ) {
    perror("Could not store private key 'decrypt.key'");
    return EXIT_FAILURE;
  }

  if (store_pk("encrypt.key",pk) < 0 ) {
    perror("Could not store public key 'encrypt.key'");
    return EXIT_FAILURE;
  } 
 
  return EXIT_SUCCESS; 
  
}

int sign_main() {

  unsigned char pk[crypto_sign_PUBLICKEYBYTES]; /* Alice's public key */
  unsigned char sk[crypto_sign_SECRETKEYBYTES];
  
  if ( lock_mem(pk,crypto_box_PUBLICKEYBYTES) < 0) {
    fprintf(stderr,"ERROR (mkpskey): Could not lock public key memory (%s)\n",
                    strerror(errno));
    return EXIT_FAILURE;
  } 

  if ( lock_mem(sk,crypto_box_SECRETKEYBYTES)  < 0) {
    fprintf(stderr,"ERROR (mkpskey): Could not lock public key memory (%s)\n",
                    strerror(errno));
    return EXIT_FAILURE;
  } 

  crypto_sign_keypair(pk, sk); /* can not fail? */

  if (_store_key("sign.key",SK_MODE,sk,crypto_sign_SECRETKEYBYTES)  < 0 ) {
    perror("Could not store private key 'sign.key'");
    return EXIT_FAILURE;
  }

  if (_store_key("verify.key",PK_MODE,pk,crypto_sign_PUBLICKEYBYTES)  < 0 ) {
    perror("Could not store public key 'verify.key'");
    return EXIT_FAILURE;
  } 
 
  return EXIT_SUCCESS; 
  
}




int rmac_main() {

  unsigned char k[RMAC_KEY_SIZE]; 
  
  if ( lock_mem(k,RMAC_KEY_SIZE) < 0) {
    fprintf(stderr,"ERROR (mkpskey): Could not lock key");	
    return EXIT_FAILURE;
  } 

  randombytes_buf(k,RMAC_KEY_SIZE);

  if (_store_key("rmac.key",SK_MODE,k,RMAC_KEY_SIZE) < 0 ) {
    perror("Could not store private key 'sign.key' ");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS; 
  
}


int main(int argc,char** argv) {

  int ret = EXIT_SUCCESS;

  if (sodium_init() < 0) {
    fprintf(stderr,"ERROR (mkpskey): Could not initialize libsodium");
    return EXIT_FAILURE;
  }  

  if (argc == 3) {	

    char oldwd[PATH_MAX];
    if (getcwd(oldwd,PATH_MAX) == NULL) {
      fprintf(stderr,"ERROR (mkpskey): Could get working directory (%s) "
                    ,strerror(errno));
      return EXIT_FAILURE;
    }  

    if (chdir(argv[2]) < 0) {
      fprintf(stderr,"ERROR (mkpskey): Could not access %s (%s) "
                    ,argv[2],strerror(errno));
      return EXIT_FAILURE;
    }  

    if (strcmp(argv[1],"decrypt") == 0) ret = decrypt_main();
    if (strcmp(argv[1],"sign") == 0) ret = sign_main();
    if (strcmp(argv[1],"rmac") == 0) ret = rmac_main();

    if (chdir(oldwd) < 0) {
      fprintf(stderr, "WARNING (mkpskey): Changing back to old working" 
                                         "directors failed. (%s)\n"
                    ,strerror(errno));
    }  

    return ret;
  }

  fprintf(stderr,
            "INVALID USAGE\n"
	    "   FIXME: ..." );

  return EXIT_FAILURE; 
  
}

