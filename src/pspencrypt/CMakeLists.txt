cmake_minimum_required(VERSION 2.8.9)
project(pspencrypt C)
 
file(GLOB SOURCES "${CMAKE_SOURCE_DIR}/src/pspencrypt/*.c")
message($SOURCES) 
add_executable(pspencrypt ${SOURCES})
