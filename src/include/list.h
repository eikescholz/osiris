/* There is no C99 or posix standard interface to this
 * 
 * NOTE: List are supposed to not be circular
 *
 */

#ifndef LIST_H
#define LIST_H
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct List_t {
  struct List_t* tail;
  void*   head;
};

typedef struct List_t* List;

/* the empty list will be called NIL
 * and is defined as a non null constant
 * for error dedection on allocation 
 */

#define NIL ((List)-1) 


static inline List cons(void* head,List tail) {

   List ret = malloc(sizeof(*ret));
   if (ret == NULL) return NULL;
   ret->head = head;
   ret->tail = tail;
   return ret;
}

static inline bool isNil(const List l) {
  return (l == NIL);
}

static inline void* head(const List ls) {
  if (isNil(ls) ) {
    printf("PANIC: used head on empty list");
    exit(EXIT_FAILURE); }
  return ls->head;
}



#if 0
static inline size_t headSize(const List ls) {
  if (isNil(ls) ) {
    printf("PANIC: used headSize on empty list");
    exit(EXIT_FAILURE); }
  return ls->headSize;
}
#endif

static inline List tail(const List ls) {
  return ls->tail;
}

static inline size_t listLength(const List ls) {
  size_t i = 0;
  for (List cs = ls; cs != NIL; cs = tail(cs)) ++i;
  return i;
}


/* *not* a deep delete */
static inline void deleteList(List* next) {  
  
   if (*next == NIL) return;

   do {
      List cur = *next;
      *next = tail(cur);
      free(cur); }
   while ( *next != NIL );

}

static inline void** allocArrayFromList(const List ls, size_t n) {
   if (n == 0) return NULL;

   void** data = malloc(n*sizeof(void*));
   if (data == NULL) return NULL;
   
   size_t i = 0;
   for (List cs = ls; cs != NIL; cs = tail(cs)) {
     if (i >= n) break;
     data[i] = cs->head;
     ++i;
   }

   return data;
 
}	

#endif
