#ifndef PS_CORE_H
#define PS_CORE_H
/* master include for the persistent socket core API */

#include <sys/wait.h>
#include "psglobals.h"
#include "psd.h"
#include "sysutils.h"
#include "cryptoutils.h"

#define PSCORE_DEFAULT_SOCKTIMEOUT 100000 /* 100 milli seconds */
#define PSCORE_MAX_BIN_SIZE 750


_Static_assert( sizeof(PSS) + PSPKT_RMAC_SIZE == PSPKT_FOOTER_SIZE, 
                 "Static signature footer size invalid" );


/* TODO: Find a proper place for the following typedef */

typedef BD_T(IP6Addr) IP6Addr_BD;

struct psd_proof_t {
  unsigned char hash[HASH_SIZE];
  uint64n_t signatureCount; /* for sender faultTol */
  PSS signature[1]; /* actually variably sized, with size faultTol*/ 
} __attribute__ ((packed));

typedef struct psd_proof_t PSPRF;

typedef BE_T(PSPRF) PSPRF_BE;
typedef BD_T(PSPRF) PSPRF_BD;

static inline size_t psd_proofSize(size_t sigCount) {
  return sizeof(struct psd_proof_t) - sizeof(PSS) + sigCount*sizeof(PSS) ;
}

struct verified_psd_t { 
  PS_D*  datagram;
  size_t replicationCount; /* for receiver faultTol */ 
  struct psd_proof_t proof;
} __attribute__ ((packed));

typedef struct verified_psd_t VPSD;

typedef BE_T(VPSD) VPSD_BE;
typedef BD_T(VPSD) VPSD_BD;

static inline size_t vpsd_size(size_t sigCount) {
  return sizeof(struct verified_psd_t) - sizeof(PSS) + sigCount*sizeof(PSS);
}

struct deadlineQueueEntry_t {
  MicroTime deadline;
  IP6Addr addr;
  uint64_t port;
  PSPKT_BD* bd;
} __attribute__ ((packed)) ;

typedef struct deadlineQueueEntry_t DQE;
typedef BE_T(DQE) DQE_BE;
typedef BD_T(DQE) DQE_BD;

typedef int(*DeadlineEntryHandler)( const char* deadlineDirPath,
                                    DQE* de );

int ps_parallel_process_deadlineEntries(
        const char* deadlineDirPath,
        MicroTime deadline,
        DeadlineEntryHandler entry_main,
        char* handlerName );

DQE_BD* ps_deadlineQueue_load( 
           const char* deadlineDirPath,
           MicroTime deadline
         );

int ps_psd_cmp(const PS_D* a,const PS_D* b);

int vpsdBe_cmp(const VPSD_BE *a, const VPSD_BE *b );

int ps_pspktBe_cmp(const PSPKT_BE *a, const PSPKT_BE *b );
 
void delete_DQE_BD(DQE_BD* dqe);

int ps_deadlineQueue_load_entry(
            DQE* ret,
            const char* deadlineDirPath,
            MicroTime deadline,
            IP6Addr* addr,
            uint64_t port
          ); 

int ps_deadlineQueue_store( 
         PSPKT_BD* bd,
         const char* deadlineDirPath, 
         IP6Addr* dst_addr,
         uint64_t dst_port
         ); 

int ps_deadlineQueue_unlink( 
           const char* deadlineDirPath,
           MicroTime deadline
         );



static inline void mk_psd_nonce(psd_nonce_t* n) {

  if ( crypto_box_NONCEBYTES > sizeof(psd_nonce_t) ){
    fprintf(stderr,"INTERNAL ERROR: Used crypto algorithm uses nonce"
                   "with more then 256 bits! This is a bug. If you see"
                   "this message file a bug report!\n");
    exit(EXIT_FAILURE);
  }

  randombytes_buf(n,sizeof(*n));  

}


/* we use that often */

struct packet_info_t {
  unsigned char signerPK[PSD_PK_SIZE];
  struct in6_addr replicatorAddr; 
  PSPKT* packet;
} __attribute__ ((packed));


typedef struct packet_info_t PI;
typedef BD_T(PI) PI_BD;
typedef BE_T(PI) PI_BE;



int ps_service_init(const char* mainIdent,int argc, char** argv);
int ps_core_init(const char* mainIdent);
int ps_receiver_init(const char* mainIdent,int argc, char** argv);

/* some common components with logging */
int ps_setenv_PSSRCIP( IP6Addr* addr );

int ps_setenv_PSSRCPORT( uint64_t port );

int ps_setenv_PSDSTIP( IP6Addr* addr );

int ps_setenv_PSDSTPORT( uint64_t port );

int ps_setenv_PSDINTERVAL( MicroTime dt );
 
bool ps_getenv_PSDSTIP( IP6Addr* dst_addr );

bool ps_getenv_PSDSTPORT( uint64_t* dst_port );

bool ps_getenv_PSSRCIP( IP6Addr* addr );

bool ps_getenv_PSSRCPORT( uint64_t* port );

bool ps_getenv_PSDINTERVAL( MicroTime* dt );
 
PSPKT_BD* ps_read_serviceInput();

int ps_read_receiverInput(PS_D_BD **pkts, PSPRF_BD **prfs);



bool ps_verify_pktbd_dst_addr( const PSPKT_BD* bd, const IP6Addr* dst_addr );
bool ps_verify_psdbd_dst_addr( const PS_D_BD* bd, const IP6Addr* dst_addr );

bool ps_verify_pktbd_src_addr( const PSPKT_BD* bd, const IP6Addr* src_addr );
bool ps_verify_psdbd_src_addr( const PS_D_BD* bd, const IP6Addr* src_addr );

bool ps_verify_pktbd_src_port( const PSPKT_BD* bd, const uint64_t src_port );
bool ps_verify_psdbd_src_port( const PS_D_BD* bd, const uint64_t src_port );

bool ps_verify_pktbd_dst_port( const PSPKT_BD* bd, const uint64_t dst_port );
bool ps_verify_psdbd_dst_port( const PS_D_BD* bd, const uint64_t dst_port );


int ps_drop_privileges();


PSPKT_BD* ps_merge_and_free(PSPKT_BD** bins );

PSPKT_BD** ps_alloc_and_split( PSPKT_BD* bd, 
                               int (*cmp)(const PSPKT_BE*,const PSPKT_BE*),
                               size_t* retCount );

int ps_load_auth_pk(
        uint8_t* pk,
        const IP6Addr* addr,
        uint64_t port );

int ps_load_auth_sk(
        uint8_t* sk,
        const IP6Addr* addr,
        uint64_t  port );

int ps_load_port_sk(
        uint8_t* sk,
        const IP6Addr* addr,
        uint64_t port );

int ps_load_node_sk(
        uint8_t* sk,
        const IP6Addr* addr );

extern char** environ;

pid_t ps_fork_exec(char* const exec, char* const arg1name, const BD* arg1bd,
                                     char* const arg2name, const BD* arg2bd );

void ps_mk_sockName(
     char* name,
     IP6Addr* addr,
     uint64_t port);

void* ps_malloc(size_t size);

void ps_mk_portShmbdName(
     char* name,
     MicroTime time,
     IP6Addr* dst_addr,
     uint64_t dst_port,
     const char* suffix );


void ps_mk_nodeShmbdName(
     char* name,
     MicroTime time,
     IP6Addr* addr,
     const char* suffix );


void ps_mk_recvShmbdName(
     char* name,
     MicroTime time,
     IP6Addr* dst_addr,
     uint64_t dst_port,
     const char* suffix ); 

int ps_load_sockTimeout(
        MicroTime* ret,
        const IP6Addr* addr,
        uint64_t port );


pid_t ps_fork(const char* logName);


void ps_execve(const char* filename,char *const argv[],char* const envp[]);


PI_BD* sortBySrc_normalize_and_load_mlocked_pks( 
        PSPKT_BD* bd, 
        struct in6_addr* dst_addr );

VPSD_BD* ps_verify_signatures ( 
           PI_BD* pi_bd
         ); 


VPSD_BD* ps_verify( 
    PSPKT_BD*  bd, 
    IP6Addr* dst_addr, 
    const char* deadlineEntryPath /* log info */
   );

size_t ps_inplace_strip_too_small(PSPKT_BD* bd); 

size_t ps_inplace_strip_duplicates(PSPKT_BD* bd); 

void ps_load_clusterAddr( 
        IP6Addr* ret,
        const IP6Addr* addr 
        );

size_t ps_load_clusterByzFaultTol( const IP6Addr* senderAddr );

IP6Addr_BD* ps_load_clusterNodes( const IP6Addr* senderAddr );

size_t ps_load_clusterNodeCount( const IP6Addr* clusterAddr );


void ps_load_packetSender( IP6Addr* ret, const PSPKT* pkt);

void ps_load_packetReceiver( IP6Addr* ret, const PSPKT* pkt);

int ps_try_load_packetReplicator( IP6Addr* ret, const PSPKT* pkt );

void ps_load_packetSigner( IP6Addr* ret, const PSPKT* pkt );

int ps_mk_packetDatagramHash(uint8_t* hash, const PSPKT* pkt);


void fprint_pspkt( FILE* strm, const PSPKT* pkt); 

int ps_baseIntervalTimeCmp(const MicroTime* a, const MicroTime* b );

size_t ps_inplace_normalize_and_sign( 
        PSPKT_BD* bd, 
        const unsigned char* sk,
        const char* mainIdent ); 

pid_t ps_waitpid(pid_t cld,const char *name );

void fprint_pspkt_entry (
      FILE* strm,
      PSPKT* pkt,
      size_t entrySize );

void fprint_psd_entry (
      FILE* strm,
      PS_D* pkt,
      size_t entrySize );

void fprint_hash(FILE*strm, const uint8_t* data);
bool is_complete_pspkt(void* data, size_t dataSize);


void fprint_psd_header(
      FILE* strm,
      const PS_D* psd);


void fprint_signature( 
       FILE* strm,
       const PSS* sign );


void fprint_proof(
      FILE* strm,
      const PSPRF* prf );


#endif
