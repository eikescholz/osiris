/* Defines Batch Data Hanling Interface */
#ifndef BATCHDATA_H
#define BATCHDATA_H
#include "uninterruptableIO.h"
#include "list.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <bsd/stdlib.h> /* for mergesort, I'm to lazy to copy */
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

/* just data - not associated with a file 
 * A batch has allways a predefined numer of entries.
 * The entries may be empty with size 0 and/or no
 * entry data pointer NULL
 */

#define type_check(t,e) (  ((struct { t val; }){e}).val  )

#define BE_T(t) struct BE_ ## t {         /* batch data entry */\
  size_t size;  \
  t* data;        /* pointers to entry data */\
} __attribute__ ((packed)) 

typedef BE_T(uint8_t) BE;


#define BD_T(t) struct BD_ ## t {         /* a batch data, upper case like DIR */\
  size_t entry_count;\
  size_t entry_capacity;\
  struct BE_ ## t *entry;        /* pointers to entry data */\
\
  size_t data_size; /* size of all linked data */  \
  /* a simple stack heap, never frees stuff */\
  size_t stack_capacity;\
  size_t stack_size;\
  unsigned char*  stack;\
} 

typedef int (*BE_COMPARE)(const BE*,const BE*);

typedef int (*STD_COMPARE)(const void*,const void*);


typedef BD_T(uint8_t) BD;


static inline BE* link_batchentry(size_t i,BD* bd,void* data,size_t n) {
  
  if (i >= bd->entry_capacity) {
    errno = EBADSLT;
    return NULL;
  }  

  if (i >= bd->entry_count) {
    bd->entry_count = i+1;
  } else {
    bd->data_size -= bd->entry[i].size;
  } 
  
  bd->data_size += n;

  BE* ret = &bd->entry[i];

  ret->data = data;
  ret->size = n;

  return ret;

}

#define add_batchentry(t,i,bd,data,n) \
        ((struct BE_ ## t *) _add_batchentry(i, \
                        (BD*)type_check(struct BD_ ## t*,bd),  \
                        (void*)type_check(t*,data), \
                        n ))


static inline BE* _add_batchentry(size_t i,BD* bd,const void* data,size_t n) {

  void* new_data; 
  BE* ret; 
  
  if (i >= bd->entry_capacity) {
    errno = EBADSLT;
    return NULL;
  }  
 
  if (i < bd->entry_count) {
    if ( bd->entry[i].size == n ) { /* may overwrite if sizes match */
      ret = &bd->entry[i];
      new_data = ret->data;
      goto COPY_DATA;
    } else {
      errno = EMSGSIZE;
      return NULL;
    }           
  }
 
  /* is there is space to allocate an entry ? */
  if (bd->stack_capacity - bd->stack_size < n) { 
    errno = ENOMEM;
    return NULL;
  }
  
  new_data = (uint8_t*)bd->stack + bd->stack_size;
  bd->stack_size += n;

  ret = link_batchentry(i,bd,new_data,n); 
  if (ret == NULL) return NULL;

COPY_DATA:

  if (data != NULL) memcpy(new_data,data,n);
 
  return ret;

}



static inline BE* _new_batchentry(BD* bd, const void* data, size_t n) {

  return _add_batchentry(bd->entry_count,bd,data,n);  

}

#define new_batchentry(t,bd,data,n) \
        ((struct BE_ ## t *) _new_batchentry( \
                        (BD*)type_check(struct BD_ ## t*,bd),  \
                        (const void*)type_check(const t*,data), \
                        n ))

static inline BE* new_batchentry_link(BD* bd, void* data, size_t n) {

  return link_batchentry(bd->entry_count,bd,data,n);  

}

/* creates an empty batch data structure with a capacity hint */

#define new_batchdata(t,ec,sz) \
        ((struct BD_ ## t *) _new_batchdata(ec,sz)) 

static inline BD* _new_batchdata( 
                size_t entry_capacity,
                size_t stack_capacity) { 

   BD* bd = malloc(sizeof(*bd));
   if (bd == NULL) {
     errno = ENOMEM;
     return NULL;
   }

   /* TODO: Rename entry_count to entryCount,
    *              entry_count to entryCapacity 
    *              stack_capacity to dataCapacity,
    *              and stack_size to dataSize 
    */
  
   bd->data_size = 0;   
   bd->entry_count = 0;
   bd->entry_capacity = entry_capacity;
   bd->stack_capacity = stack_capacity;
   bd->stack_size = 0;
   bd->entry = malloc( entry_capacity * sizeof(*bd->entry));
   if (bd->entry == NULL) {
     free(bd); 
     errno = ENOMEM;
     return NULL;
   }

   if (stack_capacity != 0) {
     bd->stack = malloc( stack_capacity );
     if (bd->stack == NULL) {
       free(bd->entry);
       free(bd);
       errno = ENOMEM;
       return NULL;
     }
   } else {
     bd->stack = NULL; /* borrowed data form somewhere else */
   }

   return bd;
}


#define delete_batchdata(t,bd) \
         (_delete_batchdata((BD*)type_check(struct BD_ ## t*,bd)))


static inline void _delete_batchdata(BD* bd) {
   if (bd != NULL) free(bd->entry);
   if (bd != NULL) free(bd->stack); /* for shallow copies this is NULL ! */
   free(bd);
}



#define be_has_equal_prev(t,i,bd,cmp) \
          _be_has_equal_prev(i, \
                        (BD*)type_check(struct BD_ ## t*,bd),\
                        (BE_COMPARE)be_compare_type_check(t,cmp) )



static inline 
bool _be_has_equal_prev( 
        const size_t i, 
        const BD* bd,
        int (*compare)(const BE*,const BE*) ) {
 
  if ( 0 < i && i < bd->entry_count ) {
    return ( compare(&bd->entry[i-1],&bd->entry[i] ) == 0 );
  }

  return false; /* returns false for out of bound indices */

}


#define be_skip_equal_span(t,iAddr,bd,cmp) \
          _be_skip_equal_span(iAddr, \
                        (BD*)type_check(struct BD_ ## t*,bd),\
                        (BE_COMPARE)be_compare_type_check(t,cmp) )



static inline
void _be_skip_equal_span( 
        size_t* _i, 
        const BD* bd,
        int (*compare)(const BE*,const BE*) ) {
  
  size_t i = *_i;

  while ( i + 1 < bd->entry_count 
          && (compare( &bd->entry[ i ] ,&bd->entry[ i + 1 ] ) == 0) ) {
   ++i;
  }

  ++i;

  *_i = i;
  
}


/* TODO: Define generic sizeof interface 
 * i.e. for a type t there is sizeof_t function
 * taking a pointer to a value as argument, 
 * so 
 */

#define is_in_batchdata(t,bd,data,size) \
          _is_in_batchdata( (BD*)type_check(struct BD_ ## t*,bd), \
                            (const uint8_t*)type_check(t*,data),\
                            size )

static inline
bool _is_in_batchdata(const BD* bd, const uint8_t* data, size_t size) {

   for (size_t i=0; i < bd->entry_count; ++i) {
     if (bd->entry[i].size == size) {
       if ( memcmp(bd->entry[i].data,data,size) == 0 ) return true; 
     } 
   }

   return false;

}



#if 0
static inline int set_batchdata_capacities(
        BD* bd,
        size_t new_entry_count,
        size_t min_capacity) { 
   
   void* new_entry;
   void* new_data;
   size_t required_capacity,new_capacity;

   if (bd == NULL) {
     errno = EINVAL;
     return -1;
   }
 
   if (new_entry_count == 0) {
     delete_batchdata(bd);
     return 0;
   }

   new_entry = realloc(bd->entry, new_entry_count * sizeof(*bd->entry));
   if (new_entry == NULL) {
     errno = ENOMEM;
     return -1;
   }

   if (new_entry_count != 0) {
     required_capacity  = bd->entry[new_entry_count-1].data - bd->data;
     required_capacity += bd->entry[new_entry_count-1].size;
   } else {
     required_capacity = 0; 
   }
 
   new_capacity = (required_capacity > min_capacity) ? required_capacity
                                                     : min_capacity ; 

   if (new_capacity != 0) {
     new_data = realloc(bd->data, new_capacity);
     if (new_data == NULL) {
       free(new_entry);
       errno = ENOMEM;
       return -1;
     }
   } else {
     free(bd->data);
     bd->data = NULL; /* borrowed data form somewhere else */
   }

   bd->data = new_data;
   bd->entry = new_entry;
   bd->entry_count = new_entry_count;

   return 0;
}


#endif 


/* elements of the list must be batch entries */
static inline BD* new_batchdata_from_list(const List ls) { 

  size_t len = 0;
  size_t size = 0; 

  for(List cs = ls; cs != NIL; cs = tail(cs)) {
    BE* be = head(cs); 
    size += be->size;
    len += 1;
  };

  BD* bd = new_batchdata(uint8_t,len,size);
  if (bd == NULL) return NULL;

  size_t i = 0;
  for(List cs = ls; cs != NIL; cs = tail(cs)) {
    BE* be = head(cs); 
    _add_batchentry(i,bd,be->data,be->size);
    ++i;
  };

  return bd;
}
 
/* read from file discrCould not bind    
 *
 * TODO: Use mmap
 *
 * TODO:
 *  The offset table can indicate filling status in the following sense.
 *  All unused entries are set to size 0. The last used entry can
 *  be changed in size, so updated, and thus be interpreted as
 *  "free" for update. So a cur entry count can be derived form
 *  an offset table (if actual entries are non-zero ... 
 *  Adding the entry before the last, just determines the size of
 *  both then ... OK. That's a nice very stupid format.  
 *
 *  Actually if the last offset is smaller then the file size
 *  that should indicate freeness ... 
 * 
 */

static inline BD* read_batchdata(int fd) {
  
  size_t i;
  uint64_t first_offset = 0;
  int status = uninterruptable_read(fd,&first_offset,sizeof(first_offset));
  if ( status ==  -1) return NULL;
  if ( first_offset < 2*sizeof(uint64_t) ) { errno = EIO; return NULL; } /* corrupted */  
  if ( first_offset % sizeof(uint64_t) != 0 ) { errno = EIO; return NULL; } /* corruption has no predefined errno ? */ 

  size_t entry_capacity = (first_offset/sizeof(size_t)) - 1;

  size_t* offset = malloc(first_offset); /* that is size for num of entries +1 for bathefile size */
  if (offset == NULL) return NULL;

  offset[0] = first_offset;

  /* read the rest of the offset table */
  status = uninterruptable_read( fd,
                                 &offset[1],
                                 entry_capacity*sizeof(uint64_t) );

  if ( status ==  -1) goto FAIL0;

  /* input validation for security reasons */
  for (i = 0; i < entry_capacity; ++i) {
    if (offset[i+1] < offset[i]) {
      errno = EINVAL; 
      goto FAIL0; }
  }

  size_t size = offset[entry_capacity]-offset[0];
 
  BD* bd = new_batchdata(uint8_t,entry_capacity,size);  
  if (bd == NULL) goto FAIL0;

  /* read entries */
  for(size_t i = 0; i < bd->entry_capacity; ++i) {
    size_t entrySize = offset[i+1] - offset[i];
    if (_add_batchentry(i,bd,NULL,entrySize) == NULL) {
      printf("Adding Batchentry Failed\n");
      goto FAIL1;}
    status = uninterruptable_read(fd,bd->entry[i].data,bd->entry[i].size);
    if (status == -1) goto FAIL1;
  }

  /* reconstruct addable entries if possible */
  for ( i = 0; i < bd->entry_capacity; ++i ) {
    if ( offset[entry_capacity-1-i] != offset[entry_capacity] ) break;
    bd->entry_count--;
  }

  free(offset);
  
  return bd;

  FAIL1:
   _delete_batchdata(bd);
  FAIL0:
   free(offset);

  return NULL;
  
}


#define load_batchfile(t,path) (struct BD_ ## t*)_load_batchfile(path)

static inline  BD* _load_batchfile(const char* path) {
  int fd = uninterruptable_flopen(path, O_RDONLY,0640);
  if (fd == -1) return NULL;
  BD* bd = read_batchdata(fd); /* assumes one batch per file */
  uninterruptable_close(fd);  /* TODO: Ok to ignore return status ? */
  return bd;
}

/* FIXME: ? Fix batch data format to be machine independent ? */
/* write a batch to a file descriptor */

#define write_batchdata(t,fd,bd) \
        _write_batchdata(fd,(BD*)type_check(struct BD_ ## t*,bd))
 
static inline int _write_batchdata(int fd,const BD* bd) {
      
  ssize_t status;
  size_t i;
  uint64_t offsetTable_size;
  size_t size;

  if (bd == NULL) {
     errno = EINVAL;
     return -1; }

  if (bd->entry_capacity == 0) return 0;

  offsetTable_size = (bd->entry_capacity+1) * sizeof(uint64_t);

  uint64_t* offset = malloc(offsetTable_size); 
  if (offset == NULL) return -1;
  
  offset[0] = offsetTable_size;

  for(i=0;i<bd->entry_capacity;++i) {
    size = (i<bd->entry_count) ? bd->entry[i].size : 0 ; 
    offset[i+1] = offset[i] + size; 
  }

  uninterruptable_write(fd,offset,offsetTable_size); 
  if (fd == -1) goto FAIL;

  for(i=0;i<bd->entry_count;++i) {
    status = uninterruptable_write(fd,bd->entry[i].data,bd->entry[i].size);
    if (status == -1) goto FAIL; }

  free(offset);

  return 0;   

  FAIL:
    free(offset);
    return -1; 
}


#define store_batchfile(t,path,bd) \
          _store_batchfile( path, \
                            (BD*)type_check(struct BD_ ## t*,bd) )


static inline int _store_batchfile(const char* path,const BD* bd) {
  int fd = uninterruptable_flopen(path, O_CREAT | O_TRUNC | O_WRONLY,0640);
  if (fd == -1) return -1;
  if ( _write_batchdata(fd,bd ) < 0) return -1;
  if ( fsync(fd) < 0 ) return -1;
  uninterruptable_close(fd);  /* TODO: Ok to ignore return status ? */
  return 0;
}


#define sort_batchdata(t,bd,cmp) \
          _sort_batchdata( \
                        (BD*)type_check(struct BD_ ## t*,bd),\
                        (BE_COMPARE)be_compare_type_check(t,cmp) )



#define be_compare_type_check(t,e) \
        (  ((struct { int (*val)(const struct BE_ ## t *,\
                                 const struct BE_ ## t *); }){e}).val  )

#define sort_batchdata(t,bd,cmp) \
          _sort_batchdata( \
                        (BD*)type_check(struct BD_ ## t*,bd),\
                        (BE_COMPARE)be_compare_type_check(t,cmp) )



/* stable inplace */

static inline int  _sort_batchdata(BD* bd,BE_COMPARE compar) {
  return mergesort( bd->entry,
                    bd->entry_count,
                    sizeof(*bd->entry),
                    (STD_COMPARE)compar);
}


static inline int batchentryCmp(const BE* a,const BE* b) {

  if (a->size < b->size) return -1;
  if (a->size > b->size) return 1;
  
  int r = memcmp( a->data, b->data, a->size );
 
  if ( r > 0 ) return 1;
  if ( r < 0 ) return -1;
  return 0;
}



static inline int  bd_inplace_strip_dupilcates( BD* bd )  {

  if (bd->entry_count == 0) return 0;

  if ( sort_batchdata(uint8_t,bd,batchentryCmp) == -1) return -1;

  size_t size = 0;
  size_t j = 0;
  for(size_t i = 0; i < bd->entry_count ; ++i ) {
    if ( batchentryCmp(&bd->entry[j],&bd->entry[i+1]) != 0) {
      size += bd->entry[i].size;
      link_batchentry(j,bd,bd->entry[i].data,bd->entry[i].size);     
      ++j;
    }
  }

  bd->entry_count = j;
  bd->entry_capacity = j;
  bd->stack_capacity = size;

  BE* new_entry = realloc(bd->entry,j*sizeof(*new_entry));
  if (new_entry != NULL) bd->entry = new_entry;

  /* reallocating data is a bit requires updating pointers so omitted */

  return j;

}



/* retuns NULL terminated array of batchdata whose entryies
 * are equal under the given compare function
 *
 * NODE: argument gets sorted
 */
#define alloc_split_batchdata(t,bd,cmp,n) \
          ((struct BD_ ## t**)_alloc_split_batchdata( \
                        (BD*)type_check(struct BD_ ## t*,bd),\
                        (BE_COMPARE)be_compare_type_check(t,cmp),\
                        n ))


static inline BD** _alloc_split_batchdata(
        BD* bd,
        BE_COMPARE compar,
        size_t* retCount ) {
  size_t i,j,k,bin_count,cur_bin;  
 
  if ( sort_batchdata(uint8_t,bd,compar) < 0) return NULL;

  /* count number of required bind */
  bin_count = 1;
  for(i = 1; i < bd->entry_count; ++i) {
    if ( compar(&bd->entry[i-1],&bd->entry[i] ) != 0) bin_count++;
  }

  BD** ret = malloc((bin_count+1)*sizeof(BD*));
  if (ret == NULL) return NULL;

  /* count size of every bin and allocate batchdata */
  cur_bin = 0;
  j = 1;
  for(i = 1; i < bd->entry_count; ++i) {
    if ( compar(&bd->entry[i-1],&bd->entry[i] ) != 0) {
      ret[cur_bin] = _new_batchdata(j,0);
      if (ret[cur_bin] == NULL) goto FAIL;
      ++cur_bin;
      j=1; }
    else {
      ++j;
    }
  }
  
  ret[cur_bin] = _new_batchdata(j,0);
  if (ret[cur_bin] == NULL) goto FAIL;
 
  assert(cur_bin+1 == bin_count);

  /* copy entries */
  k = 0;
  for(i=0; i < bin_count; ++i) {
    ret[i]->data_size = 0;
    for(j=0; j < ret[i]->entry_capacity; ++j) {
      ret[i]->entry[j] = bd->entry[k];
      ret[i]->data_size += bd->entry[k].size;
      ++k;
    }
    ret[i]->entry_count = ret[i]->entry_capacity;
  } 

  ret[i] = NULL;

  if (retCount != NULL) *retCount = bin_count;

  return ret;

FAIL:
  
  for (k=0;k<cur_bin;++k) _delete_batchdata(ret[k]);
  
  free(ret);
  
  return NULL; 

}


#define split_batchdata(t,bd,n,ret) \
                (_split_batchdata( \
                        (BD*)type_check(struct BD_ ## t*,bd),\
                        n,\
                        (BD**)type_check(struct BD_ ## t**,ret)))


static inline int _split_batchdata(BD* bd,size_t n,BD** part) {
  
  size_t i = 0; 
  
  if ( n == 0) {
    errno = EINVAL;
    return -1;
  }    

  size_t c = bd->entry_count; 
  size_t k = c/n;
  size_t r = c%n;

  //fprintf(stderr,"c %zu - k %zu - r %zu - n %zu\n",c,k,r,n);
  memset(part,0,n*sizeof(*part)); 

  for(size_t i=0; i < n ; ++i) {
    part[i] = _new_batchdata(k+1,0); 
  }

  for (i = 0; i < r; ++i  ) {
    if ( part[i] == NULL) {
      for (size_t j = 0; j < i; ++j) _delete_batchdata(part[j]);      
      errno = ENOMEM;
      return -1;
    }

    for (size_t j = 0; j < k+1; ++j) {
      part[i]->entry[j]   = bd->entry[i*(k+1)+j];      
      part[i]->data_size += bd->entry[i*(k+1)+j].size; 
    }
    part[i]->entry_count = k+1;
    part[i]->entry_capacity = k+1;
  }

  /* now : i == n-1 and the size of the last batch is */

  for (i = r; i < n; ++i  ) {
    if ( part[i] == NULL) {
      for (size_t j = 0; j < i; ++j) _delete_batchdata(part[j]);      
      errno = ENOMEM;
      return -1;
    }

    for (size_t j = 0; j < k; ++j) {
      part[i]->entry[j]   = bd->entry[r*(k+1)+(i-r)*k+j];      
      part[i]->data_size += bd->entry[r*(k+1)+(i-r)*k+j].size; 
    }
    part[i]->entry_count = k;
    part[i]->entry_capacity = k;
  }

  return 0;
}



#define new_merged_batchdata(t,bds) \
        ((struct BD_ ## t *) _new_merged_batchdata( \
                        (BD**)type_check(struct BD_ ## t**,bds) ))


static inline BD* _new_merged_batchdata(BD** bd){
  size_t c = 0;
  size_t s = 0;
  for( BD** cur = bd; *cur != NULL; ++cur ) {
    c += (*cur)->entry_count;
    for(size_t i=0;i< (*cur)->entry_count;++i) {
      s += (*cur)->entry[i].size;
    }
  }
 
  BD* ret = _new_batchdata(c,s);
  if (ret == NULL) return NULL;

  size_t j = 0; 
  for ( BD** cur = bd; *cur != NULL; ++cur ) {
    for ( size_t i = 0; i < (*cur)->entry_count ; ++i ) {
      _add_batchentry(j,ret,(*cur)->entry[i].data,(*cur)->entry[i].size);
      ++j;
    }
  }

  return ret;

}


#if 0 /* needed anymore */
/* TODO: clean up this interface */
/* BE_MAPS take a entry i in args and stores a result for entry i in 
 * results computed using data in arg. 
 * it returns the batchentry of the new result, or NULL on error 
 */
typedef void (*BE_INPLACE_MAP)(BE* be, void* arg);

static inline 
BD* bd_inplace_map(BE_INPLACE_MAP f,void* arg,const BD* bd,BD* ret) {
  
  size_t n = 0; /* number of successful maps */
  for(size_t i = 0; i < bd->entry_count ; ++i ) {
    f(arg,bd) != NULL ) n++ ; /* n used for error detection */
  }
 
  return ret;

}
#endif 

#define bd_inplace_strip(t,bd) \
          _bd_inplace_strip( \
                        (BD*)type_check(struct BD_ ## t*,bd) )

static inline size_t _bd_inplace_strip(BD* bd) { 
  if (bd == NULL) return 0;

  size_t size = 0;
  size_t j = 0;
  for(size_t i = 0; i < bd->entry_count ; ++i ) {
    if (bd->entry[i].data != NULL) {
      size += bd->entry[i].size;
      link_batchentry(j,bd,bd->entry[i].data,bd->entry[i].size);
      ++j;
    }     
  }

  bd->entry_count = j;
  bd->entry_capacity = j;
  bd->stack_capacity = size;

  BE* new_entry = realloc(bd->entry,j*sizeof(*new_entry));
  if (new_entry != NULL) bd->entry = new_entry;

  /* reallocating data is a bit requires updating pointers so omitted */

  return j;
}


#define compare_batchdata(t,a,b) \
          _compare_batchdata( \
                (BD*)type_check(struct BD_ ## t*,a),\
                (BD*)type_check(struct BD_ ## t*,b)   )


static inline int _compare_batchdata(const BD* a, const BD* b ) {

  if (a->entry_count > b->entry_count) return 1;
  if (a->entry_count < b->entry_count) return -1;

  for (size_t i=0; i < a->entry_count; ++i) {
     if (a->entry[i].size > b->entry[i].size) {
        printf("\nEntry %zu differ in size (%zu > %zu) !\n",i,
                 a->entry[i].size, b->entry[i].size );       
       return 1;
     }
     if (a->entry[i].size < b->entry[i].size) {
        printf("\nEntry %zu differ in size (%zu < %zu) !\n",i,
                 a->entry[i].size, b->entry[i].size );       
        
       return -1;
     }
     int r = memcmp(a->entry[i].data,b->entry[i].data,b->entry[i].size);
     if ( r != 0) {
       printf("\nEntry %zu differs!\n",i);
       return r;
     }
  }

  return 0;

}

#define shallow_copy_batchdata(t,bd) \
  (struct BD_ ## t*)_shallow_copy_batchdata( \
                (BD*)type_check(struct BD_ ## t*,bd)  )

static inline BD* _shallow_copy_batchdata(BD* bd) {

  BD* ret = _new_batchdata(bd->entry_count,0);
  if ( ret == NULL ) return ret;

  ret->entry_count = bd->entry_count;

  for (size_t i = 0; i< bd->entry_count; ++i) {
    ret->entry[i] = bd->entry[i];
  }

  ret->data_size = bd->data_size; 
 
  return ret;

}



#define deep_copy_batchdata(t,bd) \
          (struct BD_ ## t*)_deep_copy_batchdata( \
                        (BD*)type_check(struct BD_ ## t*,bd)  )


static inline BD* _deep_copy_batchdata(const BD* bd) {
  size_t i;

  BD* ret = _new_batchdata(bd->entry_count,bd->stack_size);

  for (i = 0; i< bd->entry_count; ++i) {
    if( _add_batchentry(i,ret,bd->entry[i].data,bd->entry[i].size) == NULL) 
     goto FAIL;
  }

  return ret;

FAIL:
  _delete_batchdata(ret);
  return NULL;
}


static inline void empty_batchdata(BD* bd) {
  bd->entry_count = 0;
  bd->data_size = 0;
  bd->stack_size = 0;
}




/* shared memory batchdata implementation
 * Uses a single block of mapped memory to hold everything position independent
 *
 * This is not good for sorting entries, but a BD representation can easily
 * be made. And sorted.  
 */


struct ShmBd_t {         /* a batch data, upper case like DIR */
  uint64_t entryCount;   /* only for creation and filling up */    
  uint64_t dataCapacity; 
  uint8_t* data; /* has the format of a store batchdata file */
  char name[NAME_MAX+1];   
};

typedef struct ShmBd_t ShmBd;

static inline bool shmbd_name_valid(const char* name ) {

  if (    strlen(name) == 0
       || name[0] != '/' 
       || strlen(name) > NAME_MAX
       || strchr(&name[1],'/') != NULL ) {          
    return false;
  }
 
  return true;
  
}


static inline const uint64_t* shmbd_offset(const ShmBd* shmbd) {
  return (const uint64_t*)shmbd->data; 
}


static inline uint64_t shmbd_entryCapacity(const ShmBd* shmbd) {
  if (shmbd->dataCapacity == 0) return 0; /* nothing there */
  const uint64_t* offset = shmbd_offset(shmbd); 
  return (offset[0]/sizeof(*offset))-1; 
}


static inline uint64_t shmbd_entrySize(uint64_t i,const ShmBd* shmbd) {
  const uint64_t* offset = shmbd_offset(shmbd); 
  return offset[i+1]-offset[i];
}


static inline uint64_t shmbd_dataSize(const ShmBd* shmbd) {
  if (shmbd->entryCount == 0) return 0;
  const uint64_t* offset = shmbd_offset(shmbd); 
  uint64_t n = shmbd->entryCount;
  return offset[n]-offset[0];
}

/*
static inline uint64_t shmbd_size_of_block(ShmBd* shmbd) {
  uint64_t n = shmbd->entryCount;
  return (n+1)*sizeof(uint64_t)+shmbd_size_of_data(shmbd);
}
*/

static inline uint8_t* shmbd_entryData(uint64_t i,ShmBd* shmbd) {
  uint64_t* offset = (uint64_t*)shmbd->data;
  return shmbd->data + offset[i];
}

/* batchdata created with this can not be deep deleted.
 * the content must be unmapped */


/* TODO: Add type check interface to shmbd */
#define new_batchdata_from_shmbd(t,shmbd) \
        ((struct BD_ ## t *) _new_batchdata_from_shmbd(shmbd)) 


static inline BD* _new_batchdata_from_shmbd(ShmBd* shmbd) { 

  uint64_t n = shmbd->entryCount;  

  BD* bd = _new_batchdata( n, shmbd_dataSize(shmbd) );
  if (bd == NULL) return NULL;

  /*set input batchdata to full to avoid surprises*/
  bd->stack_size = shmbd_dataSize(shmbd); 
  bd->entry_count = n;

  for(uint64_t i=0; i < n; ++i) {
    bd->data_size += shmbd_entrySize(i,shmbd);
    bd->entry[i].size = shmbd_entrySize(i,shmbd);
    bd->entry[i].data = shmbd_entryData(i,shmbd);
  };

  return bd;
}




static inline int shmbd_append(ShmBd* shmbd,void* data, size_t size) {

  if ( size  > shmbd->dataCapacity - shmbd_dataSize(shmbd) ) {
    errno = ENOMEM;
    return -1;
  }

  uint64_t* offset = (uint64_t*)shmbd->data; 

  uint8_t* dst = shmbd_entryData(shmbd->entryCount,shmbd);

  uint64_t i = shmbd->entryCount++;
 
  offset[i+1] = offset[i] + size;
 
  memcpy(dst, data, size); 

  assert( shmbd_entrySize(i,shmbd) == size );

  return 0;

}




static inline int shmbd_append_be(BE* be,ShmBd* shmbd) {

  return shmbd_append(shmbd,be->data,be->size);

}

/* its called take, since it unlinks the object once it is opened. */


static inline int shmbd_load_from_fd(
        ShmBd* ret,
        int fd ) {

  struct stat st;
 
  ret->name[0] = 0;
 
  if ( fstat(fd,&st) < 0 ) return -1;
 
  ret->data = mmap( NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  if ( ret->data == MAP_FAILED ) return -1; 

  const uint64_t* offset = shmbd_offset(ret); 
  
  ret->entryCount = (offset[0]/sizeof(*offset))-1; 

  ret->dataCapacity = shmbd_dataSize(ret);

  return 0;
}


static inline int shmbd_mmap_file_ro(
        ShmBd* ret,
        const char* path ) {

  int fd;
  struct stat st;

  ret->name[0] = 0;
 
  fd = uninterruptable_flopen(path,O_RDONLY,0);
  if (fd == -1) return -1; 
 
  if ( fstat(fd,&st) < 0 ) return -1;
 
  ret->data = mmap(NULL,st.st_size,PROT_READ,MAP_SHARED,fd,0);
  if ( ret->data == MAP_FAILED ) return -1; 
  
  if ( uninterruptable_flock(fd,LOCK_UN) < 0 ) return -1;
  
  const uint64_t* offset = shmbd_offset(ret); 
  
  ret->entryCount = (offset[0]/sizeof(*offset))-1; 

  ret->dataCapacity = shmbd_dataSize(ret);

  uninterruptable_close(fd);

  return 0;
}
 

static inline int shmbd_unload( ShmBd* ret ) {
  uint64_t count = shmbd_entryCapacity(ret);
  uint64_t msize = (count+1)*sizeof(uint64_t)+ret->dataCapacity;
 
  if ( munmap(ret->data,msize) < 0 ) return - 1;
 
  return 0; 
}



/* its called take, since it unlinks the object once it is opened. */


static inline int shmbd_load(
        ShmBd* ret,
        const char* name ) {

  int fd;
 
  if (! shmbd_name_valid(name) ) {
    errno = EINVAL;
    return -1;
  }
  
  fd = shm_open(name,O_RDWR,0640);
  if (fd == -1) return -1; 

  if ( shmbd_load_from_fd(ret,fd) ) return -1;

  strncpy(ret->name,name,NAME_MAX+1);

  if ( uninterruptable_close(fd) < 0 ) {
    shmbd_unload(ret); /* this really should not be able to fail */
    return -1;
  }

  return 0;
}
 


static inline int shmbd_create(
        ShmBd* ret,
        const char* name, 
        uint64_t entryCapacity,
        uint64_t dataCapacity  ) {
  
  if (! shmbd_name_valid(name) ) {
    errno = EINVAL;
    return -1;
  }

  if ( entryCapacity == 0 ) {
    errno = EBADSLT;
    return -1;
  }
  
  mode_t mask = umask(0);

  int fd = shm_open(name,O_RDWR|O_CREAT|O_EXCL,0660);
  if (fd == -1) return -1; 

  umask(mask);

  uint64_t offsetTableSize = (entryCapacity+1)*sizeof(uint64_t);
  uint64_t msize = offsetTableSize + dataCapacity;

  if ( ftruncate(fd,msize) == -1 ) return -1;

  ret->data = mmap(NULL,msize,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  if ( ret->data == MAP_FAILED ) return -1; 

  strcpy(ret->name,name);

  ret->entryCount = 0;
  ret->dataCapacity = dataCapacity;

  if ( uninterruptable_close(fd) < 0 ) {
    shmbd_unload(ret); /* this really should not be able to fail */
    return -1;
  }

  uint64_t* offset = (uint64_t*)ret->data; 

  offset[0] = offsetTableSize ;
  
  return 0;
 
}


static inline int shmbd_from_batchdata(
   ShmBd* shmbd,
   const char* name,
   const BD* bd) {

  if ( bd->entry_count == 0 ) {
    errno = EBADSLT;
    return -1;
  }
 
  if ( shmbd_create(  shmbd,
                      name ,
                      bd->entry_count,
                      bd->data_size )
          == -1 ) return -1;

  for(size_t i=0; i < bd->entry_count; ++i) {
    if ( shmbd_append_be(&bd->entry[i],shmbd) == -1 ) {
      int tmp = errno;
      shmbd_unload(shmbd);
      errno = tmp;
     return -1;
    }
  } 

  return 0;
}


static inline int shmbd_put_batchdata(
   const BD* bd,             
   const char* name ) {

  ShmBd shmbd;

  if ( shmbd_from_batchdata( &shmbd, name , bd) == -1 ) return -1;

  assert(bd->entry_count == shmbd.entryCount);
  
  shmbd_unload(&shmbd);

  return 0;

}



static inline int shmbd_unlink( ShmBd* ret ) {

  /* if already unlinked do nothing */
  if ( ret->name[0] == 0 ) return 0;
  
  if ( shm_unlink(ret->name) < 0 ) return - 1;
  ret->name[0] = 0; /*clear name to indicate that it is unlinked */

  return 0;
}

 



#endif
