#ifndef CRYPTOUTILS_H
#define CRYPTOUTILS_H

#include "microtime.h"

#include <sysutils.h>
#include <sodium.h>
#include <sys/stat.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/mman.h>

/* TODO: Make proper library 
 * For this output signature, and mac sizes are fixed. 
 */

#define HASH_SIZE 64  /* also used for raw signature data size */ 

#define RMAC_KEY_SIZE 32 /* TODO: Not a constant in new lib interface */

#include "uninterruptableIO.h"


/* public keys are other readable, groups usually are limited in size
 * so that puting them into a separate group asks for trouble */
 
#define PK_MODE (0440|S_IFREG)
#define SK_MODE (0400|S_IFREG)

#define PSD_DSK_SIZE crypto_box_BEFORENMBYTES
#define PSD_SK_SIZE crypto_box_SECRETKEYBYTES
#define PSD_PK_SIZE crypto_box_PUBLICKEYBYTES

#define RG_SEEDBYTES ( crypto_stream_chacha20_NONCEBYTES + \
                       crypto_stream_chacha20_KEYBYTES)


static inline void pseudo_randombytes(
    void* buf, 
    unsigned long long n,
    const unsigned char* seed) {
  const unsigned char* nonce = &seed[0];
  const unsigned char* key   = &seed[crypto_stream_chacha20_NONCEBYTES];
  crypto_stream_chacha20(buf,n,nonce,key);
}


static inline void inc_seed(unsigned char* seed) {
  sodium_increment(seed,RG_SEEDBYTES); 
}
 


#if 0 /* interface proposal not jet implemented */

/* we want the algorithms to be a given by a carefully human
 * managed and printable list. So the number of algorithms
 * can be, in fact, as motivation, should be quite limited
 * 
 * So we take 16 bit identifies, where the first
 * three bits indicate classes. the 13th bit identifies
 * if the key is the second key of a key pair 
 * And the last 13 bits identify the algorithm and all its
 * parameters like Key sizes, outputs sizes, etc
 *
 * That makes 8192 possible algorithms defined in each class. 
 * That is still a lot ...
 *
 */
 

typedef uint16_t KeyType;

struct key_t {
  KeyType type; /* the ident determines the key size */
  /* nonce_t nonce; */ 
  /* MircoTime ExpireDate; */ /* 0 for does not expire */ 
  uint8_t byte[];
} __attribute__((packed));

typedef struct key_t Key;


enum key_class_t {
  SINATURE_KEY = 0,      /* uses split keys (public,private) */
  RMAC_KEY = 1,           /* uses split keys for one or each endpoint */
  ENCRYPT_KEY = 2,       /* uses split keys (public,private) */
  SHARED_SECRET_KEY = 3, /* for internal use, shall never be stored */
};

#define SINATURE_KEY_MASK (SINATURE_KEY<<13)
#define MAC_KEY_MASK (MAC_KEY<<13)
#define ENCRYPT_KEY_MASK (ENCRYPT_KEY<<13)
#define SHARED_SECRET_KEY_MASK (SHARED_SECRET_KEY<<13)

typedef enum key_class_t KeyClass;

/* rmacs are computed with a shared secret of each node 
 * so that fore each connection there is a derivable secret
 */

enum rmac_alg_e {
  RMAC_XOR_POLY1305 = 0, /* requires "split" key parts to be equal */
};

enum sign_alg_e {
  SIG_ED25519_32 = 0, /* public key size of 32 private 64 (includes seed) */
};

enum encrypt_alg_e {
  ENC_X25519_XSalsa20_32 = 0, /* Key sizes of 32 bytes */
};


static inline  KeyClass keyClass(const Key* key) {
  return (key->type >> 14); /* fist two bits */
}

static inline bool keyIsSecond(const Key* key) {
  return ((key->type | (1<<12) ) >> 13);
}

static inline uint16_t keyAlgID(const Key* key) {
  return (key->type << 3) >> 6; /* 10 bits after the first three */
}

static inline int generateKeyPair(Key* fst,Key* snd,const KeyType type) {
  assert(0 || fst || snd || type || "implemented");
  return 0;
} 

/* derives shared key from a split key, for example a public private
 * key pair. for two key pairs (a,b) and (x,y) this must satisfy
 * 
 *   mergeKey(a,y) == mergeKey(x,b)
 *
 * for key pairs of the form (a,a) and (x,x) this means commutative   
 * Keys must be of the same type and same key class. 
 */

static inline int mergeKey(Key* rest,Key* local, Key* peer) {
  assert(0 || rest || local || peer || "implemented");
  return 0;
}

/* mac are shared secret key authenticates 
 * They are assumed to take a nonce.
 *
 */ 

typedef uint8_t Rmac[RMAC_SIZE];

struct rmac_alg_t {

  int (*auth)( Rmac* rmac, /* the storage place of the result */
               const uint8_t* msg,
               const size_t msgLen,
               const Key* secret,
               const uint8_t* nonce );

  int (*verify)( const Rmac rmac,
                 const uint8_t* msg,
                 const size_t msgLen,
                 const Key* secretKey,
                 const uint8_t* nonce );
};


typedef struct mac_alg_t MacAlg;

/* encryption always assumes a (maybe derived) secret shared key 
 */

struct encrypt_alg_t {

  int (*encrypt)( uint8_t* cypher, /* the storage place of the result */
                  const uint8_t* plain,
                  const size_t len, /* length of both plain and cypher text*/
                  const Key* secret,
                  const uint8_t* nonce );

  int (*decrypt)( const uint8_t* plain,
                  const uint8_t* cypher,
                  const size_t len,
                  const Key* secret,
                  const uint8_t* nonce );
};

typedef struct encrypt_alg_t EncryptAlg;

typedef uint8_t Signature[SIGNATURE_SIZE];

struct sign_alg_t {

  int (*sign)( Signature* sig, /* the storage place of the result */
               const uint8_t* msg,
               const size_t msgLen,
               const Key* secretKey,
               const uint8_t* nonce );

  int (*verify)( const Signature* sig,
                 const uint8_t* msg,
                 const size_t msgLen,
                 const Key* publicKey,
                 const uint8_t* nonce);
};


typedef struct sign_alg_t SignAlg;

/* this maps key to the implementations indicated by the type 
 * Returns NULL on error  
 */

static inline MacAlg* macAlg(const Key* key) {
  assert(0 || key || "Implemented" );
  return NULL;
}

static inline EncryptAlg* encryptAlg(const Key* key) {
  assert(0 || key || "Implemented" );
  return NULL;
}

static inline SignAlg* signAlg(uint64_t keyType) {
  assert(0 || keyType || "Implemented" );
  return NULL;
}

#endif 


static inline int lock_mem(void* _p , size_t n) {
  
  intptr_t p = (intptr_t)_p;
  int PAGESIZE = sysconf(_SC_PAGESIZE);
  intptr_t p0 = (p/PAGESIZE)*PAGESIZE;
  size_t size = ((n+PAGESIZE-1)/PAGESIZE) * PAGESIZE;
  int ret;

  memset(_p,0,n); /* populate pages (necessary?) */

  do {
   ret = mlock((void*)p0,size);
  } while ( ret == -1 && errno == EAGAIN );

  if (errno == EAGAIN) errno = 0; 

  return ret;
} 


static inline int clear_mem(void* _p , size_t n) {

  intptr_t p = (intptr_t)_p;
  int PAGESIZE = sysconf(_SC_PAGESIZE);
  intptr_t p0 = (p/PAGESIZE)*PAGESIZE;
  size_t size = ((n+PAGESIZE-1)/PAGESIZE) * PAGESIZE;

  memset(_p,0,n);

  return munlock((void*)p0,size);

} 




static inline int _load_key(const char* path,unsigned char* k,size_t size) {

  if ( sodium_mlock(k,size) < 0) {
    errno = ENOLCK;     
    return -1;
  } 

  errno = 0;

  return load_from_file(path,k,size);

}

static inline int load_pk(const char* path,unsigned char* k) {
  struct stat st;

  int r = stat(path,&st);
  if ( r < 0 ) return r;
  if (st.st_mode != PK_MODE) {
    errno = EKEYREJECTED ;
    return -1; }

  return _load_key(path,k,crypto_box_PUBLICKEYBYTES);

}

static inline int load_sk(const char* path,unsigned char* k) {
  struct stat st;
  
  int r = stat(path,&st); 
  if ( r < 0 ) return r;
  if (st.st_mode != SK_MODE) {
    errno = EKEYREJECTED;
    return -1; }

  return _load_key(path,k,crypto_box_SECRETKEYBYTES);

}

static inline int load_auth_pk(const char* path,unsigned char* k) {
  struct stat st;

  int r = stat(path,&st);
  if ( r < 0 ) return r;
  if (st.st_mode != PK_MODE) {
    errno = EKEYREJECTED;
    return -1; }

  return _load_key(path,k,crypto_sign_PUBLICKEYBYTES);

}

static inline int load_auth_sk(const char* path,unsigned char* k) {
  struct stat st;
  
  int r = stat(path,&st); 
  if ( r < 0 ) return r;
  if (st.st_mode != SK_MODE) {
    errno = EKEYREJECTED;
    return -1; }

  return _load_key(path,k,crypto_sign_SECRETKEYBYTES);

}




/* storing the key clears and unlocks the memory */
static inline int _store_key(const char* path,mode_t mode,unsigned char* k, size_t size) {
 int r;

 r = store_as_file(path,mode,k,size);
 if (r < 0) return r;

 return clear_mem(k,size); /* zeros the memory, too */

}

static inline int store_pk(const char* path,unsigned char *k) {

  return _store_key(path,PK_MODE,k,crypto_box_PUBLICKEYBYTES); 

}


static inline int store_auth_pk(const char* path,unsigned char *k) {

  return _store_key(path,PK_MODE,k,crypto_sign_PUBLICKEYBYTES); 

}


static inline int store_sk(const char* path,unsigned char *k) {

  return _store_key(path,SK_MODE,k,crypto_box_SECRETKEYBYTES); 

}


static inline int store_auth_sk(const char* path,unsigned char *k) {

  return _store_key(path,SK_MODE,k,crypto_sign_SECRETKEYBYTES); 

}


static inline int clear_pk(unsigned char* k) {
  
  return clear_mem(k,crypto_box_PUBLICKEYBYTES);

}

static inline int clear_sk(unsigned char* k) {
  
  return clear_mem(k,crypto_box_SECRETKEYBYTES);

}


static inline int clear_auth_pk(unsigned char* k) {
  
  return clear_mem(k,crypto_sign_PUBLICKEYBYTES);

}

static inline int clear_auth_sk(unsigned char* k) {
  
  return clear_mem(k,crypto_sign_SECRETKEYBYTES);

}

static inline int clear_dsk(unsigned char* k) {
  
  return clear_mem(k,PSD_DSK_SIZE);

}

/* load derived shared key */ 
static inline int load_dsk(
        const char* pubPath,
        const unsigned char* sk,
        unsigned char* dsk) {

  unsigned char pk[crypto_box_PUBLICKEYBYTES];

  if ( lock_mem(dsk,crypto_box_BEFORENMBYTES) < 0 ) {
    errno = ENOLCK; /* reuse of errno ... */
    return -1;
  }
  
  if ( load_pk(pubPath,pk) < 0 ) {
    return -1; }

  if ( crypto_box_beforenm(dsk,pk,sk) < 0 ) {
     clear_pk(pk);
     errno = EDOM; /* an other errno reuse */
     return -1;
  };

  clear_pk(pk);
 
  return 0;

}





#endif
