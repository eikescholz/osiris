/* some system interface utilities
 *
 *
 * */

#ifndef SYSUTILS_H
#define SYSUTILS_H
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <stdint.h>
#include <netinet/in.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <sched.h>
#include <poll.h>
#include <stdbool.h>
#include <assert.h>

#include <uninterruptableIO.h>

#ifndef _POSIX_SAVED_IDS 
#error "Posix saved ids feature required!"
#endif
#if (_POSIX_SAVED_IDS == -1)
#error "Posix saved ids feature required!"
#endif

/* network notation types
 * TODO: is there a way to make this
 * work without setter routines ?
 */

typedef uint8_t uint8n_t; 
typedef struct uint16n_t{uint8_t data[2];} __attribute__((packed)) uint16n_t;
typedef struct uint32n_t{uint8_t data[4];} __attribute__((packed)) uint32n_t;
typedef struct uint64n_t{uint8_t data[8];} __attribute__((packed)) uint64n_t;

typedef int8_t int8n_t; 
typedef struct int16n_t{uint8_t data[2];} __attribute__((packed)) int16n_t;
typedef struct int32n_t{uint8_t data[4];} __attribute__((packed)) int32n_t;
typedef struct int64n_t{uint8_t data[8];} __attribute__((packed)) int64n_t;


typedef struct in6_addr IP6Addr; 

struct sysuser_t {
  uid_t uid;
  gid_t gid;
};



static inline int get_file_usership(
   const char* path,
   struct sysuser_t* u) {
  
  struct stat st;
 
  if ( stat(path,&st) < 0) return -1;
 
  u->gid = st.st_gid;
  u->uid = st.st_uid;
                      
  return 0;

}



/* For use as root, drops all privileges
 * and overwrites the saved uid */
static inline int become_user(const struct sysuser_t* u) {    

  if ( setregid(u->gid,u->gid) < 0 ) {
    perror("Could not set group id\n");
    return -1; }
  
  if ( setreuid(u->uid,u->uid) < 0) {
    perror("Could not set user id\n");
    return -1; }

  return 0;

}


static inline void get_effective_user(struct sysuser_t* u) {
  u->uid = geteuid();
  u->gid = getegid();
}

static inline void get_real_user(struct sysuser_t* u) {
  u->uid = getuid();
  u->gid = getgid();
}

static inline int become_real_user() {
  struct sysuser_t ru;
  get_real_user(&ru);
  return become_user(&ru);
}  




/* updates euid and egid but leaves saved ids unchanged
 * returns the calling effecitve user to the arg if non NULL
 * */
static inline void be_real_user(struct sysuser_t* eu) {

  if (eu != NULL) get_effective_user(eu);

  int r = seteuid(getuid());
  if ( r < 0 ) { /* should be impossible ... */
    perror("INTERNAL ERROR: could not set euid to uid ! ... ?");
    exit(EXIT_FAILURE);
  }

  r = setegid(getgid());
  if ( r < 0 ) { /* should be impossible ... */
    perror("INTERNAL ERROR could not set egid to gid ! ... ?");
    exit(EXIT_FAILURE);
  }
}



static inline uint64_t bswap_uint64(uint64_t i) {
  char *p = (char*)&i;
  union {
    char byte[sizeof(uint64_t)];
    uint64_t val;
  } tmp;

  for(size_t i=0; i< sizeof(uint64_t);++i)
    tmp.byte[i] = p[sizeof(uint64_t)-1-i];  

  return tmp.val; 
}

static inline void rmemcpy(uint8_t* dst,const uint8_t* p,size_t n) {

  for(size_t i=0; i < n; ++i)
    dst[i] = p[n-1-i];  

}


static inline int host_is_little_endian(void) {
  union {
    uint64_t i;
    char c[sizeof(uint64_t)];
  } bint;
  
  bint.i = 0x1;
  return bint.c[0] == 0x1; 
}

#if 0 /* deprecated interface */
static inline uint64_t hton_uint64(uint64_t i) {
  if (host_is_little_endian() ) return bswap_uint64(i);
  return i;
}

static inline uint64_t ntoh_uint64(uint64_t i) {
  if (host_is_little_endian() ) return bswap_uint64(i);
  return i;
}
#endif

static inline void set_uint16n(uint16n_t* dst,uint16_t val) {
  uint8_t* data = (uint8_t*)&val;
  if (host_is_little_endian() ) rmemcpy(dst->data,data,sizeof(val));
  memcpy(dst->data,data,sizeof(val));
} 


static inline void set_uint32n(uint32n_t* dst,uint32_t val) {
  uint8_t* data = (uint8_t*)&val;
  if (host_is_little_endian() ) rmemcpy(dst->data,data,sizeof(val));
  memcpy(dst->data,data,sizeof(val));
} 

static inline void set_uint64n(uint64n_t* dst,uint64_t val) {
  uint8_t* data = (uint8_t*)&val;
  if (host_is_little_endian() ) rmemcpy(dst->data,data,sizeof(val));
  memcpy(dst->data,data,sizeof(val));
} 


static inline uint16_t get_uint16n(const uint16n_t* loc) {
  uint16_t ret;
  uint8_t* data = (uint8_t*)&ret;
  if (host_is_little_endian() ) rmemcpy(data,loc->data,sizeof(ret));
  memcpy(data,loc->data,sizeof(ret));
  return ret;
} 


static inline uint32_t get_uint32n(const uint32n_t* loc) {
  uint32_t ret;
  uint8_t* data = (uint8_t*)&ret;
  if (host_is_little_endian() ) rmemcpy(data,loc->data,sizeof(ret));
  memcpy(data,loc->data,sizeof(ret));
  return ret;
} 


static inline uint64_t get_uint64n(const uint64n_t* loc) {
  uint64_t ret;
  uint8_t* data = (uint8_t*)&ret;
  if (host_is_little_endian() ) rmemcpy(data,loc->data,sizeof(ret));
  memcpy(data,loc->data,sizeof(ret));
  return ret;
} 

static inline void set_int16n(int16n_t* dst,int16_t val) {
  uint8_t* data = (uint8_t*)&val;
  if ( host_is_little_endian() ) rmemcpy(dst->data,data,sizeof(val) );
  memcpy(dst->data,data,sizeof(val));
} 

static inline void set_int32n(int32n_t* dst,int32_t val) {
  uint8_t* data = (uint8_t*)&val;
  if (host_is_little_endian() ) rmemcpy(dst->data,data,sizeof(val));
  memcpy(dst->data,data,sizeof(val));
} 

static inline void set_int64n(int64n_t* dst,int64_t val) {
  uint8_t* data = (uint8_t*)&val;
  if (host_is_little_endian() ) rmemcpy(dst->data,data,sizeof(val));
  memcpy(dst->data,data,sizeof(val));
} 


static inline int16_t get_int16n(const int16n_t* loc) {
  int16_t ret;
  uint8_t* data = (uint8_t*)&ret;
  if (host_is_little_endian() ) rmemcpy(data,loc->data,sizeof(ret));
  memcpy(data,loc->data,sizeof(ret));
  return ret;
} 


static inline int32_t get_int32n(const int32n_t* loc) {
  int32_t ret;
  uint8_t* data = (uint8_t*)&ret;
  if (host_is_little_endian() ) rmemcpy(data,loc->data,sizeof(ret));
  memcpy(data,loc->data,sizeof(ret));
  return ret;
} 


static inline int64_t get_int64n(const int64n_t* loc) {
  uint64_t ret;
  uint8_t* data = (uint8_t*)&ret;
  if (host_is_little_endian() ) rmemcpy(data,loc->data,sizeof(ret));
  memcpy(data,loc->data,sizeof(ret));
  return ret;
} 


static inline int cmp_uint16n(uint16n_t* _a,uint16n_t* _b) {
  uint16_t a = get_uint16n(_a);
  uint16_t b = get_uint16n(_b);
  if (a < b ) return -1;
  if (a > b ) return +1;
  return 0;
} 


static inline int cmp_uint32n(uint32n_t* _a,uint32n_t* _b) {
  uint32_t a = get_uint32n(_a);
  uint32_t b = get_uint32n(_b);
  if (a < b ) return -1;
  if (a > b ) return +1;
  return 0;
} 


static inline int cmp_uint64n(uint64n_t* _a,uint64n_t* _b) {
  uint64_t a = get_uint64n(_a);
  uint64_t b = get_uint64n(_b);
  if (a < b ) return -1;
  if (a > b ) return +1;
  return 0;
} 

static inline int cmp_int16n(int16n_t* _a,int16n_t* _b) {
  int16_t a = get_int16n(_a);
  int16_t b = get_int16n(_b);
  if (a < b ) return -1;
  if (a > b ) return +1;
  return 0;
} 


static inline int cmp_int32n(int32n_t* _a,int32n_t* _b) {
  int32_t a = get_int32n(_a);
  int32_t b = get_int32n(_b);
  if (a < b ) return -1;
  if (a > b ) return +1;
  return 0;
} 


static inline int cmp_int64n(int64n_t* _a,int64n_t* _b) {
  int64_t a = get_int64n(_a);
  int64_t b = get_int64n(_b);
  if (a < b ) return -1;
  if (a > b ) return +1;
  return 0;
} 


/* memcmp should be lexicographic oder but dos not seem to be 
 * explicit about it in the standard ... so this is it 
 * on bytes as base */

/* That implements the literal recursive definition on the lexicographic
 * order, for same sized strings */

static inline 
int _lxcmp(const uint8_t* s1, const uint8_t* s2, size_t i, size_t n) {

  if ( *s1 < *s2) return -1;

  /* if the compiler does not tail call optimize that - screw it! */

  if ( *s1++ == *s2++ ) {
     if ( i+1 == n) return 0; 
     return _lxcmp(s1,s2,i+1,n);
  }

  return 1;

}
 
static inline
int lxcmp(const void* s1, const void* s2, size_t n) {
  
  return _lxcmp((uint8_t*)s1,(uint8_t*)s2,0,n);

}



static inline void ip6cpy(struct in6_addr* dst,const struct in6_addr* src) {
  memcpy(dst,src,sizeof(struct in6_addr));
}

static inline void ip6swp(struct in6_addr* a,struct in6_addr* b) {
  struct in6_addr tmp;
  ip6cpy(&tmp,a);
  ip6cpy(a,b);
  ip6cpy(b,&tmp); 
}

static inline 
void in6_addr2sockaddr(const struct in6_addr* addr,
                       struct sockaddr_in6* sockaddr) {
  memset(sockaddr,0,sizeof(*sockaddr));
  sockaddr->sin6_family = AF_INET6;
  ip6cpy( &sockaddr->sin6_addr, addr);
  
}

static inline int ip6bind(int fd, const struct in6_addr* addr) {
  struct sockaddr_in6 sockaddr;
  in6_addr2sockaddr(addr,&sockaddr);
  return bind(fd,(struct sockaddr*)&sockaddr,sizeof(sockaddr)); 
}


static inline int ip6cmp(
                const struct in6_addr* a,
                const struct in6_addr* b) {
  int r = memcmp(a,b,sizeof(*a)); 
  if ( r > 0) return 1;
  if ( r < 0) return -1;
  return 0;

}




static inline
ssize_t ip6sendto(int sockfd, const void *buf, size_t len, int flags,
                  const struct in6_addr* dst) {

  struct sockaddr_in6 sockaddr;
  in6_addr2sockaddr(dst,&sockaddr);
  errno = 0;
  int ret;

  if (len == 0) return 0;

 TRY_SEND:
 
  poll(&(struct pollfd){ .fd = sockfd,.events = POLLOUT },1,-1); 

  ret = sendto(sockfd, buf, len, flags, 
                    (struct sockaddr*)&sockaddr, sizeof(sockaddr) );

  if ( ret == -1 && errno == ENOBUFS ) {
    /* tried to wait with poll and POLLOUT which does not mitigate ENOBUFS*/
     sched_yield();
     goto TRY_SEND;
  }

  return ret;
}

/* dst must point to a buffer of at least INET6_ADDRSTRLEN
 *
 * TODO: do manual conversion that never fails 
 *      (provided INET6_ADDRSTRLEN is big enough, which it should)
 *
 * also excapes : with x
 */

static inline
char* ip6ntop(const struct in6_addr* src,char* dst) {
  if ( inet_ntop(AF_INET6,src,dst,INET6_ADDRSTRLEN) == NULL) {
    perror("INTERNAL ERROR: Could not sucessfully execute inet_ntop");
    exit(EXIT_FAILURE);  
  };
  
  return dst;
}

/* the introducing the sock notation for use as pathnames */
static inline
char* ip6ntos(const struct in6_addr* src,char* dst) {
 
  ip6ntop(src,dst);

  for(char* cur = dst; *cur != 0; ++cur) {
    *cur = toupper(*cur);
    if (*cur == ':') *cur = 'x'; 
  }

  return dst;
}


static inline
int ip6pton(const char* src,struct in6_addr* dst) {

  size_t n = strlen(src)+1;
  char buf[n];
  strcpy(buf,src);

  for(char* cur = buf; *cur != 0; ++cur) {
    if (*cur == 'x') *cur = ':'; 
  }
  
 if ( inet_pton(AF_INET6,buf,dst) <= 0) return -1;
 return 0;

}

static inline int store_as_file(
    const char* path,
    mode_t mode,
    void* buf, 
    size_t size) {
  int fd;
 
  ssize_t ssize = size;
  
  if ( size > SSIZE_MAX) {
    ssize = SSIZE_MAX;
    size = SSIZE_MAX; 
  }
 
  fd = uninterruptable_open(path,O_CREAT | O_TRUNC | O_WRONLY, mode);
  if (fd == -1) return -1;
  int sz  = uninterruptable_write(fd,buf,size);
  if (sz != ssize) return -1;
  uninterruptable_close(fd);
  return 0;

}


static inline int atomic_store_as_file(const char* path,mode_t mode,void* buf, size_t size) {
  int fd;
 
  ssize_t ssize = size;
  
  if ( size > SSIZE_MAX) {
    ssize = SSIZE_MAX;
    size = SSIZE_MAX; 
  }
 
  fd = uninterruptable_flopen(path,O_CREAT | O_TRUNC | O_WRONLY, mode);
  if (fd == -1) return -1;
  if (uninterruptable_flock(fd,LOCK_EX) < 0) return -1;
  int sz  = uninterruptable_write(fd,buf,size);
  fsync(fd);
  uninterruptable_flock(fd,LOCK_UN);
  if (sz != ssize) return -1;
  uninterruptable_close(fd);
  return 0;
}

static inline int load_from_file(const char* path,void* buf, size_t size) {
  int fd;
  struct stat st;
  ssize_t ssize = size;
  
  if ( size > SSIZE_MAX) {
    ssize = SSIZE_MAX;
    size = SSIZE_MAX; 
  }

  fd = uninterruptable_flopen(path,O_RDONLY,0);
  if (fd == -1) return -1;
  if (fstat(fd,&st) < 0) return  -1;
  if (st.st_size != ssize) {
    errno = EMSGSIZE; /* close enough, file size must match */
    return -1;}
  int sz  = uninterruptable_read(fd,buf,size);
  if (sz != ssize) return -1;
  uninterruptable_close(fd);
  return 0;

}


static inline int atomic_load_from_file(
                const char* path,
                void* buf, 
                size_t size ) {
  int fd;
  struct stat st;
  ssize_t ssize = size;
  
  if ( size > SSIZE_MAX) {
    ssize = SSIZE_MAX;
    size = SSIZE_MAX; 
  }

  fd = uninterruptable_open(path,O_RDONLY,0);
  if (fd == -1) return -1;
  if (uninterruptable_flock(fd,LOCK_EX) < 0) return -1; 
  if (fstat(fd,&st) < 0) return  -1;
  if (st.st_size != ssize) {
    errno = EMSGSIZE; /* close enough, file size must match */
    return -1;}
  int sz  = uninterruptable_read(fd,buf,size);
  uninterruptable_flock(fd,LOCK_UN); 
  if (sz != ssize) return -1;
  uninterruptable_close(fd);
  return 0;

}



static inline void* mload_file(const char* path) {
  int fd;
  struct stat st;
   

  fd = uninterruptable_open(path,O_RDONLY,0);
  if (fd == -1) return NULL;
  
  if (fstat(fd,&st) < 0) return NULL;
  
  size_t size = st.st_size;   
  ssize_t ssize = size;

  if ( size > SSIZE_MAX) {
    ssize = SSIZE_MAX-1;
    size = SSIZE_MAX-1; 
  }

  /* we append a terminating 0 for convenience */
  uint8_t *ret = malloc(size+1);
  if (ret == NULL) {
    errno = ENOMEM;
    return NULL;
  }

  int sz  = uninterruptable_read(fd,ret,size);
  
  if (sz != ssize) {
    free(ret);
    return NULL; 
  }

  ret[size] = 0;

  uninterruptable_close(fd);
  
  return (void*)ret;

}






static inline char* vmprintf(const char* fmt, va_list _ap) {
   int size = 0;
   char *p = NULL;  
   va_list ap;

   /* Determine required size */
   va_copy(ap,_ap);
   size = vsnprintf(p, size, fmt, ap);
   va_end(ap);


   if (size < 0) return NULL;

   size++;   /* For '\0' */
   p = malloc(size);
   if (p == NULL) return NULL;

   va_copy(ap,_ap); 
   size = vsnprintf(p, size, fmt, ap);
   va_end(ap);


   if (size < 0) {
     free(p);
     return NULL;
   }
   return p;

}
 
static inline char* mprintf(const char* fmt, ...) {
   char *p ;
   va_list ap;

   va_start(ap, fmt);
   p = vmprintf(fmt, ap);
   va_end(ap);

   return p;

}

static inline int vstoref_as_file(
        const char* path,
        const char* fmt,
        va_list _ap ) {
   va_list ap;
   char* buf;
   
   /* Determine required size */
   va_copy(ap,_ap);
   buf = mprintf(fmt, ap);
   va_end(ap);
  
   if (buf == NULL) return -1;

   int ret = store_as_file(path,0444,buf,strlen(buf));

   free(buf);

   return ret;
}


static inline int storef_as_file(
        const char* path,
        const char* fmt,
        ... ) {
   va_list ap;
   int ret;
 
   printf("\n\nStoring to %s\n\n",path);
   va_start(ap, fmt);
   ret  = vstoref_as_file(path,fmt, ap);
   va_end(ap);

   return ret;

}

static inline int vloadf_from_file(
    const char* path,
    const char* fmt,
    va_list ap ) {

  int fd;
  struct stat st;

  fd = uninterruptable_open(path,O_RDONLY,0);
  if (fd == -1) return -1;
  
  if (fstat(fd,&st) < 0) return  -1;

  char* buf = malloc(st.st_size+1);
  buf[st.st_size] = 0;

  if (buf == NULL) {
    errno = ENOMEM;
    return -1;
  }
  
  int sz  = uninterruptable_read(fd,buf,st.st_size);
  if (sz != st.st_size) goto FAIL;

  int ret = vsscanf(buf,fmt,ap);

  free(buf);

  uninterruptable_close(fd);
  if (ret == EOF) return -1;
  return ret;

FAIL: 

  free(buf);
  
  return -1;

}

static inline int loadf_from_file(
        const char* path,
        const char* fmt,
        ... ) {
   va_list ap;
   int ret;
 
   va_start(ap, fmt);
   ret  = vloadf_from_file(path,fmt, ap);
   va_end(ap);

   return ret;

}

static inline bool is_multicast(const struct in6_addr* addr ) {
  if (addr->s6_addr[0] == 0xFF) return true;
  return false;
}


/* vstpcpy(buf,"foo","bar",NULL) */
static inline char* vstpcpy(char* dest, ... ) {
  va_list ap;  
  char* src;
  char* ret = dest;

  va_start(ap,dest);
  src = va_arg(ap,char*);
  while( src != NULL) {
    ret = stpcpy(ret,src);
    src = va_arg(ap,char*);
  }
  va_end(ap);

  return ret;
}




#endif

