#ifndef PSD_PATHS_H
#define PSD_PATHS_H

#include "psglobals.h"


/* the deepest paths are of the form
 * /var/sock/$SRCNODEIP/$SRCPORTIP/$CRYTOOP/$DSTNODEIP/$DSTPORT.*
 *
 * path should be readable with standard dirent, i.e. each directory name is 
 * max 255 characters. We need 7 additional bytes for the separators
 * and one additional byte is required to store terminating 0 byte.
 * 
 */

#define PSD_MAX_PATH_SIZE (7*255 + 7 + 1)

/* The root is reset for the tests to avoid the need for setting
 * up a chroot environment with all libs just for testing */

#define PSD_ROOT_PREFIX "/var"

#define PSD_MAX_ROOT_PATH_SIZE (2*255+2+1)


/******************************************************************************
 * general hosts paths                                                        *
 *****************************************************************************/

static inline char* psd_mkRootPrefix(char* pathBuf) {
  
  char* rootprefix = getenv("PSROOTPREFIX");
  if ( rootprefix == NULL) rootprefix = PSD_ROOT_PREFIX;

  if ( strlen(rootprefix) > PSD_MAX_ROOT_PATH_SIZE - strlen("/ps")) {
    fprintf(stderr,"ERROR: Environment PSROOTPREFIX too long (max is %d)\n",
                    PSD_MAX_ROOT_PATH_SIZE);
    exit(EXIT_FAILURE); 
  }  
  
  return stpcpy(pathBuf,rootprefix); 
}


static inline char* psd_mkRootPath(char* pathBuf) { 
  return stpcpy(psd_mkRootPrefix(pathBuf),"/ps");
}


static inline char* psd_mkRawSendPath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/psrawsend");
}

static inline char* psd_mkRawReceivePath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/psrawreceive");
}

static inline char* psd_mkSendPath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/pssend");
}

static inline char* psd_mkCkrmacPath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/psnckrmac");
}

static inline char* psd_mkMkrmacPath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/psnmkrmac");
}

static inline char* psd_mkQueuePath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/pspqueue");
}

static inline char* psd_mkQueueDaemonPath(char* pathBuf) {
  return stpcpy(psd_mkRootPath(pathBuf),"/bin/pspqueued");
}




/******************************************************************************
 * Known node paths                                                           *
 *****************************************************************************/

static inline char* psd_mkNodePath(char* pathBuf,const struct in6_addr* addr) {

  char ipstr[INET6_ADDRSTRLEN];

  ip6ntos(addr,ipstr); 

  return vstpcpy(psd_mkRootPath(pathBuf),"/",ipstr,NULL); 

}

static inline char* psd_mkLocalNodePath(char* pathBuf,const struct in6_addr* addr) {

  char ipstr[INET6_ADDRSTRLEN];

  ip6ntos(addr,ipstr); 

  return vstpcpy(psd_mkRootPath(pathBuf),"/local/",ipstr,NULL); 

}



static inline char* psd_mkRmacKeyPath(
        char* pathBuf,
        const struct in6_addr* addr ) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/rmac.key"); /* route auth */

}



/******************************************************************************
 * Peer node paths                                                            *
 *****************************************************************************/

static inline char* psd_mkNodeEncryptPath( 
       char* pathBuf,
       const struct in6_addr* addr) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/encrypt");


}

static inline char* psd_mkNodeEncryptKeyPath(
                char* pathBuf,
                const struct in6_addr* dst_addr ) {

  char* nodePathEnd = psd_mkNodePath(pathBuf,dst_addr);

  return stpcpy(nodePathEnd,"/encrypt.key");

}


static inline char* psd_mkSockPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port 
    ) {

  const size_t portStrSz = 2*sizeof(uint64_t)+1;
  char portStr[portStrSz];

  if ( snprintf(portStr,portStrSz,"%lX",port) < 1) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    exit(EXIT_FAILURE);
  }    
 
  return vstpcpy(psd_mkNodePath(pathBuf,addr),"/",portStr,NULL); 
   
}

static inline char* psd_mkNodeClusterAddressPath( 
       char* pathBuf,
       const struct in6_addr* addr) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/clusterAddress");

}


/******************************************************************************
 * local node paths                                                           *
 *****************************************************************************/

static inline char* psd_mkNodeDecryptPath( 
       char* pathBuf,
       const struct in6_addr* addr) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/decrypt");

}


static inline char* psd_mkNodeDecryptKeyPath(
        char* pathBuf,
        const struct in6_addr* addr ) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/decrypt.key"); /* secret */

}




/******************************************************************************
 * Peer port paths                                                            *
 *****************************************************************************/

static inline char* psd_mkPortEncryptPath(
   char* pathBuf,
   const struct in6_addr* addr,
   uint64_t port ) {

  return stpcpy(psd_mkSockPath(pathBuf,addr,port),"/encrypt");

}


static inline char* psd_mkPortEncryptKeyPath(
                char* pathBuf,
                const struct in6_addr* host_addr,
                uint64_t host_port ) {
  
  return stpcpy(psd_mkSockPath(pathBuf,host_addr,host_port),"/encrypt.key");

}


static inline char* psd_mkPortVerifyKeyPath(
                char* pathBuf,
                const struct in6_addr* host_addr,
                uint64_t host_port ) {

  return stpcpy(psd_mkSockPath(pathBuf,host_addr,host_port),"/verify.key");

}




/*****************************************************************************
 * Port paths  (Socket paths would be more accurate)                         *
 *****************************************************************************/


static inline char* psd_mkPortPath(
    char* pathBuf,
    const struct in6_addr* addr,
    uint64_t port 
    ) {

  const size_t portStrSz = 2*sizeof(uint64_t)+1;
  char portStr[portStrSz];

  if ( snprintf(portStr,portStrSz,"%lX",port) < 1) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    exit(EXIT_FAILURE);
  }    
 
  return vstpcpy(psd_mkNodePath(pathBuf,addr),"/",portStr,NULL); 
   
}



static inline char* psd_mkPortDecryptKeyPath(
        char* pathBuf,
        const struct in6_addr* addr,
        uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/decrypt.key");

}


static inline char* psd_mkPortSignKeyPath(
        char* pathBuf,
        const struct in6_addr* addr,
        uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/sign.key");

}


static inline char* psd_mkPortDecryptPath(
   char* pathBuf,
   const struct in6_addr* addr,
   uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/decrypt");

}

static inline char* psd_mkPortVerifyPath(
   char* pathBuf,
   const struct in6_addr* addr,
   uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/verify");

}


static inline char* psd_mkPortReceivePath(
   char* pathBuf,
   const struct in6_addr* addr,
   uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/receive");

}

static inline char* psd_mkPortSignPath(
   char* pathBuf,
   const struct in6_addr* addr,
   uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/sign");

}



static inline char* psd_mkPortTimeoutPath(
   char* pathBuf,
   const struct in6_addr* addr,
   uint64_t port ) {

  return stpcpy(psd_mkPortPath(pathBuf,addr,port),"/timeout");

}




/*****************************************************************************
 * Cluster paths i.e. Multicast nodes                                        *
 *****************************************************************************/


static inline char* psd_mkClusterByzFaultTolPath(
   char* pathBuf,
   const struct in6_addr* addr ) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/byzFaultTol");

}

static inline char* psd_mkClusterNodesPath(
   char* pathBuf,
   const struct in6_addr* addr ) {

  return stpcpy(psd_mkNodePath(pathBuf,addr),"/nodes");

}







/*****************************************************************************
 * Deadline paths                                                            *
 *****************************************************************************/

static inline char* psd_mkIncomingPath(
  char* pathBuf ) {

  return stpcpy(psd_mkRootPath(pathBuf),"/in");

}

/* used later for ACK processing and auto resend */
static inline char* psd_mkOutgoingPath(
  char* pathBuf ) {

  return stpcpy(psd_mkRootPath(pathBuf),"/out");

}



static inline char* psd_mkDeadlinePath(
  char* pathBuf,
  const char* deadlineDir,
  MicroTime deadline ) {

  MicroTime dt = ps_baseInterval;
 
  const size_t strSz = 2*sizeof(uint64_t)+1;
  char deadlineStr[strSz];

  MicroTime t0 = ((deadline+dt-1)/dt)*dt; /* round to interval borders */

  if ( snprintf(deadlineStr,strSz,"%lX",t0) < 1) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    exit(EXIT_FAILURE);
  }    
 
  return vstpcpy( pathBuf, deadlineDir, "/" ,deadlineStr, NULL ); 

}



static inline char* psd_mkDeadlineEntryPath(
  char* pathBuf,
  const char* deadlineDirPath,
  MicroTime deadline,
  const struct in6_addr* dst_addr,
  uint64_t dst_port ) {

  const size_t strSz = 2*sizeof(uint64_t)+1;
  char portStr[strSz];
  char ipstr[INET6_ADDRSTRLEN];

  if ( snprintf(portStr,strSz,"%lX",dst_port) < 1) {
    perror("INTERNAL ERROR: snprintf could not convert number to hex\n");
    exit(EXIT_FAILURE);
  }    
 
  ip6ntos(dst_addr,ipstr); 

  return vstpcpy( psd_mkDeadlinePath(pathBuf,deadlineDirPath,deadline),
                  "/",ipstr,"p",portStr, NULL ); 

}



#endif 
