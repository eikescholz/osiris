/* Some utilities for compact pointer lists
 * i.e. NULL terminated arrays of NON-NULL pointers,
 * used for example as environ in the standard
 * but does not seem to have associated standard algorithms.
 */

#ifndef CPL_H
#define CPL_H
#include <stdlib.h>

typedef void* CPL; 

size_t cpl_length(const CPL* cpl) {

  size_t n = 0;
  while (*cpl++ != NULL) ++n;

  return n;

}

void* alloc_cpl_copy(const CPL* cpl) {

  size_t n = cpl_length(cpl); 
  
  void** ret = malloc(sizeof(*ret));
  if (ret == NULL) return NULL;

  for(size_t i = 0; i<n; ++i ) ret[i] = cpl[i];
  
  return ret;

}

/* strips null pointers and returnes length cpl of
 * array must contains at least one NULL
 * so return == size is an "error"
 * */
size_t inplace_array_to_cpl(CPL* array, size_t size) {
  size_t j = 0;
  for (size_t i=0; i<size; ++i)  {
    if (array[i] != NULL) {
     array[j] = array[i];
     j++;
    }
  }
  if (j == size) return j; /* possible error can not store terminating NULL */
  
  array[j] = NULL; /* terminate cpl */

  return j;

}



#endif
