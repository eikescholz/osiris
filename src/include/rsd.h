/* Basic definitions for replicated datagram sockets */ 

#ifndef RSD_H
#define RSD_H  


struct rsd_header_t
 struct in6_addr addr
 uint64_t port;
 nonce_t nonce; 
} __attribute__ ((packed));


struct rsd_batchentry_header_t {
  struct ip6_header_tail_t ip6;
  struct psd_header_t psd;
  struct rsd_header_t rsd;
} __attribute__ ((packed));

struct rsd_batchentry_t {
  size_t size;
  struct {
     struct rsd_batchentry_header_t header;
     uint8_t payload[1]; /* dynamic, but pedantic syntax checker  ... */
  } data;
} __attribute ((packed));


#define RSD_MAX_PAYLOAD_SIZE (PSD_MAX_PAYLOD_SIZE - sizeof(struct rsd_header_t))

_Static_assert(RSD_MAX_PAYLOAD_SIZE >= 1024, "Wasteful Header Size Detected" );



#endif
