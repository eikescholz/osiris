/* Some system level read and write wrappers.
 *
 * Everything inline. I don't bother with creating a linker object for this
 */

#ifndef UNINTERRUPTABLEIO_H
#define UNINTERRUPTABLEIO_H

#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <errno.h>
#include <sched.h>
#include <bsd/libutil.h>

#define OPEN_RETRY_MAX 255

_Static_assert(sizeof(size_t) == sizeof(ssize_t),"Possible integer overflow plaque on target system detected. Compilation aborted!");

static inline int uninterruptable_open(const char *path, int oflag,mode_t mode) {
  int fd = -1;
  size_t retry_count = 0;
  
  while ( fd == -1 && retry_count < OPEN_RETRY_MAX ) {
    fd = open(path,oflag,mode);
    if (fd == -1 && errno == EINTR) continue;      
    if (fd == -1 && errno != EINTR) break;
    sync(); /* accessing a lot of files parallel caused prem denied */
    ++retry_count;
  } 
      
  return fd;
}

static inline int uninterruptable_flopen(const char *path, int oflag,mode_t mode) {
  int fd = -1;
  size_t retry_count = 0;
  
  while ( fd == -1 && retry_count < OPEN_RETRY_MAX ) {
    fd = flopen(path,oflag,mode);
    if (fd == -1 && errno == EINTR) continue;      
    if (fd == -1 && errno != EINTR) break;
    sync(); /* accessing a lot of files parallel caused prem denied */
    ++retry_count;
  } 
      
  return fd;
}


static inline int uninterruptable_flock(int fd,int op) {
  int status = -1;
  do {
    status = flock(fd,op);
  } while (status == -1 && errno == EINTR); 
 
  return status;
}


static inline ssize_t uninterruptable_read(int fildes, void *buf, size_t nbyte) {

  if (nbyte == 0) read(fildes,buf,0); /* restore system read semantics  */
  
  ssize_t snbyte = (nbyte > SSIZE_MAX) ? SSIZE_MAX : (ssize_t)nbyte ;

  ssize_t count = 0;
  ssize_t n;
  while(count < snbyte) {
    n = read(fildes,buf,snbyte-count);
    count += n;
    if (count != 0 && n == 0) break; /* eof case */ 
    if (count == 0 ) return -1; /* Having not enough data is treated as an error */
    if (count == -1 && errno == EINTR) continue;
    if (count == -1) break;
  };

  return count;
}

static inline ssize_t uninterruptable_write(int fildes, const void *buf, size_t nbyte) {

  ssize_t snbyte = (nbyte > SSIZE_MAX) ? SSIZE_MAX : (ssize_t)nbyte ;

  ssize_t count = 0;
  while(count < snbyte) {
    count += write(fildes,buf,snbyte-count);
    if (count == -1 && errno == EINTR) continue;
    if (count == -1) break;
  };
 
  return count;
}

static inline int uninterruptable_close(int fd) {
  int status = -1;
  do {
    status = close(fd);
  } while (status == -1 && errno == EINTR); 
 
  return status;
}


#endif
