#ifndef PS_GLOBALS_H
#define PS_GLOBALS_H
#include "batchdata.h"
#include "microtime.h"

#include <stdatomic.h>

extern const char* ps_mainIdent;
extern char* ps_inputName;
extern char* ps_proofInputName;

extern ShmBd ps_inputShmbd;
extern ShmBd ps_proofInputShmbd;


extern MicroTime ps_inputBatchTime;
extern atomic_size_t curBatchId;

extern MicroTime ps_baseInterval;


#endif
