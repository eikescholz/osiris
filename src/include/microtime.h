/* Simplified timer interface.  
 * It provides the microsconds since the unix epoch 
 * 
 * 1970-01-01 00:00:00 +000000 (UTC).
 *
 * Time Stamps as paths are ... representation
 *
 *   2018/April/05d/24h/32m/23.200300s 
 *
 *  Where the separator may be "\" or ":" instead of "/"
 *  but only one separator is used in a single string stamp. 
 *
 */

#ifndef MICROTIME_H
#define MICROTIME_H
#include <stdint.h>
#include <time.h>

/* default to 100 miliseconds syncronisation precision 
 * if kernel does not provide a precision hint at al */
#define MICROTIME_NTP_DEFAULT_PRECISION 100000

typedef int64_t MicroTime; 
#define MICROTIME_MAX INT64_MAX

MicroTime microtime(MicroTime* loc);

MicroTime microtics(MicroTime* loc);  

MicroTime microepoch();

int64_t microepoch_nanoprecision(); 

/* persistent programs should call this periodically 
 * to get precision updates */

int64_t microtime_precision(); 

int64_t microtime_adjust_precision(); 


static inline int microsleep(MicroTime t,MicroTime* rem) {
  struct timespec nreq;
  struct timespec nrem;

  nreq.tv_sec  = t/1000000;
  nreq.tv_nsec = (t%1000000)*1000;
        
  int ret = nanosleep(&nreq,&nrem);

  if (rem != NULL) {
    *rem = nrem.tv_sec*1000000 + (nrem.tv_nsec/1000);
  }

  return ret;

}

int microtime_string(char* ret,MicroTime t);



#endif
