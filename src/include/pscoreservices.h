#ifndef PSCORE_SERVICES_H
#define PSCORE_SERVICES_H

int psd_main(int argc,char**argv);

int fexec_main(int argc,char** argv);

int psrawreceive_main(int argc,char** argv);
int psnckrmac_main(int argc,char** argv);
int psndecrypt_main(int argc,char** argv);
int pspdecrypt_main(int argc,char** argv);
int pspverify_main(int argc,char** argv);

int pspqueue_main(int argc,char** argv);
int pspqueued_main(int argc,char** argv);
int pspvote_main(int argc,char** argv);

int psecho_main(int argc,char** argv);

int pssend_main(int argc,char** argv);
int pspsign_main(int argc,char** argv);
int pspencrypt_main(int argc,char** argv);
int psnencrypt_main(int argc,char** argv);
int psnmkrmac_main(int argc,char** argv);
int psrawsend_main(int argc,char** argv);




#endif
