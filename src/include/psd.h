#ifndef PERSISTANT_SOCKET_DATAGRAM_H 
#define PERSISTANT_SOCKET_DATAGRAM_H

#include "batchdata.h"
#include "sysutils.h"
#include "cryptoutils.h"
#include "psd/paths.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <assert.h>
#include <sodium.h>
#include <dirent.h>
#include <stdlib.h>

#define PS_PROTO_NUM 59  /* number spec for testing purposes */

#define IP6_HEADER_SIZE 40
#define IP6_GUARANTEED_MTU 1280
#define PS_MAX_IP6PACKET_SIZE IP6_GUARANTEED_MTU

#define PSPKT_MAX_SIZE IP6_GUARANTEED_MTU

#define PS_NONCE_BYTES 16

#define PS_NONCESTR_BYTES ( 2*PS_NONCE_BYTES + 1 )

#define PSPKT_SIGNATURE_SIZE 80 

#define PSPKT_RMAC_SIZE 16

#define PSD_HEADER_SIZE 88

#define PSD_MAX_PAYLOAD_SIZE (PS_MAX_IP6PACKET_SIZE \
                                 - PSD_HEADER_SIZE \
                                 - PSPKT_SIGNATURE_SIZE \
                                 - PSPKT_RMAC_SIZE )

#define PSD_IP6HEADER_SIZE 32

#define PSPKT_FOOTER_SIZE (PSPKT_RMAC_SIZE + PSPKT_SIGNATURE_SIZE)

#define PSPKT_MIN_SIZE (PSD_HEADER_SIZE + PSPKT_FOOTER_SIZE)


_Static_assert( PSD_MAX_PAYLOAD_SIZE == 1096,
                "Inconsistent packet/datagram definitions detected"); 

typedef uint8_t psd_nonce_t[PS_NONCE_BYTES];

/* psd header as in internet protocol stack */

struct psd_header_t {
 uint32n_t vcf; /* version class and flow */
 uint16n_t packetSize; 
 uint8n_t nextHeader;
 uint8n_t hopLimit;
 IP6Addr src_addr;
 IP6Addr dst_addr;
 /* Above entries are the IPv6 Header */
 psd_nonce_t nonce;  
 int64n_t mpid;  
 /* the following data will be encrypted by the node-to-node encryption */
 uint64n_t src_port; 
 uint64n_t dst_port;
 int64n_t deadline; 
} __attribute__ ((packed));

#define PSPKT_NORMALFORM_VCF 0x60000000

struct pspkt_signature_t {
  unsigned char data[HASH_SIZE];
  IP6Addr addr;
} __attribute__ ((packed));

typedef struct pspkt_signature_t PSS;


_Static_assert( sizeof(struct psd_header_t) == PSD_HEADER_SIZE,
                "Inconsistent psd header size definitions" );


struct ps_packet_t {
  struct  psd_header_t header;
  uint8_t payload[PSD_MAX_PAYLOAD_SIZE]; /* may be smaller  ... */
  uint8_t padding[PSPKT_FOOTER_SIZE];
} __attribute__ ((packed));

typedef struct ps_packet_t PSPKT;

_Static_assert(sizeof(struct ps_packet_t) == IP6_GUARANTEED_MTU, 
                  "Invalid PSD Packet Header Construction Detected" );

struct ps_datagram_t { 
  struct psd_header_t header;
  uint8_t payload[PSD_MAX_PAYLOAD_SIZE]; /* may be smaller  ... */
} __attribute__ ((packed));


typedef struct ps_datagram_t PS_D; /* TODO: transition api makd PSD again */

typedef BE_T(PSPKT) PSPKT_BE;
typedef BE_T(PS_D)  PS_D_BE;
typedef BD_T(PSPKT) PSPKT_BD;
typedef BD_T(PS_D)  PS_D_BD;
typedef int (*PSPKT_COMPARE)( const PSPKT_BE*, const PSPKT_BE*);



struct ps_packet_footer_t {
  PSS signature;
  union {
    uint8_t rmac[PSPKT_RMAC_SIZE]; /* during trasmission */
    IP6Addr replicator;       /* for packet in normalform */
  } end;
} __attribute__ ((packed));

_Static_assert( sizeof(struct ps_packet_footer_t) == PSPKT_FOOTER_SIZE,
                "Inconsistent footer size definitions" );

typedef struct ps_packet_footer_t PSPKTF;


static inline size_t ps_packetSize(const PSPKT* pkt) {
  return get_uint16n(&pkt->header.packetSize);
}

static inline size_t ps_packetDatagramSize(const PSPKT* pkt) {
  return ps_packetSize(pkt) - PSPKT_FOOTER_SIZE;
}

static inline size_t ps_datagramSize(const PS_D* psd) {
  return ps_packetDatagramSize((const PSPKT*)psd);
}

static inline PS_D* ps_packetDatagram( PSPKT* pkt) {
  return (PS_D*)(pkt); /* just a cast ! */
}

static inline const PS_D* ps_const_packetDatagram(const PSPKT* pkt) {
  return (const PS_D*)(pkt); /* just a cast ! */
}

static inline PSPKTF* ps_packetFooter(PSPKT* pkt) {
  return (PSPKTF*)((uint8_t*)pkt + ps_packetSize(pkt) - PSPKT_FOOTER_SIZE);
}

static inline const PSPKTF* ps_const_packetFooter(const PSPKT* pkt) {
  return (const PSPKTF*)ps_packetFooter((PSPKT*) pkt);
}


static inline PSS* ps_packetSignature(PSPKT* pkt) {
  return & ps_packetFooter(pkt)->signature;
}   

static inline const PSS* ps_const_packetSignature( const PSPKT* pkt) {
  return (const PSS*)ps_packetSignature((PSPKT*)pkt);
}   


static inline IP6Addr* ps_packetSigner(PSPKT* pkt) {
  return &ps_packetSignature(pkt)->addr;
}   

static inline const IP6Addr* ps_const_packetSigner(const PSPKT* pkt) {
  return (const IP6Addr*)ps_packetSigner((PSPKT*)pkt);
}   

_Static_assert( PSPKT_RMAC_SIZE == sizeof(IP6Addr),
                "Footer size invariant violated" );

/* for APIs a Socket */
struct persistant_socket_t {
  IP6Addr  addr;
  uint64n_t port;
}; 

typedef struct persistant_socket_t PS;

typedef BE_T(PS) PS_BE;
typedef BD_T(PS) PS_BD;

static inline
int ps_pton(const char* str, PS* ps ) {

  char buf[NAME_MAX+1];
  char* addrName, *portName, *end;

  strncpy( buf, str, NAME_MAX);
   
  buf[NAME_MAX] = 0 ;
  
  addrName = buf;
  portName = buf;

  while (*portName != 0 && *portName != 'p' && *portName != '/' ) portName++ ;
  if (! (*portName == 'p' || *portName == '/') ) goto FAIL;

  *portName++ = 0; 

  set_uint64n(&ps->port,strtoll(portName,&end,16));
  if ( *end != 0 ) goto FAIL;

  if ( ip6pton( addrName, &ps->addr) < 0 ) goto FAIL;

  return 0;

FAIL:

  errno = EINVAL;
  return -1;  

}





void fprint_pspkt( FILE* strm, const PSPKT* pkt );
void fprint_psd( FILE* strm, const PS_D* psd );
void fprint_pspkt_bd( FILE* strm, const PSPKT_BD* bd );

PSPKT_BE* new_pspkt_batchentry(
              PSPKT_BD* bd,
              IP6Addr* srcIp,
              IP6Addr* dstIp,
/*              uint8_t type, */
              psd_nonce_t* nonce,
              uint64_t mpid,
              uint64_t time,
              uint64_t srcPort,
              uint64_t dstPort,
              uint8_t* payload,
              size_t payloadBytes );

PSPKT_BE* 
    new_random_pspkt_batchentry(
        PSPKT_BD* bd,
        unsigned char* seed, 
        IP6Addr* srcIp,
        IP6Addr* dstIp,
        uint64_t srcPort,
        uint64_t dstPort,
        size_t payloadBytes); 


/* returns true on success */
bool getenv_ip6addr(const char* name,IP6Addr* ret);

/* returns true on success */
bool getenv_port(const char* name,uint64_t* ret);


enum psd_cryptmode_e {
  PSD_NODECRYPTO,
  PSD_PORTCRYPTO
};



struct psd_pverify_arg_t {
  const unsigned char* pk; /* public key for checking auth */
  const char* logName;
};


void ps_inplace_pverify_be(
        PSPKT_BE* be,
        const struct psd_pverify_arg_t* va );

struct psd_psign_arg_t {
  const unsigned char* sk; /* secret key sign key for the src ip addr */
  const char* logName;
};


void ps_inplace_psign_be(
       PSPKT_BE* be, /* plain be */
       const struct psd_psign_arg_t* sa );


struct psd_rmac_arg_t {
  const unsigned char* sk; /* secret key sign key for the src ip addr */
  unsigned char* keyBuf; /* must be mlocked */ 
  const char* logName;
};

/* this appends the route MAC */
void ps_inplace_mk_beRmac (
       PSPKT_BE* be, /* plain be */
       const struct psd_rmac_arg_t* ra );


void ps_inplace_ck_beRmac (
       PSPKT_BE* be, /* plain be */
       const struct psd_rmac_arg_t* ra );


struct psd_cryptarg_t {
  enum psd_cryptmode_e mode;
  unsigned char* shared_secret_key;
  const char* logName;
};

/* this function is for both node and port decryption */
void ps_inplace_decrypt_be(
       PSPKT_BE* be,
       const struct psd_cryptarg_t* ca);


/* this function is for both node and port decryption */
void ps_inplace_encrypt_be(
       PSPKT_BE* be, /* plain be */
       const struct psd_cryptarg_t* ca );

int pspktBeSrcPortCmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeSrcPortCmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeDstPortCmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeDstPortCmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeNonceCmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeNonceCmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeSrcIP6Cmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeSrcIP6Cmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeDstIP6Cmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeSrcCmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeSrcDstIP6Cmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeSrcDstIP6Cmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeSrcCmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeSrcCmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeDstCmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeDstCmp(const PS_D_BE* _a,const PS_D_BE* _b);


int pspktBeSrcDstCmp(const PSPKT_BE* _a,const PSPKT_BE* _b);
int   psdBeSrcDstCmp(const PS_D_BE* _a,const PS_D_BE* _b);

int pspktBeDeadlineIntervalCmp(const PSPKT_BE* a,const PSPKT_BE* b);



bool verify_pktbd_dst_ip(
  const PSPKT_BD* bd,
  const IP6Addr* r);


bool verify_pktbd_src_ip(
  const PSPKT_BD* bd,
  const IP6Addr* r);

bool verify_pktbd_src_port(
  const PSPKT_BD* bd,
  uint64_t port);

bool verify_pktbd_dst_port(
  const PSPKT_BD* bd,
  uint64_t port);


size_t ps_inplace_encrypt( 
        PSPKT_BD* bd, 
        enum psd_cryptmode_e mode,                       
        const unsigned char* sk,
        const IP6Addr* src_ip,
        const char* mainIdent );

size_t ps_inplace_decrypt( 
        PSPKT_BD* bd,  
        enum psd_cryptmode_e mode,
        const unsigned char* sk, 
        const IP6Addr* dst_ip,
        const char* mainIdent );


/* returns number of successes */
size_t ps_inplace_mk_rmac( 
          PSPKT_BD* bd, 
          const unsigned char* sk,
          const char* mainIdent );

size_t ps_inplace_ck_rmac( 
        PSPKT_BD* bd, 
        const unsigned char* sk,
        const char* mainIdent );



size_t ps_inplace_normalize_and_sign( 
        PSPKT_BD* bd, 
        const unsigned char* sk,
        const char* mainIdent );

size_t ps_inplace_verify( 
        PSPKT_BD* bd, 
        const unsigned char* pk,
        const char* mainIdent );


int ps_send_psd_batchdata(
        const PSPKT_BD* bd, 
        int rawsock,
        const char* mainIdent);



int load_rmac_sk( const IP6Addr* srcAddr,
                  const IP6Addr* dstAddr,
                  uint8_t* rk );

bool is_local_psnode(const IP6Addr* addr );


/* collected information about the persistent socets
 * encoded in and target path 
 */

struct psockinfo_t {
  size_t node_count;
  size_t local_node_count;
  IP6Addr* node_addr;
  bool*  isLocal;
  size_t* port_count;
  uint64_t** port;
};

void free_psockinfo(struct psockinfo_t* psi);

struct psockinfo_t* alloc_psockinfo_of(const char* path);

void fprint_psockinfo(FILE* stream,struct psockinfo_t* pi);

#endif
