cmake_minimum_required(VERSION 2.8.9)
project(pspverify C)
 
file(GLOB SOURCES "${CMAKE_SOURCE_DIR}/src/pspverify/*.c")
 
add_executable(pspverify ${SOURCES})

