set terminal pdf
set output "ProcessingTimesPerPacket.pdf"
set xlabel "Number Of Pending Packets Processed Simultaneously"
set ylabel "Seconds"
show ylabel
set yrange [0:10]
set ytics 1
plot "benchmark.dat" using 1:($2/1000000)\
     title "Minimal Processing Time Per Packet" with linespoints,\
     "benchmark.dat" using 1:($3/1000000)\
     title "Average Processing Time Per Packet" with linespoints,\
     "benchmark.dat" using 1:($4/1000000)\
     title "Maximal Processing Time Per Packet" with linespoints,\
     1 with dots lc rgb "black" notitle 

