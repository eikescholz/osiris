set terminal pdf
set output "PacktesPerSecond.pdf"
set xlabel "Number Of Pending Packets Processed Simultaneously"
set ylabel "Packets Processed Per Second"
set yrange [0:15000]
set ytics 1000
plot "benchmark.dat" using 1:6 with linespoints notitle,\
     10000 with dots lc rgb "black" notitle 


