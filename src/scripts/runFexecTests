#!/bin/bash

cp src/fexec/fexec tmp  
cp scripts/fexecTestStateWriter tmp

chown root.root tmp/fexec
chmod u+s tmp/fexec
chmod g+s tmp/fexec

export uid=`id -r -u`
export euid=`id -u`
export gid=`id -r -g`
export egid=`id -g`

getent passwd fexec_tester  > /dev/null
if [ ! $? -eq 0 ]; then
  echo "Creating system user fexec_tester for testing";
  useradd -M fexec_tester;
fi

MCK=$MEMCHECK
MEMCHECK="" 

begin_test "File executer arguments test"

ought="euid=$euid uid=$uid egid=$egid gid=$gid cmd= tmp/fexecTestStateWriter arg1 arg2 arg3 arg4"

  check scripts/simpleFexecTestScript0
  check "[ '`cat tmp/fexecState`' = '$ought' ]"

ought="euid=$euid uid=$uid egid=$egid gid=$gid cmd= tmp/fexecTestStateWriter arg1 scripts/simpleFexecTestScript1 arg3 bar"

  check scripts/simpleFexecTestScript1 foo bar bla fasel
  check "[ '`cat tmp/fexecState`' = '$ought' ]"

end_test



begin_test "File executer non shebang rejection test"
  check "! tmp/fexec tmp/fexecTestStateWriter 2>/dev/null"
end_test

begin_test "File executer permission check test"

  cp scripts/simpleFexecTestScript0 tmp/fexecTest

  chmod a-x tmp/fexecTest
  chmod a-r tmp/fexecTest
  chmod a-w tmp/fexecTest

  chown fexec_tester.fexec_tester tmp/fexecTest

  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ---------- "
  chmod u+x tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ----x----- "
  chmod u-x tmp/fexecTest 
  chmod u+r tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: -r-------- "
  chmod u+x tmp/fexecTest 
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: -r-x------  " 
  chmod u-rx tmp/fexecTest

  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ------x--- "
  chmod g-x tmp/fexecTest 
  chmod g+r tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ----r----- "
  chmod g+x tmp/fexecTest 
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ----r-x---  " 
  chmod g-rx tmp/fexecTest
  
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ---------x "
  chmod o-x tmp/fexecTest 
  chmod o+r tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: -------r-- "
  chmod o+x tmp/fexecTest 
  check " tmp/fexec tmp/fexecTest 2>/dev/null # Perm:  -------r-x  " 
  chmod o-rx tmp/fexecTest

  chmod u+x tmp/fexecTest
  chmod u+s tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ---s------ "

  rm tmp/fexecState

  chmod u-x tmp/fexecTest 
  chmod u+r tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: -r-S------ "
  chmod u+x tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest  2>/dev/null # Perm: -r-s------ " 
  chmod u-rxs tmp/fexecTest

  chmod g+x tmp/fexecTest
  chmod g+s tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ------s---"
  chmod g-x tmp/fexecTest 
  chmod g+r tmp/fexecTest
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ----r-S---"
  chmod g+x tmp/fexecTest 
  check "! tmp/fexec tmp/fexecTest 2>/dev/null # Perm: ----r-s---" 
  chmod g-rxs tmp/fexecTest

end_test

export tuid=`id -r -u fexec_tester`
export teuid=`id -u fexec_tester`
export tgid=`id -r -g fexec_tester`
export tegid=`id -g fexec_tester`

sleep 1
begin_test "File executer setuid bit test"


  chmod u+rxs tmp/fexecTest
  chmod o+rx tmp/fexecTest

ought="euid=$teuid uid=$uid egid=$egid gid=$gid cmd= tmp/fexecTestStateWriter arg1 arg2 arg3 arg4"

  check tmp/fexec tmp/fexecTest
  sleep 1
  check "[ '`cat tmp/fexecState`' = '$ought' ]"

  chmod u-rxs tmp/fexecTest


end_test

begin_test "File executer setgid bit test"

  rm tmp/fexecState  

  chmod g+rxs tmp/fexecTest
   
ought="euid=$euid uid=$uid egid=$tegid gid=$gid cmd= tmp/fexecTestStateWriter arg1 arg2 arg3 arg4"

  check tmp/fexec tmp/fexecTest
  check "[ '`cat tmp/fexecState`' = '$ought' ]"

  chmod u+rxs tmp/fexecTest
 
  rm tmp/fexecState  

ought="euid=$teuid uid=$uid egid=$tegid gid=$gid cmd= tmp/fexecTestStateWriter arg1 arg2 arg3 arg4"

  check tmp/fexec tmp/fexecTest
  check "[ '`cat tmp/fexecState`' = '$ought' ]"

  chmod ug-rxs tmp/fexecTest

end_test

rm tmp/fexecTest
rm tmp/fexec
rm tmp/fexecTestStateWriter

