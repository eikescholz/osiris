cmake_minimum_required (VERSION 2.8.11)
project (OSIRIS C)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -pedantic -Wextra")

find_package(PkgConfig)

pkg_check_modules(LIBSODIUM REQUIRED libsodium)

include_directories(src/include 
                    src/libescore
                    src/libeacore
                    ${LIBSODIUM_INCLUDE_DIRS})
link_libraries(${LIBSODIUM_LIBRARIES})

pkg_check_modules(LIBBSD REQUIRED libbsd)

include_directories(src/include ${LIBBSD_INCLUDE_DIRS})
link_libraries(${LIBBSD_LIBRARIES})

if(UNIX AND NOT APPLE)
    link_libraries(rt)
endif()

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
link_libraries(${CMAKE_THREAD_LIBS_INIT})

add_subdirectory(src/libmicrotime)
link_libraries(microtime)

add_subdirectory(src/libpscore)
link_libraries(pscore)

add_subdirectory(src/libescore)
link_libraries(escore)

add_subdirectory(src/libeacore)
link_libraries(eacore)


add_subdirectory(src/psd)
add_subdirectory(src/psrawsend)
add_subdirectory(src/psrawreceive)
add_subdirectory(src/pssend)


add_subdirectory(src/fexec)
add_subdirectory(src/pspsign)
add_subdirectory(src/pspverify)
add_subdirectory(src/psndecrypt)
add_subdirectory(src/psnencrypt)
add_subdirectory(src/pspencrypt)
add_subdirectory(src/pspdecrypt)
add_subdirectory(src/psnmkrmac)
add_subdirectory(src/psnckrmac)

add_subdirectory(src/essend)
add_subdirectory(src/esreceive)

add_subdirectory(src/eacommit)
add_subdirectory(src/eareceive)





add_subdirectory(src/mkpskey)
add_subdirectory(src/bdt)
#add_subdirectory(src/dumpdsk)
add_subdirectory(src/apitest )

add_subdirectory(src/psecho )


#add_custom_target(test-data ALL
#                  DEPENDS microtime pscore psd psrawsend psrawreceive pssend fexec pspsign pspverify psndecrypt psnencrypt pspencrypt pspdecrypt mkpskey bdt psecho escore apitest
                  #                  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/testdata ${CMAKE_BINARY_DIR}/testdata)

add_custom_target(scripts ALL
                  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/src/scripts ${CMAKE_BINARY_DIR}/scripts)

add_custom_target(texDoc ALL
                  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/tex
                  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/tex/AccMath
                  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/tex/PSDP-Paper
                  COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/doc/PSDP-Paper/PSDP.tex  ${CMAKE_BINARY_DIR}/tex/PSDP-Paper/PSPD.tex
                  )



