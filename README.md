Osiris - PSDP Reference Implementation
======================================

The osiris project is the initial reference implementation of the persistent 
socket datagram protocol. It has been done by the WissTec R&D Services UG for 
the Kaufmann Goetz Trading & Investment GmbH.

Included with the currently non-free, unreleased material is a mathematical 
proof of the byzantine failure tolerance for the consensus algorithm designed 
for this protocol. 

This initial code and intellectual property is beyond doubt owned by 
WissTec R&D Services UG. Cryptographic proof of ownership has been 
certified by the Notary Harald Schleicher in Berlin under the
notarial register 149/2018 and 77/2018.

This intellectual property is now (14.12.2019) released under a BSD3 clause 
license. See LICENSE.md. 

It is at the moment not cost effective for us to set up auto DevOps, although
the code has an elaborate test suite. It tests all main components in a 
[Valgrind](https://valgrind.org/) memory debugging virtual machine.

The code has been developed and tested under Debian 9.11. Due to
some kernel patch differences the security concept does not seem to work with
other distributions. As far as it has been analysed the incompatibilities seem 
to stem  from security "enhancing" deviations from the POSIX specified behavior, 
but that is not established due to lack of time for in depth analysis. 

You should be able to run the test suite by the following steps executed in
a shell: 

  - git clone https://gitlab.com/eikescholz/osiris.git
  - mkdir osiris.debug
  - cd osiris.debug
  - cmake ../osiris 
  - make
  - sudo ./scripts/test 

The packet handling daemon needs root rights to acquire a raw ipv6 socket.
In non-test setups the rights are  immediately dropped after that. Sadly 
valgrind has problems with a program changing its user/group, so that
dropping rights is not performed for most tests. 

As of 17.01.2020 the test (usually) finishes without errors on 
Debian GNU/Linux 9.11. Also there are a lot of missing bug fixes from a further year
of development, that could not be open source released for legal reasons. 
Because of that and the fact that the test-suite runs on randomly generated data, 
it may fail occasionally.

Since nobody pays for the development at the moment the Project is stalled.
