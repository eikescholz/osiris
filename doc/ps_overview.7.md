

# NAME

ps_overview - Persistent Socket Architecture Overview

# DESCRIPTION 

Persistent sockets are the communication endpoints of the associated persistent
socket protocol. Their design provide a solution to the following internet 
protocol design objectives:

  - Security up to military grade requirements with integrated 
    DDoS mitigation mechanisms.  

  - Flexible authenticated end-point to end-point communication.

  - Service Replication Support for High Availability applications, 
    with configurable levels of byzantine failure tolerance.

  - Flexible end-point to end-point data encryption. 

  - Massive parallel connection and data throughput though batch processing

  - Realtime datagram transmission handling  

  - Low latency 

  - Reliability by replications

  - Reliability by repetition

  - Connections persist over a system shutdown and (re-)start sequence

The above list is sorted in order of priority regarding the resolution of
design trade-offs. Further it is assumed that the applications using persistent
sockets does *not* require the following:
 
  - Fast dynamic connections, i.e. fast connection creation and destruction. 

  - Robustness regarding to out of sync UTC real-time system clocks. 

  - Small traffic bandwidth footprint prioritized over reliability 

  - Incompatibility to a reliable real-time fail fast and fail early approach 

  - IPv6 independence 

  - Minimized latency 

## Persistent Sockets 

Persistent sockets are file system entries under /var/ps. A socket is given
by a peer of a IPv6 address and a 64Bit port number. The for a socket is
then in the directory /var/ps/$ADDR/$PORT where addr is the IPv6 address
in a modified standard notation where all '.' are replaces with 'x' hand
hex characters are upper case. This is done to make the path compatible with
the POSIX portable file name character set. The port is stored as a hexadecimal
number with upper case characters. The same socket can be identified by the
strings $ADDR/$PORT or ${ADDR]p${PORT}. 

Using the file system to store connection information in the file systems 
allows to keep usage of system resources like open file descriptors for 
parallel handled connections to a minimum. Further having persistent 
connections reduces latency since once a connection is established no protocol 
overhead for reestablishing a connection is required after application and/or 
system restart. However depending on the quality of the file systems caching
mechanism, accessing the file system adds extra latency in comparison with
other protocol designs. For details of the file system representation of 
connections see ps_overview(5).

Persistent sockets can have multiple connections, where a connection entails
the following data
  
  - Cryptographic authentication key for the peer

  - Cryptographic encryption keys for the peer

  - Cryptographic keys for the route MACs based DDoS mitigation
  
A socket can be connected to multiple peers, since connection for persistent
sockets is not an abstract analog to a physical single cable connection, but
an analog to a social connection: You know the peer, can identify and 
confidentially communicate with him. 


## High Availability Clusters and Replicated Persistent Sockets

The design of the persistent sockets and its protocol includes support for
replicating sockets over clusters for high availability requirements with a 
configurable level of byzantine failure tolerance. A high availability cluster
is given by

  - A set of nodes that is IPv6 adresses 
  - A single IPv6 Muilticast address
  - A single shared port
  - For each node dedicated node local authentication keys
  - Cluster shared encryption keys 
  - All cluster nodes must have the same base interval. 
  - (Tentative) Cluster Shared route message authentication keys

The encryption keys are shared for simplicity and speed. This has low security
implications since a single compromised key is sufficient to read the data 
anyway.

The route message authentication keys are shared for simplicity and low latency
reasons. This decision is not finalized and will require some benchmarking and
in field operation experience for a conclusive design decision.  

For brevity a cluster with a single node are allowed, but do not require
the assignment of a multicast address. However, if no multicast address
is assigned, it is not possible to add nodes to that cluster later.
 
A packet send to a cluster must be send to all its nodes. Each node
with then send copies of the received packet to all of the members using
IPv6 multicast to keep traffic requirements handleable. Each cluster node
checks the number of copies received from different sources against the 
configured byzantine failure tolerance level, and discards the packet
if the level is not reached. Unless an error not mitigate able trough the
byzantine failure tolerance level occurs, this creates uniformly equal
input batches on all of the clusters replicated socket. The replicated
application is required to be deterministic in the sense that the same
input will generate the same output on all nodes, like a purely functional
program. 



## Batch Processing Architecture

The primary means to archive support for massive parallel connection and
data transmission handling is archived by a batch processing architecture.
For this each node or cluster has a configured base Intervall dt. All
packets arriving during the time ((n-1)*dt,n*dt] for any n = 1,2,3 
is aggregated into one data batch. The batch is referred to by the time
n*dt as name usually represented has a hexadecimal number. 
Furhter more times are meassured in microseconds sind the unix epoch. 

This way any number of different connections can be processed on one batch,
and only system ressources to handle one batch are required. 
However this approach usually creates addtional latency in comparence to
process an arriving packet immediately. However the system is designed
requires to operate with small dt to keep in memory buffer requirements low.

A unified ordering is imposed on the packets in the batches independent from
the actual time of arrival of the packets, so that batches are the same
on all nodes of a cluster. These batches can directly be used to form
the blocks of blockchains as it is done by the eidetic socket protocol,
see es_overview(7) that will create an practically immutable history of
socket inputs by cryptographically interlocking input blockchains for
commuincating persistant sockets. 


## Realtime Application Support 

Realtime support is almost a byproduct of using time intervals to define
the content of a single batch. The sender of a packet to a persistent socket
datagram defines the arrival deadline for the datagram. This deadline 
determins into which batch a packet is sorted and that it is dropped after
the deadline has surely passed. Sound cluster syncronization regarding the
content of a batch can be archived by adding some further
additional latency and wait with the processing of the batch for the
interval ((n-1)*dt,n*dt] longer then the nodes clock syncronization precision.
Clusters that require low latency response time must have precisely 
syncronised clocks. 

For communicating peers the requirent is not that strict, if senders set
the packets deadlines sufficiently large, which also implies necessary
latencies. In conclusing the realtime support consequently priotieses
thoughput for lateny. However, well syncronized clocks, and fast hardware
allow for very low latencies.

Node that clock syncronization is only required between actually 
communicating nodes. For a huge network, there could thus be quite 
syncronization time drift, and adding new connections might require to 
improve time syncronizsation. This is seen as a rubustness feature, 
eliminating the need of single clock authrity, other then physical 
measurements. In conclusion persisent sockets solve the global syncronisation
problem by reference to a physical measurement, that in princpile can be
done by everyone independently and parallel and is: Measure the unix epoch 
in micorseconds.  



## The Persistent Socket Protocol 

The persistent socket protocol is a transmission layer protocol based on IPv6.
It is a byzantine failure tolerant cluster to cluster communication protocol. 

Since this is uncommon it is necessary to define what sending and recevieng 
means in terems of the cluster nodes. As discussed a replicated persiseent 
socket has two associated IPv6 adresses: The node address and its broadcast
address. For non replicated sockets the broadcast address is the the node
address. A packet is defined as complete IPv6 packet smaller or equal to
the guaranteed MTU of the IPv6 protocol (i.e. 1280 Bytes). 

For packets following nomenclature is used:

    Original Packet: Packet send to a node address of a cluster

    Replicated Packet: Packet send to the multicast address of a cluster.  

The nomenclature indicates that the multicast address is not used for
primary data transmission. This is indeed the case since using multicast in
this case would conflict with the byzantine failure tolerant design, that
requires actual copies to be send, at best though different routes depending
on the destination node. The failure tolerant communication between clusters
introduces the need for a further definition, sine the same information is
send replicated, a name for a specific specific unit of information transfere
from one cluster to a other is needed. This unit is called a datagram, 



Furthermore the failure tolerance context 
introduces a distinction between receiving a packet and accepting a packet,
where a packet is uniquly identified by an UID:

 - Packet Receival: 
     A packet arrives at the network interface, is not dropped
     by the kernel and received by the persistent socket stack
     impementation.

 - Packet Acceptance: 
    A packet is accepted if all of the following conditions are met:

    1. The packet has the most number of replication from different 
       source noces.

    2. The number of replications including the original packet from 
       different signers is bigger than the sending clusters bytantine 
       failure tolerance

    3. The number of replications including the original packet from 
       different replicators is bigger then the receiving clusters 
       byzantine failure tolerance. 
                           
The full recival-acceptance algorithm exectued on a node is given by multiple
communicating processes 

The receive process:

  1. Wait for and recive a packet from the kernel. 

  2. Check the route mac. On failure drop packet and goto 1.

  3. If the packet is an ACK packet, mark the packet as send in the 
     outgoing packet batch queue, and goto 1.

  4. If the packet is an original packet and the receiving cluster has 
     more then one node, resend it with the rawsend process to the 
     destination cluster multicast addres and goto 1. This case shall 
     update the destionation and source IPvP packet header fields.

  5. Decrypt the packet using a node wide encryption

  6. Decrypt the packet using a port specific encryption

  7. Verify its senders signatures. If this fails goto 1. 
  
  8. Check if the deadline has passed, if so drop the packet and goto 1. 

  9. Put the decrypted packet into the batch of a queue of batches 
     waiting for their deadlines to arive. 

The Verification Process:

  1. Wait until the most imminent deadline of the deadline queue has 
     passed. 

  2. Use the packet UIDs to make bins of candidates of the true packet 
     data for a given uid in a batch. 

  3. For each bin check which packet candidate can be accepted by
     the above criteria. If there are two candidates with equal amount
     of candidates, drop the packet, and log a compromised cluster 
     suspicion. If the packet gets accpeted but there where different 
     validly signed candidates log a compormized node suspicion. 
 
  4. Pass packets to the dispatch process:

The Dispatch Process:
    
  1. Split the Accepted packets into dedicated batches for each (local) 
     destination port. Sort each packet into a normalform order.  

  2. For each port wait for the previously executed receiveal program 
     encoded in the persistent socket filesystem structure to terminate 
     if applicable and exectue it agin with the new dedicated packet 
     batch. 
  
The Send Process:

  1. Sign input batch

  2. Encrypt batch with port specific encryption

  3. Encrypt batch with node specific encryption

  4. Generate route mac

  5. Pass the packet to the raw send process.  


The Raw Send Process:

  1. Send packets out over interfaces,

  2. Put packets into an outgoing deadline queue indicating the resend 
     time configured in the persistent socket file system.


The Resend Process:

  1. Wait until the most imminent out-deadline has passed.

  2. Resend all packets that are not marked as acknowledged ore timed out

  3. Remove all acknowledged and timed out packets from the outgoing 
     batch queue. 

  4. Re-queue all remaining packets with a new resend deadline to the 
     outgoing queue. 

To make use of multiple cores the above precesses may be split into
sub-processes and threads. Further waiting for a deadline implies to wait
until the deadline in the clock plus the clocks Synchronisation precision has
passed. 

The above algorithms define the node local behavior but do not determine
full cluster to cluster communication, especially if the clusters have a 
different number of nodes. Lets assume that cluster A with m nodes 
communicates with a cluster B with n nodes, where without losss of
gnerality it is assumed that m <= n:

 1. Cluster A sends a datagram to cluster B :
    Node numer i, i = 0, 1, 2,  ... of cluster A sends a packet to each
    the nodes i,i+m, i+2m, i+3m, ... of B, so that each node of B has
    one designated packet. This implies that some nodes of A sends multiple
    packets.   

 2. Cluster B sends a datagram to cluster A :
    Node j of cluster B sends a packet to th 
    node j%m. This imples that some nodes of A may receive mulitplie original
    packets signed by different sending nodes. 



#The Persistent Socket Protocol Packet

To explain the format of the ip6 packets of the persisent socket datagram
protocol it is first necessary to explain how two clussters can send 
messages to each other. For this we use the following nomenclature

  - Sender Address:   The sending clusters multicast address or 
                      the senders node addres if the sender is not a cluster

  - Receiver Address: The receiving clusters multicast address or the senders
                      node address if the sender is not a cluster   

A message is decompoed into parts that are packet into a datagram, where 
the datagram is an IPv6 packet with the following structure: 


```                                           
                                       Size
 +---------------------------------+ 
 | Verion, Class and Flow Fields   |     4 
 +---------------------------------+ 
 | Packet Size                     |     2 
 +---------------------------------+  
 | Next Header (PS_PROTO_NUM)      |     1 
 +---------------------------------+ 
 | Hop Limit                       |     1  
 +---------------------------------+ 
 | Sender Address                  |    16 
 +---------------------------------+ 
 | Receiver Address                |    16 
 +---------------------------------+ 
 | Message UID                     |    16
 +---------------------------------+  
 | Message Part ID                 |     8 
 +---------------------------------+ 
 | Deadline                        |     8 
 +---------------------------------+  
 | Source Port                     |     8 
 +---------------------------------+ 
 | Destination Port                |     8
 +---------------------------------+ 
 | Payload                         |  1096 (Max) 
 +---------------------------------+ 
                                      ----------
                                      1184 (Max)
```

 - The Next Header is PS_PROTO_NUM is not jet defined and set to 
   59 (IPv6 No Next Header).

 - The message uid is a number used once, may be generated randomly,
   but all nodes of a cluster must generate the same UID, for the same
   datagram. 
  
 - The message part id is a signed integer, and is negated for 
   acknowledgements packets (see below). If the message fits into
   one packet it may be generated randomly. 
  
 - The deadline is a signed 64Bit integer counting 
   micro seconds since the unix epoch. Packets whose deadline has passed
   will be dropped by routers and destination hosts. 

 - The source port is an unsigned 64Bit integer analog to udp/tcp ports
 
 - The destination is an unsigned 64Bit integer analog to udp/tcp ports



Datagrams are actually never send over the network, since they lack the
fine structure to represent communication between distinct nodes in 
communicating clusters. They are in fact equal on all nodes of a cluster. 

A unencrypted packet, be it a original one ore a replicated one has the 
following derived structure: 

```                                           
                                             Size
 +-------------------------------------+ 
 | +---------------------------------+ | 
 | | Verion, Class and Flow Fields   | |     4 
 | +---------------------------------+ | 
 | | Packet Size                     | |     2 
 | +---------------------------------+ |  
 | | Next Header (PS_PROTO_NUM)      | |     1 
 | +---------------------------------+ | 
 | | Hop Limit                       | |     1  
 | +---------------------------------+ | 
 | | Source Address                  | |    16 
 | +---------------------------------+ | 
 | | Destination Address             | |    16 
 | +---------------------------------+ | 
 | | Nonce                           | |    16
 | +---------------------------------+ |  
 | | Message Part ID                 | |     8 
 | +---------------------------------+ | 
 | | Deadline                        | |     8 
 | +---------------------------------+ |  
 | | Source Port                     | |     8 
 | +---------------------------------+ | 
 | | Destination Port                | |     8
 | +---------------------------------+ | 
 | | Payload                         | |  1096 (Max) 
 | +---------------------------------+ | 
 | Signature by Source Port            |    80 
 +-------------------------------------+ 
                                       ----------
                                          1264 (Max)
```

 - The signature is a digital signature of a 512Bit hash of the datagram
   where the hash algrithm is specified by the signer, followed by
   the I

 - The none is the message uid xord with the signing nodes IPv6 addr,
   so that the datagram uid becomes a number used once per packet.

 - If the message part ID contains relevant user information
   it the application should encrypt it (perseving the sign 
   of it). 

After the unecrypted packet is signed its payload will be encrypted
with soure and destination port specific keys. After that
evarything after the Message part Id will be encrypted with source
and destination node specific keys, and route mac is appended
to the encrypted packet, which leads to the final form of
a psersisent socket protocol packet wich is actually transmitted: 

```                                           
                                             Size
 +-------------------------------------+ 
 | +---------------------------------+ | 
 | | Verion, Class and Flow Fields   | |     4 
 | +---------------------------------+ | 
 | | Packet Size                     | |     2 
 | +---------------------------------+ |  
 | | Next Header (PS_PROTO_NUM)      | |     1 
 | +---------------------------------+ | 
 | | Hop Limit                       | |     1  
 | +---------------------------------+ | 
 | | Source Address                  | |    16 
 | +---------------------------------+ | 
 | | Destination Address             | |    16 
 | +---------------------------------+ | 
 | | Nonce                           | |    16
 | +---------------------------------+ |  
 | | Message Part ID                 | |     8 
 | +---------------------------------+ | 
 | | Encrypted Data                  | |  1200 (Max) 
 | +---------------------------------+ | 
 |   Route MAC                         |    16 
 +-------------------------------------+
                                       ----------
                                          1280 (Max)
```

 - The Route mac is generated using and shared secret key algorithm specified 
   by the sending persistent socket and the datagram as data input. 

 
In the following we will use the following nomenclature:

 - Everything before the payload is called header,

 - Everything after the payload is called footer. 

All Integers including the 64Bit ones are in network notation. 

The fields that are not defined by the usual IPv6 semantics have the 
following meaning


The nonce and message part ID can be combined into a 24Byte nonce for crypto
algorithms. 

The full decrypted packet has the format

 
From the decrypted packet the datagram which is the same for
all nodes of a cluster can be created. 




  
#SEE ALSO
ps_overview(7),ps_overview(5), microtime(7)
