# NOTES ON PERSISTENT DATAGRAM SOCKETS:

## Definitions ##

 - We use the term packet to refer to the ipv6 packet
 - We use the term datagram to refer to the packet without the footer data 
   regarding security. Adding the full ipv6 packet header increases
   storage requirement by 8 bytes for much more clarity, which is tolarble.  

To mitigate DDoS attacks packets need some kind of MAC authenticating the 
origin. However full public key signatures are expensive. Since persistent
sockets define persistent connections, a shared secret key for the involved
nodes are routers per connection is enough. To allow routers to store only
keys per known host and not per connection, the shared secret key for
a connection must be derived form the shared secret node key. 

To be able to provide proof in court without delousing any private keys, the
decrypted message must be signed using an asymmetric signature scheme.

```
                                           Size
  
   +-------------------------------------+
   | +---------------------------------+ | 
   | | Verion, Class and Flow Fields   | |   4 
   | +---------------------------------+ | 
   | | Packet Size                     | |   2 
   | +---------------------------------+ | 
   | | Next Header (PS_PROTO_NUM)      | |   1 
   | +---------------------------------+ | 
   | | Hop Limit                       | |   1 
   | +---------------------------------+ | 
   | | Source Address                  | |   16 
   | +---------------------------------+ | 
   | | Destination Address             | |   16 
   | +---------------------------------+ |  
   | | Nonce                           | |   16 
   | +---------------------------------+ | 
   | | Message Part ID                 | |   8 
   | +---------------------------------+ |
   | | Deadline                        | |   8 
   | +---------------------------------+ | 
   | | Node and Port Encrypted Data    | | 1192 (Max)  
   | +---------------------------------+ |
   | Route MAC                           |   16 
   +-------------------------------------+
 

                                         -------- 
                                           1280 (Max)
```

The deadline is not a current timestamp. In prior designs it was assumed that
future time can be forbidden for DDoS mitigation, but that was an inflexible 
design. Then futhre timestamps where allowed. But then they can be deadlines,
which are future times by definitions. By this the dealine can be used for
DDoS mitigation again and port timeout data can be used to determine the
earliest possible queue time if given. 


The RMAC is computed against the normalform see below;


The message part id is a int64 t, it will be negative to indicate
ACK packets. The ids UINT64_MAX-256 to UINT64_MAX are reserved,
for special message types, i.a. key autoconf etc 

The packet type gives some primitive sets data classification:
Packet Type:
  00: Data
  01: Replay Request (if routes have buffers, they may respond, instead of dst)
 Extension Options for higher level protocols can be provided by higher 
 level protocols with additional data. 
  02: Packet Received 
  03: Packet Dropped

The packet is of normal form 


Later used to implement key updates for rmac keys, and may other 
autoconf stuff. Well could include a replay request, for buffering
and eidetic memory reconstruction. A replay can be passed through
to the eidetic service if any there. Could add some features like
ACK for receival etc. Persistent sockets are the configurable
regarding their possible reliability.  


The message part id can be used application specific, for example
a range for messages that don't fit into one packet. If a message
is one packet, a random number will do fine. 
Including the message part id and timestamp yields nonces 
for rmac alga rums up to 32 byte. The nonce can be generated random.
The birthday theorem still yields a security of 2^64 for collisions. 

After node and port deception the package has the form

The signature is a 64 byte signatue of a 64 byte hash of the packet
and the singers 16 byte ipv6 addr. 

An application can use a single dedicated port as signing authority if
required, as for the example for eidetic ports. 
The signature can now be used as proof of authenticity in court. 

The route MAC and the source ports signature include the part of
the IPv6 header containing the source and destination addresses. 

The Route MAC is 16 bytes, for example by a Poly1305 algorithm. 


The Time stamped None is 32 Bytes
The time stamped nonce is first a big endian MicroTime Stamp
i.e. 64-Bit integer counting microseconds since the unix epoch. 
Then an 24 byte nonce, that is randomly generateable with negligable
collision chance, but enught byte to construct something unique
form Process id, thread id and a counter (if a process id is not
used within a microsecond ...) 

With respect to a host system a node can be in
three categories:

  1)   Known: The host can check the rmack.
  2)   Peer:  The host can have a connected psd socket.  
  3)   Local : The host runs the node, 
  3.0) Local Blank: Node has no sockets 
  3.1) Local Unconnected: Has only unconnected socket, but at last one.
  3.2) Local Connected: Has at least one connected local socket

Persistent datagram sockets can be connected to multiple destinations. 
Sockets are connected, if it is possible to send and receive datagram
between them. 

Persistent data in the file system:


Directory: /var/ps/ 
           Global and local node data

For a known node the following file system entries must be given: 

  Directory: /var/ps/$IP  
             Data of node $IP. 
             Path type is "node path"

  File:      /var/ps/$IP/rmac.key 
             Shared secret key for route authentication.
             Type: "Route Authentication Key" 

  Optional File: /var/ps/$IP/timeout
             Contains a test integer timout in microseconds.
             If existent router may drop packtes, that are
             too late according to their time stamp.
             If not existent timeout is assumed to be infinite for
             routing purposes.  


For a peer node the following additional file system entries must be given:

  File:      /var/ps/$IP/encrypt.key 
             Public key for node encryption 
             Type: "Node Encryption Key"

  Directory: /var/ps/$IP/$PORT 
             Data of persistent socket $IP.$PORT. 
             Path type is "sock path".
 
  File:      /var/ps/$IP/$PORT/verify.key 
             Key for verifying the signature of $IP/$PORT 
             Type: "Port Verification Key".

  File:      /var/ps/$IP/$PORT/encrypt.key 
             Key for verifying the signature of $IP/$PORT 
             Type: "Port Encryption Key".

For a local blank node $LIP the following additional file system entries must 
be given:

  File:      /var/ps/locals/$LIP 
             File whose existence indicates that 
             /var/psdsock/$LIP is a local node.  
             The file contains the $LIP in with ':' as separators 

  File:      /var/ps/$LIP/decrypt.key 
             Secret key for node decryption 
             Name: "Node Decrypt Key"


For a local unconnected node with persistent datagram socket $LIP/$LPORT the
following additional file system entries must be given:

  Directory: /var/ps/$LIP/$LPORT
             Data of persistent socket $IP.$PORT.
             Its existence is implied by the existence of peer node data.
             Is called "local sock path"

  File:      /var/ps/$IP/$PORT/decrypt.key 
             Secret key for port decryption 
             Name: "Port Decrypt Key"

  File:      /var/ps/$IP/$PORT/sign.key 
             Secret key for port signatures 
             Name: "Port Sign Key"


For a local connected node with a persistent datagram socket $LIP/$LPORT 
connected to a peer socket $RIP/$RPORT the following additional file system 
entries must be given:


  Directory: /var/ps/$LIP/$LPORT/$RIP 
             Existence of the directory indicates that the local socket 
             $LIP/$LPORT has a conncetion to $RIP. (i.e. there is at least one 
             connecting port)
             *IMPORTANT* this is *NOT* a link to /var/psdsock/$RIP, if it where
             all ports of $RIP would be connected.
             No path type not used directly.


  File:      /var/ps/$LIP/$LPORT/$RIP/$RPORT 
             File whose existence indicates that the local 
             socket $LIP/$LPORT is 
             connected to the peer socket $RIP/$RPORT.
             Path type is "peer sock path" 
             The file contains the string $RIP/$RPORT with
             as separator for $RIP 

The strings of ports and IPS are always syntactically distinct.
IPS contain the separator ':' or 'x' ports may contain '.' as separator.
Otherwise ips and ports are hex-ascii numbers.  

A cluster is given by a multicast $MIP with the normal node structure.
Additionally 

  File:      /var/ps/$MIP/nodeAdresses
             Space separated list of node ips
            
Further each node with $IP shares the same encryption keys for port
and node level. The port authenitfication keys and the rmac keys must be
different, and the multicast node has its on additnal rmac key. All these
rmac keys are used.  Further each node has the file

  File:      /var/ps/$IP/clusterAddress
             Contines the clusters multicast address the node belongs to.
 

NOTES ON REPLICATED SYSTEMS:

Important for a distributed system we need to distinguish the
following.
 
 - The source address: Refers to what is stored in the ipv6 src header field
                       It may by the sender or the singer address. 

 - The sender address: Sending clusters mulitcast ipv6 address
 - The signer address: Sending and singing nodes ipv6 address
 
 - The destination address: Refers to what is stored in the ipv6 dst header 
                            field. It may be the verifier of the reciver 
                            address. 

 - The verifier address: Address of the recving and verifying clusters node
 - The receiver address: Receiving clusters mulitcast ipv6 address

#### Normal Form and Transmission Form ####
 
A packet is in transmission form if
  
  - The source address is the signer address
  - The dstination address is the recvings nodes address
 

A packet is in replication form if
  
  - The source address is the replicating Nodes address
  - The destination address is the receiver multicast address
 

A datagram is in normalform if
  - Source address is the sender multicast address
  - Destingation address is the receiver multicast address

  The replicator address can not be recovered from the normal form. 

A packet is in normalform if
  - Its datagram is in normalform and
  - The footer is a psd_signature_t (i.e. the signer address for the rmac)
 

For packet signatures/hashes the ipv6 header fields are filled
with the sender and receiver addresses. So that header and palyload
for a replicated packet are exactly identical. The rmac fields
are computed with the actual addresses send over the interface.
The check and make rmac services also update the ip6 fields as
required. For a valid packet ckrmac will store the source address
in the rmac field, and set the ip6 header source field to to
sender address. It leaves the destination untouched. The
destination is during verification. 

A host/cluster communicating with a cluster will need to add local
nodes to match the clusters replication level. There are enough ipv6
addresses. 

Packet states in replication

 1. Send from signer to verifier (dst addr is verfier addr)
    Signatuer is for ip6 src and dst field filled with reciver and
    sernder multicast addresses.
 
 2. Verifier sends replicates to its cluster (dst addr is receiver addr)
    Must check rmacs, or DDoS the cluster might become easy.
    Well we can add creating new rmacs as well, increases latency
    however. But keys need to be read from storage in any case. 




sources are untouched. So no problem (I was confused about this before.)

TODO: OUTDATED

The replication protocol is the eidetic datagram protocol, it just
does not required the data to be stored. Replication is handled
by the ps socket protocol, so the eidetic datagram protcol just
needs to determin the true message and store it into a blockchain. 

Lets assume the algorithm is replicated over N processes with the ids

  p0 ... pN.
   
then we annotate the states and inputs of the porcess ids. That is:

  in_n(pK)  : The n-th input of process k 
  out_n(pK) : The n-th output of process k
  st_n(pK)  : The n-th state of process k.

The replication is called flawless in iteration n if

  in_n(p0)  == in_n(p1)  == ... == in_n(pN)
  st_n(p0)  == st_n(p1)  == ... == st_n(pN)
  out_n(p0) == out_n(p1) == ... == out_n(pN)

Since f is a function and assuming flawlessness of the inputs implies 
flawlessness of the states and outputs. 

As a policy for the replication system the following is assumed: 
If an error in the computation of the new state occurs it is treated as an 
severe error, that requires administrator intervention track down the cause 
which is very likely either unreliable/broken hardware or malicious 
manipulation. 

So make this match replicated sockets communicating with each other
must be of the same size. If two clusters of different sizes want
to communicate which each other, the smaller cluster must define
nodes that take multiple persistent sockets. By this a message send
to a cluster is always signed with a different key for each receiving
node, and routing might be different for each of the node destinations.


```     
    |                                 |                                 |
  Node 1                            Node 2                            Node 3
   | |                               | |                               | |
   | +-------------------------------|-+-------------------------------|-+
   +---------------------------------+---------------------------------+
```

In the above example the link layer connections are just replicated twice.
It is not assumed to be byzantine fail tolerant just fail tolerant. 

Too keep buffer requirement limited, a periodic timeout must be configured,
so that after timeout and a clock sync precision its the of a period are
a well defined batch when no error, packet loss, TTL expiration of a packet
occurred. The TTL is part of a replicated sockets persistent configuration
and not inferable form the packet it self. More precise, it is defined
by the receiving replicated socked, as it will just drop packets that are too
old. 


The layout of the payload for a eidetic datagram packet is thus: 
```
     +---------------------------------------+  
     | Prev Block Hash                       |    64 
     +---------------------------------------+  
     | Prev Block Time                       |     8 
     +---------------------------------------+  
     | Eidetic Message Payload               |  1024 (Max) 
     +---------------------------------------+  

                                               ---------
                                                 1096 (max)
```

The hash does not depend on the packet data so it would be displaced
in a footer. 
There is no src dest substructure, ps ports are so many that applications
just can used sequential ports if multiple storage locations are needed. 


So 400Bit Hash values are used for chaining blocks.
The hash is to the block prior to the packet creation time.
The time tag of the persistent socket datagram is not used,
since it may be a future value at creation.

The eidetic sockets can use replay requests to rebuild the blockchain. 

The best format for the message block needs to be decided. 

A eidetic socket can have up to 256 input blockchains. This allows
to store all of the 4 trading steps in one eidetic socket, so that
a account id is exactly cluster mulicast ip and a port. Possibly
saving further bytes for the trading messages. 


EIDETIC DATAGRAM PROTOCOL:

This is the same protocol as the replicated datagram protocol. It just
has additional guaranties:

   - All receive batches are stored to disk
   - The storage also includes all involved public keys to provide
     a proper database of evidence, especially old keys that where
     used by peers but replaced with new ones!
   - The store must include the used ids of the used hash algorithms. 
   


SECURITY DESIGN:

Assumed Attacker Capabilities:

  System is attacked by adversaries with state actor capability, e.g.
  military and/or intelligence services with state of the art  
  technology, knowledge and "unlimited" resources.


Technical Security

  Primary Security Mechanism:

    1) Non-Locality / no single Point of failures.
     - So replication with byzantine fault tolerance. 
    
    2) Robustness

    3) Mathematical rigor for the abstract system. 
     - The non-local technical components implement an mathematical system
       with proven secure properties. 

    Secondary Security Mechanisms:   
     - Everything that is fast and makes corrupting components of the
       system harder. 

MAKING CONNECTIONS:

Assuming we have two sockets A/a and B/b that should be connected for the first
time. Then the following conditions need to be satisfied.

 - A needs to know B's rmac key (shared secret key). 
 - A needs to know B's node (public) encryption key. 
 - A needs to know B's port b (public) encryption key. 
 - A needs to know B's port b (public) verification key. 

and via versa for B.

So lets assume that A wants to connect to B. A dos know the socket of B 
required but B does not know anything about A's wish. 

1) A needs to create the data specifying the requested connection his socket
   id and his pairs socket id.  
2) Then A needs to pack link and his (public) key data 
3) Transmits its to B to request
   a connection. (e.g. by walking to him ...)
3) If he accepts B needs to import A data
4) If he accepts B needs to pack his (public) key data
5) B needs to transmit its data back to A otherwise A is not able to connect.
6) A needs to import B data into his socket. 

If a wants to reveal data to B only if B reveals data to him. The 6 stages
are then necessary, but steps 3-5 can be automated by B and step 4 could 
be automated by A. However since
A has not the rmac.key of at the time the automation is set up, this
can not be done with the psd spec so far. However we can add packet
types to the protocol if they are smaller then ip6+psd header. If
we require the signature to always use full 64 byte, additonal 64 bytes
as well. The back packet can has a valid rmac, but the routes can not
know it, and we would like them to autoconf. (later, just need to
name tools properly)

So step names are

1) link     by A 
2) export   of A (exports links)  
3) request  of A
4) import   of B (imports links) 
5) export   of B
6) request  of B
7) import   of A 

with full automation it and A knowings B's ip it can become a two step
workflow: 

listen by A: 1,2,3 and sets up auto response for step 7
connect by B: 3,4,5 and triggers step 7  accept  by B
  
The rmac.key is 8 bytes, for one host ... for a TAN big but acceptable.
Well format "0000-0000-0000-0000". The rmac itself should of course
not be the TAN, but a one time pad could be used.


# NOTES ON REPLECATED 

Assume that the same data should be replicated over several sockets. This
means the following:

A replicates socket is given by 

 - Its associated cluster, i.e. a set of nodes
 - Its associated port number. This port number must be the same
   on each of the clusters nodes
 - All nodes of the cluster have the same node en-/decryption keys
 - The port on each node of the cluster uses the same port en-/decryption keys
 - Every port of each once of the cluster 
   has its own **private** port signature key

All the usage of the same encryption keys is motivated by the fact
that breaking into one of the nodes allows to read the replicated messages
anyway, and haveing the same key allows for significan speed advantages
in the replication.

Replication does the following: If a packet is send to the port,
and the second dst addres the broadcast address, of the cluster,
the second and first destination address will be swapt and the
packet be resend.  

A replicated port receiving and n times replicated message m means the
following.

  - For each cluster node the main port gets a m signed
    by the k-th sender (i.e. an m-k) 

This implies after replication that (no packet loss assumes)

  - All cluster nodes receive the messages m-1 to m-n. And can
    select the mayority as the valid message. 
  
In the above example each of the replicated messages each of these will be 
singed with a different port key, so the messages differ, and the nonce must
be different for each **sending** socket, since they have different
port signature keys. So we define the nonce for the k-th copy
by xoring the nonce with the **src** address. For the rmac creation
the destination addres is allways replaced with the destination multicast
address.

so we have the message nonce, the signatures are deterministic
and do not need nonces.
 - The port encryption nonce is the message nonce.
 - The Node encryption nonce is message nonce xored with the 
   dst multicast address (if no repl just the dest addr). 
The RMac none is node encryption nonce xored with the src address.

(For deterministic guaranteed collision free nonce generation, this should be
considerd.) 

Having the destination replaced with a multicast address for the rmac
computation allows an attacker to create random destinations address
for a packet. However the routers still can check if the packet is
on a valid route and discard it. So the same packet could be mutipled
and send to each node of a cluster, but for communicating of equal
size each cluster sends a packet to one node, so the routers still
can discard astray packets. 

ABSOLUTE OR RELATIVE VOTING:

To determine the true form of a message the absolute mayoirty should be
used. I.e. for n times replication allways n/2+1 messages must aggrees.
Otherwise the system can be broken by taking over a minority and
dos enough other nodes (if one node is overtaken the attacker as at
laeast on valid rmac key, so a dos should not be ruled out):

However relative voting increases reliability against random independent
node failure - So this is an application and not decided in this protocoll
level. If for example nodes tend to get physically destructed, having
realtive voting increases the number of nodes that must go done before
overall system failure.

 

### Communication between clusters of different Size ###


Let m < n.

 1. m -> n : 
     A cluster with m nodes sendes a datagram to a cluster with
     n nodes. Then the maximum byzantine failure tolerance the conncetion
     can provide is around (m/2) regarding receival authenticity. 
     Node i of m sends to node for k = 0,1,2,3... and i+k*m < n , i
     sends a packet to node to i+k*m.
     (Here a node might to send a datagram multiple times.)

 2. n -> m
    Now the provided byzantineFaultTol is about (n/2) 
    node j of m sends his pakct to j%m, so that the nodes of m may recive
    multpile packets. 

In any case the fault tolerance is authentication fault tolerance.

Regarding the pure fact of received a single packet ordinal or replicated
needs to arrive at a node. To regarding the true form of input batches,
they do not have a meaningful byzantine fault tolerance level, since
they can contain input of clusters with different number of nodes. 
Can this be used to desync the cluster by sending different versions
to each cluster node? Well the receiving cluster drops the input
if there is no packet version with most votes, assuming that the
replication packets arrive with high reliability, either all nodes
select the same packet or discard it. So the cluster could be desynced if
the replication network can be DDoSed, which is of no surprise what soever.
If the sync networks becomes too unreliable so does the sync ... in any
kind of system. So that seems to be OK.

Next problem is if a malicious node can desync the consensus in the other
nodes. Well it can not change data in packets to be replicated since they
are signed, so no, not by itself, the attaker needs to sign different packets,
send it to the owned node, that it can try to desync, which is still not
possible sine replication are send to multicast destinations, a node can
no single out other nodes to receive different replications, so the routing
in the replication network needs to be compromised to attack.
Then each node can be made seen different true forms of packets, which still
are signed by the attacking nodes socket. The system is then desynced but
provides cryptographic proof of by who's machine. Where here system is
allways a single service not the accoting network at completely. So
single accounts may be attacked if the Synchronisation network is sufficiently
unreliable of compromised. Since nodes can not distinguish from whom 
replications where send, there can not be a strict formal byzantine failure
guarantee for that. Maybe this can be made better later. 

 - Add an other footer field, a permanent signer field,
   i.e. ... good, kills the reuse of mac space with an signer addr
   in the tramsission. I.e. signatures look all the same in the system. 
   and require the replicator to overwirte the 
   source address with its own address. Then repliations can be distinguesed by
   sources and each each source gets one vote.
   So we can do provide the fault tol for the sync too. 
   ... two more days of work I guess ...

 - For internal communication the destination field should always contain
   the verifier address since the corresponding receiver address is
   a simple lookup. Determining a verifier address form a receiver address
   is complex, if made possible. So a replicated packet contains the
   verifier in its source address ... and the multicast addr in its 
   destination.  
   So if the destination is a multicast its are replication packet ?
   (maybe use a additional flag in the type field ? to indicate )
   The system voting system should accept one replication packet per
   node. But when to check


### The eidetic accounting protocol ###

The theory for this protocol is given in a latex written theory paper, this
section just discusses implementation designs. 

While the protocol is fair, the initial stage of it is not symmetric. To decide
which party is Alice and bob in the theory paper the debit account numbers of
the trading partners are compared. The partner with the lower account number
called "minor" will have the role of Alice the other is the mayor. For two
pardners doing different exchanges with different accounts, the roles can
such switched but are well defined for each transaction. (TODO: put this 
distinction into the math paper). 

The persistent sockets used by Alice and Bob to send orders and requests and
receive acknowledgements, are log-sockets. They are there for logging purposes,
and do no accounting, since different endpoints may use the same account, 
and there should be no need for them to synchronize.  

From the trading partners transaction issuing componetents there is
the following nomenclature

  - Our inventory, i.e. owned by "us", not necessarily by the onwer of the 
    component. 
  - Their inventory,

For the accountant service will use the same nomenclature our for the account
being updated. The services commiting to a transaction or receiving the 
settlement notification are called clients with their and our qualifications
as well.  

The deadline for an exchange must be stored in the exchange packet data
and not be derived form socket timeouts. The timeouts may be reconfigured
later, causing loss of informations about a agreed upon deadline, and speed
loss for looking up complete tranaction records.  

The 5 phases of the transaction protol have 4 subdeadlines

 1. commitment deadline 
 2. validation deadline - Accouters have a local consistency proof
 3. clearing deadline   - Accouters have a global consistency proof
 4. settlement deadline - Trading partners have global consistency proof



